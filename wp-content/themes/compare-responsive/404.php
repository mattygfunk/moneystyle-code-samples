<?php get_header(); ?>

	<div class="twelve columns">

    	<article>
		
        	<h1><?php _e('404 Not Found', 'framework'); ?></h1> 
			
			<div class="post-content">
		        <p style="margin-bottom:150px;"><?php _e('Sorry, the page you requested cannot be found.', 'framework') ?></p>
			</div>

        </article>

    </div>

    <?php //get_sidebar(); ?>

<?php get_footer(); ?>