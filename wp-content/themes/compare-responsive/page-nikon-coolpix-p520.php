<?php get_header(); ?>

	<div class="nine columns single-product push_three">

	    <?php if ( have_posts() ) : ?>
	    
	        <?php while ( have_posts() ) : the_post(); ?>
	        	<article>
		        	<h1><?php the_title(); ?></h1>
		        	<section>
			        	<div class="row info-header">
			        		<div class="price">
			        			<img src="<?php bloginfo('template_directory') ?>/img/comet.png" alt="Comet">
			        			<p>Price <span>&pound;275</span> excl. P&amp;P</p>
			        		</div>
			        		
			        		<div class="share">
			        			//share icons
			        		</div>
			        	</div>

			        	<div class="row row-shortlinks">
							<ul>
								<li class="skiplink"><a class="btn-label" href="#" gumby-goto="#overview" gumby-update gumby-offset="-10">Overview</a></li>
								<li class="skiplink"><a class="btn-label" href="#" gumby-goto="#technical-details" gumby-update gumby-offset="-10">Technical Details</a></li>
								<li class="skiplink"><a class="btn-label" href="#" gumby-goto="#reviews" gumby-update gumby-offset="-10">Product Reviews</a></li>
							</ul>
			        	</div>
		        	</section>
	
					<section id="overview" class="row product-overview post-content">

					<?php
						//Featured Image
			        	if (function_exists('has_post_thumbnail') && has_post_thumbnail() ) {
			        		the_post_thumbnail('slider');
			        	}
			        ?>

				    <?php the_content(); ?>

					</section>

					<section id="technical-details" class="row technical-details">
						<h2 class="header-line">Technical Details</h2>
						<table>
							<tr>
								<th>Product Type</th>
								<td>Digital Camera - compact</td>
							</tr>

							<tr>
								<th>Sensor Resolution</th>
								<td>18.1 Megapixel</td>
							</tr>

							<tr>
								<th>3D Technology</th>
								<td>Effective Sensor Resolution</td>
							</tr>

							<tr>
								<th>Optical Sensor Type</th>
								<td>Yes</td>
							</tr>

							<tr>
								<th>Total Pixels</th>
								<td>BSI-CMOS</td>
							</tr>

							<tr>
								<th>Effective Sensor Resolution</th>
								<td>18,910,000 pixels</td>
							</tr>
						</table>
					</section>
					
					<section>
						<div id="reviews" class="row">
							<h2 class="header-line">Product Reviews</h2>
						</div>
						
						<div class="row-reviews">

							<!-- review begin -->
							<div class="row review">
								<div class="row row-head">
									<div class="author">
										<h5>Review by Chris Rowe</h5>
										<p>reviewed on 04/08/2013</p>
									</div>
									<div class="reviews">
										<div class="star-2-0"></div>
									</div>
								</div>
								<div class="row review-content">
									<p>This company deserves 0 rating. Avoid at all costs. First of all they use fake 5 star reviews and take down any negative reviews - this should be hint what business they are.<br>
		They were running similar business with company named WeAreElectricals. which went bancrupt and complaints were the same. Do not know why they are doing this business. because the offered price difference from large retailers is not that big.</p>
								</div>
							</div>
							<!-- review end -->

							<!-- review begin -->
							<div class="row review">
								<div class="row row-head">
									<div class="author">
										<h5>Review by Olaf Spinkel</h5>
										<p>reviewed on 04/08/2013</p>
									</div>
									<div class="reviews">
										<div class="star-4-5"></div>
									</div>
								</div>
								<div class="row review-content">
									<p>Vivamus dapibus tortor non dolor malesuada auctor ut eget sem. Integer ut elit vitae justo pellentesque pharetra sodales ac arcu. Nunc at dolor ac mi lobortis tempus ac nec velit. Duis et adipiscing tortor. Phasellus auctor magna nulla, sed posuere purus posuere nec. Aliquam sapien purus, ultrices ac ante eget, malesuada pretium arcu.</p>

									<p>Phasellus cursus enim id ipsum mattis, a posuere enim aliquam. Quisque nibh ligula, pharetra quis cursus eu, venenatis vitae mauris. Praesent vehicula mattis erat, vitae tincidunt libero hendrerit id. Fusce mattis eu sapien consectetur molestie. Morbi commodo malesuada diam.</p>
								</div>
							</div>
							<!-- review end -->

							<!-- review begin -->
							<div class="row review">
								<div class="row row-head">
									<div class="author">
										<h5>Review by Chris Rowe</h5>
										<p>reviewed on 04/08/2013</p>
									</div>
									<div class="reviews">
										<div class="star-4-0"></div>
									</div>
								</div>
								<div class="row review-content">
									<p>This company deserves 0 rating. Avoid at all costs. First of all they use fake 5 star reviews and take down any negative reviews - this should be hint what business they are.<br>
		They were running similar business with company named WeAreElectricals. which went bancrupt and complaints were the same. Do not know why they are doing this business. because the offered price difference from large retailers is not that big.</p>
								</div>
							</div>
							<!-- review end -->

						</div>

						<div class="row">
							<h2 class="header-line">Write A Review</h2>
						</div>

						<div class="row row-write-review-head">

							<div class="four columns">
								<ul>
									<li class="field">
										<label for="name">Name</label>
										<input class="text input" id="name" type="text" />
									</li>
								</ul>
							</div>

							<div class="four columns">
								<ul>
									<li class="field">
										<label for="email">Email Address</label>
										<input class="email input" id="email" type="email" />
									</li>
								</ul>
							</div>
							<div class="four columns form-rating">
								<p>Rating</p>
								<div class="reviews">
									<div class="star-0-0"></div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="twelve columns">
								<ul>
									<li class="field">
										<label for="message">Review</label>
										<textarea class="input textarea" name="message" rows="3"></textarea>
									</li>
								</ul>
							</div>
						</div>

						<div class="row row-submit">
							<button class="medium primary btn metro rounded" type="submit">Submit Review</button>
						</div>
					</section>

	            </article>
	        <?php endwhile; ?>
     
	    <?php endif; // end have_posts() check ?>

    </div>

    <?php get_sidebar(); ?>

<?php get_footer(); ?>