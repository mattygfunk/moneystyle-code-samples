<?php
//include 'headerfile.php';
/*
 * Plugin Name: Product search
 * Plugin URI: http://www.awesem.com
 * Description: A widget that allows the integration of a sidebar search module
 * Version: 1.1
 * Author: AWESEM
 * Author URI: http://www.awesem.com
 */

/*
 * Add function to widgets_init that'll load our widget.
 */
add_action( 'widgets_init', 'tz_product_search_widget' );

/*
 * Register widget.
 */
function tz_product_search_widget() {
	register_widget( 'TZ_product_search_widget' );
}

/*
 * Widget class.
 */
class TZ_product_search_widget extends WP_Widget {
	/* ---------------------------- */
	/* -------- Widget setup -------- */
	/* ---------------------------- */
	function __construct() {	
		
		/* Widget settings */
		$widget_ops = array( 'classname' => 'tz_product_search_widget tz_SPC_widget sticky-scroll-box', 'description' => __('A widget that allows the integration of a sidebar search module.', 'framework') );
		
		/* Widget control settings */
		$control_ops = array( 'width' => 225, 'id_base' => 'tz_product_search_widget' );

		/* Create the widget */
		$this->WP_Widget( 'tz_product_search_widget', __('COMPARE: Sidebar Product Search', 'framework'), $widget_ops, $control_ops );
		
	}

	/* ---------------------------- */
	/* ------- Display Widget -------- */
	/* ---------------------------- */
	function widget( $args, $instance ) {
		global $wp_query;
		extract( $args );
		
		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		
		$minPrice = $instance['minPrice'];
		$maxPrice = $instance['maxPrice'];
		/*
        $hide_filter_btn_input = ($instance['hide_filter_btn'] == "1"  ? "display:none;" : "");
        $hide_keyword_input = ($instance['hide_keywords'] == "1" ? "display:none;" : "");
        $hide_category_input = ($instance['hide_category'] == "1" ? "display:none;" : "");
        $hide_speed_input = ($instance['hide_speed'] == "1" ? "display:none;" : "");
        $hide_sending_location_input = ($instance['hide_sending_location'] == "1" ? "display:none;" : "");
        $hide_pickup_location_input = ($instance['hide_pickup_location'] == "1" ? "display:none;" : "");
        $hide_volume_input = ($instance['hide_volume'] == "1" ? "display:none;" : "");
        $hide_payment_type_input = ($instance['hide_payment_type'] == "1" ? "display:none;" : "");
        $hide_withdrawal_type_input = ($instance['hide_withdrawal_type'] == "1" ? "display:none;" : "");
        $hide_aml_input = ($instance['hide_aml'] == "1" ? "display:none;" : "");
        $hide_other_input = ($instance['hide_other'] == "1" ? "display:none;" : "");
        */
        
        
        $hide_brand_input = ($instance['hide_brand'] == "1" ? "display:none;" : "");
        $hide_price_slider_input = ( $instance['hide_price_slider'] == "1" ? "display:none;" : "");
        
        if($maxPrice == 0){
            $maxPrice = ceil(aw_get_min_max_price('max'));
        }
        if($minPrice == 0){
            $minPrice = floor(aw_get_min_max_price('min'));
        }
		$step = $instance['step'];
        if($maxPrice == 0 && $minPrice == 0 ){
            $minPrice = 0;
            $maxPrice = 10;
        } elseif ($maxPrice == $minPrice && $minPrice != 0 && $maxPrice != 0){
            $maxPrice = $maxPrice + 10;
        }

        $ajaxImgHtml = '<p style="width:100%; text-align:center; margin-top:20px;"><img src="' . get_template_directory_uri() . '/img/ajax-loader.gif" / ></p>';
        $wrapHtml = '<span class="wrap" />';
        $searchPlaceholder = __('Search...','framework'); ?>
        <script type='text/javascript'>
        var ajax_mode = false;			
        </script>
		
		<?php
        // If at least one component visible
        if($instance['hide_filter_btn'] != "1" || $instance['hide_keywords'] != "1" || $instance['hide_category'] != "1" || $instance['hide_brand'] != "1" || $instance['hide_price_slider'] != "1" || $instance['hide_speed'] != "1" || $instance['hide_sending_location'] != "1" || $instance['hide_pickup_location'] != "1"  || $instance['hide_volume'] != "1" || $instance['hide_payment_type'] != "1" || $instance['hide_withdrawal_type'] != "1" || $instance['hide_aml'] != "1" || $instance['hide_other'] != "1"){
            /* Before widget (defined by themes). */
            echo $before_widget;
        }

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;
		?>
		<form action="../r/" method="get" <?php if($instance['hide_filter_btn'] == "1" && $instance['hide_keywords'] == "1" && $instance['hide_category'] == "1" && $instance['hide_brand'] == "1" && $instance['hide_price_slider'] == "1" && $instance['hide_speed'] == "1" && $instance['hide_sending_location'] == "1" && $instance['hide_pickup_location'] == "1" && $instance['hide_volume'] == "1" && $instance['hide_payment_type'] == "1" && $instance['hide_aml'] == "1" && $instance['hide_other'] == "1" && $instance['hide_withdrawal_type'] == "1") echo " style='display:none;'"; ?>>
		
		<div class="product_order_select_wrapper" style="display:none;">
		<?php $product_order_options = aw_get_product_order_options();
		$order_by = $product_order_options["order_by"];
		//$order_by = get_product_order_by_database_field_from_friendly_name($order_by);
		$order = $product_order_options["order"];
		$options_array = array(
			array(
				'text'=>'Minimum price ascending',
				'value'=>'min_price,asc',
				'selected'=>(($order=='asc' && $order_by=='min_price')?'selected':'')
			),
			array(
				'text'=>'Minimum price descending',
				'value'=>'min_price,desc',
				'selected'=>(($order=='desc' && $order_by=='min_price')?'selected':'')
			),
			array(
				'text'=>'Product name ascending',
				'value'=>'title,asc',
				'selected'=>(($order=='asc' && $order_by=='title')?'selected':'')
			),
			array(
				'text'=>'Product name descending',
				'value'=>'title,desc',
				'selected'=>(($order=='desc' && $order_by=='title')?'selected':'')
			),
					array(
				'text'=>'Date added ascending',
				'value'=>'date_added,asc',
				'selected'=>(($order=='asc' && $order_by=='date_added')?'selected':'')
			),
			array(
				'text'=>'Date added descending',
				'value'=>'date_added,desc',
				'selected' =>(($order=='desc' && $order_by=='date_added')?'selected':'')
			)
                );
		$select_box = '<select class="main_compare_products_order_by_select_box compare_products_order_by_select_box" id="compare_products_order_by_select_box'. $this->number .'">';
		foreach($options_array as $option){
			$select_box.='<option value="'.$option['value'].'" '.$option['selected'].'>'.$option['text'].'</option>';
		}
		$select_box.= '</select>';
		echo $select_box;	
		
		?>
		</div>
		<span class="clear"></span>

		<?php
		/*
		<script type="text/javascript">
			$(document).ready(function () {  
		  var top = $('.sticky-scroll-box').offset().top;
		  $(window).scroll(function (event) {
		    var y = $(this).scrollTop();
		    if (y >= top)
		      $('.sticky-scroll-box').addClass('fixed');
		    else
		      $('.sticky-scroll-box').removeClass('fixed');
		    $('.sticky-scroll-box').width(200);
		  });
			});
		</script>
		*/
		               
		 
		$receiving_country   = $_GET["receiving_country"];
		$sending_country = $_GET["sending_country"];
		$amount      = $_GET["amount"];

		// sanitise variables
		$amount      = filter_var($amount, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		$sending_country = filter_var($sending_country, FILTER_SANITIZE_SPECIAL_CHARS);
		$receiving_country   = filter_var($receiving_country, FILTER_SANITIZE_SPECIAL_CHARS);

		// connect to database
		$conn = connect_to_database();

		// get currency and currencysymbol
		list($currency, $currencysymbol) = get_currency_currencysymbol($conn, $sending_country);

		$amount = number_format((float) $amount, 0, '.', '');
		?>
		<h5>Amount</h5>
		<div class="input-group input-group-lg">
			<span class="input-group-addon" id="sizing-addon1"><?php echo $currencysymbol;?></span>
			<input type="text" name="amount" value="<?php echo $amount;?>"  class="form-control" aria-describedby="sizing-addon1" style="width:100%; font-size: 20px; padding: 5px;">
		</div>
		<h5>Sending From</h5>
		<select name="sending_country" style="margin:4px;width:100%; font-size: 20px; padding: 5px;">
		<?php
			/*
			$sql = "SELECT * FROM countries WHERE 1 ORDER BY Country ASC";
			$bankrow = mysqli_query($conn, $sql);
			if(! $bankrow )
			{
			  die('Could not get data: ' . mysqli_error($conn));
			}
			while($row = mysqli_fetch_array($bankrow, MYSQLI_ASSOC))
			{
			    $currency = "{$row['Currency']}";
			    if ($sending_country == "{$row['Country']}")
			    {
			        echo "<option value=\"{$row['Country']}\" SELECTED>{$row['Country']} ($currency)</option>";
			    }   
			    else
			    {
			        echo "<option value=\"{$row['Country']}\">{$row['Country']} ($currency)</option>";
			    }
			}   
			*/
			print_countries_selection_box($conn, $sending_country);
			//$receiving_country = $remittanceroute1;
		?>
		</select>
		<br>
		<h5>Destination</h5>
		<select name="receiving_country" style="margin:4px;width:100%; font-size: 20px; padding: 5px;">
		<?php
			/*
			$sql = "SELECT * FROM countries WHERE 1 ORDER BY Country ASC";
			$retval = mysqli_query($conn, $sql);
			if(! $retval )
			{
			  die('Could not get data: ' . mysql_error($conn));
			}
			while($row = mysqli_fetch_array($retval, MYSQLI_ASSOC))
			{
				$tocurrency = "{$row['Currency']}";
				if ($receiving_country == "{$row['Country']}")
				{
					echo "<option value=\"{$row['Country']}\" SELECTED>{$row['Country']} ($tocurrency)</option>";
				}	
				else
				{
					echo "<option value=\"{$row['Country']}\">{$row['Country']} ($tocurrency)</option>";
				}
			} 
			*/
			print_countries_selection_box($conn, $receiving_country);
		?>
		</select>
		<br>
		<?php
		print_r(error_get_last());
		$volume   = $_GET["volume"];
		$volume   = filter_var($volume, FILTER_SANITIZE_SPECIAL_CHARS);
		?>

        <style>
        #block {
            height: 0;
            overflow: hidden;
            -webkit-transition: height 300ms linear;
            -moz-transition: height 300ms linear;
            -o-transition: height 300ms linear;
            transition: height 300ms linear;
        }

        label {
            cursor: pointer;
        }

        #showblock {
            display: none;
        }

        #showblock:checked + #block {
            height: 200px;
            display: block;
        }
        </style>  
		<label for="showblock">
			<a style="color: darkblue;">More Search Options ▼</a>
        </label>
        <input type="checkbox" id="showblock">
        <div id="block">
            <h5>Minimum Exchange Volume</h5>
            <div class="arrow"></div>
            <ul id="compare-sidebar-search-volume" class="" style="margin-left: 4px;">
            <li>
            	<input type="radio" name="volume" title="2000+ BTC" id="Volume1" value="2000" <?php if ($volume == "500"){ echo "checked";}?> >
            	2000+ BTC
            <li>
            	<input type="radio" name="volume" title="1000+ BTC" id="Volume2" value="1000" <?php if ($volume == "1000"){ echo "checked";}?> >
            	1000+ BTC
            <li>
            	<input type="radio" name="volume" title="50+ BTC" id="Volume3" value="50" <?php if ($volume == "50"){ echo "checked";}?> >
            	50+ BTC
            <li>
            	<input type="radio" name="volume" title="10+ BTC" id="Volume4" value="10" <?php if ($volume == "10"){ echo "checked";}?> >
            	10+ BTC
            <li>
            	<input type="radio" name="volume" title="0+ BTC" id="Volume5" value="0" <?php if ($volume == "0"){ echo "checked";}?> >
            	0+ BTC (Include unreported volumes)
            </ul>
        </div>         
        <div style="<?php echo $hide_filter_btn_input; ?>">
            <button type="submit" value="Search Providers" style="font-size: 1.4em;padding: 24px 9px;">Search Providers</button>
		</div>
		</form>
		<?php
            // If at least one component visible
            if($instance['hide_filter_btn'] != "1" || $instance['hide_keywords'] != "1" || $instance['hide_category'] != "1" || $instance['hide_brand'] != "1" || $instance['hide_price_slider'] != "1"|| $instance['hide_price_speed'] != "1" || $instance['hide_price_sending_location'] != "1" || $instance['hide_price_pickup_location'] != "1" ){
                /* After widget (defined by themes). */
                echo $after_widget;
            }
	}

	/* ---------------------------- */
	/* ------- Update Widget -------- */
	/* ---------------------------- 	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		
		if (is_numeric ($new_instance['minPrice'])) $instance['minPrice'] = floor($new_instance['minPrice']);
		if (is_numeric ($new_instance['maxPrice'])) $instance['maxPrice'] = ceil($new_instance['maxPrice']);
		if (is_numeric ($new_instance['step'])) $instance['step'] = $new_instance['step'];
		
		$instance['hide_keywords'] = $new_instance['hide_keywords'];
	
		$instance['hide_category'] = $new_instance['hide_category'];
		
		$instance['hide_speed'] = $new_instance['hide_speed'];
		
		$instance['hide_sending_location'] = $new_instance['hide_sending_location'];
		
		$instance['hide_pickup_location'] = $new_instance['hide_pickup_location'];
		
		
		$instance['hide_brand'] = $new_instance['hide_brand'];
		
		$instance['hide_price_slider'] = $new_instance['hide_price_slider'];
		
		$instance['hide_filter_btn'] = $new_instance['hide_filter_btn'];
	
		return $instance;
	}
	
	/* ---------------------------- */
	/* ------- Widget Settings ------- */
	/* ---------------------------- */
	
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	*/
 	
	function form( $instance ) {
		//print_r($instance);
		/* Get stuff for price range slider */ 
		$max_price = 0;
		$min_price = 0;
		
		/* Set up some default widget settings. */
		$defaults = array(
                    'title' => '',
                    'minPrice' => $min_price,
                    'maxPrice' => $max_price,
                    'step' => '1',		
                    'hide_keywords' => '',		
                    'hide_category' => '',		
                    'hide_brand' => '',	
                    'hide_speed' => '',
                    'hide_sending_location' => '',
                    'hide_pickup_location' => '',
                    'hide_price_slider' => '0',		
                    'hide_filter_btn' => ''
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		
		<!-- Widget Title: Text Input -->
		<script type="text/javascript">
		function resetSidebarSearchWidget(event){
			event.preventDefault();
			jQuery(".sidebar_search_widget_settings .title").val('');
			jQuery(".sidebar_search_widget_settings .minprice").val('0');
			jQuery(".sidebar_search_widget_settings .maxprice").val('0');
			jQuery(".sidebar_search_widget_settings .step").val('1');
			jQuery(".sidebar_search_widget_settings .hide_keywords").removeAttr("checked");
			jQuery(".sidebar_search_widget_settings .hide_category").removeAttr("checked");
			jQuery(".sidebar_search_widget_settings .hide_speed").removeAttr("checked");
			jQuery(".sidebar_search_widget_settings .hide_sending_location").removeAttr("checked");
			jQuery(".sidebar_search_widget_settings .hide_pickup_location").removeAttr("checked");
			jQuery(".sidebar_search_widget_settings .hide_brand").removeAttr("checked");
			jQuery(".sidebar_search_widget_settings .hide_price_slider").removeAttr("checked");
			jQuery(".sidebar_search_widget_settings .hide_filter_btn").removeAttr("checked");			
			return;
		}
		</script>

		<div class="sidebar_search_widget_settings">
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'framework') ?></label>
				<input class="widefat title" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
			</p>
			
			<!-- Widget Min Price: Text Input -->
			<p>
				<label for="<?php echo $this->get_field_id( 'minPrice' ); ?>"><?php _e('Min price:', 'framework') ?></label>
							<br>
							<label>Set this as 0 to get the minimum from the database, specify a limit if there is a slowdown, you can use the the link below to get the current minimum.</label>
				<input class="widefat minprice" id="<?php echo $this->get_field_id( 'minPrice' ); ?>" name="<?php echo $this->get_field_name( 'minPrice' ); ?>" value="<?php echo $instance['minPrice']; ?>" />
				<a href="#" onClick="jQuery(this).prev().val('<?php echo floor(aw_get_min_max_price('min')); ?>')">Get current minimum</a>
			</p>
			
			<!-- Widget Max Price: Text Input -->
			<p>
				<label for="<?php echo $this->get_field_id( 'maxPrice' ); ?>"><?php _e('Max price:', 'framework') ?></label>
							<br>
							<label>Set this as 0 to get the maximum from the database, specify a limit if there is a slowdown, you can use the the link below to get the current maximum.</label>
				<input class="widefat maxprice" id="<?php echo $this->get_field_id( 'maxPrice' ); ?>" name="<?php echo $this->get_field_name( 'maxPrice' ); ?>" value="<?php echo $instance['maxPrice']; ?>" />
				<a href="#" onClick="jQuery(this).prev().val('<?php echo ceil(aw_get_min_max_price('max')); ?>')">Get current maximum</a>
			</p>
			
			<!-- Widget Step: Text Input -->
			<p>
				<label for="<?php echo $this->get_field_id( 'step' ); ?>"><?php _e('Step:', 'framework') ?></label>
				<input class="widefat step" id="<?php echo $this->get_field_id( 'step' ); ?>" name="<?php echo $this->get_field_name( 'step' ); ?>" value="<?php echo $instance['step']; ?>" />	
			</p>
			
			<p>
				<label><?php _e('Hide widget sections:', 'framework') ?></label><br />				
				<input type="checkbox" class="hide_keywords" id="<?php echo $this->get_field_id( 'hide_keywords' ); ?>" name="<?php echo $this->get_field_name( 'hide_keywords' ); ?>" <?php echo ($instance['hide_keywords'] == "1" ? "checked='checked'" : ""); ?> value="1" /><span> <?php _e('Search box', 'framework') ?></span><br />	
				<input type="checkbox" class="hide_brand" id="<?php echo $this->get_field_id( 'hide_brand' ); ?>" name="<?php echo $this->get_field_name( 'hide_brand' ); ?>" <?php echo ($instance['hide_brand'] == "1" ? "checked='checked'" : ""); ?> value="1" /><span> <?php _e('Brands select box ', 'framework') ?></span><br />	
				<input type="checkbox" class="hide_category" id="<?php echo $this->get_field_id( 'hide_category' ); ?>" name="<?php echo $this->get_field_name( 'hide_category' ); ?>" <?php echo ($instance['hide_category'] == "1" ? "checked='checked'" : ""); ?> value="1" /><span> <?php _e('Categories select box', 'framework') ?></span><br />	
				<input type="checkbox" class="hide_price_slider" id="<?php echo $this->get_field_id( 'hide_price_slider' ); ?>" name="<?php echo $this->get_field_name( 'hide_price_slider' ); ?>" <?php echo ($instance['hide_price_slider'] == "1" ? "checked='checked'" : ""); ?> value="1" /><span> <?php _e('Price Slider', 'framework') ?></span><br />	
				<input type="checkbox" class="hide_speed" id="<?php echo $this->get_field_id( 'hide_speed' ); ?>" name="<?php echo $this->get_field_name( 'hide_speed' ); ?>" <?php echo ($instance['hide_speed'] == "1" ? "checked='checked'" : ""); ?> value="1" /><span> <?php _e('Speed', 'framework') ?></span><br />
				<input type="checkbox" class="hide_sending_location" id="<?php echo $this->get_field_id( 'hide_sending_location' ); ?>" name="<?php echo $this->get_field_name( 'hide_sending_location' ); ?>" <?php echo ($instance['hide_sending_location'] == "1" ? "checked='checked'" : ""); ?> value="1" /><span> <?php _e('Sending Location', 'framework') ?></span><br />
				<input type="checkbox" class="hide_price_slider" id="<?php echo $this->get_field_id( 'hide_filter_btn' ); ?>" name="<?php echo $this->get_field_name( 'hide_filter_btn' ); ?>" <?php echo ($instance['hide_filter_btn'] == "1" ? "checked='checked'" : ""); ?> value="1" /><span> <?php _e('Filter Button', 'framework') ?></span><br />	
				<input type="checkbox" class="hide_pickup_location" id="<?php echo $this->get_field_id( 'hide_pickup_location' ); ?>" name="<?php echo $this->get_field_name( 'hide_pickup_location' ); ?>" <?php echo ($instance['hide_pickup_location'] == "1" ? "checked='checked'" : ""); ?> value="1" /><span> <?php _e('Pick Up Location', 'framework') ?></span><br />
				<input type="checkbox" class="hide_price_slider" id="<?php echo $this->get_field_id( 'hide_filter_btn' ); ?>" name="<?php echo $this->get_field_name( 'hide_filter_btn' ); ?>" <?php echo ($instance['hide_filter_btn'] == "1" ? "checked='checked'" : ""); ?> value="1" /><span> <?php _e('Filter Button', 'framework') ?></span><br />	
			</p>
			
			<a href="#" onClick="resetSidebarSearchWidget(event);">Reset</a>
		</div>
	<?php
	}
        
        function single_row_category( $term, $level, $i, $currentCategory ){
            
            echo '<li class="field category_menu_child' . ( $level != 0 ? ' sub_item_filter submenu-' . $level : '' ) . '">
                    <label class="checkbox' . ($currentCategory == $term->slug ? ' checked' : '') . '" for="c_'.$i.'">
                    <input class="compare_attribute" type="checkbox" name="c['.$i.']" title="c_'.$i.'" value="'.$term->slug.'"' . ($currentCategory == $term->slug ? ' checked="checked"' : '') . ' />
                    <span></span> ' . $term->name . '
                    </label>
                </li>';      
                     
        }

        function single_row_speed( $term, $level, $i, $currentSpeed ){
            echo '<li class="field category_menu_child">
                    <label class="checkbox" for="b_2">
                    <input class="compare_attribute" type="checkbox" name="b[2]" title="b_2" value="fast">
                    <span></span> FastGGGG
                    </label>
                </li>';
            echo '<li class="field category_menu_child' . ( $level != 0 ? ' sub_item_filter submenu-' . $level : '' ) . '">
                    <label class="checkbox' . ($currentSpeed == $term->slug ? ' checked' : '') . '" for="c_'.$i.'">
                    <input class="compare_attribute" type="checkbox" name="c['.$i.']" title="c_'.$i.'" value="'.$term->slug.'"' . ($currentSpeed == $term->slug ? ' checked="checked"' : '') . ' />
                    <span></span> ' . $term->name . '
                    </label>
                </li>';      
                     
        }
        
        
        
        function single_row_brand( $term, $level, $i, $currentBrand ){
            
            echo '<li class="field category_menu_child' . ( $level != 0 ? ' sub_item_filter submenu-' . $level : '' ) . '">
                    <label class="checkbox' . ($currentBrand == $term->slug ? ' checked' : '') . '" for="b_'.$i.'">
                    <input class="compare_attribute" type="checkbox" name="b['.$i.']" title="b_'.$i.'" value="'.$term->slug.'"' . ($currentBrand == $term->slug ? ' checked="checked"' : '') . ' />
                    <span></span> ' . $term->name . '
                    </label>
                </li>';      
                     
        }
        
        function theListTree( $taxonomy = 'product_category', $terms, &$children, $parent = 0, $level = 0, $i = 0, $currentItem ) {

		foreach ( $terms as $key => $term ) {
                        
                    
			if ( $term->parent != $parent  )
                            continue;
			
                        echo "\t";    
                        if($taxonomy == 'product_category'){
                            $this->single_row_category( $term, $level, $i, $currentItem );
                        } else if ($taxonomy == 'product_brand'){
                            
                            $this->single_row_brand( $term, $level, $i, $currentItem );
                        } else if ($taxonomy == 'product_speed'){
                            
                            $this->single_row_speed( 'Really fast', '1', '0', '0' );
                        } else if ($taxonomy == 'product_sending_location'){
                            
                            $this->single_row_sending_location( 'Really fast', '1', '0', '0' );
                        }
                         else if ($taxonomy == 'product_pickup_location'){
                            
                            $this->single_row_pickup_location( 'Really fast', '1', '0', '0' );
                        }
                        $i++;
			
			unset( $terms[$key] );

			if ( isset( $children[$term->term_id] ) )
				$this->theListTree( $taxonomy, $terms, $children, $term->term_id, $level + 1, $i, $currentItem );
		}
	}
        
        function _get_term_hierarchy($taxonomy) {
            
            if ( !is_taxonomy_hierarchical($taxonomy) )
                    return array();
            $children = get_option("{$taxonomy}_children");

            if ( is_array($children) )
                    return $children;
            $children = array();
            $terms = get_terms($taxonomy, array('get' => 'all', 'orderby' => 'id', 'fields' => 'id=>parent'));
            foreach ( $terms as $term_id => $parent ) {
                    if ( $parent > 0 )
                            $children[$parent][] = $term_id;
            }
            update_option("{$taxonomy}_children", $children);

            return $children;
        
        }
        
        
	
}

?>
