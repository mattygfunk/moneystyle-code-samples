<?php global $aw_theme_options; ?>
<!doctype html>
<html class="no-js" itemscope="" itemtype="http://schema.org/Product" lang="en"> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">

<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

 

 <script type='text/javascript'>
  WebFontConfig = {
      google: {
          families: ['Roboto']
      }
  };

  (function(d) {
      var wf = d.createElement('script'),
          s = d.scripts[0];
      wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
      wf.async = true;
      s.parentNode.insertBefore(wf, s);
  })(document);

</script>


<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" async></script>-->



<!--<link rel="stylesheet" href="/wp-content/themes/compare-responsive/css/bootstrap-functions.css" media="none" onload="if(media!='all')media='all'" async>
<noscript><link rel="stylesheet" href="/wp-content/themes/compare-responsive/css/bootstrap-functions.css"></noscript> Bootstrap -->


<link rel='stylesheet' id='main-stylesheet-css'  href='/wp-content/themes/compare-responsive/css/gumby.css' type='text/css' media="none" onload="if(media!='all')media='all'">
<noscript><link rel="stylesheet" href="/wp-content/themes/compare-responsive/css/gumby.css"></noscript>

<link rel='stylesheet' id='custom-stylesheet-css'  href='/wp-content/themes/compare-responsive/css/custom.css' type='text/css' media="none" onload="if(media!='all')media='all'">
<noscript><link rel="stylesheet" href="/wp-content/themes/compare-responsive/css/custom.css"></noscript>

<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" async></script>-->
  
 
    <?php do_action('aw_show_compare_header_version_numbers'); ?>
    
    <?php $start_time = microtime(true); ?>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
<?php  if(isset($_GET['s'])){ ?>
        <!-- SEARCH PAGE NOINDEX -->
        <meta name="robots" content="noindex, nofollow" />
<?php } ?>



        <?php 
        if(is_home() || is_search() || is_front_page()) 
        {
            /* bloginfo('name'); echo ' | '; bloginfo('description');  */
            $title = "Compare & Save when you Send Money Worldwide";
            $description = "Compare sending money to 416 countries with 500+ banks, money transfer companies, and crypto exchanges. MoneyStyle: The world’s best remittance comparison site.";
        } 
        elseif ($wp_query->queried_object->post_name == "r")
        {
            $receiving_country   = $_GET["receiving_country"];
            $sending_country = $_GET["sending_country"];
            $receiving_country        = filter_var($receiving_country, FILTER_SANITIZE_SPECIAL_CHARS);
            $sending_country      = filter_var($sending_country, FILTER_SANITIZE_SPECIAL_CHARS);
            $title = "Send Money to $receiving_country and Save!. Start Now!"; 
            $description = "Before you send money to $receiving_country compare money transfer deals with MoneyStyle.";           
        }
        elseif ($wp_query->queried_object->post_name == "d")
        {
            $receiving_country   = $_GET["receiving_country"];
            $sending_country = $_GET["sending_country"];
            $exchange_1   = $_GET["exchange_1"];
            $exchange_2 = $_GET["exchange_2"];
            $receiving_country        = filter_var($receiving_country, FILTER_SANITIZE_SPECIAL_CHARS);
            $sending_country      = filter_var($sending_country, FILTER_SANITIZE_SPECIAL_CHARS);
            $exchange_1        = filter_var($exchange_1, FILTER_SANITIZE_SPECIAL_CHARS);
            $exchange_2      = filter_var($exchange_2, FILTER_SANITIZE_SPECIAL_CHARS);

            $title = "Sending Money to $receiving_country with $exchange_1"; 
            if ($exchange_2 != "") { $title = $title . " and $exchange_2";}  
            $title = $title . " from $sending_country."; 

            $description = "Using $exchange_1"; 
            if ($exchange_2 != "") { $description = $description . " and $exchange_2";}  
            $description = $description .  " to send money between $receiving_country and $sending_country.";          
        }
        else 
        {
            $title = "" . wp_title('', false) . " | " . get_bloginfo('name');
            $description = "Find the best money transfer deal with live prices from 500+ banks, money transfer companies, and bitcoin exchanges. MoneyStyle: The world’s best remittance comparison site.";

        }
        ?>        
        <title><?php echo $title ?></title>
        <meta name="description" content="<?php echo $description ?>">
        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
        <link rel="alternate" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
        <link rel="alternate" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />

        
<?php if( trim($aw_theme_options['tz_favicon_url']) != "" ): ?>        
        <link rel="shortcut icon" href="<?php echo $aw_theme_options['tz_favicon_url']; ?>" type="image/x-icon" />
<?php else: ?>
        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.png" type="image/x-icon" />
<?php endif; ?>
        
<?php if( trim($aw_theme_options['tz_apple_touch_icon_url']) != "" ): ?>
        <link rel="apple-touch-icon" href="<?php echo $aw_theme_options['tz_apple_touch_icon_url']; ?>" />
<?php else: ?>
        <link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/apple-touch-icon.png" />
<?php endif; ?>

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">        
              
    <?php wp_head(); ?>


    <style>
    <?php
    $tz_colorset5_c1 = $aw_theme_options['tz_colorset5_c1'];
    $tz_colorset4_c3 = $aw_theme_options['tz_colorset4_c3'];
    $tz_colorset4_c2 = $aw_theme_options['tz_colorset4_c2'];
    $tz_colorset4_c1 = $aw_theme_options['tz_colorset4_c1'];
    $tz_colorset3_c1 = $aw_theme_options['tz_colorset3_c1'];
    $tz_colorset3_c2 = $aw_theme_options['tz_colorset3_c2'];
    $tz_colorset2_c2 = $aw_theme_options['tz_colorset2_c2'];
    $tz_colorset2_c1 = $aw_theme_options['tz_colorset2_c1'];
    $tz_colorset1_c2 = $aw_theme_options['tz_colorset1_c2'];
    $tz_colorset1_c1 = $aw_theme_options['tz_colorset1_c1'];
    ?>

    /* Body BG-colour */
    body { background-color: <?php echo $tz_colorset5_c1; ?>; }
    

    /* Main menu element */
    @media only screen and (max-width: 768px) {
            .navbar ul { background: <?php echo $tz_colorset1_c1; ?>; }
    }

    
    
    @media only screen and (max-width: 767px) { 
            .navbar ul li.active .dropdown ul li a:hover { color: <?php echo $tz_colorset2_c1; ?>; }
            .navbar li .dropdown.active .dropdown ul li a:hover { color: <?php echo $tz_colorset2_c1; ?>; }
            .gumby-no-touch .navbar ul li:hover .dropdown ul li a:hover { color: <?php echo $tz_colorset2_c1; ?>; }
            .navbar ul li > a + .dropdown ul a { background: <?php echo $tz_colorset1_c2; ?>; border-bottom-color: <?php echo $tz_colorset1_c1; ?> !important; }
            .gumby-no-touch .navbar ul li:hover > a, .gumby-touch .navbar ul li.active > a { background: <?php echo $tz_colorset1_c2; ?>; }
            .gumby-no-touch .navbar ul li:hover .dropdown, .gumby-touch .navbar ul li.active .dropdown { border-top-color: <?php echo $tz_colorset1_c1; ?> !important;}
    }
    .navbar li .dropdown ul > li a { color: <?php echo $tz_colorset2_c1; ?>; }

    /* Light Blue */
    .topmain > div .logo p { color: <?php echo $tz_colorset2_c2; ?>; }

 
    @media only screen and (max-width: 768px) {
            .primary.btn { background-color: <?php echo $tz_colorset3_c1; ?>; border-color: <?php echo $tz_colorset3_c1; ?>; }
    }
    @media only screen and (min-width: 768px) {
            .gumby-no-touch .main-menu-container .main-menu > li:hover > a, .gumby-no-touch .main-menu-container .main-menu > li:focus > a, .gumby-touch .main-menu-container .main-menu > li:hover > a, .gumby-touch .main-menu-container .main-menu > li:focus > a { color: <?php echo $tz_colorset3_c1; ?>; }
            .gumby-no-touch .main-menu-container .main-menu > li .dropdown li:hover a, .gumby-no-touch .main-menu-container .main-menu > li .dropdown li:focus a, .gumby-touch .main-menu-container .main-menu > li .dropdown li:hover a, .gumby-touch .main-menu-container .main-menu > li .dropdown li:focus a { color: <?php echo $tz_colorset3_c1; ?>; }
    }


    /* Footer background */
    .master-footer, .master-footer p { background: <?php echo $tz_colorset4_c1; ?>; }

    /* $footer-header: #83929B; */
    .master-footer .widget-title { color: <?php echo $tz_colorset4_c3; ?>; }

    /* $footer-text: #c8ced2; */
    .master-footer .row { color: <?php echo $tz_colorset4_c3; ?>; }
</style>

<script>
/**
* Function that tracks a click on an outbound link in Analytics
* This function takes a valid URL string as an argument, and uses that URL string
* as the event label. Setting the transport method to 'beacon' lets the hit be sent
* using 'navigator.sendBeacon' in browser that support it.
*/

    var trackOutboundLink = function(url) {
   ga('send', 'event', 'outbound', 'click', url, {
     'transport': 'beacon',
     'hitCallback': function(){window.open(url);}
   });
}


</script>

<script type="text/javascript">
if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('/sw.js').then(function(registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function(err) {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
    });
  });
}
</script>

<!-- TrustBox script -->
<script src="//widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js" async></script>
<!-- End Trustbox script -->




<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->

<link rel='stylesheet' id='contact-form-7-css'  href='/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.1' type='text/css' media="none" onload="if(media!='all')media='all'">
<noscript><link rel="stylesheet" href="/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.1"></noscript>



<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css" media="none" onload="if(media!='all')media='all'">


<link href="/wp-content/themes/compare-responsive/css/gumby.css" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link href="/wp-content/themes/compare-responsive/css/custom.css" rel="stylesheet" media="none" onload="if(media!='all')media='all'">


<?php if (isset($_GET["fullsite"])) {
    $fullsite = $_GET["fullsite"];
} else {
    $fullsite = "";
}
$fullsite  = filter_var($fullsite, FILTER_SANITIZE_SPECIAL_CHARS);
if ($fullsite=="")
{
    $url = "https://" . $_SERVER['SERVER_NAME'] . "/wp-content/themes/compare-responsive/css/mediaqueries.css";
    echo "<link rel=\"stylesheet\" id=\"mediaqueries\"  href=\"$url\" media=\"none\" onload=\"if(media!='all')media='all'\">";
    echo "<noscript><link rel=\"stylesheet\" href=\"/wp-content/themes/compare-responsive/css/mediaqueries.css\"></noscript>";
}      
?> 

</head>

<body <?php body_class();?>>
<header class="masterheader">
    <div class="row tophead" style="padding: 4px 2px;">
        <div class="twelve columns" id="top_menu_and_social">      
        </div>
    </div>
    <div class="city-selected" style="display: table; width: 100%;min-height: 0px;">
        <article class="providers" style="display: table-cell; vertical-align: middle; text-align: center; padding: 0px;">
            <div class="info" style="margin: auto;">
                <div class="temp" style="padding: 0px;font-size: 32px;"><a id="MoneyStyle" href="<?php echo "http://" . $_SERVER['SERVER_NAME'];?>" style="color:white;"><img src="/logos/logo-top.png" alt="MoneyStyle Smart Money Transfers"></a></div>
            </div>
        </article>
        <figure style="background-image: url(<?php echo "https://" . $_SERVER['SERVER_NAME'];?>/logos/backgrounds/bitcoin.jpg)"></figure>
    </div>
    <?php 
        if( ! is_page_template('template-homepage.php') && ! is_front_page()) :
            echo '<!--<section class="row breadcrumbs"><div class="twelve columns">';
                        get_template_part('functions/views/frontend/content', 'breadcrumb');
            echo '</div></section>-->';
        endif
    ?>
    
    <link rel="stylesheet" href="/ism/css/my-slider.css" media="none" onload="if(media!='all')media='all'">
    <noscript><link rel="stylesheet" href="/ism/css/my-slider.css"></noscript>

    <script src="/ism/js/ism-2.2.min.js"></script>

    <!-- intersection observer polyfill -->
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=IntersectionObserver"></script>
   <!-- the iolazy load library -->
    <script src="/js/iolazy.min.js" defer></script>
</header>

<div class="row master-row" style="padding-left:10px;padding-right:10px;padding-top: 5px;">
