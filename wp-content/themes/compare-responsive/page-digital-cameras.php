<?php get_header(); ?>

	<div class="nine columns push_three product-listing">

	   	<article class="post-content">
        	<h1>Digital Cameras</h1>
			<p>Lorem ipsum dolor sit amet, eu case malorum qualisque pro, ad vis mollis epicuri ullamcorper. Legere omittantur id mea, alia movet forensibus in duo, nam et vide mandamus...</p>
        </article>

        <div class="listing-options">
        	<p class="listing-results">Showing <strong>340</strong> products</p>

          <div class="listing-params">
            <fieldset>
              <legend>Products per page</legend>
              <ul>
                <li class="field">
                  <div class="picker">
                    <select>
                      <option>20</option>
                      <option>30</option>
                      <option>50</option>
                      <option>75</option>
                    </select>
                  </div>
                </li>
                
              </ul>
            </fieldset>

            <fieldset>
              <legend>Sort By</legend>
              <ul>
                <li class="field">
                  <div class="picker">
                    <select>
                      <option>Popularity: Low to High</option>
                      <option>Popularity: High to Low</option>
                      <option>Price: Low to High</option>
                      <option>Price: High to Low</option>
                    </select>
                  </div>
                </li>
                
              </ul>
            </fieldset>

            <fieldset>
              <legend>View</legend>
              <ul id="list-toggle" class="list-toggle">
                <li class="active">
                  <a href="#" class="btn-form">
                    <i class="icon-list"></i>
                  </a>
                </li>
                <li>
                  <a href="#" class="btn-form">
                    <i class="icon-layout"></i>
                  </a>
                </li>
              </ul>
            </fieldset>
          </div>
        </div>

        <div class="product-listing-container grid-view">

          <!-- Product 1 begin -->
          <div class="product">
            <div class="product-photo">
              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/static/categories-photo.jpg" alt="">
            </div>

            <div class="product-desc">
              <h2>Nikon Coolpix P520 - Limited Edition</h2>
              <p>Cras aliquam dictum dui, rutrum pulvinar tortor pharetra ut. Quisque quis metus id arcu consectetur volutpat. Nam eleifend posuere malesuada. In hac habitasse platea dictumst... <a href="#">View more</a></p>
              <div class="reviews">
                <div class="star-4-5"></div>
                <p>14 Reviews</p>
              </div>
            </div>

            <div class="product-view">
              <div>
                <p class="price">from <span>&pound;170</span></p>
                <a href="#" class="retailers">15 Retailers</a>
              </div>
              <div class="medium primary btn metro rounded">
                <a href="#">Compare Prices</a>
              </div>
            </div>

          </div>
          <!-- Product 1 end -->

          <div class="product-1"></div><!-- <div class="product-X clearfix-X"></div> i=0; loop { if(i%2) echo .clearfix-2, if(i%3) echo .clearfix-3 }
          .product-X, X = i%3+1 (1,2,3,1,2,3,1,2,3,...)
          -->

          <!-- Product 2 begin -->
          <div class="product">
            <div class="product-photo">
              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/static/categories-photo.jpg" alt="">
            </div>

            <div class="product-desc">
              <h2>Nikon Coolpix P520</h2>
              <p>Cras aliquam dictum dui, rutrum pulvinar tortor pharetra ut. Quisque quis metus id arcu consectetur volutpat. Nam eleifend posuere malesuada. In hac habitasse platea dictumst... <a href="#">View more</a></p>
              <div class="reviews">
                <div class="star-4-5"></div>
                <p>14 Reviews</p>
              </div>
            </div>

            <div class="product-view">
              <div>
                <p class="price">from <span>&pound;170</span></p>
                <a href="#" class="retailers">15 Retailers</a>
              </div>
              <div class="medium primary btn metro rounded">
                <a href="#">Compare Prices</a>
              </div>
            </div>

          </div>
          <!-- Product 2 end -->

          <div class="product-2 clearfix-2"></div> <!-- <div class="clearfix-X"></div> i=0; loop { if(i%2) echo .clearfix-2, if(i%3) echo .clearfix-3 } -->

          <!-- Product 3 begin -->
          <div class="product">
            <div class="product-photo">
              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/static/categories-photo.jpg" alt="">
            </div>

            <div class="product-desc">
              <h2>Nikon Coolpix P520</h2>
              <p>Cras aliquam dictum dui, rutrum pulvinar tortor pharetra ut. Quisque quis metus id arcu consectetur volutpat. Nam eleifend posuere malesuada. In hac habitasse platea dictumst... <a href="#">View more</a></p>
              <div class="reviews">
                <div class="star-4-5"></div>
                <p>14 Reviews</p>
              </div>
            </div>

            <div class="product-view">
              <div>
                <p class="price">from <span>&pound;170</span></p>
                <a href="#" class="retailers">15 Retailers</a>
              </div>
              <div class="medium primary btn metro rounded">
                <a href="#">Compare Prices</a>
              </div>
            </div>

          </div>
          <!-- Product 3 end -->

          <div class="product-3 clearfix-3"></div><!-- <div class="clearfix-X"></div> i=0; loop { if(i%2) echo .clearfix-2, if(i%3) echo .clearfix-3 } -->

          <!-- Product 4 begin -->
          <div class="product">
            <div class="product-photo">
              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/static/categories-photo.jpg" alt="">
            </div>

            <div class="product-desc">
              <h2>Nikon Coolpix P520</h2>
              <p>Cras aliquam dictum dui, rutrum pulvinar tortor pharetra ut. Quisque quis metus id arcu consectetur volutpat. Nam eleifend posuere malesuada. In hac habitasse platea dictumst... <a href="#">View more</a></p>
              <div class="reviews">
                <div class="star-4-5"></div>
                <p>14 Reviews</p>
              </div>
            </div>

            <div class="product-view">
              <div>
                <p class="price">from <span>&pound;170</span></p>
                <a href="#" class="retailers">15 Retailers</a>
              </div>
              <div class="medium primary btn metro rounded">
                <a href="#">Compare Prices</a>
              </div>
            </div>

          </div>
          <!-- Product 4 end -->

          <div class="product-1 clearfix-2"></div><!-- <div class="clearfix-X"></div> i=0; loop { if(i%2) echo .clearfix-2, if(i%3) echo .clearfix-3 } -->

          <!-- Product 5 begin -->
          <div class="product">
            <div class="product-photo">
              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/static/categories-photo.jpg" alt="">
            </div>

            <div class="product-desc">
              <h2>Nikon Coolpix P520</h2>
              <p>Cras aliquam dictum dui, rutrum pulvinar tortor pharetra ut. Quisque quis metus id arcu consectetur volutpat. Nam eleifend posuere malesuada. In hac habitasse platea dictumst... <a href="#">View more</a></p>
              <div class="reviews">
                <div class="star-4-5"></div>
                <p>14 Reviews</p>
              </div>
            </div>

            <div class="product-view">
              <div>
                <p class="price">from <span>&pound;170</span></p>
                <a href="#" class="retailers">15 Retailers</a>
              </div>
              <div class="medium primary btn metro rounded">
                <a href="#">Compare Prices</a>
              </div>
            </div>

          </div>
          <!-- Product 5 end -->

          <div class="product-2"></div><!-- <div class="clearfix-X"></div> i=0; loop { if(i%2) echo .clearfix-2, if(i%3) echo .clearfix-3 } -->

          <!-- Product 6 begin -->
          <div class="product">
            <div class="product-photo">
              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/static/categories-photo.jpg" alt="">
            </div>

            <div class="product-desc">
              <h2>Nikon Coolpix P520</h2>
              <p>Cras aliquam dictum dui, rutrum pulvinar tortor pharetra ut. Quisque quis metus id arcu consectetur volutpat. Nam eleifend posuere malesuada. In hac habitasse platea dictumst... <a href="#">View more</a></p>
              <div class="reviews">
                <div class="star-4-5"></div>
                <p>14 Reviews</p>
              </div>
            </div>

            <div class="product-view">
              <div>
                <p class="price">from <span>&pound;170</span></p>
                <a href="#" class="retailers">15 Retailers</a>
              </div>
              <div class="medium primary btn metro rounded">
                <a href="#">Compare Prices</a>
              </div>
            </div>

          </div>
          <!-- Product 6 end -->

          <div class="product-3 clearfix-2 clearfix-3"></div><!-- <div class="clearfix-X"></div> i=0; loop { if(i%2) echo .clearfix-2, if(i%3) echo .clearfix-3 } -->

        </div>

        <!-- Pagination -->
        <div class="paginate"><span class="page-numbers current">1</span>
          <a class="page-numbers" href="#">2</a>
          <a class="next page-numbers" href="#">Next »</a>
        </div>

    </div>

    <?php get_sidebar(); ?>

<?php get_footer(); ?>