<?php get_header(); ?>

	<div class="nine columns blog-column push_three">

	    <?php if ( have_posts() ) : ?>
	    
	        <?php while ( have_posts() ) : the_post(); ?>
	        	<!--<article>-->
		        	<p class="meta-tags">
		        		
		        	<span><?php the_title(); ?></span>
					<?php
						//Featured Image
			        	if (function_exists('has_post_thumbnail') && has_post_thumbnail() ) {
			        		echo '<a href="'.get_permalink().'">';
			        		the_post_thumbnail('blog-post'); 
			        		echo '</a>';
			        	}
			        ?>
					
					<div class="post-content">
				        <?php the_content(); ?>
					</div>

	            <!--</article>-->
            
               
            
	        <?php endwhile; ?>
     
	    <?php endif; // end have_posts() check ?>

    <!--</div>-->

    <?php get_sidebar(); ?>

<?php get_footer(); ?>