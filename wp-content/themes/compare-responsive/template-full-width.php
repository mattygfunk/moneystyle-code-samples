<?php
/**
 * Template Name: Full Width
 */
?>
<?php get_header(); ?>

<div class="twelve columns blog-column">

    <?php if ( have_posts() ) : ?>

        <?php while ( have_posts() ) : the_post(); ?>
                <article>
                        
                        <!--<h1><?php // the_title(); ?></h1>-->
                                <?php
                                        //Featured Image
                                if (function_exists('has_post_thumbnail') && has_post_thumbnail() ) {
                                        echo '<a href="'.get_permalink().'">';
                                        the_post_thumbnail('blog-post'); 
                                        echo '</a>';
                                }
                        ?>

                                <div class="post-content">
                                <?php the_content(); ?>
                                </div>

            </article>
            <?php // comments_template(); ?>
        <?php endwhile; ?>

    <?php endif; // end have_posts() check ?>

</div>

<?php get_footer(); ?>