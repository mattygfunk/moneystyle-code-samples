<?php

function generate_search_results($conn, $amount, $sending_country, $receiving_country, $volumetocheck)
{
    $mem_var = new Memcached();
    $mem_var->addServer("127.0.0.1", 11211);
    $key = md5("generate_search_results$amount, $sending_country, $receiving_country, $volumetocheck");
    $response = $mem_var->get("$key");
    if ($response) {
        $search_results = $response;
        echo "generate_search_results$amount, $sending_country, $receiving_country, $volumetocheck<br>";
    }
    else 
    {
        // get general route information
        list($sending_currency, $sending_currency_symbol) = get_currency_currencysymbol($conn, $sending_country);
        list($receiving_currency, $receiving_currency_symbol) = get_currency_currencysymbol($conn, $receiving_country);
        list($exchange_rate, $last_updated_exchange_rate) = get_exchange_rate("$sending_currency$receiving_currency", $conn);
        list($exchange_rate_inverse, $last_updated_exchange_rate_sent_back) = get_exchange_rate("$receiving_currency$sending_currency", $conn);

        $routes_available_between_fromcountry_and_receiving_country = array(); 

        $cryptos_supported = array("BTC", "ETH", "LTC", "XRP", "BCH");

        
        foreach ($cryptos_supported as $key => $crypto) {
            $routes_available_between_fromcountry_and_receiving_country  = get_crypto_routes_available($conn, $sending_currency, $receiving_currency, $volumetocheck, $crypto, $routes_available_between_fromcountry_and_receiving_country);
        }

        
        $routes_available_between_fromcountry_and_receiving_country = get_bank_or_mto_routes_available($routes_available_between_fromcountry_and_receiving_country, $conn, $sending_country, $receiving_country);

        $search_results = array();

        // for all of the routes available, calculate their fees and add them to the $search_results array
        foreach ($routes_available_between_fromcountry_and_receiving_country as $route) 
        {
            $exchange_2 ="";

            $exchange_1                       = $route[0];
            $exchange_2                        = $route[1];
            $recordnumber                  = $route[2];
            $withdrawal_name                = $route[3];
            $funding_name                   = $route[4];
            $crypto                        = $route[5];

            $search_results = get_record_generate_fees_totals_and_add_to_array($conn, $volumetocheck, $exchange_1, $exchange_2, $crypto, $recordnumber, $funding_name, $withdrawal_name, $sending_currency, $receiving_currency, $search_results, $amount, $sending_currency_symbol, $receiving_currency_symbol, $sending_country, $receiving_country, $exchange_rate, $exchange_rate_inverse);

        }

        $mem_var->set($key, $search_results, time() + 5);// or die("search_results Keys Couldn't be Created");
    }

    print_r(error_get_last());
    return $search_results;
}

function get_record_generate_fees_totals_and_add_to_array($conn, $volumetocheck, $exchange_1, $exchange_2, $crypto, $recordnumber, $funding_name, $withdrawal_name, $sending_currency, $receiving_currency, $search_results, $amount, $sending_currency_symbol, $receiving_currency_symbol, $sending_country, $receiving_country, $exchange_rate, $exchange_rate_inverse)
{
    // IF IT's A BITCOIN ROUTE
    if ($exchange_2 != "") 
    {

        $exchange_1_record = get_record_for_crypto_exchange($conn, $volumetocheck, $exchange_1, $sending_currency, $crypto);

        list($acceptsbankdeposit, $acceptsbankdepositfee, $acceptsbankdepositpercentage, $acceptscreditcards, $acceptscreditcardsfee, $acceptscreditcardspercentage, $acceptsdebitcards, $acceptsdebitcardsfee, $acceptsdebitcardspercentage, $acceptssepa, $acceptssepafee, $acceptssepapercentage, $acceptsother1, $acceptsother1name, $acceptsother1fee, $acceptsother1percentage, $acceptsother2, $acceptsother2name, $acceptsother2fee, $acceptsother2percentage) = get_funding_variables_from_buyfrom_record($exchange_1_record); 

        // GET SECOND BITCOIN EXCHANGE INFORMATION
        $exchange_2_record = get_record_for_crypto_exchange($conn, $volumetocheck, $exchange_2, $receiving_currency, $crypto);


        if ($funding_name == "Bank Deposit") {
            $funding_fee  = $acceptsbankdepositfee;
            $funding_percentage = $acceptsbankdepositpercentage;                        
        }
        elseif ($funding_name == "Credit Cards") {
            $funding_fee  = $acceptscreditcardsfee;
            $funding_percentage = $acceptscreditcardspercentage;
        }
        elseif ($funding_name == "Debit Cards") {
            $funding_fee  = $acceptsdebitcardsfee;
            $funding_percentage = $acceptsdebitcardspercentage;
        }
        elseif ($funding_name == "Cash") {
            $funding_fee  = $acceptscashfee;
            $funding_percentage = $acceptscashpercentage;
        }
        elseif ($funding_name == "SEPA") {
            $funding_fee  = $acceptssepafee;
            $funding_percentage = $acceptssepapercentage;
        }
        elseif ($acceptsother1 == "Yes") {
            $funding_name = $acceptsother1name;
            $funding_fee  = $acceptsother1fee;
            $funding_percentage = $acceptsother1percentage;
        }
        elseif ($acceptsother2 == "Yes") {
            $funding_name = $acceptsother2name;
            $funding_fee  = $acceptsother2fee;
            $funding_percentage = $acceptsother2percentage;
        }  
        else
        {
            echo "!!!! funding_name is weird !!!! Currently:" . $funding_name;
        }

        if ($funding_fee=="")
        {
            $funding_fee = 0;
        }

        list($withdrawalstobankdeposit, $withdrawalstobankdepositfee, $withdrawalstobankdepositpercentage, $withdrawalstocreditcards, $withdrawalstocreditcardsfee, $withdrawalstocreditcardspercentage, $withdrawalstodebitcards, $withdrawalstodebitcardsfee, $withdrawalstodebitcardspercentage, $withdrawalstosepa, $withdrawalstosepafee, $withdrawalstosepapercentage, $withdrawalstoother1, $withdrawalstoother1name, $withdrawalstoother1fee, $withdrawalstoother1percentage, $withdrawalstoother2, $withdrawalstoother2name, $withdrawalstoother2fee, $withdrawalstoother2percentage) = get_withdrawal_variables_from_sellto_record($exchange_2_record);


        if ($withdrawal_name == "Bank Deposit") {
            $withdrawal_fee     = $withdrawalstobankdepositfee;
            $withdrawal_percentage = $withdrawalstobankdepositpercentage;
        }
        elseif ($withdrawal_name == "Cash") {
            $withdrawal_fee     = $withdrawalstocashfee;
            $withdrawal_percentage = $withdrawalstocashpercentage;
        }
        elseif ($withdrawal_name == "Credit Cards") {
            $withdrawal_fee     = $withdrawalstocreditcardsfee;
            $withdrawal_percentage = $withdrawalstocreditcardspercentage;
        }
        elseif ($withdrawal_name == "Debit Cards") {
            $withdrawal_fee     = $withdrawalstodebitcardsfee;
            $withdrawal_percentage = $withdrawalstodebitcardspercentage;
        }
        elseif ($withdrawal_name == "SEPA") {
            $withdrawal_fee     = $withdrawalstosepafee;
            $withdrawal_percentage = $withdrawalstosepapercentage;
        }
        elseif ($withdrawal_name == $withdrawalstoother1name) {
            //$withdrawal_name = $withdrawalstoother1name;
            $withdrawal_fee     = $withdrawalstoother1fee;
            $withdrawal_percentage = $withdrawalstoother1percentage;
        }
        elseif ($withdrawal_name == $withdrawalstoother2name) {
            //$withdrawal_name = $withdrawalstoother2name;
            $withdrawal_fee     = $withdrawalstoother2fee;
            $withdrawal_percentage = $withdrawalstoother2percentage;
        }
        else
        {
            echo "if ($withdrawal_name == $withdrawalstoother2name - $withdrawalstoother1name)<br>";
            echo "!!!! withdrawal_name is weird !!!! Currently:" . $withdrawal_name;
        }


        $search_results = generate_fees_and_totals_and_add_route_to_fullresults_array($conn, $search_results, $exchange_1_record, $exchange_2_record, $sending_country, $sending_currency, $receiving_country, $receiving_currency, $receiving_currency_symbol, $amount, $funding_name, $funding_fee, $funding_percentage, $exchange_1, $exchange_2, $withdrawal_name, $withdrawal_fee, $withdrawal_percentage, $crypto);
    
    }
    else //  IT's A BANK OR MTO
    {
        // Search banks_and_mtos for the specific record
        $bank_or_mto_record = get_bank_or_mto_record($conn, $exchange_1, $recordnumber, $sending_country, $receiving_country);

        list($speed_actual, $period_from, $fee, $exchange_rate_margin, $sending_location, $firm_type, $date, $paymentinstrument) = get_variables_from_bank_or_mto_record($bank_or_mto_record);

        //get the website
        $website = get_website($exchange_1, $conn);
        
        list($total_cost, $total_in_receiving_currency, $total_fees, $exchange_rate_margin_fees) = generate_fees_and_totals_bank_or_mto($amount, $exchange_rate_inverse, $exchange_rate, $exchange_rate_margin, $fee);

        $search_results = add_result_to_search_results_array($search_results, "", "", "", "", "", "", "", "", "", "", "", "", $paymentinstrument, "", "", "", "", $exchange_1, "", "", "", "", "",  "", $exchange_rate_margin, $exchange_rate_margin_fees, $sending_location, $firm_type, "", $speed_actual, "", "", $period_from, $date, $fee, $website, $amount, $total_fees, $total_cost, $total_in_receiving_currency, $exchange_rate, $exchange_rate_inverse, $sending_country, $receiving_country, $sending_currency, $receiving_currency, $receiving_currency_symbol, $paymentinstrument, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
    }  
    
    
    print_r(error_get_last());

    return $search_results;   
}

// subroutine of generate_fullresults 
function generate_fees_and_totals_and_add_route_to_fullresults_array($conn, $search_results, $exchange_1_record, $exchange_2_record, $sending_country, $sending_currency, $receiving_country, $receiving_currency, $receiving_currency_symbol, $amount, $funding_name, $funding_fee, $funding_percentage, $exchange_1, $exchange_2, $withdrawal_name, $withdrawal_fee, $withdrawal_percentage, $crypto)
{
    

    list($volume, $buying_crypto_percentage, $buying_crypto_fee, $ask_price, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $network_transaction_fee) = load_buyfrom_description_variables($exchange_1_record);

    list($exchange_2_volume, $selling_crypto_percentage, $selling_crypto_fee, $bid_price, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices) = load_sellto_description_variables($exchange_2_record);

    //get the website
    $website = get_website($exchange_1, $conn);
    $exchange_2_website = get_website($exchange_2, $conn);

    

    //get the real exchange rate
    list($exchange_rate, $last_updated_exchange_rate) = get_exchange_rate("$sending_currency$receiving_currency", $conn);

    //get the real exchange rate when sent back
    list($exchange_rate_inverse, $last_updated_exchange_rate_sent_back) = get_exchange_rate("$receiving_currency$sending_currency", $conn);

	list($total_cost, $total_in_receiving_currency, $total_fees, $network_transaction_fee_currency, $network_transaction_fee, $arbitrage, $selling_crypto_fee, $selling_crypto_fee_currency, $selling_crypto_percentage_currency, $withdrawal_fee_currency, $withdrawal_percentage_currency, $buying_crypto_percentage, $buying_crypto_fee, $funding_percentage_currency, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_percentage_amount_en_route, $withdrawal_fee_amount_en_route, $withdrawal_percentage_amount_en_route) = generate_fees_and_totals_bitcoin($amount, $funding_fee, $buying_crypto_fee, $buying_crypto_percentage, $ask_price, $selling_crypto_percentage, $bid_price,  $exchange_rate_inverse, $exchange_rate, $funding_percentage, $withdrawal_fee, $withdrawal_percentage, $selling_crypto_fee, $network_transaction_fee);
  

    // Bitcoin Exchange Default Variables
    $firm_type                = "Bitcoin Exchange";
    $product                 = "Bitcoin Exchange";
    $sending_location         = "Online";
    $pick_up_method            = "Bank Account";
    $speed_actual = "Less than one hour";
    $period_from = "Almost Live";
    $coverage = "High";
    $paymentinstrument = "N/A";
    $date = "Not used?";
    $fee = "";
    $exchange_rate_margin_fees = "";
    $exchange_rate_margin = "";

    print_r(error_get_last());
 
    $search_results = add_result_to_search_results_array($search_results, $funding_name, $funding_fee, $funding_percentage, $buying_crypto_fee, $buying_crypto_percentage, $network_transaction_fee, $network_transaction_fee_currency, $arbitrage, $selling_crypto_percentage, $selling_crypto_percentage_currency, $selling_crypto_fee, $selling_crypto_fee_currency, $withdrawal_name, $withdrawal_percentage, $withdrawal_percentage_currency, $withdrawal_fee, $withdrawal_fee_currency, $exchange_1, $ask_price, $volume, $exchange_2, $exchange_2_volume, $bid_price, $exchange_2_website, $exchange_rate_margin, $exchange_rate_margin_fees, $sending_location, $firm_type, $product, $speed_actual, $pick_up_method, $coverage, $period_from, $date, $fee, $website, $amount, $total_fees, $total_cost, $total_in_receiving_currency, $exchange_rate, $exchange_rate_inverse, $sending_country, $receiving_country, $sending_currency, $receiving_currency, $receiving_currency_symbol, $paymentinstrument, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_fee_amount_en_route, $withdrawal_percentage_amount_en_route, $withdrawal_fee_amount_en_route);
    
    print_r(error_get_last());

    return $search_results;

}

function add_result_to_search_results_array($search_results, $funding_name, $funding_fee, $funding_percentage, $buying_crypto_fee, $buying_crypto_percentage, $network_transaction_fee, $network_transaction_fee_currency, $arbitrage, $selling_crypto_percentage, $selling_crypto_percentage_currency, $selling_crypto_fee, $selling_crypto_fee_currency, $withdrawal_name, $withdrawal_percentage, $withdrawal_percentage_currency, $withdrawal_fee, $withdrawal_fee_currency, $exchange_1, $ask_price, $volume, $exchange_2, $exchange_2_volume, $bid_price, $exchange_2_website, $exchange_rate_margin, $exchange_rate_margin_fees, $sending_location, $firm_type, $product, $speed_actual, $pick_up_method, $coverage, $period_from, $date, $fee, $website, $amount, $total_fees, $total_cost, $total_in_receiving_currency, $exchange_rate, $exchange_rate_inverse, $sending_country, $receiving_country, $sending_currency, $receiving_currency, $receiving_currency_symbol, $paymentinstrument, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_fee_amount_en_route, $withdrawal_percentage_amount_en_route, $withdrawal_fee_amount_en_route)
{
    print_r(error_get_last());
    if (!is_bitcoin_exchange_route($exchange_2))
    {
        $funding_name = $paymentinstrument;
    }
    $result = array(
        // bitcoin calculations
        $funding_name,
        $funding_fee,
        $funding_fee_amount_en_route,

        $funding_percentage,
        $funding_percentage_currency,
        $funding_percentage_amount_en_route,

        $buying_crypto_fee,
        $buying_crypto_fee_currency,
        $buying_crypto_fee_amount_en_route,

        $buying_crypto_percentage,
        $buying_crypto_percentage_currency,
        $buying_crypto_percentage_amount_en_route,

        $network_transaction_fee,
        $network_transaction_fee_currency,
        $network_transaction_fee_amount_en_route,

        $arbitrage,

        $selling_crypto_percentage,
        $selling_crypto_percentage_currency,
        $selling_crypto_percentage_amount_en_route,


        $selling_crypto_fee,
        $selling_crypto_fee_currency,
        $selling_crypto_fee_amount_en_route,

        $withdrawal_name,
        $withdrawal_percentage,
        $withdrawal_percentage_currency,
        $withdrawal_percentage_amount_en_route,

        $withdrawal_fee,
        $withdrawal_fee_currency,
        $withdrawal_fee_amount_en_route,

        // bitcoin descriptions
        $exchange_1,
        $ask_price,
        $volume,
        $exchange_2,
        $exchange_2_volume,
        $bid_price,
        $exchange_2_website,
        $last_updated_exchange_1_fees, 
        $last_updated_exchange_1_prices, 
        $last_updated_exchange_2_fees, 
        $last_updated_exchange_2_prices,

        // mto calcs
        $exchange_rate_margin,
        $exchange_rate_margin_fees,

        // general descriptions
        $sending_location,
        $firm_type,
        $product,
        $speed_actual,
        $pick_up_method,
        $coverage,
        $period_from,
        $date,
        $fee,
        $coverage,
        $website,
        
        // general calcs
        $amount,
        $total_fees,
        $total_cost,
        $total_in_receiving_currency,
        
        $exchange_rate,
        $exchange_rate_inverse,

        // route fields
        $sending_country,
        $receiving_country,
        $sending_currency,
        $receiving_currency,
        $crypto,
        );


    array_push($search_results, $result);
    //print_r($result);
    return $search_results;
}


// subroutine of both generate_fullresults and generate_fees_and_totals_and_add_route_to_fullresults_array to calculate fees and totals along the route
function generate_fees_and_totals_bitcoin($amount, $funding_fee, $buying_crypto_fee, $buying_crypto_percentage, $ask_price, $selling_crypto_percentage, $bid_price,  $exchange_rate_inverse, $exchange_rate, $funding_percentage, $withdrawal_fee, $withdrawal_percentage, $selling_crypto_fee, $network_transaction_fee)
{
    // DO THE CALCULATIONS TO WORK OUT THE TOTAL RECEIVED IN DESTINATION CURRENCY
    $amountenroute = deduct_funding_fee($amount, $funding_fee);
    $funding_fee_amount_en_route = $amountenroute;

    list($amountenroute, $funding_percentage_currency) = deduct_funding_percentage($funding_percentage, $amountenroute, $funding_fee, $amount);
    $funding_percentage_amount_en_route = $amountenroute;

    list($amountenroute, $buying_crypto_fee, $buying_crypto_fee_currency) = deduct_buying_crypto_fee($buying_crypto_fee, $amountenroute, $ask_price, $funding_fee, $funding_percentage, $amount);
    $buying_crypto_fee_amount_en_route = $amountenroute;

    list($amountenroute, $buying_crypto_percentage_currency) = deduct_buying_crypto_percentage($buying_crypto_percentage, $amountenroute, $ask_price, $buying_crypto_fee, $funding_percentage, $funding_fee, $amount);
    $buying_crypto_percentage_amount_en_route = $amountenroute;

    list($amountenroute, $network_transaction_fee, $network_transaction_fee_currency) = deduct_network_fee($amountenroute, $ask_price, $network_transaction_fee);
    $network_transaction_fee_amount_en_route = $amountenroute;

    list($amountenroute, $arbitrage, $amountenroute_at_exchange_one_currency, $amountenroute_at_exchange_two_currency) = deduct_arbitrage($amountenroute, $ask_price, $bid_price, $exchange_rate_inverse, $buying_crypto_fee, $funding_percentage, $funding_fee, $buying_crypto_percentage, $amount, $network_transaction_fee);
    
    list($amountenroute, $selling_crypto_fee, $selling_crypto_fee_currency) = deduct_selling_bitcoin_fee($selling_crypto_fee, $amountenroute, $exchange_rate_inverse, $ask_price, $bid_price, $buying_crypto_fee, $funding_percentage, $funding_fee, $buying_crypto_percentage, $amount, $network_transaction_fee);
    $selling_crypto_fee_amount_en_route = $amountenroute;

    list($amountenroute, $selling_crypto_percentage_currency) =  deduct_selling_bitcoin_percentage($selling_crypto_percentage, $amountenroute, $ask_price, $exchange_rate_inverse, $bid_price, $selling_crypto_fee, $buying_crypto_fee, $funding_percentage, $funding_fee, $buying_crypto_percentage, $amount, $network_transaction_fee);
    $selling_crypto_percentage_amount_en_route = $amountenroute;

    list($amountenroute, $withdrawal_fee_currency) = deduct_withdrawal_fee($amountenroute, $withdrawal_fee, $exchange_rate_inverse, $selling_crypto_percentage, $selling_crypto_fee_currency, $buying_crypto_fee, $funding_percentage, $funding_fee, $bid_price, $ask_price, $buying_crypto_percentage, $amount, $network_transaction_fee);
    $withdrawal_fee_amount_en_route = $amountenroute;

    list($amountenroute, $withdrawal_percentage_currency) = deduct_withdrawal_percentage($withdrawal_percentage, $amountenroute, $exchange_rate_inverse, $selling_crypto_percentage, $selling_crypto_fee_currency, $buying_crypto_fee, $funding_percentage, $funding_fee, $bid_price, $ask_price, $buying_crypto_percentage, $amount, $selling_crypto_fee, $network_transaction_fee);
    $withdrawal_percentage_amount_en_route = $amountenroute;
    // END OF CALCULATIONS TO WORK OUT THE MONEY RECEIVED AFTER WITHDRAWAL

    if(is_nan($withdrawal_percentage_currency))
    {
        $withdrawal_percentage_currency = 0;
    }

    if(is_nan($selling_crypto_percentage_currency))
    {
        $selling_crypto_percentage_currency = 0;
    }

    if(is_nan($buying_crypto_percentage_currency))
    {
        $buying_crypto_percentage_currency = 0;
    }
    //print_r(error_get_last());

    //echo "$funding_fee + $funding_percentage_currency + $buying_crypto_fee_currency + $buying_crypto_percentage_currency + $network_transaction_fee_currency+ $selling_crypto_fee_currency + $selling_crypto_percentage_currency + $withdrawal_percentage_currency + $withdrawal_fee_currency;
    //$total_cost = $total_fees + $arbitrage";
    // here's another way to calculate the total is correct
    $total_fees = $funding_fee + $funding_percentage_currency + $buying_crypto_fee_currency + $buying_crypto_percentage_currency + $network_transaction_fee_currency+ $selling_crypto_fee_currency + $selling_crypto_percentage_currency + $withdrawal_percentage_currency + $withdrawal_fee_currency;
    $total_cost = $total_fees + $arbitrage;

    $total_in_receiving_currency = $amountenroute;

    
    $total_fees = number_format((float) $total_fees, 2, '.', '');
    $total_in_receiving_currency = number_format((float) $total_in_receiving_currency, 2, '.', '');
    $total_cost = number_format((float) $total_cost, 2, '.', '');
    

    return array($total_cost, $total_in_receiving_currency, $total_fees, $network_transaction_fee_currency, $network_transaction_fee, $arbitrage, $selling_crypto_fee, $selling_crypto_fee_currency, $selling_crypto_percentage_currency, $withdrawal_fee_currency, $withdrawal_percentage_currency, $buying_crypto_percentage, $buying_crypto_fee, $funding_percentage_currency, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_percentage_amount_en_route, $withdrawal_fee_amount_en_route, $withdrawal_percentage_amount_en_route);
}


function generate_fees_and_totals_bank_or_mto($amount, $exchange_rate_inverse, $exchange_rate, $exchange_rate_margin, $fee)
{
    $exchange_rate_margin_fees = (($amount - $fee) * $exchange_rate_margin) / 100;
    $total_fees = $exchange_rate_margin_fees + $fee;
    $total_cost         = $total_fees;
    $total_in_receiving_currency = ($amount - $total_fees) * $exchange_rate;


    $total_fees = number_format((float) $total_fees, 2, '.', '');
    $total_in_receiving_currency = number_format((float) $total_in_receiving_currency, 2, '.', '');
    $total_cost = number_format((float) $total_cost, 2, '.', '');

    return array($total_cost, $total_in_receiving_currency, $total_fees, $exchange_rate_margin_fees);
}



// ADD BANK AND MTO ROUTES TO $routes_available_between_fromcountry_and_receiving_country
function get_bank_or_mto_routes_available($routes_available_between_fromcountry_and_receiving_country, $conn, $sending_country, $receiving_country)
{
    $sql = "SELECT BankName, RecordNumber FROM `banks_and_mtos` WHERE `FromCountry` = '$sending_country' AND  `ToCountry` = '$receiving_country'";

    $retval = mysqli_query($conn ,$sql);
    if (!$retval) {
        die('Could not get bank specific exchange data: ' . mysqli_error() . '<br>sql=' . $sql . '<br>');
    }

    if (mysqli_num_rows($retval) > 0) {

        while ($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)){

            $name                          = "{$row['BankName']}";
            $recordnumber                  = "{$row['RecordNumber']}";
            
            $route                        = array(
                $name,
                "",
                $recordnumber,
                "",
                "",
                ""
            );
            
            array_push($routes_available_between_fromcountry_and_receiving_country, $route);
        }
    }

    mysqli_free_result($retval);
    return $routes_available_between_fromcountry_and_receiving_country;
}

// ADD BITCOIN ROUTES TO $routes_available_between_fromcountry_and_receiving_country
function get_crypto_routes_available($conn, $sending_currency, $receiving_currency, $volumetocheck, $crypto, $routes_available_between_fromcountry_and_receiving_country)
{
    // Get the list of exchanges accepting the from currency, and which meet the specified volume minimum
    $sqladdition = generate_sql_addition($volumetocheck, $crypto);
    $sql         = "SELECT Name, Volume, AcceptsBankDeposit, AcceptsCreditCards, AcceptsDebitCards, AcceptsSEPA, AcceptsOther1, AcceptsOther1Name, AcceptsOther2, AcceptsOther2Name FROM bitcoin_exchanges WHERE `Currency` = '$sending_currency' AND `Crypto` = '$crypto' AND `IsActive` = 'True' " . $sqladdition;
    $retval      = mysqli_query($conn, $sql);
    if (!$retval) 
    {
        die('Could not get bitcoin exchange data: ' . mysqli_error($conn) . '<br>sql=' . $sql . '<br>');
    }
    
    // While there are exchanges available to receive the funds
    while ($row = mysqli_fetch_array($retval)) {
        $name                           = "{$row['Name']}";
        $volume                         = "{$row['Volume']}";
        $acceptsbankdeposit             = "{$row['AcceptsBankDeposit']}";
        $acceptscreditcards             = "{$row['AcceptsCreditCards']}";
        $acceptsdebitcards              = "{$row['AcceptsDebitCards']}";
        $acceptssepa                    = "{$row['AcceptsSEPA']}";
        $acceptsother1                  = "{$row['AcceptsOther1']}";
        $acceptsother1name              = "{$row['AcceptsOther1Name']}";
        $acceptsother2                  = "{$row['AcceptsOther2']}";
        $acceptsother2name              = "{$row['AcceptsOther2Name']}";
        
        if ($acceptsbankdeposit == "Yes") {
            $funding_name = "Bank Deposit";
            $routes_available_between_fromcountry_and_receiving_country = discover_routes_for_funding_method($conn, $routes_available_between_fromcountry_and_receiving_country, $funding_name, $name, $receiving_currency, $volumetocheck, $crypto);
        }
        if ($acceptscreditcards == "Yes") {
            $funding_name = "Credit Cards";
            $routes_available_between_fromcountry_and_receiving_country = discover_routes_for_funding_method($conn, $routes_available_between_fromcountry_and_receiving_country, $funding_name, $name, $receiving_currency, $volumetocheck, $crypto);
        }
        if ($acceptsdebitcards == "Yes") {
            $funding_name = "Debit Cards";
            $routes_available_between_fromcountry_and_receiving_country = discover_routes_for_funding_method($conn, $routes_available_between_fromcountry_and_receiving_country, $funding_name, $name, $receiving_currency, $volumetocheck, $crypto);
        }
        if ($acceptssepa == "Yes") {
            $funding_name = "SEPA";
            $routes_available_between_fromcountry_and_receiving_country = discover_routes_for_funding_method($conn, $routes_available_between_fromcountry_and_receiving_country, $funding_name, $name, $receiving_currency, $volumetocheck, $crypto);
        }
        if ($acceptsother1 == "Yes") {
            $funding_name = $acceptsother1name;
            $routes_available_between_fromcountry_and_receiving_country = discover_routes_for_funding_method($conn, $routes_available_between_fromcountry_and_receiving_country, $funding_name, $name, $receiving_currency, $volumetocheck, $crypto);
        }
        if ($acceptsother2 == "Yes") {
            $funding_name = $acceptsother2name;
            $routes_available_between_fromcountry_and_receiving_country = discover_routes_for_funding_method($conn, $routes_available_between_fromcountry_and_receiving_country, $funding_name, $name, $receiving_currency, $volumetocheck, $crypto);
        }
    }            
    
    return $routes_available_between_fromcountry_and_receiving_country;
}

# subroutine of get_bitcoin_results to discover the routes that are available for each funding method
function discover_routes_for_funding_method($conn, $routes_available_between_fromcountry_and_receiving_country, $funding_name, $name, $receiving_currency, $volumetocheck, $crypto)
{
    
    // GET THE LIST OF BITCOIN EXCHANGES THAT DELIVER FUNDS IN THE TO CURRENCY
    $sqladdition = generate_sql_addition($volumetocheck, $crypto);
    $sql         = "SELECT Name FROM bitcoin_exchanges WHERE `Currency` = '$receiving_currency' AND `IsActive` = 'True' " . $sqladdition;
    //echo $sql;
    $returnvalue = mysqli_query($conn, $sql);
    if (!$returnvalue) {
        die('Could not get data: ' . mysqli_error($conn));
    }
    
    
    // GET THE LIST OF EXCHANGES THAT DELIVERS FUNDS IN TO CURRENCY WITH THE SPECIFIED OPTIONS
    while ($rowofproviders = mysqli_fetch_array($returnvalue)) 
    {
        $exchange_2       = "{$rowofproviders['Name']}";

        $sqladdition   = generate_sql_addition($volumetocheck, $crypto);
        $sql           = "SELECT WithdrawalsToBankDeposit, WithdrawalsToCreditCards, WithdrawalsToDebitCards, WithdrawalsToSEPA, WithdrawalsToOther1, WithdrawalsToOther1Name, WithdrawalsToOther2, WithdrawalsToOther2Name FROM bitcoin_exchanges WHERE `Name` = '$exchange_2' AND `Currency` = '$receiving_currency'  AND `IsActive` = 'True' " . $sqladdition;
        $data          = mysqli_query($conn, $sql);
        if (!$data) {
            die('Could not get bitcoin exchange data: ' . mysqli_error() . '<br>sql=' . $sql . '<br>');
        }


        // GO THROUGH EVERY EXCHANGE THAT DELIVERS FUNDS IN TO CURRENCY WITH THE SPECIFIED OPTIONS
        while ($row = mysqli_fetch_array($data)) 
        {
            $recordnumber                           = "";
            $withdrawalstobankdeposit               = "{$row['WithdrawalsToBankDeposit']}";
            $withdrawalstocreditcards               = "{$row['WithdrawalsToCreditCards']}";
            $withdrawalstodebitcards                = "{$row['WithdrawalsToDebitCards']}";
            $withdrawalstosepa                      = "{$row['WithdrawalsToSEPA']}";
            $withdrawalstoother1                    = "{$row['WithdrawalsToOther1']}";
            $withdrawalstoother1name                = "{$row['WithdrawalsToOther1Name']}";
            $withdrawalstoother2                    = "{$row['WithdrawalsToOther2']}";
            $withdrawalstoother2name                = "{$row['WithdrawalsToOther2Name']}";

            if ($withdrawalstobankdeposit == "Yes") {
                $withdrawal_name = "Bank Deposit";
                $routes_available_between_fromcountry_and_receiving_country = add_to_routes_available_on_this_route_array($routes_available_between_fromcountry_and_receiving_country, $name, $exchange_2, $recordnumber, $withdrawal_name, $funding_name, $crypto);
            }
            if ($withdrawalstocreditcards == "Yes") {
                $withdrawal_name = "Credit Cards";
                $routes_available_between_fromcountry_and_receiving_country = add_to_routes_available_on_this_route_array($routes_available_between_fromcountry_and_receiving_country, $name, $exchange_2, $recordnumber, $withdrawal_name, $funding_name, $crypto);
            }
            if ($withdrawalstocreditcards == "Yes") {
                $withdrawal_name = "Debit Cards";
                $routes_available_between_fromcountry_and_receiving_country = add_to_routes_available_on_this_route_array($routes_available_between_fromcountry_and_receiving_country, $name, $exchange_2, $recordnumber, $withdrawal_name, $funding_name, $crypto);
            }
            if ($withdrawalstosepa == "Yes") {
                $withdrawal_name = "SEPA";
                $routes_available_between_fromcountry_and_receiving_country = add_to_routes_available_on_this_route_array($routes_available_between_fromcountry_and_receiving_country, $name, $exchange_2, $recordnumber, $withdrawal_name, $funding_name, $crypto);
            }
            if ($withdrawalstoother1 == "Yes") {
                $withdrawal_name = $withdrawalstoother1name;
                $routes_available_between_fromcountry_and_receiving_country = add_to_routes_available_on_this_route_array($routes_available_between_fromcountry_and_receiving_country, $name, $exchange_2, $recordnumber, $withdrawal_name, $funding_name, $crypto);
            }                    
            if ($withdrawalstoother2 == "Yes") {
                $withdrawal_name = $withdrawalstoother2name;
                $routes_available_between_fromcountry_and_receiving_country = add_to_routes_available_on_this_route_array($routes_available_between_fromcountry_and_receiving_country, $name, $exchange_2, $recordnumber, $withdrawal_name, $funding_name, $crypto);
            }  
        }
    }
    return $routes_available_between_fromcountry_and_receiving_country;
}

# subroutine of discover_routes_for_funding_method to add the route to the $routes_available_between_fromcountry_and_receiving_country array
function add_to_routes_available_on_this_route_array($routes_available_between_fromcountry_and_receiving_country, $name, $exchange_2, $recordnumber, $withdrawal_name, $funding_name, $crypto)
{
    // PUSH THE RESULT TO THE RESULTS ARRAY
    $route = array(
        $name,
        $exchange_2,
        $recordnumber,
        $withdrawal_name,
        $funding_name,
        $crypto
    );
    array_push($routes_available_between_fromcountry_and_receiving_country, $route);
    return $routes_available_between_fromcountry_and_receiving_country;
}
// END OF GETTING A LIST OF ROUTES THAT ARE AVAILBLE


// FUNCTIONS FOR A SEARCH RESULT
function print_search_result($sending_currency, $conn, $sending_country, $receiving_country, $receiving_currency, $funding_fee, $funding_name, $website, $exchange_1, $sending_location, $firm_type, $product, $speed_actual, $pick_up_method, $coverage, $period_from, $date, $amount, $sending_currency_symbol, $receiving_currency_symbol, $exchange_2, $exchange_2_volume, $bid_price, $exchange_2_website, $network_transaction_fee, $withdrawal_name, $withdrawal_fee, $total_fees, $arbitrage, $total_cost, $total_in_receiving_currency, $exchange_rate_inverse, $ask_price, $network_transaction_fee_currency, $selling_crypto_percentage_currency, $selling_crypto_percentage, $exchange_rate_margin, $exchange_rate_margin_fees, $exchange_rate, $fee, $search_results, $volumetocheck, $funding_percentage, $buying_crypto_percentage, $withdrawal_fee_currency, $selling_crypto_fee, $buying_crypto_fee, $withdrawal_percentage, $withdrawal_percentage_currency, $listoffundingtowithdrawalmethods, $viewthisroute, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $selling_crypto_fee_currency, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency)
{
    print_r(error_get_last());
    //$exchange_1                    = preg_replace('/[^A-Za-z0-9\-\' .]/', '', $exchange_1);
    $exchange_1                    = preg_replace('/\/\\//', '', $exchange_1);
    
    $resultnumber = rand();
    setlocale(LC_MONETARY,"en_US"); 

    $logofile = strtolower($exchange_1);
    $logofile = preg_replace('/\s+/', '', $logofile);
    $logofile = $logofile . "-70px.png";

    $exchange_2logofile = strtolower($exchange_2);
    $exchange_2logofile = preg_replace('/\s+/', '', $exchange_2logofile);
    $exchange_2logofile = $exchange_2logofile . "-70px.png";


    $url= "../d/?Amount=".$amount."&FromCountry=".$sending_country."&receiving_country=".$receiving_country."&withdrawalname=".$withdrawal_name."&funding_name=".$funding_name."&buyfrom=".$exchange_1."&sellto=".$exchange_2;
    
    $amount = number_format((float) $amount, 2, '.', '');
    list($amount_whole, $amount_decimal) = explode('.', $amount);

    $total_in_receiving_currency = number_format((float) $total_in_receiving_currency, 2, '.', '');
    list($total_in_receiving_currency_whole, $total_in_receiving_currency_decimal) = explode('.', $total_in_receiving_currency);

    if ($total_cost<0)
    {
        $total_cost_positive = $total_cost * -1;
        $total_cost_tocurrency = $total_cost * -1 * $exchange_rate;
    }
    else
    {
        $total_cost_positive = $total_cost;
        $total_cost_tocurrency = $total_cost * $exchange_rate;
    }
    $total_cost_positive = number_format((float) $total_cost_positive, 2, '.', '');
    list($total_cost_currency_whole, $total_cost_currency_decimal) = explode('.', $total_cost_positive);

    $total_cost_tocurrency = number_format((float) $total_cost_tocurrency, 2, '.', '');
    list($total_cost_tocurrency_whole, $total_cost_tocurrency_decimal) = explode('.', $total_cost_tocurrency);


    ?>
    <div class="row" style="display: table; padding:12px;background: #e8ffff; height: 110px;border-top: grey 1px; border-style: solid;">
        <div class="two columns" style="display: table-cell; float: none; vertical-align: middle;">
            <div class="amount_whole" style="font-size: 26px;">
                <?php 
                    $amount_whole = number_format ($amount_whole, 0);
                    echo "$sending_currency_symbol$amount_whole";
                ?></div><div class="amount_decimal">.<?php echo "$amount_decimal";?></div><br>
            <div class="currency"><?php echo $sending_currency;?></div>
        </div>

        <div class ="one column" style="display: table-cell; float: none; vertical-align: middle;">
            <img src="../logos/angle-right.png" style="max-height:40px; vertical-align: middle;" class=".img-responsive" alt="Send <?php echo "$receiving_currency_symbol$amount from $sending_country";?>">
        </div>
<?php
    if ($exchange_2 != "")
    {
        // If it's a bitcoin exchange print small provider1 and provider2 columns with a arrow between them
        // Print the provider1 column -->
        ?>
        <div class ="three columns" style="display: table-cell; float: none; vertical-align: middle; text-align: center;">
            <img src="../logos/providers-70px/<?php echo $logofile;?>" class=".img-responsive img-rounded" style="width: 75%; max-height: 62px;" alt="<?php echo $exchange_1;?> Logo" id="exchange_1_logo">
        </div>
        <div class ="one column" style="display: table-cell; float: none; vertical-align: middle;">
            <?php
            if ($crypto=="ETH")
            {
                echo "<img src=\"../logos/angle-right-ethereum.png\" style=\"max-height:40px; vertical-align: middle;\" class=\".img-responsive\" alt=\"Ethereum\">";
            }
            elseif ($crypto=="BTC") 
            {
                echo "<img src=\"../logos/angle-right-bitcoin.png\" style=\"max-height:40px; vertical-align: middle;\" class=\".img-responsive\" alt=\"Bitcoin\">";
            }
            elseif ($crypto=="XRP") 
            {
                echo "<img src=\"../logos/angle-right-ripple.png\" style=\"max-height:40px; vertical-align: middle;\" class=\".img-responsive\"  alt=\"Ripple\">";
            }
            elseif ($crypto=="LTC") 
            {
                echo "<img src=\"../logos/angle-right-litecoin.png\" style=\"max-height:40px; vertical-align: middle;\" class=\".img-responsive\"  alt=\"Litecoin\">";
            }
            elseif ($crypto=="BCH") 
            {
                echo "<img src=\"../logos/angle-right-bitcoin-cash.png\" style=\"max-height:40px; vertical-align: middle;\" class=\".img-responsive\"  alt=\"Bitcoin Cash\">";
            }
            ?>
        </div>
        <?php // Print the provider2 column ?>
        <div class ="three columns" style="display: table-cell; float: none; vertical-align: middle; text-align: center;">
            <img src="../logos/providers-70px/<?php echo $exchange_2logofile;?>" class=".img-responsive img-rounded" style="width: 75%; max-height: 62px;" alt="<?php echo $exchange_2;?> Logo" id="exchange_2_logo">
        </div>
        <?php
    }
    // If it's a traditional provider print an extra large provider1 column
    else
    {
        ?>
        <div class ="seven columns" style="display: table-cell; float: none; vertical-align: middle; text-align: center;">
            <img src="../logos/providers-70px/<?php echo $logofile;?>" class=".img-responsive img-rounded" style="width: 75%; max-height: 62px;max-width: 190px;" alt="<?php echo $exchange_1;?> Logo">
        </div>

        <?php
    }
?>
        <div class ="one column" style="display: table-cell; float: none; vertical-align: middle;">
            <img src="../logos/angle-right.png" style="max-height:40px; vertical-align: middle;" class=".img-responsive" alt="Receive <?php echo "$receiving_currency_symbol$total_in_receiving_currency_whole$total_in_receiving_currency_decimal in $receiving_country";?>">
        </div><!-- Print the Amount Received column -->  
        <div class ="two columns" style="display: table-cell; float: none; vertical-align: middle;">
            <div class="amount_whole" style="font-size: 26px;">
                <?php 
                    $total_in_receiving_currency_whole = number_format ($total_in_receiving_currency_whole, 0); 
                    echo "$receiving_currency_symbol$total_in_receiving_currency_whole";
                ?></div><div class="amount_decimal">.<?php echo "$total_in_receiving_currency_decimal";?></div><br>
            <div class="currency"><?php echo $receiving_currency;?></div>
        </div>
    </div>
    <div class="row" style="display: table;">
        <div class="twelve columns" style="">
            <div class="row" style="background-color: rgba(187, 233, 255, 0.6);border: 0px; display: table; text-align: center;padding:15px;">
            <?php
                if ($exchange_2=="")
                {
                    $date = str_replace("Apr","04",$date);
                    $date = str_replace("/","-",$date);
                    $datetime = new DateTime($date);
                    $date = $datetime->format('Y-m-d');
                    $last_updated = $date;
                }
                else
                {
                    if ($last_updated_exchange_1_prices>$last_updated_exchange_2_prices)
                    {
                        $last_updated = $last_updated_exchange_1_prices;
                    }
                    else
                    {
                        $last_updated = $last_updated_exchange_2_prices;
                    }
                }
                            
                if ($viewthisroute==false)
                {
                    ?>
                        <div class="twelve columns" style="display: table-cell; float: none; vertical-align: middle;">
                    <?php
                }
                else
                {
                    ?>
                        <div class="six columns" style="display: table-cell; float: none; vertical-align: middle;">
                    <?php
                }
                if($total_cost<0)
                {
                    $color = "black";
                }
                else
                {
                    $color = "black";
                }    
                ?>
                <div class="you_make_or_pay" style="text-shadow: 2px 2px 2px rgb(125, 125, 1);color: <?php echo $color;?>">
                    <div style="display: inline;"><?php if($total_cost<0){ echo "Make"; } else { echo "Pay";}?></div> 
                    <div class="amount_whole">
                        <?php echo "$receiving_currency_symbol$total_cost_tocurrency_whole";?></div>.<div class="amount_decimal" style="font-size: 16px;"><?php echo $total_cost_tocurrency_decimal;?>
                        <div class="currency"><?php echo $receiving_currency;?></div>
                    </div>
                    <div style="display: inline;"><?php if($total_cost<0){ echo "Extra"; } else { echo "In Fees";}?></div>
                </div>
                (
                <div class="amount_whole"><?php echo "$sending_currency_symbol$total_cost_currency_whole";?></div>.<div class="amount_decimal"><?php echo $total_cost_currency_decimal;?></div> 
                <div class="currency"><?php echo $sending_currency;?></div>
                )
                <br>

                <div style="text-align:center;color: black;font-size: 12px;margin: -4px;">
                Updated: <?php echo time_elapsed_string($last_updated);?>    
                </div>


                </div>

                <?php
                if ($viewthisroute==true)
                {
                    ?>
                    <div class="six columns" style="display: table-cell; float: none; vertical-align: middle;">
                        <form class="form-style-9" action="./d/" style="padding: 12px 0px 0px 0px;margin:0px;">
                            <input type="hidden" name="amount" value="<?php echo $amount;?>">
                            <input type="hidden" name="sending_country" value="<?php echo $sending_country;?>">
                            <input type="hidden" name="receiving_country" value="<?php echo $receiving_country;?>">
                            <input type="hidden" name="exchange_1" value="<?php echo $exchange_1;?>">
                            <input type="hidden" name="exchange_2" value="<?php echo $exchange_2;?>">
                            <input type="hidden" name="funding_name" value="<?php echo $funding_name;?>">
                            <input type="hidden" name="withdrawal_name" value="<?php echo $withdrawal_name;?>">
                            <input type="hidden" name="crypto" value="<?php echo $crypto;?>">
                            <input type="hidden" name="volume" value="<?php echo $volumetocheck;?>">
                            <button type="submit" value="View Best Deal" style="">Receive <?php echo "$receiving_currency_symbol$total_in_receiving_currency_whole";?><span class="amount_decimal" style="color:#bdbdbd;">.<?php echo "$total_in_receiving_currency_decimal";?></span></button>
                        </form>
                    </div>
                    <?php
                }
            ?>
            </div>




        </div>
    </div>
    <?php
}


function print_search_placeholder($resultnumber, $viewthisroute, $sending_currency_symbol, $receiving_currency_symbol, $amount, $sending_currency, $receiving_currency, $logofile, $exchange_2logofile, $exchange_1, $exchange_2, $crypto, $last_updated, $total_in_receiving_currency_whole, $total_in_receiving_currency_decimal, $total_cost, $exchange_rate, $exchange_rate_inverse)
{
    ?>
    <div class="row" style="display: table; padding:12px;background: #e8ffff; height: 110px;border-top: grey 1px; border-style: solid;">
        <div class="two columns" style="display: table-cell; float: none; vertical-align: middle;">
            <div class="amount_whole" style="font-size: 26px;"><?php  $amount = number_format ($amount, 0); echo "$sending_currency_symbol$amount";?></div>
            <br>
            <div class="currency"><?php echo $sending_currency;?></div>
        </div>

        <div class ="one column" style="display: table-cell; float: none; vertical-align: middle;">
            <img src="../logos/angle-right.png" style="max-height:40px; vertical-align: middle;" class=".img-responsive" alt="">
        </div>


        <?php
            if (is_nan($total_cost))
            {
                $total_cost = 0;
            }

            if ($total_cost<0)
            {
                $total_cost_positive = $total_cost * $exchange_rate * -1;
                $total_cost_sending_currency_positive = $total_cost * -1;
            }
            else
            {
                $total_cost_positive = $total_cost * $exchange_rate;
                $total_cost_sending_currency_positive = $total_cost;
            }
            $total_cost_positive = number_format((float) $total_cost_positive, 2, '.', '');
            list($total_cost_positive_whole, $total_cost_positive_decimal) = explode('.', $total_cost_positive);

            $total_cost_sending_currency_positive = number_format((float) $total_cost_sending_currency_positive, 2, '.', '');
            list($total_cost_sending_currency_positive_whole, $total_cost_sending_currency_positive_decimal) = explode('.', $total_cost_sending_currency_positive);


            if ($exchange_2!="")
            {
                ?>
                    <div class ="three columns" style="display: table-cell; float: none; vertical-align: middle; text-align: center;">
                        <img src="<?php echo $logofile;?>" class=".img-responsive img-rounded" style="width: 75%; max-height: 62px;" alt="Logo" id="exchange_1_logo">
                    </div>
                    <?php
                        if ($crypto=="ETH")
                        {
                            $image = "../logos/angle-right-ethereum.png";
                        }
                        elseif ($crypto=="BTC") 
                        {
                            $image = "../logos/angle-right-bitcoin.png";
                        }
                        elseif ($crypto=="XRP") 
                        {
                            $image = "../logos/angle-right-ripple.png";
                        }
                        elseif ($crypto=="LTC") 
                        {
                            $image = "../logos/angle-right-litecoin.png";
                        }
                        elseif ($crypto=="BCH") 
                        {
                            $image = "../logos/angle-right-bitcoin-cash.png";
                        }


                        ?>
                    <div class ="one column" style="display: table-cell; float: none; vertical-align: middle;">
                        <img style="max-height:40px; vertical-align: middle;" class=".img-responsive" alt="" id="crypto_logo" src="<?php echo $image;?>">
                    </div>
                    <div class ="three columns" style="display: table-cell; float: none; vertical-align: middle; text-align: center;">
                        <img src="<?php echo $exchange_2logofile;?>" class=".img-responsive img-rounded" style="width: 75%; max-height: 62px;" alt="Logo" id="exchange_2_logo">
                    </div>
                <?php
            }
            // If it's a traditional provider print an extra large provider1 column
            else
            {
                ?>
                <div class ="seven columns" style="display: table-cell; float: none; vertical-align: middle; text-align: center;">
                    <img src="<?php echo $logofile;?>" class=".img-responsive img-rounded" style="width: 75%; max-height: 62px;max-width: 190px;" alt="<?php echo $exchange_1;?> Logo">
                </div>

                <?php
            }
            ?>
        <div class ="one column" style="display: table-cell; float: none; vertical-align: middle;">
            <img src="../logos/angle-right.png" style="max-height:40px; vertical-align: middle;" class=".img-responsive" alt="Receive">
        </div>
        <div class ="two columns" style="display: table-cell; float: none; vertical-align: middle;">
            <div class="amount_whole" style="font-size: 26px;"><?php echo $receiving_currency_symbol;?><span id="total_in_receiving_currency_whole"><?php $total_in_receiving_currency_whole = number_format($total_in_receiving_currency_whole, 0); echo $total_in_receiving_currency_whole;?></span></div><div class="amount_decimal">.<span id="total_in_receiving_currency_decimal"><?php echo $total_in_receiving_currency_decimal;?></span></div>
            <br>
            <div class="currency"><?php echo $receiving_currency;?></div>
        </div>
    </div>
    <div class="row" style="display: table;">
        <div class="twelve columns" style="">
            <div class="row" style="background-color: rgba(187, 233, 255, 0.6);border: 0px; display: table; text-align: center;padding:15px;">

                <?php       
                    if ($viewthisroute==false)
                    {
                        ?>
                            <div class="twelve columns" style="display: table-cell; float: none; vertical-align: middle;">
                        <?php
                    }
                    else
                    {
                        ?>
                            <div class="six columns" style="display: table-cell; float: none; vertical-align: middle;">
                        <?php
                    }
                ?>
                    <div class="you_make_or_pay" style="text-shadow: 2px 2px 2px rgb(125, 125, 1);">
                        <div style="display: inline;"><span id="make_pay"><?php if($total_cost<0){ echo "Make"; } else { echo "Pay";}?>
                            <div class="amount_whole">
                        <?php echo "$receiving_currency_symbol$total_cost_positive_whole";?></div>.<div class="amount_decimal" style="font-size: 16px;"><?php echo $total_cost_positive_decimal;?>
                        <div class="currency"><?php echo $receiving_currency;?></div></span>
                        </div>
                        <?php if($total_cost<0){ echo "Extra"; } else { echo "In Fees";}?>
                    </div></div> 

                    ( <div class="amount_whole"><?php echo $sending_currency_symbol;?><span id="total_cost_positive_whole"><?php echo $total_cost_sending_currency_positive_whole;?></span></div>.<div class="amount_decimal"><span id="total_cost_positive_decimal"><?php echo $total_cost_sending_currency_positive_decimal;?></span></div> <div class="currency"><?php echo $sending_currency;?></div> )
                    <br>

                    <div style="text-align:center;color: black;font-size: 12px;margin: -4px;">
                        Updated: <span id="lastupdated"><?php echo time_elapsed_string($last_updated);?></span>
                        </div>
                        <?php
                        if ($viewthisroute==true)
                        {
                            ?>
                                <span data-toggle="collapse" data-target="#<?php echo "$resultnumber"?>">
                                    <a style="color: darkblue;"> Fees &amp; Summary &#x25BC;</a>
                                </span>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <?php
                if ($viewthisroute==true)
                {
                    ?>
                    <div class="six columns" style="display: table-cell; float: none; vertical-align: middle;">
                        <form class="form-style-9" action="./d/" style="padding: 12px 0px 0px 0px;margin:0px;">
                            <input type="hidden" name="Amount">
                            <input type="hidden" name="FromCountry">
                            <input type="hidden" name="receiving_country">
                            <input type="hidden" name="buyfrom">
                            <input type="hidden" name="sellto">
                            <input type="hidden" name="funding_name">
                            <input type="hidden" name="withdrawalname">
                            <input type="hidden" name="crypto">
                            <input type="hidden" name="volume">
                            <button type="submit" value="View Best Deal" style="">Receive <span id="tocurrencysymbol"></span><span id="total_in_receiving_currency_whole"></span><span class="amount_decimal" style="color:#bdbdbd;"></span>.<span id="total_in_receiving_currency_decimal"></span></button>
                        </form>
                    </div>

                    <div class="collapse"  id="<?php echo $resultnumber;?>">
                        <div class="row" style="">
                            <div class="twelve columns" style="text-align: center;">
                                <h3><span id="buyfrom"></span> to <span id="sellto"></span> Fees</h3>

                                
                                <?php
                                if ($exchange_2!="")
                                {
                                    echo '<span data-toggle="collapse" data-target=".detail_fee' . $resultnumber . '"><a style="color: darkblue;"> Show Detailed Fees &#x25BC;</a></span>';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row" style="background: transparent;text-align: center;">
                            <div class ="twelve columns" style="">
                                <form class="form-style-9" action="./d/" style="padding: 12px 0px 0px 0px;">
                                    <input type="hidden" name="Amount">
                                    <input type="hidden" name="FromCountry">
                                    <input type="hidden" name="receiving_country">
                                    <input type="hidden" name="buyfrom">
                                    <input type="hidden" name="sellto">
                                    <input type="hidden" name="funding_name">
                                    <input type="hidden" name="withdrawalname">
                                    <button type="submit" value="View Best Deal" style="">View This Route</button>
                                </form>
                            </div>
                        </div>
                    </div>



                    <?php
                }
            ?>

        </div>
    </div>
    <?php
}

// subroutine of print_search_result to print a bitcoin result fee breakdown
function print_fee_breakdown_bitcoin($funding_name, $funding_fee, $funding_percentage, $network_transaction_fee, $withdrawal_name, $withdrawal_fee, $withdrawal_percentage, $withdrawal_fee_currency, $arbitrage, $total_cost, $total_in_receiving_currency, $exchange_rate_inverse, $ask_price, $amount, $selling_crypto_percentage, $withdrawal_percentage_currency, $network_transaction_fee_currency, $selling_crypto_percentage_currency, $sending_currency, $receiving_currency, $exchange_rate_margin, $exchange_rate_margin_fees, $total_fees, $exchange_1, $exchange_2, $fee, $sending_currency_symbol, $receiving_currency_symbol, $exchange_rate, $bid_price, $buying_crypto_percentage, $selling_crypto_fee, $buying_crypto_fee, $resultnumber, $listoffundingtowithdrawalmethods, $speed_actual, $coverage, $period_from, $firm_type, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $last_updated)
{
    print_r(error_get_last());

    if ($crypto == "BTC")
    {
        $crypto_name = "Bitcoin";
    }
    elseif ($crypto == "ETH")
    {
        $crypto_name = "Ethereum";
    }
    elseif ($crypto == "XRP")
    {
        $crypto_name = "Ripple";
    }
    elseif ($crypto == "BCH")
    {
        $crypto_name = "Bitcoin Cash";
    }
    elseif ($crypto == "LTC")
    {
        $crypto_name = "Litecoin";
    }
    else
    {
        echo "unrecognised crypto: $crypto";
    }
    
    $amountenroute = $amount - $funding_fee;
    $amountenroute = number_format((float) $amountenroute, 2, '.', '');
    ?>

    <table style="text-align:center;" class="table table-striped center">
        <tr>
            <td>
                <h3>Fee Name</h3>
            </td>
            <td>
                <h3>Total</h3>
            </td>
            <td >
                <h3>Charged By</h3>
            </td>
            <td>
                <h3>Money En Route</h3>
            </td>
        </tr>
        <?php
        $amountenroute = deduct_funding_fee($amount, $funding_fee);

        $amountenroute_formatted = number_format((float) $amountenroute, 2, '.', ',');
        $funding_fee = number_format((float) $funding_fee, 2, '.', '');
        ?>
        <tr>
            <td class="fee-breakdown-table">
                <?php echo "Sending $funding_name Fee ($sending_currency_symbol$funding_fee  $sending_currency)";?>:
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$sending_currency_symbol$funding_fee $sending_currency";?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo $exchange_1;?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$sending_currency_symbol$amountenroute_formatted $sending_currency";?>
            </td>
        </tr>
        <?php
        // Get the amount left after the buyfee percentage is deducted

        list($amountenroute, $funding_percentage_currency) = deduct_funding_percentage($funding_percentage, $amountenroute, $funding_fee, $amount);

        $amountenroute_formatted = number_format((float) $amountenroute, 2, '.', ',');
        $funding_percentage_currency = number_format((float) $funding_percentage_currency, 2, '.', '');
        ?>
        <tr>
            <td class="fee-breakdown-table">
                <?php echo "Sending $funding_name Percentage ($funding_percentage%):";?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$sending_currency_symbol$funding_percentage_currency $sending_currency";?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo $exchange_1;?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$sending_currency_symbol$amountenroute_formatted $sending_currency";?>
            </td>
        </tr>
        <?php
        print_r(error_get_last());

        list($amountenroute, $buying_crypto_fee, $buying_crypto_fee_currency) = deduct_buying_crypto_fee($buying_crypto_fee, $amountenroute, $ask_price, $funding_fee, $funding_percentage, $amount);

        $buying_crypto_fee = number_format((float) $buying_crypto_fee, 2, '.', '');
        $amountenroute_formatted = number_format((float) $amountenroute, 8, '.', '');
        $buying_crypto_fee_currency = number_format((float) $buying_crypto_fee_currency, 2, '.', '');
        ?>
        <tr>
            <td class="fee-breakdown-table">
                Buying <?php echo $crypto_name;?> Fee (<span id="buying_crypto_fee"><?php echo "$buying_crypto_fee</span> BTC";?>):
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$sending_currency_symbol<span id=\"buying_crypto_fee_currency\">$buying_crypto_fee_currency</span> $sending_currency";?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo $exchange_1;?>
            </td>
            <td class="fee-breakdown-table">
                <span id="buying_crypto_fee_amount_en_route"><?php echo $amountenroute_formatted;?></span>  <?php echo $crypto;?> 
            </td>
        </tr>
        <?php
        list($amountenroute, $buying_crypto_percentage_currency) = deduct_buying_crypto_percentage($buying_crypto_percentage, $amountenroute, $ask_price, $buying_crypto_fee, $funding_percentage, $funding_fee, $amount);
        
        $buying_crypto_percentage_currency = number_format((float) $buying_crypto_percentage_currency, 2, '.', '');
        $amountenroute_formatted = number_format((float) $amountenroute, 8, '.', '');
        ?>
        <tr>
            <td class="fee-breakdown-table">
                Buying <?php echo $crypto_name;?> Percentage (<span id="buying_crypto_percentage"><?php echo "$buying_crypto_percentage";?></span>%):
            </td>
            <td class="fee-breakdown-table">
                <?php echo $sending_currency_symbol;?><span id="buying_crypto_percentage_currency"><?php echo $buying_crypto_percentage_currency;?></span> <?php echo $sending_currency;?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo $exchange_1;?>
            </td>
            <td class="fee-breakdown-table">
                <span id="buying_crypto_percentage_amount_en_route"><?php echo $amountenroute_formatted;?></span>  <?php echo $crypto;?> 
            </td>
        </tr>
        <?php
        list($amountenroute, $network_transaction_fee, $network_transaction_fee_currency) = deduct_network_fee($amountenroute, $ask_price, $network_transaction_fee);

        $amountenroute_formatted = number_format((float) $amountenroute, 8, '.', '');
        $network_transaction_fee_currency = number_format((float) $network_transaction_fee_currency, 2, '.', '');
        ?>
        <tr>
            <td class="fee-breakdown-table">
                <?php echo $crypto_name;?> Transaction Fee (<span id="network_transaction_fee"><?php echo $network_transaction_fee;?></span> <?php echo $crypto;?>)
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$sending_currency_symbol<span id=\"network_transaction_fee_currency\">$network_transaction_fee_currency</span> $sending_currency";?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo $crypto_name;?> Network
            </td>
            <td class="fee-breakdown-table">
                <span id="network_transaction_fee_amount_en_route"><?php echo $amountenroute_formatted;?></span>  <?php echo $crypto;?>
            </td>
        </tr>
        <tr>
            <td class="fee-breakdown-table">
                Arbitrage:
            </td>
            <td class="fee-breakdown-table">
                <?php 
                list($amountenroute, $arbitrage, $amountenroute_at_exchange_one_currency, $amountenroute_at_exchange_two_currency) = deduct_arbitrage($amountenroute, $ask_price, $bid_price, $exchange_rate_inverse, $buying_crypto_fee, $funding_percentage, $funding_fee, $buying_crypto_percentage, $amount, $network_transaction_fee);
                
                $amountenroute_at_exchange_one_currency = number_format((float) $amountenroute_at_exchange_one_currency, 2, '.', '');
                $amountenroute_at_exchange_two_currency = number_format((float) $amountenroute_at_exchange_two_currency, 2, '.', '');
                $ask_price = number_format((float) $ask_price, 2, '.', '');
                $bid_price = number_format((float) $bid_price, 2, '.', '');
                $arbitrage = number_format((float) $arbitrage, 2, '.', '');
                ?>
                
                <?php echo "$sending_currency_symbol<span id=\"arbitrage\">$arbitrage</span> $sending_currency";?>
            </td>
            <td class="fee-breakdown-table">
            </td>
            <td class="fee-breakdown-table">
            </td>
        </tr>

        <?php
        list($amountenroute, $selling_crypto_fee, $selling_crypto_fee_currency) = deduct_selling_bitcoin_fee($selling_crypto_fee, $amountenroute, $exchange_rate_inverse, $ask_price, $bid_price, $buying_crypto_fee, $funding_percentage, $funding_fee, $buying_crypto_percentage, $amount, $network_transaction_fee);

        $selling_crypto_fee_currency = number_format((float) $selling_crypto_fee_currency, 2, '.', '');
        $amountenroute_formatted = number_format((float) $amountenroute, 8, '.', '');
        ?>
        <tr>
            <td class="fee-breakdown-table">
                Selling <?php echo $crypto_name;?> Fee <?php echo "(<span id=\"selling_crypto_fee\">$selling_crypto_fee</span> $crypto)";?>:
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$sending_currency_symbol<span id=\"selling_crypto_fee_currency\">$selling_crypto_fee_currency</span> $sending_currency";?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo $exchange_2;?>
            </td>
            <td class="fee-breakdown-table" >
                <span id="selling_crypto_fee_amount_en_route"><?php echo $amountenroute_formatted;?></span>  <?php echo $crypto;?>
            </td>
        </tr>
        <?php
        
        
        
        list($amountenroute, $selling_crypto_percentage_currency) =  deduct_selling_bitcoin_percentage($selling_crypto_percentage, $amountenroute, $ask_price, $exchange_rate_inverse, $bid_price, $selling_crypto_fee, $buying_crypto_fee, $funding_percentage, $funding_fee, $buying_crypto_percentage, $amount, $network_transaction_fee);
        
        $amountenroute_formatted = number_format((float) $amountenroute, 2, '.', ',');
        $selling_crypto_percentage_currency = number_format((float) $selling_crypto_percentage_currency, 2, '.', '');
        ?>
        <tr>
            <td class="fee-breakdown-table">
                Selling <?php echo $crypto_name;?> Percentage (<?php echo "<span id=\"selling_crypto_percentage\">$selling_crypto_percentage</span>";?>%):
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$sending_currency_symbol<span id=\"selling_crypto_percentage_currency\">$selling_crypto_percentage_currency</span> $sending_currency";?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo $exchange_2;?>
            </td>
            <td class="fee-breakdown-table" >
                <?php echo "$receiving_currency_symbol"?><span id="selling_crypto_percentage_amount_en_route"><?php echo $amountenroute_formatted;?></span> <?php echo $receiving_currency;?>
            </td>
        </tr>

        <?php
        list($amountenroute, $withdrawal_fee_currency) = deduct_withdrawal_fee($amountenroute, $withdrawal_fee, $exchange_rate_inverse, $selling_crypto_percentage, $selling_crypto_fee_currency, $buying_crypto_fee, $funding_percentage, $funding_fee, $bid_price, $ask_price, $buying_crypto_percentage, $amount, $network_transaction_fee);
        
        $amountenroute_formatted = number_format((float) $amountenroute, 2, '.', ',');
        $withdrawal_fee = number_format((float) $withdrawal_fee, 2, '.', '');
        $withdrawal_fee_currency = number_format((float) $withdrawal_fee_currency, 2, '.', '');
        ?>
        <tr>
            <td class="fee-breakdown-table">
                Withdrawal To <?php echo $withdrawal_name;?> Fee <?php echo "($receiving_currency_symbol<span id=\"withdrawal_fee\">$withdrawal_fee</span> $receiving_currency)";?>:
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$sending_currency_symbol<span id=\"withdrawal_fee_currency\">$withdrawal_fee_currency</span> $sending_currency";?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo $exchange_2;?>
            </td>
            <td class="fee-breakdown-table" >
                <?php echo "$receiving_currency_symbol<span id=\"withdrawal_fee_amount_en_route\">$amountenroute_formatted</span> $receiving_currency";?>
            </td>
        </tr>
        <?php
        // Get the amount left after the withdrawal percentage fees
        list($amountenroute, $withdrawal_percentage_currency) = deduct_withdrawal_percentage($withdrawal_percentage, $amountenroute, $exchange_rate_inverse, $selling_crypto_percentage, $selling_crypto_fee_currency, $buying_crypto_fee, $funding_percentage, $funding_fee, $bid_price, $ask_price, $buying_crypto_percentage, $amount, $selling_crypto_fee, $network_transaction_fee);
        
        $amountenroute_formatted = number_format((float) $amountenroute, 2, '.', ',');
        $withdrawal_percentage_currency = number_format((float) $withdrawal_percentage_currency, 2, '.', '');

        ?>
        <tr>
            <td class="fee-breakdown-table">
                Withdrawal Percentage <?php echo "(<span id=\"withdrawal_percentage\">$withdrawal_percentage</span>%)";?>:
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$sending_currency_symbol<span id=\"withdrawal_percentage_currency\">$withdrawal_percentage_currency</span> $sending_currency";?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo $exchange_2;?>
            </td>
            <td class="fee-breakdown-table">
                <?php echo "$receiving_currency_symbol<span id=\"withdrawal_percentage_amount_en_route\">$amountenroute_formatted</span> $receiving_currency";?>
            </td>
        </tr>
    </table>
    Last Updated: <span id="last_updated_fees_table"><?php echo $last_updated;?></span>
    <?php
}

function print_result_summary($funding_fee, $funding_percentage_currency, $buying_crypto_fee_currency, $buying_crypto_percentage_currency, $network_transaction_fee_currency, $selling_crypto_fee_currency, $selling_crypto_percentage_currency, $withdrawal_percentage_currency, $withdrawal_fee_currency, $sending_currency, $receiving_currency, $firm_type, $speed_actual, $amount, $total_in_receiving_currency, $period_from, $listoffundingtowithdrawalmethods, $arbitrage, $sending_currency_symbol, $exchange_rate_inverse, $exchange_rate)
{  
    $total_fees = $funding_fee + $funding_percentage_currency + $buying_crypto_fee_currency + $buying_crypto_percentage_currency + $network_transaction_fee_currency+ $selling_crypto_fee_currency + $selling_crypto_percentage_currency + $withdrawal_percentage_currency + $withdrawal_fee_currency;
    $total_cost = $total_fees + $arbitrage;
    //echo "tc:$total_cost";
    print_r(error_get_last());
    
    // method 1
    $totalreceived = $amount - $total_cost;
    $totalreceivedintocurrency = $totalreceived * $exchange_rate;
    
    $total_fees = number_format((float) $total_fees, 2, '.', '');
    $total_cost = number_format((float) $total_cost, 2, '.', '');
    $totalreceivedintocurrency = number_format((float) $totalreceivedintocurrency, 2, '.', '');
    
    ?>
    <div class="row" style="display: table; background: transparent;">
        <div class="four columns" style="display: table-cell; font-size:14px;text-align: center;">
            <h3>Totals</h3>
            Received: <?php echo "$sending_currency_symbol$totalreceived $sending_currency";?><br>
            Cost: <?php echo "$sending_currency_symbol$total_cost $sending_currency";?><br>
            Fees: <?php echo "$sending_currency_symbol$total_fees $sending_currency";?><br>
            Arbitrage: <?php echo "$sending_currency_symbol$arbitrage $sending_currency";?><br>
        </div>
        <div class ="four columns" style="text-align: center;">
            <h3>Funding / Withdrawal</h3>
            <?php echo "$listoffundingtowithdrawalmethods<br>";?>
        </div>
        <div class="four columns" style="text-align: center;"> 
            <h3>Summary</h3>
            <?php
            if ($speed_actual=="Less than one hour")
            {
                $speedicon ="speed-icon-very-fast";
            } elseif ($speed_actual=="Same day")
            {
                $speedicon ="speed-icon-same-day";
            } elseif ($speed_actual=="Next day")
            {
                $speedicon ="speed-icon-next-day";
            } elseif ($speed_actual=="2 days")
            {
                $speedicon ="speed-icon-2-days";
            } elseif ($speed_actual=="3-5 days")
            {
                $speedicon ="speed-icon-3-5-days";
            } elseif (($speed_actual=="6 days or more")||($speed_actual=="N/A"))
            {
                $speedicon ="speed-icon-very-slow";
            }
            else
            {
                echo "<h1>speedicon no good! $speed_actual</h1>";
            }



            if ($period_from=="LIVE")
            {
                $period_fromicon ="last-checked-icon-live";
            } else
            {
                $period_fromicon ="last-checked-icon-date";
            }

            if ($firm_type=="Bitcoin Exchange")
            {
                $firm_typeicon ="firm-type-bitcoin";
            } elseif (($firm_type=="Money Transfer Operator")||($firm_type=="Money Transfer Operator / Post office")||($firm_type=="Post office")||($firm_type=="Bank / Money Transfer Operator")||($firm_type=="Non-Bank FI")||($firm_type=="Money Transfer Operator / Building Society")||($firm_type=="Bank/Post office")||($firm_type=="Credit Union"))
            {
                $firm_typeicon ="firm-type-mto";
            } else
            {
                $firm_typeicon ="firm-type-bank";
            }

            $effective_exchange_rate = $total_in_receiving_currency / $amount;

            ?>

            <img src="../logos/icons/<?php echo $speedicon;?>.png" style="max-height: 18px;" alt="<?php echo $speed_actual;?>">
            Speed: <?php echo $speed_actual;?><br>

            <img src="../logos/icons/<?php echo $firm_typeicon;?>.png" style="max-height: 18px;" alt="<?php echo $firm_type;?>">
            Firm Type: <?php echo $firm_type;?><br>
            
            <img src="../logos/icons/effective-exchange-rate.png" style="max-height: 18px;" alt="<?php echo "Effective Exchange Rate $effective_exchange_rate";?>">
            <?php echo "$sending_currency/$receiving_currency: $effective_exchange_rate";?>
            <br>
        </div>
    </div>
    <?php
}


// subroutine of print_search_result to print a bank or mto result fee breakdown
function print_fee_breakdown_bank_or_mto($total_in_receiving_currency, $exchange_rate_inverse, $amount, $sending_currency, $receiving_currency, $exchange_rate_margin, $exchange_1, $fee, $sending_currency_symbol, $receiving_currency_symbol, $exchange_rate)
{
    $amountenroute = deduct_funding_fee($amount, $fee);
    $amountenroute = number_format((float) $amountenroute, 2, '.', '');
    $fee = number_format((float) $fee, 2, '.', '');
    list($amountenroute, $funding_percentage_currency) = deduct_funding_percentage($exchange_rate_margin, $amountenroute, $fee, $amount);
    $funding_percentage_currency = number_format((float) $funding_percentage_currency, 2, '.', '');
    $amountenroute = number_format((float) $amountenroute, 2, '.', '');

    $totalinfromcurrency = $amount - $fee - $funding_percentage_currency;
    $total_fees = $fee + $funding_percentage_currency;
    $totalinfromcurrency = number_format((float) $totalinfromcurrency, 2, '.', '');
    
    $total_fees = number_format((float) $total_fees, 2, '.', '');
    ?>
    <table style="text-align:center;" class="table table-striped center">
        <tr>
            <td>
                <h5>Fee Name</h5>
            </td>
            <td>
                <h5>Total</h5>
            </td>
        </tr>
        <tr>
            <td>
                Fee:
            </td>
            <td>
                <?php echo "$sending_currency_symbol$fee $sending_currency";?>
            </td>
        </tr>
        <tr>
            <td>
                Exchange Rate Margin <?php echo "($exchange_rate_margin%):";?>
            </td>
            <td>
               <?php echo " $sending_currency_symbol$funding_percentage_currency $sending_currency";?>
            </td>
        </tr>
        <tr>
            <td>
                <h5>Total Cost:</h5>
            </td>
            <td>
                <strong><?php echo "$sending_currency_symbol$total_fees $sending_currency";?></strong>
            </td>
        </tr>
        <tr>
            <td>
                <h5>Total Received:</h5>
            </td>
            <td>
                <strong><?php echo "$sending_currency_symbol$totalinfromcurrency $sending_currency ($receiving_currency_symbol$total_in_receiving_currency $receiving_currency";?> )</strong>
            </td>
        </tr>
    </table>

   <?php
}
// END OF FUNCTIONS FOR A SEARCH RESULT

// GENERATE THE SQL ADDTION TO CHECK FOR VOLUME
function generate_sql_addition($volumetocheck, $crypto)
{
    
    if($crypto=="ETH")
    {
        $volumetocheck = $volumetocheck * 10;
    }
    elseif($crypto=="LTC")
    {
        $volumetocheck = $volumetocheck * 40;
    }
    elseif($crypto=="BCH")
    {
        $volumetocheck = $volumetocheck * 8;
    }
    elseif($crypto=="XRP")
    {
        $volumetocheck = $volumetocheck * 8000;
    }

    $sqladdition = "AND `Volume` >= '" . $volumetocheck ."'";


    if ($volumetocheck == 'N/A') {
        $sqladdition = "";
    }
    
    $sqladdition = $sqladdition . "AND `Crypto` = '" . $crypto . "'";

    return $sqladdition;
}

// FUNCTIONS TO CALCULATE SPECIFIC FEES
function deduct_funding_fee($amount, $funding_fee)
{
    $amountenroute = $amount - $funding_fee;
    return $amountenroute;
}

function deduct_funding_percentage($funding_percentage, $amountenroute, $funding_fee, $amount)
{
    $amountenroutebeforefee = $amountenroute;

    $amountenroute *= (1 - $funding_percentage / 100);

    $funding_percentage_currency = round($amountenroutebeforefee - $amountenroute, 2); 

    return array($amountenroute, $funding_percentage_currency);
}
 

function deduct_buying_crypto_fee($buying_crypto_fee, $amountenroute, $ask_price, $funding_fee, $funding_percentage, $amount)
{    
    $amountenroute  = $amountenroute / $ask_price;

    $amountenroute  = $amountenroute - $buying_crypto_fee;
    
    $buying_crypto_fee_currency =  $buying_crypto_fee * $ask_price;

    return array($amountenroute, $buying_crypto_fee, $buying_crypto_fee_currency); 
}


function deduct_buying_crypto_percentage($buying_crypto_percentage, $amountenroute, $ask_price, $buying_crypto_fee, $funding_percentage, $funding_fee, $amount)
{
    // establish amount before fee
    $amountenroutebeforefee = $amountenroute;

    // multiply by the amountenroute by the percentage
    $amountenroute *= (1 - $buying_crypto_percentage / 100);
    
    // calculate the difference aftermultiplying by the percentage
    $buying_crypto_percentage_currency = ($amountenroutebeforefee - $amountenroute) * $ask_price;

    return array($amountenroute, $buying_crypto_percentage_currency); 
}



function deduct_network_fee($amountenroute, $ask_price, $network_transaction_fee)
{
    $amountenroute        = $amountenroute - $network_transaction_fee;

    // Get the transaction fee cost in the from country's currency            
    $network_transaction_fee_currency = $network_transaction_fee * $ask_price;

    return array($amountenroute, $network_transaction_fee, $network_transaction_fee_currency);  
}

function deduct_arbitrage($amountenroute, $ask_price, $bid_price, $exchange_rate_inverse, $buying_crypto_fee, $funding_percentage, $funding_fee, $buying_crypto_percentage, $amount, $network_transaction_fee)
{
    $amountenroute_at_exchange_one_currency = $amountenroute * $ask_price;
    $amountenroute_at_exchange_two_currency = ($amountenroute * $bid_price) * $exchange_rate_inverse;
    $arbitrage = $amountenroute_at_exchange_one_currency - $amountenroute_at_exchange_two_currency;

    //echo "$amountenroute_at_exchange_one_currency = $amountenroute * $ask_price;<br>";
    //echo "$amountenroute_at_exchange_two_currency = ($amountenroute * $bid_price) * $exchange_rate_inverse;<br>";
    //echo "$arbitrage = $amountenroute_at_exchange_one_currency - $amountenroute_at_exchange_two_currency;<br>";
    return array($amountenroute, $arbitrage, $amountenroute_at_exchange_one_currency, $amountenroute_at_exchange_two_currency);
}

function deduct_selling_bitcoin_fee($selling_crypto_fee, $amountenroute, $exchange_rate_inverse, $ask_price, $bid_price, $buying_crypto_fee, $funding_percentage, $funding_fee, $buying_crypto_percentage, $amount, $network_transaction_fee)
{
    $amountenroute  = $amountenroute - $selling_crypto_fee;
    $selling_crypto_fee_currency = $selling_crypto_fee * $bid_price * $exchange_rate_inverse;

    return array($amountenroute, $selling_crypto_fee, $selling_crypto_fee_currency); 
}


function deduct_selling_bitcoin_percentage($selling_crypto_percentage, $amountenroute, $ask_price, $exchange_rate_inverse, $bid_price, $selling_crypto_fee, $buying_crypto_fee, $funding_percentage, $funding_fee, $buying_crypto_percentage, $amount, $network_transaction_fee)
{
    // establish amount before fee
    $amountenroutebeforefee = $amountenroute;

    // multiply by the amountenroute by the percentage
    $amountenroute *= (1 - $selling_crypto_percentage / 100);
    
    // calculate the difference aftermultiplying by the percentage
    $selling_crypto_percentage_currency = ($amountenroutebeforefee - $amountenroute) * $bid_price * $exchange_rate_inverse;
    
    // convert to tocurrency
    $amountenroute = $amountenroute * $bid_price;
    
    return array($amountenroute, $selling_crypto_percentage_currency);
}


function deduct_withdrawal_fee($amountenroute, $withdrawal_fee, $exchange_rate_inverse, $selling_crypto_percentage, $selling_crypto_fee_currency, $buying_crypto_fee, $funding_percentage, $funding_fee, $bid_price, $ask_price, $buying_crypto_percentage, $amount, $network_transaction_fee)
{
    if($withdrawal_fee=="")
    {
        $withdrawal_fee = 0;
    }  

    $withdrawal_fee_currency = $withdrawal_fee * $exchange_rate_inverse;
    $amountenroute = $amountenroute - $withdrawal_fee;

    return array($amountenroute, $withdrawal_fee_currency);
}

function deduct_withdrawal_percentage($withdrawal_percentage, $amountenroute, $exchange_rate_inverse, $selling_crypto_percentage, $selling_crypto_fee_currency, $buying_crypto_fee, $funding_percentage, $funding_fee, $bid_price, $ask_price, $buying_crypto_percentage, $amount, $selling_crypto_fee, $network_transaction_fee)
{
    if($withdrawal_percentage=="")
    {
        $withdrawal_percentage = 0;
    }

    // establish amount before fee
    $amountenroutebeforefee = $amountenroute;

    // multiply by the amountenroute by the percentage
    $amountenroute *= (1 - $withdrawal_percentage / 100);
    
    // calculate the difference aftermultiplying by the percentage
    $withdrawal_percentage_currency = ($amountenroutebeforefee - $amountenroute) * $exchange_rate_inverse;

    return array($amountenroute, $withdrawal_percentage_currency);
}
// END OF FUNCTIONS TO CALCULATE SPECIFIC FEES


function is_amount_extra_large($conn, $amount, $sending_country)
{
    $normal_amount = get_default_amount($conn, $sending_country);
    if (($normal_amount*20) < $amount)
    {
    echo "<div style=\"text-align: center;\"><strong>Warning:</strong> You're trying to send a very large amount, and we don't take into account market depth which will effect your crypto prices.</div>";
    }
}

function get_default_amount($conn, $sending_country)
{
    $mem_var = new Memcached();
    $mem_var->addServer("127.0.0.1", 11211);
    $key = md5("get_default_amount$sending_country");
    $response = $mem_var->get("$key");
    if ($response) {
        $amount = $response;
    }
    else 
    {
        $sql    = "SELECT SurveyedAmountInLocalCurrency2 FROM `banks_and_mtos`  WHERE `FromCountry` = '$sending_country' LIMIT 1";
        $retval = mysqli_query($conn, $sql);
        if (!$retval) {
            die('Could not get data: ' . mysqli_error($conn));
        }
        $row = mysqli_fetch_array($retval, MYSQLI_ASSOC);
        
        $amount = $row["SurveyedAmountInLocalCurrency2"];
        
        if(is_numeric($amount))
        {
            $amount = $amount * 2;
        }

        if($amount<1000)
        {
            $amount=1000;
        }

        list($sending_currency, $sending_currency_symbol) = get_currency_currencysymbol($conn, $sending_country);
        $apicurrencies      = $sending_currency . "USD";
        list($usd_exchange_rate, $usd_last_updated_exchange_rate)   = get_exchange_rate($apicurrencies, $conn);

        if ($usd_exchange_rate < 0.01)
        {
            $amount = 1000000;
        }

        mysqli_free_result($retval);
        $mem_var->set($key, $amount, time() + 14400);// or die("get_default_amount Keys Couldn't be Created");
    }

    return $amount;
}

// Get the currency symbol for the from country
function get_currency_currencysymbol($conn, $sending_country)
{
    $mem_var = new Memcached();
    $mem_var->addServer("127.0.0.1", 11211);
    $key = md5("get_currency$sending_country");
    $response = $mem_var->get("$key");
    if ($response) {
        $sending_currency = $response;
        //echo "cached";
    }
    else 
    {

        $sql              = "SELECT `Currency` FROM countries WHERE `Country` = '$sending_country' LIMIT 1";
        //echo $sql;
        $retval           = mysqli_query($conn, $sql);
        if (!$retval)
        {
            echo "Could not get data " . mysqli_error($conn);
            echo "\n$sql";
        }

        if($row = mysqli_fetch_array($retval, MYSQLI_ASSOC))
        {

            //print_r($row);
            $sending_currency       = $row['Currency'];

            if ($sending_currency=="")
            {
                echo "error! is there no record for $sending_country in the countries database?\n";
            }
            else
            {
               mysqli_free_result($retval); 
                print_r(error_get_last());
                $mem_var->set($key, $sending_currency, time() + 14400);// or die("sending_currency Keys Couldn't be Created");
            }
        }
    }

    $mem_var = new Memcached();
    $mem_var->addServer("127.0.0.1", 11211);
    $key = md5("get_currencysymbol$sending_country");
    $response = $mem_var->get("$key");
    if ($response) {
        $sending_currency_symbol = $response;
    }
    else 
    {

        $sending_currency_symbol   = "";

        $sql              = "SELECT `CurrencySymbolUnicode` FROM countries WHERE `Country` = '$sending_country' LIMIT 1";
        $retval           = mysqli_query($conn, $sql);
        if (!$retval)
        {
            die('Could not get data A: ' . mysqli_error($conn));
        }

        $row = mysqli_fetch_array($retval, MYSQLI_ASSOC);
        $sending_currency_symbol = "{$row['CurrencySymbolUnicode']}";

        if ($sending_currency_symbol != "") 
        {
            $sending_currency_symbol = "&#$sending_currency_symbol;";
            mysqli_free_result($retval);
        }
        
        $mem_var->set($key, $sending_currency_symbol, time() + 14400);// or die("sending_currency_symbol Keys Couldn't be Created");

    }   

    return array($sending_currency, $sending_currency_symbol);
}

function get_exchange_rate($apicurrencies, $conn)
{
    $mem_var = new Memcached();
    $mem_var->addServer("127.0.0.1", 11211);
    $key = md5("get_exchange_rate$apicurrencies");
    $response = $mem_var->get("$key");
    if ($response) {
        $exchange_rate = $response;
    }
    else 
    {
        $sql = "SELECT * FROM `exchange_rates` WHERE `CurrencyPair` = '$apicurrencies'";
        $retval = mysqli_query($conn ,$sql);
        if (!$retval) {
            die('Could not get exchange rate data: ' . mysqli_error());
        }
        $row = mysqli_fetch_array($retval, MYSQLI_ASSOC);
        $exchange_rate = "{$row['Rate']}";
        $last_updated_exchange_rate = "{$row['Last Updated']}";

        if (empty($exchange_rate))
        {
            //$exchange_rate = retrieve_exchange_rate($apicurrencies);
            if ($exchange_rate == "")
            {
                $exchange_rate = "";
                echo "<h5>blank exchange rate: $apicurrencies</h5>\n";
            }

            if (strlen($apicurrencies) != 6)
            {
                echo "<h1>error! apicurrencies should be 6 characters! (Currently: api:$apicurrencies)</h1>\n";
            }
        }

        mysqli_free_result($retval);
        $mem_var->set($key, $exchange_rate, time() + 7);// or die("apicurrencies Keys Couldn't be Created");
    }

    $mem_var = new Memcached();
    $mem_var->addServer("127.0.0.1", 11211);
    $key = md5("last_updated_exchange_rate$apicurrencies");
    $response = $mem_var->get("$key");
    if ($response) {
        $last_updated_exchange_rate = $response;
    }
    else 
    {
        $sql = "SELECT * FROM `exchange_rates` WHERE `CurrencyPair` = '$apicurrencies'";
        $retval = mysqli_query($conn ,$sql);
        if (!$retval) {
            die('Could not get exchange rate data: ' . mysqli_error());
        }
        $row = mysqli_fetch_array($retval, MYSQLI_ASSOC);
        $exchange_rate = "{$row['Rate']}";
        $last_updated_exchange_rate = "{$row['Last Updated']}";

        if (empty($exchange_rate))
        {
            //$exchange_rate = retrieve_exchange_rate($apicurrencies);
            if ($exchange_rate == "")
            {
                $exchange_rate = "";
                echo "<h5>blank exchange rate: $apicurrencies</h5>\n";
            }

            if (strlen($apicurrencies) != 6)
            {
                echo "<h1>error! apicurrencies should be 6 characters! (Currently: api:$apicurrencies)</h1>\n";
            }
        }

        mysqli_free_result($retval);
        $mem_var->set($key, $last_updated_exchange_rate, time() + 7);// or die("apicurrencies Keys Couldn't be Created");
    }

    return array($exchange_rate, $last_updated_exchange_rate);
}

function get_website($name, $conn)
{
    $sql = "SELECT * FROM `providers` WHERE `name` = '$name' LIMIT 1";
    $retval = mysqli_query($conn ,$sql);
    if (!$retval) {
        die('Could not get website data: ' . mysqli_error());
    }
    $row = mysqli_fetch_array($retval, MYSQLI_ASSOC);
    $website = "{$row['website']}";

    mysqli_free_result($retval);
    return $website;
}

function retrieve_exchange_rate($apicurrencies)
{
    $url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22" . $apicurrencies;
    $url = $url . "%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";

    $output = curl_download($url);

    $exchangeratedata = json_decode($output, true);
    
    if(isset($exchangeratedata['query']['results']['rate']['Rate']))
    {
        $exchange_rate = $exchangeratedata['query']['results']['rate']['Rate'];
    }
    else
    {
        $exchange_rate = 0;
    }
    return $exchange_rate;
}


function print_route_table_header($sending_country, $receiving_country)
{
    $sending_countrylowercase = strtolower($sending_country);
    $receiving_countrylowercase = strtolower($receiving_country);

    $sending_countrylowercase = trim($sending_countrylowercase);
    $receiving_countrylowercase = trim($receiving_countrylowercase);

    $sending_countrylowercase = str_replace(' ', '', $sending_countrylowercase);
    $receiving_countrylowercase = str_replace(' ', '', $receiving_countrylowercase);
        
    ?>
    <div class="row" style="display: table;text-align: center;background: #f5f5f5;">
        <div class="three columns" style="display: table-cell;">
            <h4>Send</h4>
        </div>
        <div class="three columns" style="display: table-cell;">
            <h4><?php echo $sending_country;?></h4>
            <img style="width:40px;" src="../logos/flags/<?php echo $sending_countrylowercase?>.png" alt="<?php echo $sending_country?> Flag">
        </div>
        <div class="three columns" style="display: table-cell;">
            <h4><?php echo $receiving_country;?></h4>
            <img style="width:40px;" src="../logos/flags/<?php echo $receiving_countrylowercase?>.png" alt="<?php echo $receiving_country?> Flag">
        </div>
        <div class="three columns" style="display: table-cell;">
            <h4>Receive</h4>
        </div>
    </div>
    <?php
}

function curl_download($Url)
{
    
    // is cURL installed yet?
    if (!function_exists('curl_init')) {
        die('Sorry cURL is not installed!');
    }
    
    // OK cool - then let's create a new cURL resource handle
    $ch = curl_init();
    
    // Now set some options (most are optional)
    
    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, $Url);
    
    // Set a referer
    curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
    
    // User agent
    curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
    
    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HEADER, 0);
    
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    // Timeout in seconds                                                       
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

    curl_setopt($ch,CURLOPT_ENCODING , "gzip");

    // Download the given URL, and return output
    $output = curl_exec($ch);
    
    // Close the cURL resource, and free system resources
    curl_close($ch);
    
    return $output;
}

function connect_to_database()
{
    $dbhost = '';
    $dbuser = '';
    $dbpass = '';

    
    $conn   = mysqli_connect($dbhost, $dbuser, $dbpass);
    //echo mysqli_get_host_info($conn);
    if (!$conn) {
    die('Could not connect: ' . mysqli_error($conn));
    }

    mysqli_select_db($conn, 'mattgcom_moneystyle') or die(mysqli_error($conn));
    return $conn;
}

function load_result_variables($search_results, $key)
{
    $funding_name = $search_results[$key][0];
    $funding_fee = $search_results[$key][1];
    $funding_fee_amount_en_route = $search_results[$key][2];

    $funding_percentage = $search_results[$key][3];
    $funding_percentage_currency = $search_results[$key][4];
    $funding_percentage_amount_en_route = $search_results[$key][5];

    $buying_crypto_fee = $search_results[$key][6];
    $buying_crypto_fee_currency = $search_results[$key][7];
    $buying_crypto_fee_amount_en_route = $search_results[$key][8];

    $buying_crypto_percentage = $search_results[$key][9];
    $buying_crypto_percentage_currency = $search_results[$key][10];
    $buying_crypto_percentage_amount_en_route = $search_results[$key][11];

    $network_transaction_fee = $search_results[$key][12];
    $network_transaction_fee_currency = $search_results[$key][13];
    $network_transaction_fee_amount_en_route = $search_results[$key][14];

    $arbitrage = $search_results[$key][15];

    $selling_crypto_percentage = $search_results[$key][16];
    $selling_crypto_percentage_currency = $search_results[$key][17];
    $selling_crypto_percentage_amount_en_route = $search_results[$key][18];

    $selling_crypto_fee = $search_results[$key][19];
    $selling_crypto_fee_currency = $search_results[$key][20];
    $selling_crypto_fee_amount_en_route = $search_results[$key][21];

    $withdrawal_name = $search_results[$key][22];
    $withdrawal_percentage = $search_results[$key][23];
    $withdrawal_percentage_currency = $search_results[$key][24];
    $withdrawal_percentage_amount_en_route = $search_results[$key][25];

    $withdrawal_fee = $search_results[$key][26];
    $withdrawal_fee_currency = $search_results[$key][27];
    $withdrawal_fee_amount_en_route = $search_results[$key][28];

    // bitcoin descriptions
    $exchange_1 = $search_results[$key][29];
    $ask_price = $search_results[$key][30];
    $volume = $search_results[$key][31];
    $exchange_2 = $search_results[$key][32];
    $exchange_2_volume = $search_results[$key][33];
    $bid_price = $search_results[$key][34];
    $exchange_2_website = $search_results[$key][35];
    $last_updated_exchange_1_fees = $search_results[$key][36]; 
    $last_updated_exchange_1_prices = $search_results[$key][37];
    $last_updated_exchange_2_fees = $search_results[$key][38];
    $last_updated_exchange_2_prices = $search_results[$key][39];

    // mto calcs
    $exchange_rate_margin = $search_results[$key][40];
    $exchange_rate_margin_fees = $search_results[$key][41];

    // general descriptions
    $sending_location = $search_results[$key][42];
    $firm_type = $search_results[$key][43];
    $product = $search_results[$key][44];
    $speed_actual = $search_results[$key][45];
    $pick_up_method = $search_results[$key][46];
    $coverage = $search_results[$key][47];
    $period_from = $search_results[$key][48];
    $date = $search_results[$key][49];
    $fee = $search_results[$key][50];
    $coverage = $search_results[$key][51];
    $website = $search_results[$key][42];
    
    // general calcs
    $amount = $search_results[$key][53];
    $total_fees = $search_results[$key][54];
    $total_cost = $search_results[$key][55];
    $total_in_receiving_currency = $search_results[$key][56];

    $exchange_rate = $search_results[$key][57];
    $exchange_rate_inverse = $search_results[$key][58];

    // route fields
    $sending_country = $search_results[$key][59];
    $receiving_country = $search_results[$key][60];
    $sending_currency = $search_results[$key][61];
    $receiving_currency = $search_results[$key][62];
    $crypto = $search_results[$key][63];

    return array($funding_name, $funding_fee, $funding_percentage, $buying_crypto_fee, $buying_crypto_percentage, $network_transaction_fee, $network_transaction_fee_currency, $arbitrage, $selling_crypto_percentage, $selling_crypto_percentage_currency, $selling_crypto_fee, $selling_crypto_fee_currency, $withdrawal_name, $withdrawal_percentage, $withdrawal_percentage_currency, $withdrawal_fee, $withdrawal_fee_currency, $exchange_1, $ask_price, $volume, $exchange_2, $exchange_2_volume, $bid_price, $exchange_2_website, $exchange_rate_margin, $exchange_rate_margin_fees, $sending_location, $firm_type, $product, $speed_actual, $pick_up_method, $coverage, $period_from, $date, $fee, $coverage, $website, $amount, $total_fees, $total_cost, $total_in_receiving_currency, $exchange_rate, $exchange_rate_inverse, $sending_country, $receiving_country, $sending_currency, $receiving_currency, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_fee_amount_en_route, $withdrawal_percentage_amount_en_route, $withdrawal_fee_amount_en_route);
}


function get_record_for_crypto_exchange($conn, $volumetocheck, $exchange, $currency, $crypto)
{



    $mem_var = new Memcached();
    $mem_var->addServer("127.0.0.1", 11211);
    $key = md5("get_record_for_crypto_exchangegetprices$volumetocheck, $exchange, $currency, $crypto, ");
    $response = $mem_var->get("$key");
    if ($response) {
        $exchange_record = $response;
    }
    else 
    {
        $mem_var = new Memcached();
        $mem_var->addServer("127.0.0.1", 11211);
        $key2 = md5("get_record_for_crypto_exchange$volumetocheck, $exchange, $currency, $crypto");
        $response = $mem_var->get("$key2");
        if ($response) {
            $exchange_record = $response;
        }
        else 
        {
            $sqladdition = generate_sql_addition($volumetocheck, $crypto);
            $sql = "SELECT * FROM bitcoin_exchanges WHERE `Name` = '$exchange' AND `Currency` = '$currency' AND `Crypto` = '$crypto' $sqladdition";
            //echo $sql;
            $record_for_crypto_exchange = mysqli_query($conn, $sql);
            if (!$record_for_crypto_exchange) {
                die('Could not get data C: ' . mysqli_error($conn));
            }
            $exchange_record = mysqli_fetch_array($record_for_crypto_exchange, MYSQLI_ASSOC);


            mysqli_free_result($record_for_crypto_exchange);

            $mem_var->set($key2, $exchange_record, time() + 14400);// or die("exchange_record Keys Couldn't be Created");
        }
 
        $sqladdition = generate_sql_addition($volumetocheck, $crypto);
        $sql = "SELECT `AskPrice`, `BidPrice`, `Last Updated Prices` FROM bitcoin_exchanges WHERE `Name` = '$exchange' AND `Currency` = '$currency' AND `Crypto` = '$crypto' $sqladdition";
        print_r(error_get_last());
        $prices_for_crypto_exchange = mysqli_query($conn, $sql);
        if (!$prices_for_crypto_exchange) {
            die('Could not get data C: ' . mysqli_error($conn));
        }
        $prices = mysqli_fetch_array($prices_for_crypto_exchange, MYSQLI_ASSOC);

        $exchange_record['AskPrice'] = $prices['AskPrice'];
        $exchange_record['BidPrice'] = $prices['BidPrice'];
        $exchange_record['Last Updated Prices'] = $prices['Last Updated Prices'];

        mysqli_free_result($prices_for_crypto_exchange);


        $mem_var->set($key, $exchange_record, time()+2);// or die("exchange_record Keys Couldn't be Created");
    }

    return $exchange_record;
}



function get_bank_or_mto_record($conn, $exchange_1, $recordnumber, $sending_country, $receiving_country)
{
    $sql    = "SELECT * FROM `banks_and_mtos` WHERE `BankName` = '$exchange_1' AND `FromCountry` = '$sending_country' AND `ToCountry` = '$receiving_country' LIMIT 1";
    $retval = mysqli_query($conn, $sql);
    if (!$retval) {
        die('Could not get bank or MTO record: ' . mysqli_error($conn));
    }
    $bank_or_mto_record = mysqli_fetch_array($retval, MYSQLI_ASSOC);

    mysqli_free_result($retval);
    return $bank_or_mto_record;
}

function is_bitcoin_exchange_route($exchange_2)
{
    if ($exchange_2=="")
    {
        return false;
    }
    else
    {
        return true;
    }
}


function get_funding_variables_from_buyfrom_record($exchange_1_record)
{
    $acceptsbankdeposit             = "{$exchange_1_record['AcceptsBankDeposit']}";
    $acceptsbankdepositfee          = "{$exchange_1_record['AcceptsBankDepositFee']}";
    $acceptsbankdepositpercentage   = "{$exchange_1_record['AcceptsBankDepositPercentage']}";
    $acceptscreditcards             = "{$exchange_1_record['AcceptsCreditCards']}";
    $acceptscreditcardsfee          = "{$exchange_1_record['AcceptsCreditCardsFee']}";
    $acceptscreditcardspercentage   = "{$exchange_1_record['AcceptsCreditCardsPercentage']}";
    $acceptsdebitcards              = "{$exchange_1_record['AcceptsDebitCards']}";
    $acceptsdebitcardsfee           = "{$exchange_1_record['AcceptsDebitCardsFee']}";
    $acceptsdebitcardspercentage    = "{$exchange_1_record['AcceptsDebitCardsPercentage']}";
    $acceptssepa                    = "{$exchange_1_record['AcceptsSEPA']}";
    $acceptssepafee                 = "{$exchange_1_record['AcceptsSEPAFee']}";
    $acceptssepapercentage          = "{$exchange_1_record['AcceptsSEPAPercentage']}";
    $acceptsother1                  = "{$exchange_1_record['AcceptsOther1']}";
    $acceptsother1name              = "{$exchange_1_record['AcceptsOther1Name']}";
    $acceptsother1fee               = "{$exchange_1_record['AcceptsOther1Fee']}";
    $acceptsother1percentage        = "{$exchange_1_record['AcceptsOther1Percentage']}";
    $acceptsother2                  = "{$exchange_1_record['AcceptsOther2']}";
    $acceptsother2name              = "{$exchange_1_record['AcceptsOther2Name']}";
    $acceptsother2fee               = "{$exchange_1_record['AcceptsOther2Fee']}";
    $acceptsother2percentage        = "{$exchange_1_record['AcceptsOther2Percentage']}";
    //print_r($exchange_1_record);

    return array($acceptsbankdeposit, $acceptsbankdepositfee, $acceptsbankdepositpercentage, $acceptscreditcards, $acceptscreditcardsfee, $acceptscreditcardspercentage, $acceptsdebitcards, $acceptsdebitcardsfee, $acceptsdebitcardspercentage, $acceptssepa, $acceptssepafee, $acceptssepapercentage, $acceptsother1, $acceptsother1name, $acceptsother1fee, $acceptsother1percentage, $acceptsother2, $acceptsother2name, $acceptsother2fee, $acceptsother2percentage);
}

function get_withdrawal_variables_from_sellto_record($exchange_2_record)
{
    $withdrawalstobankdeposit               = "{$exchange_2_record['WithdrawalsToBankDeposit']}";
    $withdrawalstobankdepositfee            = "{$exchange_2_record['WithdrawalsToBankDepositFee']}";
    $withdrawalstobankdepositpercentage     = "{$exchange_2_record['WithdrawalsToBankDepositPercentage']}";
    $withdrawalstocreditcards               = "{$exchange_2_record['WithdrawalsToCreditCards']}";
    $withdrawalstocreditcardsfee            = "{$exchange_2_record['WithdrawalsToCreditCardsFee']}";
    $withdrawalstocreditcardspercentage     = "{$exchange_2_record['WithdrawalsToCreditCardsPercentage']}";
    $withdrawalstodebitcards                = "{$exchange_2_record['WithdrawalsToDebitCards']}";
    $withdrawalstodebitcardsfee             = "{$exchange_2_record['WithdrawalsToDebitCardsFee']}";
    $withdrawalstodebitcardspercentage      = "{$exchange_2_record['WithdrawalsToDebitCardsPercentage']}";
    $withdrawalstosepa                      = "{$exchange_2_record['WithdrawalsToSEPA']}";
    $withdrawalstosepafee                   = "{$exchange_2_record['WithdrawalsToSEPAFee']}";
    $withdrawalstosepapercentage            = "{$exchange_2_record['WithdrawalsToSEPAPercentage']}";
    $withdrawalstoother1                    = "{$exchange_2_record['WithdrawalsToOther1']}";
    $withdrawalstoother1name                = "{$exchange_2_record['WithdrawalsToOther1Name']}";
    $withdrawalstoother1fee                 = "{$exchange_2_record['WithdrawalsToOther1Fee']}";
    $withdrawalstoother1percentage          = "{$exchange_2_record['WithdrawalsToOther1Percentage']}";
    $withdrawalstoother2                    = "{$exchange_2_record['WithdrawalsToOther2']}";
    $withdrawalstoother2name                = "{$exchange_2_record['WithdrawalsToOther2Name']}";
    $withdrawalstoother2fee                 = "{$exchange_2_record['WithdrawalsToOther2Fee']}";
    $withdrawalstoother2percentage          = "{$exchange_2_record['WithdrawalsToOther2Percentage']}";

    return array($withdrawalstobankdeposit, $withdrawalstobankdepositfee, $withdrawalstobankdepositpercentage, $withdrawalstocreditcards, $withdrawalstocreditcardsfee, $withdrawalstocreditcardspercentage, $withdrawalstodebitcards, $withdrawalstodebitcardsfee, $withdrawalstodebitcardspercentage, $withdrawalstosepa, $withdrawalstosepafee, $withdrawalstosepapercentage, $withdrawalstoother1, $withdrawalstoother1name, $withdrawalstoother1fee, $withdrawalstoother1percentage, $withdrawalstoother2, $withdrawalstoother2name, $withdrawalstoother2fee, $withdrawalstoother2percentage);
}


function get_variables_from_bank_or_mto_record($bank_or_mto_record)
{
    //LOAD BANK OR MTO VARIABLES
    $speed_actual                = "{$bank_or_mto_record['SpeedActual']}";
    $period_from                 = "{$bank_or_mto_record['PeriodFrom']}";
    $fee                        = "{$bank_or_mto_record['Fee']}";
    $exchange_rate_margin         = "{$bank_or_mto_record['ExchangeRateMargin']}";
    $sending_location            = "{$bank_or_mto_record['access point']}"; 
    $firm_type                   = "{$bank_or_mto_record['FirmType']}";
    $date                       = "{$bank_or_mto_record['Date']}";
    $paymentinstrument          = "{$bank_or_mto_record['payment instrument']}"; 
    return array($speed_actual, $period_from, $fee, $exchange_rate_margin, $sending_location, $firm_type, $date, $paymentinstrument);
 }


function load_buyfrom_description_variables($exchange_1_record)
{   
    $volume                  = "{$exchange_1_record['Volume']}";
    $buying_crypto_percentage = "{$exchange_1_record['BuyingBitcoinPercentage']}";
    $buying_crypto_fee        = "{$exchange_1_record['BuyingBitcoinFee']}";
    $ask_price                = "{$exchange_1_record['AskPrice']}";
    $last_updated_exchange_1_fees       = "{$exchange_1_record['Last Updated Fees']}";
    $last_updated_exchange_1_prices     = "{$exchange_1_record['Last Updated Prices']}";
    $network_transaction_fee     = "{$exchange_1_record['Withdraw Crypto Fee']}"
;
    return array($volume, $buying_crypto_percentage, $buying_crypto_fee, $ask_price, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $network_transaction_fee);
}

function load_sellto_description_variables($exchange_2_record)
{
    // LOAD VARIABLES FOR THE SECOND BITCOIN EXCHANGE
    $exchange_2_volume                   = "{$exchange_2_record['Volume']}";
    $selling_crypto_percentage       = "{$exchange_2_record['SellingBitcoinPercentage']}";
    $selling_crypto_fee              = "{$exchange_2_record['SellingBitcoinFee']}";
    $bid_price                      = "{$exchange_2_record['BidPrice']}";
    $last_updated_exchange_2_fees       = "{$exchange_2_record['Last Updated Fees']}";
    $last_updated_exchange_2_prices     = "{$exchange_2_record['Last Updated Prices']}";
 
    return array($exchange_2_volume, $selling_crypto_percentage, $selling_crypto_fee, $bid_price, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices);
}


function remove_duplicate_routes($search_results)
{
    $duplicate_keys = array();
    $key=0;
    $search_results_to_be_checked = $search_results;
    foreach ($search_results as $result) 
    {
        $transfers = array();
        $found=0;
        $key_2=0;
        foreach ($search_results_to_be_checked as $result_being_checked) 
        {
            if (($result[56]==$result_being_checked[56])&&($result[29]==$result_being_checked[29])&&($result[32]==$result_being_checked[32])&&($result[63]==$result_being_checked[63]))
            {
                array_push($transfers,"$result_being_checked[0] to $result_being_checked[22]");
                $found++; 
                if ($found> 1)
                {
                    array_push($duplicate_keys, $key_2);
                }
            }
            $key_2++;
        }
        $search_results[$key][64] = implode("<br>", $transfers);
        $key++;
    }

    foreach ($duplicate_keys as $key => $value) {
        unset($search_results[$value]);
    }
    
    return $search_results;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime('now', new DateTimezone('Africa/Bamako'));
    $ago = new DateTime($datetime, new DateTimezone('Africa/Bamako'));
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
print_r(error_get_last());





function print_countries_selection_box($conn, $selected)
{
  $mem_var = new Memcached();
  $mem_var->addServer("127.0.0.1", 11211);
  $key = md5("countries$selected");
  $response = $mem_var->get("$key");
  if ($response) {
      $country_array = $response;
  }
  else 
  {
    $sql    = "SELECT Country, Currency FROM countries WHERE 1 ORDER BY Country ASC";
    $retval = mysqli_query($conn, $sql);
    if (!$retval) {
        die('Could not get data: ' . mysqli_error($conn));
    }

    $country_and_currency_array = array();
    while ($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) 
    {
      $country_and_currency = array($row['Country'], $row['Currency']);
      array_push($country_and_currency_array, $country_and_currency);
    } 
                                                           

    foreach ($country_and_currency_array as $key => $country_array) 
    {
      $country = $country_array[0];
      $option_currency = $country_array[1];
        if ($selected == $country) {
            echo "<option value=\"$country\" SELECTED>$country ($option_currency)</option>";
            
        } else {
            echo "<option value=\"$country\">$country   ($option_currency)</option>";
        }
    }
    $mem_var->set($key, $country_array, time() + 600);// or die("apicurrencies Keys Couldn't be Created");
    print_r(error_get_last());

    mysqli_free_result($retval);
  }
}
?>