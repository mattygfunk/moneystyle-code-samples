<?php

include 'headerfile.php';



// get variables
$sending_country   = $_GET["sending_country"];
$receiving_country = $_GET["receiving_country"];
$amount      = $_GET["amount"];



if (isset($_GET["exchange_1"])) {
    $exchange_1 = $_GET["exchange_1"];
} else {
    $exchange_1 = "";
}

if (isset($_GET["exchange_2"])) {
    $exchange_2 = $_GET["exchange_2"];
} else {
    $exchange_2 = "";
}

if (isset($_GET["funding_name"])) {
    $funding_name = $_GET["funding_name"];
}

if (isset($_GET["withdrawal_name"])) {
    $withdrawal_name = $_GET["withdrawal_name"];
}

if (isset($_GET["volume"])) {
    $volumetocheck = $_GET["volume"];
} else {
    $volumetocheck = "0";
}

if (isset($_GET["crypto"])) {
    $crypto = $_GET["crypto"];
} else {
}

// sanitise variables
$amount             = filter_var($amount, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
$receiving_country    = filter_var($receiving_country, FILTER_SANITIZE_SPECIAL_CHARS);
$sending_country    = filter_var($sending_country, FILTER_SANITIZE_SPECIAL_CHARS);
$volumetocheck      = filter_var($volumetocheck, FILTER_SANITIZE_SPECIAL_CHARS);
$exchange_1         = filter_var($exchange_1, FILTER_SANITIZE_SPECIAL_CHARS);
$exchange_2         = filter_var($exchange_2, FILTER_SANITIZE_SPECIAL_CHARS);
$funding_name       = filter_var($funding_name, FILTER_SANITIZE_SPECIAL_CHARS);
$withdrawal_name    = filter_var($withdrawal_name, FILTER_SANITIZE_SPECIAL_CHARS);
$crypto             = filter_var($crypto, FILTER_SANITIZE_SPECIAL_CHARS);

$receiving_country       = urldecode($receiving_country);
$sending_country       = urldecode($sending_country);
$exchange_1            = urldecode($exchange_1);
$exchange_2            = urldecode($exchange_2);
$funding_name          = urldecode($funding_name);
$withdrawal_name       = urldecode($withdrawal_name);

// connect to database
$conn = connect_to_database();

// get currency and currencysymbol
list($sending_currency, $sending_currency_symbol) = get_currency_currencysymbol($conn, $sending_country);
// get currency and currencysymbol
list($receiving_currency, $receiving_currency_symbol) = get_currency_currencysymbol($conn, $receiving_country);

//get the real exchange rate
list($exchange_rate, $last_updated_exchange_rate) = get_exchange_rate("$sending_currency$receiving_currency", $conn);

//get the real exchange rate when sent back
list($exchange_rate_inverse, $last_updated_exchange_rate_sent_back) = get_exchange_rate("$receiving_currency$sending_currency", $conn);

$default_amount = get_default_amount($conn, $receiving_country);



// Get the list of results
//$search_results  = generate_search_results($conn, $amount, $receiving_country, $sending_country, $volumetocheck);

//$search_results = remove_duplicate_routes($search_results);

$search_results = array();
$recordnumber = "";

$search_results = get_record_generate_fees_totals_and_add_to_array($conn, $volumetocheck, $exchange_1, $exchange_2, $crypto, $recordnumber, $funding_name, $withdrawal_name, $sending_currency, $receiving_currency, $search_results, $amount, $sending_currency_symbol, $receiving_currency_symbol, $sending_country, $receiving_country, $exchange_rate, $exchange_rate_inverse);



list($funding_name, $funding_fee, $funding_percentage, $buying_crypto_fee, $buying_crypto_percentage, $network_transaction_fee, $network_transaction_fee_currency, $arbitrage, $selling_crypto_percentage, $selling_crypto_percentage_currency, $selling_crypto_fee, $selling_crypto_fee_currency, $withdrawal_name, $withdrawal_percentage, $withdrawal_percentage_currency, $withdrawal_fee, $withdrawal_fee_currency, $exchange_1, $ask_price, $volume, $exchange_2, $exchange_2_volume, $bid_price, $exchange_2_website, $exchange_rate_margin, $exchange_rate_margin_fees, $sending_location, $firm_type, $product, $speed_actual, $pick_up_method, $coverage, $period_from, $date, $fee, $coverage, $website, $amount, $total_fees, $total_cost, $total_in_receiving_currency, $exchange_rate, $exchange_rate_inverse, $sending_country, $receiving_country, $sending_currency, $receiving_currency, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_fee_amount_en_route, $withdrawal_percentage_amount_en_route, $withdrawal_fee_amount_en_route) = load_result_variables($search_results, 0);



$listoffundingtowithdrawalmethods = "$funding_name to $withdrawal_name";



$exchange_1 = preg_replace('/[^A-Za-z0-9\-\' .]/', '', $exchange_1);
$exchange_1 = preg_replace('/\/\\//', '', $exchange_1);


if ($exchange_2 != "")
{
    // Format variables
    $funding_fee             = number_format((float) $funding_fee, 2, '.', '');
}
else
{

    $fee             = number_format((float) $fee, 2, '.', '');
}
$total_fees = number_format((float) $total_fees, 2, '.', '');
?>


<!-- Start of printed page -->
<h1 style="text-align:center;">Sending Money to <?php echo $receiving_country;?></h1>
<?php is_amount_extra_large($conn, $amount, $receiving_country); ?>
<div class="row" style="">
    <div class="seven columns" style="text-align:center;padding:10px;">
        <div class="row" style="max-width: 689px;margin:auto;">
                <script>
                function get_result() {
                    
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                            var response = xmlhttp.responseText.trim();


                            if (response != "N/A")
                            {
                                var obj = JSON.parse(xmlhttp.responseText);

                                document.getElementById("total_in_receiving_currency_whole").className = "flashit";
                                document.getElementById("total_in_receiving_currency_decimal").className = "flashit";
                                document.getElementById("h1_total_in_receiving_currency").className = "flashit";
                                document.getElementById("step_by_step_total_in_receiving_currency").className = "flashit";
                                document.getElementById("step_by_step_total_in_receiving_currency2").className = "flashit";
                                document.getElementById("total_cost_positive_whole").className = "flashit";
                                document.getElementById("total_cost_positive_decimal").className = "flashit";
                                document.getElementById("make_pay").className = "flashit";
                                document.getElementById("lastupdated").className = "flashit";
                                document.getElementById("last_updated_exchange_1_fees").className = "flashit";
                                document.getElementById("last_updated_exchange_2_fees").className = "flashit";
                                document.getElementById("last_updated_exchange_1_prices").className = "flashit";
                                document.getElementById("last_updated_exchange_2_prices").className = "flashit";
                                document.getElementById("exchange_rate").className = "flashit";
                                document.getElementById("effective_exchange_rate").className = "flashit";

                                document.getElementById("buying_crypto_fee").className = "flashit";
                                document.getElementById("buying_crypto_fee_currency").className = "flashit";
                                document.getElementById("buying_crypto_fee_amount_en_route").className = "flashit";

                                document.getElementById("buying_crypto_percentage").className = "flashit";
                                document.getElementById("buying_crypto_percentage_currency").className = "flashit";
                                document.getElementById("buying_crypto_percentage_amount_en_route").className = "flashit";

                                document.getElementById("network_transaction_fee").className = "flashit";
                                document.getElementById("network_transaction_fee_currency").className = "flashit";
                                document.getElementById("network_transaction_fee_amount_en_route").className = "flashit";

                                document.getElementById("arbitrage").className = "flashit";

                                document.getElementById("selling_crypto_fee").className = "flashit";
                                document.getElementById("selling_crypto_fee_currency").className = "flashit";
                                document.getElementById("selling_crypto_fee_amount_en_route").className = "flashit";

                                document.getElementById("selling_crypto_percentage").className = "flashit";
                                document.getElementById("selling_crypto_percentage_currency").className = "flashit";
                                document.getElementById("selling_crypto_percentage_amount_en_route").className = "flashit";

                                document.getElementById("withdrawal_fee").className = "flashit";
                                document.getElementById("withdrawal_fee_currency").className = "flashit";
                                document.getElementById("withdrawal_fee_amount_en_route").className = "flashit";

                                document.getElementById("withdrawal_percentage").className = "flashit";
                                document.getElementById("withdrawal_percentage_currency").className = "flashit";
                                document.getElementById("withdrawal_percentage_amount_en_route").className = "flashit";

                                var total_in_receiving_currency = obj.result[0].total_in_receiving_currency.split(".");
                                document.getElementById("total_in_receiving_currency_whole").innerHTML = total_in_receiving_currency[0];
                                document.getElementById("total_in_receiving_currency_decimal").innerHTML = total_in_receiving_currency[1];
                                document.getElementById("h1_total_in_receiving_currency").innerHTML = obj.result[0].total_in_receiving_currency;
                                document.getElementById("step_by_step_total_in_receiving_currency").innerHTML = obj.result[0].total_in_receiving_currency;
                                document.getElementById("step_by_step_total_in_receiving_currency2").innerHTML = obj.result[0].total_in_receiving_currency;
                                var totalcost = obj.result[0].total_cost_positive.split(".");
                                document.getElementById("total_cost_positive_whole").innerHTML = totalcost[0];
                                document.getElementById("total_cost_positive_decimal").innerHTML = totalcost[1];
                                document.getElementById("make_pay").innerHTML = obj.result[0].make_pay;
                                document.getElementById("lastupdated").innerHTML = obj.result[0].lastupdated;
                                document.getElementById("last_updated_exchange_1_fees").innerHTML = obj.result[0].last_updated_exchange_1_fees;
                                document.getElementById("last_updated_exchange_2_fees").innerHTML = obj.result[0].last_updated_exchange_2_fees;
                                document.getElementById("last_updated_exchange_1_prices").innerHTML = obj.result[0].last_updated_exchange_1_prices;
                                document.getElementById("last_updated_exchange_2_prices").innerHTML = obj.result[0].last_updated_exchange_2_prices;
                                document.getElementById("last_updated_exchange_rate").innerHTML = obj.result[0].last_updated_exchange_rate;
                                document.getElementById("exchange_1_logo").src = obj.result[0].exchange_1_logo;
                                document.getElementById("exchange_2_logo").src = obj.result[0].exchange_2_logo;
                                document.getElementById("crypto_logo").src = obj.result[0].crypto_logo;
                                document.getElementById("exchange_rate").innerHTML = obj.result[0].exchange_rate;
                                document.getElementById("effective_exchange_rate").innerHTML = obj.result[0].effective_exchange_rate;


                                document.getElementById("buying_crypto_fee").innerHTML = obj.result[0].buying_crypto_fee;
                                document.getElementById("buying_crypto_fee_currency").innerHTML = obj.result[0].buying_crypto_fee_currency;
                                document.getElementById("buying_crypto_fee_amount_en_route").innerHTML = obj.result[0].buying_crypto_fee_amount_en_route;

                                document.getElementById("buying_crypto_percentage").innerHTML = obj.result[0].buying_crypto_percentage;
                                document.getElementById("buying_crypto_percentage_currency").innerHTML = obj.result[0].buying_crypto_percentage_currency;
                                document.getElementById("buying_crypto_percentage_amount_en_route").innerHTML = obj.result[0].buying_crypto_percentage_amount_en_route;

                                document.getElementById("arbitrage").innerHTML = obj.result[0].arbitrage;

                                document.getElementById("network_transaction_fee").innerHTML = obj.result[0].network_transaction_fee;
                                document.getElementById("network_transaction_fee_currency").innerHTML = obj.result[0].network_transaction_fee_currency;
                                document.getElementById("network_transaction_fee_amount_en_route").innerHTML = obj.result[0].network_transaction_fee_amount_en_route;

                                document.getElementById("selling_crypto_fee").innerHTML = obj.result[0].selling_crypto_fee;
                                document.getElementById("selling_crypto_fee_currency").innerHTML = obj.result[0].selling_crypto_fee_currency;
                                document.getElementById("selling_crypto_fee_amount_en_route").innerHTML = obj.result[0].selling_crypto_fee_amount_en_route;

                                document.getElementById("selling_crypto_percentage").innerHTML = obj.result[0].selling_crypto_percentage;
                                document.getElementById("selling_crypto_percentage_currency").innerHTML = obj.result[0].selling_crypto_percentage_currency;
                                document.getElementById("selling_crypto_percentage_amount_en_route").innerHTML = obj.result[0].selling_crypto_percentage_amount_en_route;

                                document.getElementById("withdrawal_fee").innerHTML = obj.result[0].withdrawal_fee;
                                document.getElementById("withdrawal_fee_currency").innerHTML = obj.result[0].withdrawal_fee_currency;
                                document.getElementById("withdrawal_fee_amount_en_route").innerHTML = obj.result[0].withdrawal_fee_amount_en_route;

                                document.getElementById("withdrawal_percentage").innerHTML = obj.result[0].withdrawal_percentage;
                                document.getElementById("withdrawal_percentage_currency").innerHTML = obj.result[0].withdrawal_percentage_currency;
                                document.getElementById("withdrawal_percentage_amount_en_route").innerHTML = obj.result[0].withdrawal_percentage_amount_en_route;

                                



                                var myVar = setInterval(reset_flash, 6000);

                                function reset_flash() 
                                {
                                    document.getElementById("total_in_receiving_currency_whole").className = "";
                                    document.getElementById("total_in_receiving_currency_decimal").className = "";
                                    document.getElementById("h1_total_in_receiving_currency").className = "";
                                    document.getElementById("step_by_step_total_in_receiving_currency").className = "";
                                    document.getElementById("step_by_step_total_in_receiving_currency2").className = "";
                                    document.getElementById("total_cost_positive_whole").className = "";
                                    document.getElementById("total_cost_positive_decimal").className = "";
                                    document.getElementById("make_pay").className = "";
                                    document.getElementById("lastupdated").className = "";
                                    document.getElementById("last_updated_exchange_1_fees").className = "";
                                    document.getElementById("last_updated_exchange_2_fees").className = "";
                                    document.getElementById("last_updated_exchange_1_fees").className = "";
                                    document.getElementById("last_updated_exchange_1_prices").className = "";
                                    document.getElementById("last_updated_exchange_rate").className = "";
                                    document.getElementById("exchange_rate").className = "";
                                    document.getElementById("effective_exchange_rate").className = "";

                                    document.getElementById("buying_crypto_fee").className = "";
                                    document.getElementById("buying_crypto_fee_currency").className = "";
                                    document.getElementById("buying_crypto_fee_amount_en_route").className = "";

                                    document.getElementById("buying_crypto_percentage").className = "";
                                    document.getElementById("buying_crypto_percentage_currency").className = "";
                                    document.getElementById("buying_crypto_percentage_amount_en_route").className = "";

                                    document.getElementById("arbitrage").className = "";

                                    document.getElementById("network_transaction_fee").className = "";
                                    document.getElementById("network_transaction_fee_currency").className = "";
                                    document.getElementById("network_transaction_fee_amount_en_route").className = "";

                                    document.getElementById("selling_crypto_fee").className = "";
                                    document.getElementById("selling_crypto_fee_currency").className = "";
                                    document.getElementById("selling_crypto_fee_amount_en_route").className = "";

                                    document.getElementById("selling_crypto_percentage").className = "";
                                    document.getElementById("selling_crypto_percentage_currency").className = "";
                                    document.getElementById("selling_crypto_percentage_amount_en_route").className = "";

                                    document.getElementById("withdrawal_fee").className = "";
                                    document.getElementById("withdrawal_fee_currency").className = "";
                                    document.getElementById("withdrawal_fee_amount_en_route").className = "";

                                    document.getElementById("withdrawal_percentage").className = "";
                                    document.getElementById("withdrawal_percentage_currency").className = "";
                                    document.getElementById("withdrawal_percentage_amount_en_route").className = "";
                                    }
                                
                            }
                            else
                            {
                                
                                document.getElementById("currencysymbol").innerHTML = "";

                            }
                        }
                    };
                    xmlhttp.open("GET", "../get-deal-page.php?amount=<?php echo $amount?>&sending_country=<?php echo $sending_country?>&receiving_country=<?php echo $receiving_country?>&exchange_1=<?php echo $exchange_1?>&exchange_2=<?php echo $exchange_2?>&crypto=<?php echo $crypto?>&funding_name=<?php echo urldecode($funding_name)?>&withdrawal_name=<?php echo urlencode($withdrawal_name)?>", true);
                    xmlhttp.send();
                
                }

                get_result();


                var myVar = setInterval(myTimer, 3000);

                function myTimer() {
                    get_result();
                }

                
                </script>
                <?php

                $total_in_receiving_currency
                ?>
            <h2 style="color:#304467;">Receive Approximately <?php echo $receiving_currency_symbol;?><span id="h1_total_in_receiving_currency"><?php echo $total_in_receiving_currency;?></span></h2>
            <?php

            $resultnumber = 0;
            
            $logofile = strtolower($exchange_1);
            $logofile = preg_replace('/\s+/', '', $logofile);
            $logofile = "../logos/providers-70px/" . $logofile . "-70px.png";

            $exchange_2logofile = strtolower($exchange_2);
            $exchange_2logofile = preg_replace('/\s+/', '', $exchange_2logofile);
            $exchange_2logofile = "../logos/providers-70px/" . $exchange_2logofile . "-70px.png";

            $viewthisroute = false;

            if ($exchange_2=="")
            {
                $date = str_replace("Apr","04",$date);
                $date = str_replace("/","-",$date);
                $datetime = new DateTime($date);
                $date = $datetime->format('Y-m-d');
                $last_updated = $date;
            }
            else
            {
                if ($last_updated_exchange_1_prices>$last_updated_exchange_2_prices)
                {
                    $last_updated = $last_updated_exchange_1_prices;
                }
                else
                {
                    $last_updated = $last_updated_exchange_2_prices;
                }
            }

            $total_in_receiving_currency = number_format((float) $total_in_receiving_currency, 2, '.', '');
            list($total_in_receiving_currency_whole, $total_in_receiving_currency_decimal) = explode('.', $total_in_receiving_currency);

            //$total_cost = $total_cost *-1;

            print_search_placeholder($resultnumber, $viewthisroute, $sending_currency_symbol, $receiving_currency_symbol, $amount, $sending_currency, $receiving_currency, $logofile, $exchange_2logofile, $exchange_1, $exchange_2, $crypto, $last_updated, $total_in_receiving_currency_whole, $total_in_receiving_currency_decimal, $total_cost, $exchange_rate);
                
                if ($crypto == "BTC")
                {
                    $crypto_name = "Bitcoin";
                }
                elseif ($crypto == "ETH")
                {
                    $crypto_name = "Ethereum";
                }
                elseif ($crypto == "XRP")
                {
                    $crypto_name = "Ripple";
                }
                elseif ($crypto == "BCH")
                {
                    $crypto_name = "Bitcoin Cash";
                }
                elseif ($crypto == "LTC")
                {
                    $crypto_name = "Litecoin";
                }
            ?>
            <div class="row" style="">
                <div class="twelve columns" style="text-align:center;">
                    <div style="width: 100%;margin-top: 15px;">
                        <?php
                        $url = "../r/?amount=$amount&sending_country=". rawurlencode($sending_country) . "&receiving_country=". rawurlencode($receiving_country); 
                        ?>
                        <h2>
                            Want more routes? <a href="<?php echo $url;?>">See the full results</a>.
                        </h2>
                    </div>
                </div>
            </div>




            <?php
            if ($exchange_2!="")
            {
                ?>          
                <div class="row" style="padding: 13px; display: table;">
                    <div class="six columns" style="display: table-cell;">
                        <img src="<?php echo $logofile;?>" alt="<?php echo $exchange_1;?> Logo" style="max-height: 55px;">
                        <form style="display: inline" action="<?php echo $website;?>" method="get">
                        <button onclick="trackOutboundLink('<?php echo $website;?>'); return false;" formtarget="_blank" style="font-size: 26px;padding: 23px 47px;margin-top: 0px;"><?php if ($exchange_2!="") { echo "Buy $crypto_name"; } else { echo "Get Started";} ?></button>
                        </form>
                    </div>
                    <div class="six columns" style="display: table-cell;">
                        <img src="<?php echo $exchange_2logofile;?>" alt="<?php echo $exchange_2;?> Logo" style="max-height: 55px;">
                        <form style="display: inline" method="get" action="<?php echo $exchange_2_website;?>">
                        <button onclick="trackOutboundLink('<?php echo $exchange_2_website;?>'); return false;" formtarget="_blank" style="font-size: 26px;padding: 23px 47px;margin-top: 0px;">Sell <?php echo $crypto_name;?></button>
                        </form>
                    </div>
                </div>
                 <?php
            }
            else
            {
                ?>          
                <div class="row" style="padding: 13px; display: table;">
                    <div class="twelve columns" style="display: table-cell;">
                        <img src="<?php echo $logofile;?>" style="" alt="<?php echo $exchange_1;?> Logo">
                        <form style="display: inline" action="<?php echo $website;?>" method="get">
                        <button onclick="trackOutboundLink('<?php echo $website;?>'); return false;" formtarget="_blank" style="font-size: 26px;padding: 23px 47px;margin-top: 0px;"><?php if ($exchange_2!="") { echo "Buy $crypto_name"; } else { echo "Get Started";} ?></button>
                        </form>
                    </div>
                </div>
                 <?php
            }
            ?>
        </div>
    </div>

    <script>
        $(document).on('click', '.panel-heading span.clickable', function(e){
            var $this = $(this);
            if(!$this.hasClass('panel-collapsed')) {
                $this.parents('.panel').find('.panel-body').slideUp();
                $this.addClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            } else {
                $this.parents('.panel').find('.panel-body').slideDown();
                $this.removeClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            }
        })
    </script>

    <div class="five columns" style="margin:auto; text-align: center;margin-bottom: 10px;padding:10px;" >
        <h2>Step-By-Step Instructions</h2>
        <?php
        
            if ($exchange_2=="")
            {
                ?>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">1. Register at <a href="<?php echo $website;?>"><?php echo $exchange_1;?></a>.</h3>
                            <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
                        </div>
                        <div class="panel-body collapse"><?php echo $exchange_1;?> is in <?php echo $receiving_country;?>.</div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">2. Complete ID verification.</h3>
                            <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-up"></i></span>
                        </div>
                        <div class="panel-body collapse">Complete ID verification at <a href="<?php echo $website;?>"><?php echo $exchange_1;?></a><?php if ($exchange_2!=""){echo ", while the recipient does the same at $exchange_2";}?>. This usually requires a photo ID, and sometimes a utility bill or similar document to verify your address.</div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">3. Send <?php echo "$sending_currency_symbol$amount $sending_currency";?>.</h3>
                            <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-up"></i></span>
                        </div>
                        <div class="panel-body collapse">Send the <?php echo "$sending_currency_symbol$amount $sending_currency";?> to <?php echo $exchange_1;?>.</div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">4. Receive <?php $total_in_receiving_currency = number_format((float) $total_in_receiving_currency, 2, '.', ''); echo "$receiving_currency_symbol$total_in_receiving_currency $receiving_currency in $receiving_currency";?>.</h3>
                            <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-up"></i></span>
                        </div>
                        <div class="panel-body collapse">The reciever then gets <?php echo "$receiving_currency_symbol$total_in_receiving_currency $receiving_currency";?> by <?php echo $withdrawal_name;?>.</div>
                    </div>
                <?php             
            }
            else
            {
                ?>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">1. Register at <a href="<?php echo $website;?>"><?php echo $exchange_1;?></a> and <a href="<?php echo $exchange_2_website;?>"><?php echo $exchange_2;?></a>.</h3>
                            <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-up"></i></span>
                        </div>
                        <div class="panel-body collapse">
                            <?php echo $exchange_1;?> is in <?php echo $receiving_country;?>, and <?php echo $exchange_2;?> is in <?php echo $sending_country;?>. For the purposes of demonstrating, we assume one person is doing the transfer, however if two people are involved than they only need to register to the exchange in their respective countries.
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">2. Complete ID verification.</h3>
                            <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-up"></i></span>
                        </div>
                        <div class="panel-body collapse">Complete ID verification at <a href="<?php echo $website;?>"><?php echo $exchange_1;?></a>, while the recipient does the same at <?php echo $exchange_2;?>. This usually requires a photo ID, and sometimes a utility bill or similar document to verify your address.</div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">3. Buy <?php echo "$sending_currency_symbol$amount $sending_currency";?> in <?php echo $crypto_name;?> at <?php echo $exchange_1;?>.</h3>
                            <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-up"></i></span>
                        </div>
                        <div class="panel-body collapse">Buy <?php echo $crypto_name;?> at the market rate once your account has been funded and your ID has been verified.</div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">4. Send the <?php echo $crypto_name;?> to the <?php echo $exchange_2;?> account.</h3>
                            <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-up"></i></span>
                        </div>
                        <div class="panel-body collapse">Send the <?php echo $crypto_name;?> to your <?php echo $exchange_2;?> <?php echo $crypto_name;?> address. If you haven't used <?php echo $crypto_name;?> before, it's similar to sending an email. The <?php echo $crypto_name;?> transfer will take about 60 minutes to complete.</div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">5. Sell the <?php echo $crypto_name;?> at <?php echo $exchange_2;?>.</h3>
                            <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-up"></i></span>
                        </div>
                        <div class="panel-body collapse">The reciever then sells the <?php echo $crypto_name;?> for <?php echo "$receiving_currency_symbol$total_in_receiving_currency $receiving_currency";?> and withdraws by <?php echo $withdrawal_name;?></div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">6. Withdraw <?php echo $receiving_currency_symbol;?><span id="step_by_step_total_in_receiving_currency" style="font-size: inherit;"><?php echo $total_in_receiving_currency;?></span> <?php echo "$receiving_currency in $receiving_country";?>.</h3>
                            <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-up"></i></span>
                        </div>
                        <div class="panel-body collapse">The reciever then sells the <?php echo $crypto_name;?> for <?php echo $receiving_currency_symbol;?><span id="step_by_step_total_in_receiving_currency2" style="font-size: inherit;"><?php echo $total_in_receiving_currency;?></span> <?php echo $receiving_currency;?> and withdraws by <?php echo $withdrawal_name;?></div>
                    </div>
                <?php               
            }
        ?>

        <script>
            var acc = document.getElementsByClassName("accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
              acc[i].onclick = function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight){
                  panel.style.maxHeight = null;
                } else {
                  panel.style.maxHeight = panel.scrollHeight + "px";
                } 
              }
            }
        </script>
    </div>
</div>



<div class="container">
    <div class="row" style="margin-bottom: 35px;">
        <div class="col-sm-12">
            <div class="weater">
                <div class="city-selected" style="display: table; width: 100%;">
                    <article class="providers" style="display: table-cell; vertical-align: middle; text-align: center;">
                        <div class="info" style="margin: auto;">
                            <h2 class="temp" style="color: white;">Here's <em>Every</em> Fee You'll Pay</h2>
                        </div>
                    </article>
                    
                    <figure style="background-image: url(../logos/backgrounds/question-mark.jpg)"></figure>
                </div>
                <div class="days">
                    <div class="row row-no-gutter">
                        <div class="col-md-12">
                            <div class="day">                                <?php
                                    // If the result is a specific bank
                                    if ($exchange_2 == "")
                                    {
                                        // Print Fee Breakdown
                                        print_fee_breakdown_bank_or_mto($total_in_receiving_currency, $exchange_rate_inverse, $amount, $sending_currency, $receiving_currency, $exchange_rate_margin, $exchange_1, $fee, $sending_currency_symbol, $receiving_currency_symbol, $exchange_rate);
                                    }
                                    //The result is a bitcoin exchange
                                    else 
                                    {
                                        // PRINT THE FEE BREAKDOWN FOR THIS ROUTE
                                        print_fee_breakdown_bitcoin($funding_name, $funding_fee, $funding_percentage, $network_transaction_fee, $withdrawal_name, $withdrawal_fee, $withdrawal_percentage, $withdrawal_fee_currency, $arbitrage, $total_cost, $total_in_receiving_currency, $exchange_rate_inverse, $ask_price, $amount, $selling_crypto_percentage, $withdrawal_percentage_currency, $network_transaction_fee_currency, $selling_crypto_percentage_currency, $sending_currency, $receiving_currency, $exchange_rate_margin, $exchange_rate_margin_fees, $total_fees, $exchange_1, $exchange_2, $fee, $sending_currency_symbol, $receiving_currency_symbol, $exchange_rate, $bid_price, $buying_crypto_percentage, $selling_crypto_fee, $buying_crypto_fee, $resultnumber, $listoffundingtowithdrawalmethods, $speed_actual, $coverage, $period_from, $firm_type, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto);
                                    } 
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row" style="margin-bottom: 35px;">
        <div class="col-sm-12">
            <div class="weater">
                <div class="city-selected" style="display: table; width: 100%;">
                    <article class="providers" style="display: table-cell; vertical-align: middle; text-align: center;">
                        <div class="info" style="margin: auto;">
                            <h2 class="temp" style="color: white;">MoneyStyle Has Analyzed This Route...</h2>
                        </div>
                    </article>
                    
                    <figure style="background-image: url(../logos/backgrounds/question-mark.jpg)"></figure>
                </div>
                <div class="days">
                    <div class="row row-no-gutter">

                        <div class="col-md-4">
                            <div class="day">
                                <h3>Speed</h3>
                                <?php
                                if ($speed_actual == "Less than one hour")
                                {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <div class="analysis_description"><?php echo $speed_actual;?></div>
                                    <?php
                                }
                                elseif ($speed_actual == "Same day")
                                {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <div class="analysis_description"><?php echo $speed_actual;?></div>
                                    <?php
                                }
                                elseif ($speed_actual == "Next day")
                                {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <div class="analysis_description"><?php echo $speed_actual;?></div>
                                    <?php
                                }
                                elseif ($speed_actual == "2 days")
                                {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <div class="analysis_description"><?php echo $speed_actual;?></div>
                                    <?php
                                }
                                elseif ($speed_actual == "3-5 days")
                                {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <div class="analysis_description"><?php echo $speed_actual;?></div>
                                    <?php
                                }
                                elseif ($speed_actual == "6 days or more")
                                {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <div class="analysis_description"><?php echo $speed_actual;?></div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="day">
                                <h3>Interface</h3>
                                <?php
                                if ($exchange_2!="")
                                {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <br>
                                    <div class="analysis_description">Two Companies Required</div>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <br>
                                    <div class="analysis_description">Dedicated remittance company</div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="day">
                                <h3>Security</h3>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <br>
                                <div class="analysis_description">
                                <?php
                                if ($exchange_2!="")
                                {
                                    echo "Blockchain Technology";
                                }
                                else
                                {
                                    echo "Regulated Financial Operator";
                                }

                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="days">
                    <div class="row row-no-gutter">
                        <div class="col-md-4">
                            <div class="day">
                                <h3>Summary</h3>
                                <?php
                                if (($speed_actual=="30 Minutes (after verification)")||($speed_actual=="Less than one hour"))
                                {
                                    $speedicon ="speed-icon-very-fast";
                                } 
                                elseif ($speed_actual=="Same day")
                                {
                                    $speedicon ="speed-icon-same-day";
                                } 
                                elseif ($speed_actual=="Next day")
                                {
                                    $speedicon ="speed-icon-next-day";
                                } 
                                elseif ($speed_actual=="2 days")
                                {
                                    $speedicon ="speed-icon-2-days";
                                } 
                                elseif ($speed_actual=="3-5 days")
                                {
                                    $speedicon ="speed-icon-3-5-days";
                                } 
                                elseif (($speed_actual=="6 days or more")||($speed_actual=="N/A"))
                                {
                                    $speedicon ="speed-icon-very-slow";
                                }
                                else
                                {
                                    echo "</div></div><h1>speedicon no good! $speed_actual</h1>";
                                }

                                

                                if ($period_from=="LIVE")
                                {
                                    $period_fromicon ="last-checked-icon-live";
                                }
                                else
                                {
                                    $period_fromicon ="last-checked-icon-date";
                                }

                                if ($firm_type=="Bitcoin Exchange")
                                {
                                    $firm_typeicon ="firm-type-bitcoin";
                                } 
                                elseif (($firm_type=="Money Transfer Operator")||($firm_type=="Money Transfer Operator / Post office")||($firm_type=="Post office")||($firm_type=="Bank / Money Transfer Operator")||($firm_type=="Non-Bank FI")||($firm_type=="Money Transfer Operator / Building Society")||($firm_type=="Bank/Post office")||($firm_type=="Credit Union"))
                                {
                                    $firm_typeicon ="firm-type-mto";
                                }
                                else
                                {
                                    $firm_typeicon ="firm-type-bank";
                                }

                                $effective_exchange_rate = $total_in_receiving_currency / $amount;
                                ?>
                                <div class="analysis_description">
                                    <img src="../logos/icons/<?php echo $speedicon;?>.png" style="max-height: 18px;" alt="<?php echo $speed_actual;?>">
                                    Speed: <?php echo $speed_actual;?>
                                </div>
                                <div class="analysis_description">
                                    <img src="../logos/icons/<?php echo $firm_typeicon;?>.png" style="max-height: 18px;" alt="<?php echo $firm_type;?>">
                                    Firm Type: <?php echo $firm_type;?>
                                </div>
                                <div class="analysis_description">
                                    <img src="../logos/icons/effective-exchange-rate.png" style="max-height: 18px;" alt="<?php echo "Effective Exchange Rate";?>">  Route Rate: <?php echo "$sending_currency/$receiving_currency";?> <span id="effective_exchange_rate"><?php echo $effective_exchange_rate;?></span>
                                </div>
                                <div class="analysis_description">
                                    <img src="../logos/icons/effective-exchange-rate.png" style="max-height: 18px;" alt="<?php echo "Exchange Rate $exchange_rate";?>">Exchange Rate <?php echo "$sending_currency/$receiving_currency";?>: <span id="exchange_rate"><?php echo $exchange_rate;?></span>
                                </div>

                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="day">
                                <h3>Funding / Withdrawal</h3>
                                <div class="analysis_description">
                                    <?php echo "$listoffundingtowithdrawalmethods";?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="day">
                                <h3>Last Updated</h3>
                                <div class="analysis_description">
                                    <?php
                                    if ($exchange_2=="")
                                    {
                                        ?>
                                        <img src="../logos/icons/<?php echo $period_fromicon;?>.png" style="max-height: 18px;" alt="Last Checked <?php echo $period_from;?>">
                                        Last Checked: <?php echo $period_from;?><br>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>                             
                                        
                                        <div style="display: inline;">
                                            <img src="../logos/icons/<?php echo $period_fromicon;?>.png" style="max-height: 18px;" alt="Last Updated Prices <?php echo $period_from;?>">
                                            <?php echo $exchange_1;?> Prices: <span id="last_updated_exchange_1_prices"><?php echo time_elapsed_string($last_updated_exchange_1_prices);?></span>
                                        </div><br>
                                        <div style="display: inline;">
                                            <img src="../logos/icons/<?php echo $period_fromicon;?>.png" style="max-height: 18px;" alt="Last Updated <?php echo $exchange_2;?> Prices">
                                            <?php echo $exchange_2;?> Prices: <span id="last_updated_exchange_2_prices"><?php echo time_elapsed_string($last_updated_exchange_2_prices);?></span>
                                        </div><br>
                                        <div style="display: inline;">
                                            <img src="../logos/icons/<?php echo $period_fromicon;?>.png" style="max-height: 18px;" alt="Last Updated Fees">
                                            <?php echo $exchange_1;?> Fees: <span id="last_updated_exchange_1_fees"><?php echo time_elapsed_string($last_updated_exchange_1_fees);?></span>
                                        </div><br>
                                        <div style="display: inline;">
                                            <img src="../logos/icons/<?php echo $period_fromicon;?>.png" style="max-height: 18px;" alt="Last Updated <?php echo $exchange_2;?> Fees">
                                            <?php echo $exchange_2;?> Fees: <span id="last_updated_exchange_2_fees"><?php echo time_elapsed_string($last_updated_exchange_2_fees);?></span>
                                        </div><br>
                                        
                                        
                                        <div style="display: inline;">
                                            <img src="../logos/icons/<?php echo $period_fromicon;?>.png" style="max-height: 18px;" alt="Last Updated <?php echo $exchange_2;?> Exchange Rate">
                                            Exchange Rate: <span id="last_updated_exchange_rate"><?php echo time_elapsed_string($last_updated_exchange_rate);?></span>
                                        </div><br>
                                        
                                        
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
if ($funding_name == "BankDeposit") {$funding_name = "Bank Deposit";}
if ($withdrawal_name == "BankDeposit") {$withdrawal_name = "Bank Deposit";}
$sql = "SELECT * FROM historical_route_rates WHERE `routecode` = '$receiving_country$sending_country$exchange_1$exchange_2$funding_name$withdrawal_name$crypto' ORDER BY `timestamp` ASC";
//echo $sql;
$retval = mysqli_query($conn, $sql);
$row_count = mysqli_num_rows($retval);
//echo $row_count;
if (mysqli_num_rows($retval)>0)
{
    ?>
    <div class="container">
        <div class="row" style="margin-bottom: 35px;">
            <div class="col-lg-12">
                <div class="weater">
                    <div class="city-selected" style="display: table; width: 100%;">
                        <article class="providers" style="display: table-cell; vertical-align: middle; text-align: center;">

                            <div class="info" style="margin: auto;">
                                <h2 class="temp" style="color: white;">Total Cost Sending <?php echo "$sending_currency_symbol$default_amount";?></h2>
                            </div>

                            

                        </article>
                        
                        <figure style="background-image: url(../logos/backgrounds/prices.jpg)"></figure>
                    </div>
                </div>
            </div>
                    <div class="col-lg-12">
                        <script src="https://www.gstatic.com/charts/loader.js"></script> 
                        <script>

                        $(window).resize(function(){
                          drawChart();
                        });
                      google.charts.load('current', {'packages':['corechart']});
                      google.charts.setOnLoadCallback(drawChart);

                      function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                          ['Time', 'Total Cost'],
                        <?php
                        
                        $rownumber =0;
                        $sql = "SELECT * FROM historical_route_rates WHERE `routecode` = '$receiving_country$sending_country$exchange_1$exchange_2$funding_name$withdrawal_name$crypto' ORDER BY `timestamp` ASC";
                        //echo $sql;
                        $retval = mysqli_query($conn, $sql);
                        if (!$retval) {
                            die('Could not get data ' . mysqli_error($conn));
                        }
                        while ($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
                            $total_cost = "{$row['total_cost']}";
                            $timestamp = "{$row['timestamp']}";
                            $timestamp = date( "M-d H:i", strtotime( "$timestamp" ) );
                            //$timestamp = (float)$timestamp;
                            echo "['$timestamp', $total_cost]";
                            $rownumber++;
                            if ($rownumber<(mysqli_num_rows($retval)))
                            { 
                                echo ",\n";
                            } 
                            else 
                            {
                                echo "]);";
                            }
                        }
                        ?>

                        var options = {
                          title: 'Total Cost Over Time <?php echo "($sending_currency)";?>',
                          //curveType: 'function',
                          legend: { position: 'bottom' }
                        };



                        var chart = new google.visualization.LineChart(document.getElementById('curve_chart2'));
                        chart.draw(data, options);

                      }
                        </script>
                        <div id="curve_chart2" style="margin:auto; min-height: 300px; max-width: 800px;">
                        </div>
                    </div>
        </div>
    </div>
    <?php
}
?>

<div class="container">
    <div class="row" style="margin-bottom: 35px;">
        <div class="col-sm-12">
            <div class="weater">
                <div class="city-selected" style="display: table; width: 100%;">
                    <article class="providers" style="display: table-cell; vertical-align: middle; text-align: center;">

                        <div class="info" style="margin: auto;">
                            <h2 class="temp" style="color: white;">Let's Send The Money...</h2>
                        </div>

                        

                    </article>
                    
                    <figure style="background-image: url(../logos/backgrounds/computer.jpg)"></figure>
                </div>

                <div class="days">
                    <div class="row row-no-gutter">
                        <div class="col-md-6">
                            <div class="day">
                                <h3>You should know...</h3>
                                <?php
                                // Print Route Analasis list tabs / dot points
                                if ($exchange_2 == "")
                                {
                                    print_route_analysis_list_bank_or_mto($speed_actual, $coverage, $exchange_1, $sending_country);
                                }
                                else
                                {
                                    print_route_analysis_list_bitcoin($speed_actual, $coverage, $exchange_2, $volume, $exchange_2_volume, $exchange_1, $sending_country);
                                }    
                                ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="day">
                                <h3>LET'S GO!</h3>
                                <img src="<?php echo $logofile;?>" style="width: 200px;" alt="<?php echo $exchange_1;?> Logo">
                                <div style="text-align:center;">
                                    <form style="display: inline" action="<?php echo $website;?>" method="get">
                                    <button onclick="trackOutboundLink('<?php echo $website;?>'); return false;" formtarget="_blank" style="font-size: 26px;padding: 23px 47px;margin-top: 0px;"><?php if ($exchange_2!="") { echo "Buy $crypto_name"; } else { echo "Get Started ";}?></button>
                                    </form>
                                    <br>

                                    <?php
                                    if ($exchange_2!="")
                                    {
                                        ?>
                                        <img src="<?php echo $exchange_2logofile;?>" style="width: 200px;" alt="<?php echo $exchange_2;?> Logo">
                                         &nbsp; 
                                        <form style="display: inline" method="get" action="<?php echo $exchange_2_website;?>">
                                        <button onclick="trackOutboundLink('<?php echo $exchange_2_website;?>'); return false;" formtarget="_blank" style="font-size: 26px;padding: 23px 47px;margin-top: 0px;">Sell <?php echo $crypto_name;?></button>
                                        </form>
                                         <?php
                                    }
                                    ?>
                                    <br>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div style="text-align: center;margin-top: 10px;">
If you believe any information provided is incorrect then please <a href="../contact/">contact MoneyStyle</a>.
</div>

<div style="margin:auto;text-align: center;">
    <img src="https://money.style/logos/logo-small-white.png" style="max-width: 280px" alt="MoneyStyle Logo"><br>
    <p style="text-align: center;">Tell your family. 
        <a href="https://www.facebook.com/MoneyStyle-1512843119008906/">
            <img src="../wp-content/plugins/simple-share-buttons-adder/buttons/somacro/facebook.png" style="max-width: 15px" alt="MoneyStyle's Facebook Page">
        </a>
        <a href="https://twitter.com/MoneyStyle_">
            <img src="../wp-content/plugins/simple-share-buttons-adder/buttons/somacro/twitter.png" style="max-width: 15px" alt="MoneyStyle's Twitter Page">
        </a>
    </p>
</div>





<?php
    if (isset($_GET["fullsite"])) {
        $fullsite = $_GET["fullsite"];
    } else {
        $fullsite = "";
    }
    $fullsite  = filter_var($fullsite, FILTER_SANITIZE_SPECIAL_CHARS);
?>
<div class="row" id="fullsite" style="margin-top: 15px;<?php if ($fullsite!="") { echo " display: none; ";}?>" >
    <div class="twelve columns" style="text-align: center;">
        <h3><a href="../">Home</a> | <a href="<?php echo $_SERVER['REQUEST_URI'] . "&fullsite=yes";?>">Full Site</a></h3>
    </div>
</div>

<?php
print_r(error_get_last());
//  end of page



function print_route_analysis_list_bitcoin($speed_actual, $coverage, $exchange_2, $volume, $exchange_2_volume, $exchange_1, $sending_country)
{
    ?>
    <ul class="list-group" style="max-width: 588px;margin: auto;font-size: 18px;">
        <?php

        if ($exchange_2 != "")
        {
            ?>
            <li class="list-group-item list-group-item-success"><img src="../logos/icons/route-analysis-tick.png" style="max-width: 26px;" alt="Success Tick">This is a bitcoin route.</li>
            <li class="list-group-item list-group-item-success"><img src="../logos/icons/route-analysis-tick.png" style="max-width: 26px;" alt="Success Tick">Step-by-step instructions are provided.</li>
            <?php
        }


        if (($speed_actual == "30 Minutes (after verification)")||($speed_actual == "Less than one hour")||($speed_actual == "Same day")||($speed_actual == "Next day")||($speed_actual == "2 days"))
        {
            ?>
            <li class="list-group-item list-group-item-success"><img src="../logos/icons/route-analysis-tick.png" style="max-width: 26px;" alt="Success Tick">Fast transfer: <?php echo $speed_actual;?>.</li>
            <?php
        }
        elseif (($speed_actual == "3-5 days")||($speed_actual == "6 days or more"))
        {
            ?>
            <li class="list-group-item list-group-item-warning"><img src="../logos/icons/route-analysis-warning.png" style="max-width: 26px;" alt="Warning Sign">The transfer takes longer than most, arriving: <?php echo $speed_actual;?>.</li>
            <?php
        }
        elseif ($speed_actual == "N/A")
        {
            ?>
            <li class="list-group-item list-group-item-danger"><img src="../logos/icons/route-analysis-cross.png" style="max-width: 26px;">Warning: We don't have a speed for this transfer, which means it could take longer than 6 days in some cases.</li>
            <?php
        }
        
        
        if ($exchange_2 != "")
        {
            if (($volume >= 50)&&($exchange_2_volume >= 50))
            {
                ?>
                <li class="list-group-item list-group-item-success"><img src="../logos/icons/route-analysis-tick.png" style="max-width: 26px;" alt="Success Tick">Both bitcoin exchanges are popular.</li>
                <?php
            }
            if (($volume < 50)&&($volume >= 15))
            {
                ?>
                <li class="list-group-item list-group-item-warning"><img src="../logos/icons/route-analysis-warning.png" style="max-width: 26px;" alt="Warning Sign"><?php echo $exchange_1;?> has a fairly low volume (<?php echo $volume;?> BTC), making them only suitable for sending small amounts of money.</li>
                <?php
            }
            elseif ($volume <= 15)
            {
                ?>
                <li class="list-group-item list-group-item-danger"><img src="../logos/icons/route-analysis-cross.png" style="max-width: 26px;" alt="Danger Sign">WARNING: <?php echo $exchange_1;?> has a very low or unreported volume (<?php echo $volume;?> BTC), and MoneyStyle doesn't recommend using <?php echo $exchange_1;?> until they have greater volume in this currency.</li>
                <?php
            }


            if (($exchange_2_volume < 50)&&($exchange_2_volume >= 15))
            {
                ?>
                <li class="list-group-item list-group-item-warning"><img src="../logos/icons/route-analysis-warning.png" style="max-width: 26px;" alt="Warning Sign"><?php echo $exchange_2;?> has a fairly low volume (<?php echo $exchange_2_volume;?> BTC), making them only suitable for sending small amounts of money.</li>
                <?php
            }
            elseif ($exchange_2_volume <= 15)
            {
                ?>
                <li class="list-group-item list-group-item-danger"><img src="../logos/icons/route-analysis-cross.png" style="max-width: 26px;" alt="Danger Sign">WARNING: <?php echo $exchange_2;?> has a very low volume (<?php echo $exchange_2_volume;?> BTC), and MoneyStyle doesn't recommend using <?php echo $exchange_2;?> until they have greater volume in this currency, as low volume makes it harder to trade the Bitcoins.</li>
                <?php
            }
        }      
        ?>
        <li class="list-group-item list-group-item-warning"><img src="../logos/icons/route-analysis-warning.png" style="max-width: 26px;" alt="Warning Sign">This route requires provide proof of your identity.</li>
    </ul>
    <?php
}

function print_route_analysis_list_bank_or_mto($speed_actual, $coverage, $exchange_1, $sending_country)
{
    ?>
    <ul class="list-group" style="max-width: 588px;margin: auto;font-size: 18px;">
        <?php


        if (($speed_actual == "30 Minutes (after verification)")||($speed_actual == "Less than one hour")||($speed_actual == "Same day")||($speed_actual == "Next day")||($speed_actual == "2 days"))
        {
            ?>
            <li class="list-group-item list-group-item-success"><img src="../logos/icons/route-analysis-tick.png" style="max-width: 26px;" alt="Success Tick">Fast transfer: <?php echo $speed_actual;?>.</li>
            <?php
        }
        elseif (($speed_actual == "3-5 days")||($speed_actual == "6 days or more"))
        {
            ?>
            <li class="list-group-item list-group-item-warning"><img src="../logos/icons/route-analysis-warning.png" style="max-width: 26px;" alt="Warning Sign">The transfer takes longer than most, arriving: <?php echo $speed_actual;?>.</li>
            <?php
        }
        elseif ($speed_actual == "N/A")
        {
            ?>
            <li class="list-group-item list-group-item-danger"><img src="../logos/icons/route-analysis-cross.png" style="max-width: 26px;">Warning: We don't have a speed for this transfer, which means it could take longer than 6 days in some cases.</li>
            <?php
        }
            
        ?>
        <li class="list-group-item list-group-item-warning"><img src="../logos/icons/route-analysis-warning.png" style="max-width: 26px;" alt="Warning Sign">This route requires verification, so you will be required to provide proof of your identity.</li>
    </ul>
    <?php
}

?>