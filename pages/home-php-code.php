<?php
include '/home/webapp/money.style/headerfile.php';

$database_table_name = "banks_and_mtos";

$sending_country = geoip_country_name_by_name($_SERVER['REMOTE_ADDR']);
if ($sending_country == 'Asia/Pacific Region') 
{
    $sending_country = "Australia";
}
elseif ($sending_country == 'Europe') 
{
    $sending_country = "Germany";
}
elseif ($sending_country == '') 
{
    $sending_country = "Germany";
}

// Test countries
//$sending_country = "United States";
//$sending_country = "Argentina";
//$sending_country = "United Kingdom";
//$sending_country = "Germany";
//$sending_country = "Indonesia";
//$sending_country = "China";
//$sending_country = "Japan";

if (isset($_GET["fullsite"])) {
    $fullsite = $_GET["fullsite"];
} else {
    $fullsite = "";
}

$fullsite  = filter_var($fullsite, FILTER_SANITIZE_SPECIAL_CHARS);
$sending_country = filter_var($sending_country, FILTER_SANITIZE_SPECIAL_CHARS);

// connect to database
$conn = connect_to_database();

// get currency and sending_currency_symbol
list($currency, $sending_currency_symbol) = get_currency_currencysymbol($conn, $sending_country);

// get preferred remittance routes
list ($remittanceroute1, $remittanceroute2, $remittanceroute3, $remittanceroute4, $remittanceroute5, $remittanceroute6, $remittanceroutes) = get_remittance_routes($conn, $sending_country);

// Get default amount figure
$amount = get_default_amount($conn, $sending_country);

// get the counts of banks / mtos / bitcoin exchanges
$banks_count = get_counts_inline($conn, 'Bank / Money Transfer Operator');
$banks_count = $banks_count + get_counts_inline($conn, 'Bank');

$mto_count = get_counts_inline($conn, 'Money Transfer Operator / Building Society');
$mto_count = $mto_count + get_counts_inline($conn, 'Money Transfer Operator');
$mto_count = $mto_count + get_counts_inline($conn, 'Money Transfer Operator / Post office');
$mto_count = $mto_count + get_counts_inline($conn, 'Non-Bank FI');
$mto_count = $mto_count + get_counts_inline($conn, 'Post office');

$bitcoin_exchange_count = get_counts_inline($conn, 'BitcoinExchanges');

$provider_count = $banks_count + $mto_count + $bitcoin_exchange_count;

$chunked_remittance_routes = get_chuncked_remittance_routes($conn, $sending_country);


print_r(error_get_last());
?>




<div class="row" id="search" style="margin-top: 3px;">
    <div class="seven columns" id="secondDiv" style="margin-left: 10px;margin-right: 10px;">
        <div id="slideshow" style="">
        <?php //echo do_shortcode('[metaslider id="298"]'); ?>
        <div class="ism-slider" data-play_type="loop" id="my-slider">
          <ol>
            <li>
              <img data-src="ism/image/slides/_u/1523860172086_601057.webp" class="lazyload" alt="">
              <div class="ism-caption ism-caption-1" data-delay='200'>Send Smarter Money Transfers</div>
            </li
>            <li>
              <img data-src="ism/image/slides/aircraft-479772_1280.webp" class="lazyload" alt="">
              <div class="ism-caption ism-caption-0">Sending Money Internationally?</div>
              <div class="ism-caption ism-caption-1" data-delay='200'>Compare 250+ Providers<br>Right Now</div>
              <div class="ism-caption ism-caption-2" data-delay='500'>and get the Best Rate!</div>
            </li>
            <li>
              <img data-src="ism/image/slides/money-93206_1280.webp" class="lazyload" alt="">
              <div class="ism-caption ism-caption-0">Compare Rates From</div>
              <div class="ism-caption ism-caption-1" data-delay='200'>Banks<br>Money Transfer Companies <br>Crypto Exchanges</div>
              <div class="ism-caption ism-caption-2" data-delay='500'>and find the best deal</div>
            </li>
            <li>
              <img data-src="ism/image/slides/_u/1523846048939_31428.webp" class="lazyload" alt="">
              <div class="ism-caption ism-caption-0">Including:</div>
              <div class="ism-caption ism-caption-1" data-delay='200'>Western Union - Transferwise - JPMorgan Chase<br>Coinbase - Paypal - Kraken - Bitstamp<br>Barclays - MoneyGram</div>
              <div class="ism-caption ism-caption-2" data-delay='500'>+ 250 More!</div>
            </li>
            <li>
              <img data-src="ism/image/slides/_u/1523846540862_34545.webp" class="lazyload" alt="">
              <div class="ism-caption ism-caption-0">Fast Money Transfers</div>
              <div class="ism-caption ism-caption-1" data-delay='200'>Money Arrives in Minutes</div>
              <div class="ism-caption ism-caption-2" data-delay='500'>Depending on Provider</div>
            </li>
            <li>
              <img data-src="ism/image/slides/_u/1523845329403_835744.webp" class="lazyload" alt="">
              <div class="ism-caption ism-caption-0">Cryptocurrency Support</div>
              <div class="ism-caption ism-caption-1" data-delay='200'>Get Better Deals Than Banks<br>and Money Transfer Companies</div>
              <div class="ism-caption ism-caption-2" data-delay='500'>Almost Free Network Fees</div>
            </li>
            <li>
              <img data-src="ism/image/slides/_u/1523845116047_106206.webp" class="lazyload" alt="">
              <div class="ism-caption ism-caption-0">Extended Crypto Support</div>
              <div class="ism-caption ism-caption-1" data-delay='200'>Bitcoin - Ethereum - Litecoin <br>Ripple - Bitcoin Cash</div>
              <div class="ism-caption ism-caption-2" data-delay='500'>With more coming soon.</div>
            </li>
            <li>
              <img data-src="ism/image/slides/document-428338_1280.webp" class="lazyload" alt="">
              <div class="ism-caption ism-caption-0">Make Money On The Transfer</div>
              <div class="ism-caption ism-caption-1" data-delay='200'>Exploit Price Differences<br>Between Crypto Exchanges<br></div>
              <div class="ism-caption ism-caption-2" data-delay='500'>Live Pricing</div>
            </li>
          </ol>
        </div>


        </div>
    </div>
    <div id="wrapper" style="margin-top: 8px;@media(max-width: 1000px) { width:100%; }">
        <div class="five columns" id="firstDiv" style="margin-bottom: 22px;">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="city-selected" style="display: table; width: 100%;min-height: 0px;">
                                <article class="providers" style="display: table-cell; vertical-align: middle; text-align: center; padding: 8px;">
                                    <div class="info" style="margin: auto;">
                                        <h2 class="temp" style="color:white;padding: 0px;font-size: 26px;">Send Money Worldwide</h2>
                                    </div>
                                </article>
                                <figure style="background-image: url(./logos/backgrounds/bitcoin.jpg)"></figure>
                            </div>
                            <div class="days">
                                <div  style="margin-top:5px; margin-bottom:15px; margin:auto; max-width:450px; text-align: center;">
                                    <form class="form-style-9" action="./d/" id="searchform" style="margin-bottom: 10px;">
                                        <div class="row" style=" background-color: transparent;">
                                            <div class="twelve columns" style="text-align: center;">
                                                <div style="margin-top: 15px; font-size: 24px;">Send <span id="sending_currency_symbol" style=""></span><input type="text" style="width: 90px;font-size: 24px;" class="inputs" id="amount" name="amount" placeholder="<?php echo $amount; ?>" value="<?php echo $amount; ?>"  onkeyup="show_searching();showCheapestResult();" aria-label="Amount To Send"> from</div>
                                                <select style="width: 230px; font-size: 21px" class="inputs" onchange="show_searching();showCheapestResult();" id="sending_country_selection" aria-label="Sending Country">
                                                  <?php
                                                  print_countries_selection_box($conn, $sending_country);
                                                  $receiving_country = $remittanceroute1;
                                                  ?>
                                                </select>
                                                <div style="font-size: 24px;">Receive <span id="receiving_currency_symbol" style=""></span><span id="total_in_receiving_currency" style="width:90px;"></span> in</div>
                                                <select name="receiving_country" style="width: 230px;font-size: 21px;" class="inputs" id="receiving_country_selection" onchange="show_searching();showCheapestResult();" aria-label="Receiving Country">
                                                  <?php
                                                    print_countries_selection_box($conn, $receiving_country);
                                                  ?>
                                                </select>
                                            </div>
                                        </div>

                                        

                                        <div style="font-size: 31px;color: #008405; text-shadow: 2px 2px 2px rgba(0,0,0,0.75); padding: 8px;">
                                            <span id="fees_currency_symbol"></span><span id="total_cost_in_receiving_currency"></span>
                                        </div>

                                        <div style="font-size: 20px;color: #008405; text-shadow: 2px 2px 2px rgba(0,0,0,0.75);">
                                            <span id="you_make_on_this_route"></span>
                                        </div>
                             
                                        <input type="hidden" id="exchange_1" name="exchange_1" value="">
                                        <input type="hidden" id="exchange_2" name="exchange_2" value="">
                                        <input type="hidden" id="funding_name" name="funding_name" value="">
                                        <input type="hidden" id="withdrawal_name" name="withdrawal_name" value="">
                                        <input type="hidden" id="crypto" name="crypto" value="">
                                        <input type="hidden" name="sending_country" id="sending_country" value="<?php echo $sending_country;?>">
                                        <div style="text-align:center;"> 
                                           <button type="submit" value="Best Deal" style="padding: 20px 15px; margin-bottom: 15px; color: black; font-weight: bold;" id="view_best_deal"></button>
                                        </div>
                                    </form>
                                    <script>
                                        function getlocation() 
                                        {
                                            //document.getElementById("view_full_results").action = "./r/?Amount=1";
                                        }
                                    </script>
                                    <?php 
                                    /*
                                    <form id="view_full_results" method="get" style="margin-bottom: 10px;">
                                        <input type="hidden" name="amount" id="amount2">
                                        <input type="hidden" name="sending_country" id="sending_country">
                                        <input type="hidden" name="receiving_country" id="receiving_country">
                                        <button type="submit"  id="view_full_results_button" style="font-size: 1em;padding: 6px 10px;">Full Results</button>
                                    </form>
                                    */
                                     echo "<a href=\"https://money.style/r/?amount=$amount&sending_country=$sending_country&receiving_country=$receiving_country\" style=\"color: #0048b3;\" id=\"fullresults\">Full Results</a>";
                                     print_r(error_get_last());
                                     ?>
                                    <br>
                                    <span id="lastupdated" style="color: #333333;"></span>
                                    <script>
                                      function show_searching()
                                      {
                                        document.getElementById("view_best_deal").style.background = "green";
                                          document.getElementById("view_best_deal").disabled = false;
                                          document.getElementById("view_best_deal").innerHTML = "Searching...<img src=\"./logos/loading_spinner.gif\" style=\"max-height:35px; margin-bottom:-10px;\">";
                                      }


                                        function showCheapestResult() {
                                          


                                            if (document.getElementById("amount").length == 0) 
                                            { 
                                                document.getElementById("total_cost_in_receiving_currency").innerHTML = "";
                                                return;
                                            } 
                                            else 
                                            {
                                                var xmlhttp = new XMLHttpRequest();
                                                xmlhttp.onreadystatechange = function() 
                                                {
                                                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                                                    {
                                                        var response = xmlhttp.responseText.trim();
                                                        if (response != "N/A")
                                                        {
                                                            var obj = JSON.parse(xmlhttp.responseText);
                                                            //document.getElementById("effectiveexchangerate").innerHTML = obj.result[0].effectiveexchangerate;
                                                            
                                                            var total_cost_in_receiving_currency = obj.result[0].total_cost_in_receiving_currency;
                                                            if (total_cost_in_receiving_currency < 0)
                                                            {
                                                                var positive_totalcost = obj.result[0].total_cost_in_receiving_currency * -1;
                                                                var total_cost_description = "Get";
                                                                var total_cost_footer = " Extra";
                                                            }
                                                            else
                                                            {
                                                                var positive_totalcost = obj.result[0].total_cost_in_receiving_currency;
                                                                var total_cost_description = "Pay ";
                                                                var total_cost_footer = " in Fees";
                                                                
                                                            }

                                                            positive_totalcost = parseFloat(positive_totalcost).toFixed(2);
                                                            /*
                                                            document.getElementById("you_make_on_this_route").innerHTML = "Compared With The True Exchange Rate";
                                                            document.getElementById("total_cost_in_receiving_currency").innerHTML = positive_totalcost + "<div style=\"font-size:12px;display:inline;\">" + obj.result[0].receiving_currency + "</div>" + total_cost_footer;
                                                            document.getElementById("fees_currency_symbol").innerHTML = total_cost_description + " " + obj.result[0].receiving_currency_symbol;
                                                            */
                                                            document.getElementById("sending_currency_symbol").innerHTML = obj.result[0].sending_currency_symbol;
                                                            
                                                            document.getElementById("receiving_currency_symbol").innerHTML = obj.result[0].receiving_currency_symbol;
                                                            document.getElementById("exchange_1").value = obj.result[0].exchange_1;
                                                            document.getElementById("exchange_2").value = obj.result[0].exchange_2;
                                                            document.getElementById("total_in_receiving_currency").innerHTML = obj.result[0].total_in_receiving_currency;
                                                            document.getElementById("funding_name").value = obj.result[0].funding_name;
                                                            document.getElementById("withdrawal_name").value = obj.result[0].withdrawal_name;
                                                            document.getElementById("crypto").value = obj.result[0].crypto;
                                                            document.getElementById("lastupdated").innerHTML = "Updated: " + obj.result[0].lastupdated;
                                           
                                                            document.getElementById("view_best_deal").style.background = "";
                                                            document.getElementById("view_best_deal").disabled = false;
                                                            document.getElementById("view_best_deal").innerHTML = total_cost_description + " " + obj.result[0].receiving_currency_symbol + positive_totalcost + "<div style=\"font-size:12px;display:inline;\">" + obj.result[0].receiving_currency + "</div>" + total_cost_footer;
                                                            var yourSelect = document.getElementById( "sending_country_selection" );
                                                            var yourSelect2 = document.getElementById( "receiving_country_selection" );
                                                            
                                                            document.getElementById("sending_country").value = yourSelect.options[ yourSelect.selectedIndex ].value;
                                                            document.getElementById("fullresults").href = "/r/?amount=" + document.getElementById("amount").value + "&sending_country=" + yourSelect.options[ yourSelect.selectedIndex ].value + "&receiving_country=" + yourSelect2.options[ yourSelect2.selectedIndex ].value;


                                                            /*
                                                            document.getElementById("view_full_results_button").style.background = "linear-gradient(#06b6f9, #0046af)";
                                                            document.getElementById("view_full_results_button").disabled = false;
                                                            document.getElementById("view_full_results_button").innerHTML = "Full Results";
                                                            */

                                                            if (obj.result[0].feesandarb < 0)
                                                            {
                                                                
                                                                document.getElementById("you_make_on_this_route").innerHTML = "You Beat The Real Exchange Rate! <a href=\"./make-money/\" style=\"text-decoration: underline;\">How?</a>";
                                                            }
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                            //document.getElementById("effectiveexchangerate").innerHTML = "";
                                                            document.getElementById("total_cost_in_receiving_currency").innerHTML = "";
                                                            document.getElementById("sending_currency_symbol").innerHTML = "";
                                                            document.getElementById("receiving_currency_symbol").innerHTML = "";
                                                            document.getElementById("exchange_1").value = "";
                                                            document.getElementById("exchange_2").value = "";
                                                            document.getElementById("total_in_receiving_currency").value = "";
                                                            document.getElementById("funding_name").value = "";
                                                            document.getElementById("withdrawal_name").value = "";
                                                            document.getElementById("lastupdated").value = "";
                                                            document.getElementById("crypto").value = "";

                                                            document.getElementById("view_best_deal").innerHTML = "No Data Found";
                                                            document.getElementById("view_best_deal").style.background = "grey";
                                                            document.getElementById("view_best_deal").disabled = true;

                                                            /*
                                                            document.getElementById("view_full_results_button").innerHTML = "No Data Found";
                                                            document.getElementById("view_full_results_button").style.background = "grey";
                                                            document.getElementById("view_full_results_button").disabled = true;
                                                            */

                                                            document.getElementById("you_make_on_this_route").innerHTML = "";
                                                            document.getElementById("lastupdated").innerHTML = "";
                                                            document.getElementById("fees_currency_symbol").innerHTML = "";

                                                        }
                                                    }
                                                };
                                                xmlhttp.open("GET", "get-best-total.php?amount=" + document.getElementById("amount").value + "&sending_country=" + document.getElementById("sending_country_selection").value + "&receiving_country=" + document.getElementById("receiving_country_selection").value, true);
                                                xmlhttp.send();
                                            }
                                        }

                                        getlocation();
                                        document.getElementById("view_best_deal").style.background = "green";
                                        document.getElementById("view_best_deal").disabled = false;
                                        document.getElementById("view_best_deal").innerHTML = "Searching...<img src=\"./logos/loading_spinner.gif\" style=\"max-height:35px; margin-bottom:-10px;\">";

                                        /*

                                        document.getElementById("view_full_results_button").style.background = "green";
                                        document.getElementById("view_full_results_button").disabled = true;
                                        document.getElementById("view_full_results_button").innerHTML = "Searching...";
                                        */
                                        showCheapestResult();


                                        var myVar = setInterval(myTimer, 3000);

                                        function myTimer() {
                                            showCheapestResult();
                                        }                      
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" style="">
    <div class="twelve columns" style="text-align:center;">
        <div style="color:white;text-shadow: 1.5px 1.5px 1.5px rgb(35, 35, 35);background:#035979; border-radius:15px;max-width:750px;margin:auto;margin-top:12px;margin-bottom:22px; padding:10px;">
          <?php 
          if($remittanceroute1 != "")
          {
            ?>

            Send money to: 
            <?php 
            echo "<a href=\"./r/?amount=$amount&sending_country=" . rawurlencode($sending_country) . "&receiving_country=" . rawurlencode($remittanceroute1) . "\" style=\"color: #c6d2ff;\">$remittanceroute1</a>, ";
            echo "<a href=\"./r/?amount=$amount&sending_country=" . rawurlencode($sending_country) . "&receiving_country=" . rawurlencode($remittanceroute2) . "\" style=\"color: #c6d2ff;\">$remittanceroute2</a>, ";
            echo "<a href=\"./r/?amount=$amount&sending_country=" . rawurlencode($sending_country) . "&receiving_country=" . rawurlencode($remittanceroute3) . "\" style=\"color: #c6d2ff;\">$remittanceroute3</a>, ";
            echo "<a href=\"./r/?amount=$amount&sending_country=" . rawurlencode($sending_country) . "&receiving_country=" . rawurlencode($remittanceroute4) . "\" style=\"color: #c6d2ff;\">$remittanceroute4</a>, ";
            echo "<a href=\"./r/?amount=$amount&sending_country=" . rawurlencode($sending_country) . "&receiving_country=" . rawurlencode($remittanceroute5) . "\" style=\"color: #c6d2ff;\">$remittanceroute5</a>, ";
            echo "<a href=\"./r/?amount=$amount&sending_country=" . rawurlencode($sending_country) . "&receiving_country=" . rawurlencode($remittanceroute6) . "\" style=\"color: #c6d2ff;\">$remittanceroute6</a>";
        ?> + 150 More! <?php print_r(error_get_last());?>

          <?php
        }
        ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="city-selected" style="display: table; width: 100%;">
                    <article class="providers" style="display: table-cell; vertical-align: middle; text-align: center;">
                        <div class="info" style="margin: auto;">
                            <h2 class="temp" style="color:white;">MoneyStyle's Advanced Features</h2>
                        </div>
                    </article>
                    <figure style="background-image: url(./logos/backgrounds/percent.jpg)"></figure>
                </div>

                <div class="days">
                    <div class="row row-no-gutter" style="margin-bottom: 35px;">
                        <div class="col-md-4">
                            <div class="day">
                                <div class="styledlist">
                                  <ul class="styledlist">
                                    <li class="styledlist">
                                      <img data-src="./logos/icons/006-search.png" class="styledlist lazyload" alt="Find The Lowest Price">
                                      <h3 class="styledlist">Compare <?php echo $provider_count;?> Providers</h3>
                                      <p class="styledlist">Choose the <strong>lowest rate</strong>, <strong>quickest transfer</strong>, or <strong>biggest brand name</strong>.</p>
                                    </li>
                                    <li class="styledlist">
                                      <img data-src="./logos/icons/004-coins.png"  class="styledlist lazyload" alt="Cryptocurrency Support">
                                      <h3 class="styledlist">Cryptocurrency Support!</h3>
                                      <p class="styledlist">Compare sending money through <strong>Bitcoin</strong>, <strong>Ethereum</strong>, <strong>Litecoin</strong> or <strong>Ripple</strong> through <strong><?php echo $bitcoin_exchange_count;?> exchanges</strong>.</p>
                                    </li>
                                    
                                    <li class="styledlist">
                                      <img data-src="./logos/icons/002-communications.png" class="styledlist lazyload" alt="Almost Live Prices">
                                      <h3 class="styledlist">Live Accurate Prices!</h3>
                                      <p class="styledlist"><strong>Prices delayed by seconds</strong>, with other data provided by the <strong>World Bank quarterly</strong>.</p>
                                    </li>
                                  </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="day">
                                <div class="styledlist">
                                  <ul class="styledlist">
                                    
                                    <li class="styledlist">
                                      <img data-src="./logos/icons/005-business.png"  class="styledlist lazyload" alt="Find The Lowest Price">
                                      <h3 class="styledlist">Find The Lowest Price</h3>
                                      <p class="styledlist">After <strong>every fee and charge</strong>.</p>
                                    </li>  
                                    <li class="styledlist">
                                      <img data-src="./logos/icons/003-money.png" class="styledlist lazyload" alt="Make Money on the Transfer!">
                                      <h3 class="styledlist">Make Money on the Transfer!</h3>
                                      <p class="styledlist">Buy from a cheap exchange then sell at an expensive exchange to possibly <strong>receive more money than you sent</strong>. <a href="./make-money/" style="text-decoration: underline;color: #0048b3;">More information</a>.
                                        </p>
                                    </li>
                                    <li class="styledlist">
                                      <img data-src="./logos/icons/001-money-1.png" class="styledlist lazyload" alt="Payment options">
                                      <h3 class="styledlist">Send &amp; Receive Cash</h3>
                                      <p class="styledlist">Choose from sending &amp; receiving <strong>bank deposits</strong>, <strong>cash</strong>, <strong>PayPal</strong>, <strong>SEPA &amp; more</strong>.</p>
                                    </li>

                                    

                                  </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="day">
                                <div style="min-width:150px;margin-right: 10px;"><div class="videoWrapper videoborder" style="text-align:right;">
                                    <iframe width="500" height="281"  src="https://www.youtube.com/embed/RuPybZRs8fc" style="border: 0px;" allowfullscreen title="MoneyStyle Introduction Video"></iframe>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="city-selected" style="display: table; width: 100%;">
                    <article class="providers" style="display: table-cell; vertical-align: middle; text-align: center;">
                        <div class="info" style="margin: auto;">
                            <h2 class="temp" style="color: white;">Search <?php echo $provider_count;?> Providers Including<?php print_r(error_get_last());?></h2>
                        </div>
                    </article>
                    <figure style="background-image: url(./logos/backgrounds/city-background.jpg)" id="figure"></figure>
                    <script type="text/javascript">
                      
                    </script>
                </div>

                <div class="days" style="margin:auto;">
                    <div class="row row-no-gutter" style="">
                        <div class="col-md-4">
                            <div class="day" style="text-align: left;">
                                <style type="text/css">
                                  .fling-minislide {width:300px; height:0px; padding-bottom: 300px; position:relative; margin: auto;}
                                  .fling-minislide img{ position:absolute; animation:fling-minislide 25s infinite; opacity:0; width: 300px; height: auto;}

                                  @keyframes fling-minislide {25%{opacity:1;} 40%{opacity:0;}}
                                  .fling-minislide img:nth-child(5){animation-delay:0s;}
                                  .fling-minislide img:nth-child(4){animation-delay:5s;}
                                  .fling-minislide img:nth-child(3){animation-delay:10s;}
                                  .fling-minislide img:nth-child(2){animation-delay:15s;}
                                  .fling-minislide img:nth-child(1){animation-delay:20s;}
                                </style>

                                <h3 style="text-align: center;"><?php echo $banks_count;?> Banks</h3>
                              <div class="fling-minislide">
                                  <img src="logos/sliders/chase-300.webp" alt="Slide 4" class="lazyload">
                                  <img src="logos/sliders/santander-300.webp" alt="Slide 4" class="lazyload">
                                  <img src="logos/sliders/cba-300.webp" alt="Slide 3" class="lazyload">
                                  <img src="logos/sliders/boa-300px.webp" alt="Slide 2" class="lazyload">
                                  <img src="logos/sliders/rabobank-300.webp" alt="Slide 1" class="lazyload">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="day" style="text-align: left;">
                                <h3 style="text-align: center;"><?php echo $mto_count;?> Money Transfer Operators</h3>
                                <div class="fling-minislide">
                                  <img src="logos/sliders/worldremit-300.webp" alt="Slide 4" class="lazyload">
                                  <img src="logos/sliders/moneygram-300.webp" alt="Slide 4" class="lazyload">
                                  <img src="logos/sliders/wu-300.webp" alt="Slide 3" class="lazyload">
                                  <img src="logos/sliders/paypal-300.webp" alt="Slide 2" class="lazyload">
                                  <img src="logos/sliders/transferwise-300.webp" alt="Slide 1" class="lazyload">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="day" style="text-align: left;">
                                <h3 style="text-align: center;"><?php echo $bitcoin_exchange_count;?> Bitcoin Exchanges</h3>
                                <div class="fling-minislide">
                                  <img src="logos/sliders/bitstamp-300.webp" alt="Slide 4" class="lazyload">
                                  <img src="logos/sliders/btcmarkets-300.webp" alt="Slide 4" class="lazyload">
                                  <img src="logos/sliders/kraken-300.webp" alt="Slide 3" class="lazyload">
                                  <img src="logos/sliders/gemini-300.webp" alt="Slide 2" class="lazyload">
                                  <img src="logos/sliders/coinbase-300.webp" alt="Slide 1" class="lazyload">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
  
/* lazyload.js (c) Lorenzo Giuliani
 * MIT License (http://www.opensource.org/licenses/mit-license.html)
 *
 * expects a list of:  (this);​
 * `<img src="blank.gif" data-src="my_image.png" width="600" height="400" class="lazy">`
 */

!function(window){
  var $q = function(q, res){
        if (document.querySelectorAll) {
          res = document.querySelectorAll(q);
        } else {
          var d=document
            , a=d.styleSheets[0] || d.createStyleSheet();
          a.addRule(q,'f:b');
          for(var l=d.all,b=0,c=[],f=l.length;b<f;b++)
            l[b].currentStyle.f && c.push(l[b]);

          a.removeRule(0);
          res = c;
        }
        return res;
      }
    , addEventListener = function(evt, fn){
        window.addEventListener
          ? this.addEventListener(evt, fn, false)
          : (window.attachEvent)
            ? this.attachEvent('on' + evt, fn)
            : this['on' + evt] = fn;
      }
    , _has = function(obj, key) {
        return Object.prototype.hasOwnProperty.call(obj, key);
      }
    ;

  function loadImage (el, fn) {
    var img = new Image()
      , src = el.getAttribute('data-src');
    img.onload = function() {
      if (!! el.parent)
        el.parent.replaceChild(img, el)
      else
        el.src = src;

      fn? fn() : null;
    }
    img.src = src;
  }

  function elementInViewport(el) {
    var rect = el.getBoundingClientRect()

    return (
       rect.top    >= 0
    && rect.left   >= 0
    && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
    )
  }

    var images = new Array()
      , query = $q('img.lazy')
      , processScroll = function(){
          for (var i = 0; i < images.length; i++) {
            if (elementInViewport(images[i])) {
              loadImage(images[i], function () {
                images.splice(i, i);
              });
            }
          };
        }
      ;
    // Array.prototype.slice.call is not callable under our lovely IE8 
    for (var i = 0; i < query.length; i++) {
      images.push(query[i]);
    };

    processScroll();
    addEventListener('scroll',processScroll);

}
</script>

<div class="container" id="fees_sending_1000">
    <div class="row">
        <div class="col-sm-12">
            <div class="card" style="margin-bottom: 30px;">
                <div class="city-selected" style="display: table; width: 100%;">
                    <article class="providers" style="display: table-cell; vertical-align: middle; text-align: center;">

                        <div class="info" style="margin: auto;">
                            <h2 class="temp" style="color: white;">Fees Sending <?php echo "$sending_currency_symbol$amount $currency";?>*</h2>
                        </div>
                    </article>
                    
                    <figure style="background-image: url(./logos/backgrounds/calc.jpg)"></figure>
                </div>

                <div class="days">
                    <div class="row row-no-gutter">
                        <div class="col-sm-12">
                            <div class="day">
                                <div style="font-size:22px;color:red; display: inline;"><img src="./logos/icons/red-circle.png"> Pay in fees</div> &nbsp; 
                                <div style="font-size:22px;color:green; display: inline;"><img src="./logos/icons/green-circle.png"> <a href="./make-money/" style="text-decoration: underline;color:green;">Earn after fees</a></div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="days">
                    <div class="row row-no-gutter">
                        <div class="col-sm-4">
                            <div class="day">
                                <?php
                                if(!empty($chunked_remittance_routes))
                                {
                                  print_route_summary_group($conn, $chunked_remittance_routes[0], $sending_country, $amount, $sending_currency_symbol);
                                }
                                else
                                {
                                  echo "Sorry, we can't find routes from your country.";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="day">
                                <?php
                                if(!empty($chunked_remittance_routes))
                                {
                                  print_route_summary_group($conn, $chunked_remittance_routes[1], $sending_country, $amount, $sending_currency_symbol);
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="day">
                                <?php
                                if(!empty($chunked_remittance_routes))
                                {
                                  print_route_summary_group($conn, $chunked_remittance_routes[2], $sending_country, $amount, $sending_currency_symbol);
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="day">
                            * Includes arbitrage. Rates delayed 15 minutes. Final fees are calculated during the transfer.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php print_r(error_get_last());?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="city-selected" style="display: table; width: 100%;">
                    <article class="providers" style="display: table-cell; vertical-align: middle; text-align: center;">
                        <div class="info" style="margin: auto;">
                            <h2 class="temp" style="color:white;">Are You Ready To Save?</h2>
                        </div>
                    </article>
                    
                    <figure style="background-image: url(./logos/backgrounds/piggy-bank.jpg)"></figure>
                </div>

                <div class="days">
                    <div class="row row-no-gutter" style="margin-bottom: 35px;">
                        <div class="col-sm-12">
                            <div class="day">
                                <button style="margin: 20px auto 32px auto; padding:25px 35px; font-size:24px;" onclick="location.href='#search';">Start now!</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<div class="row" id="fullsite" style="<?php if ($fullsite!="") { echo " display: none; ";}?>" >
    <div class="twelve columns" style="text-align: center;">
        <h3><a href=".?fullsite=yes">Full Site</a></h3>
    </div>
</div>
<?php print_r(error_get_last()); ?>
<div class="row" style="">
    <div class="six columns" style="text-align: center;padding:19px;">
        <strong>IMPORTANT:</strong> This site is BETA software, and should be used as a guide only. It's not guaranteed to be accurate. You must confirm all fees and charges. If using cryptocurrency transfers, you need to take appropriate security precautions, and understand that network time and confirmation times expose you to holding the cryptocurrency until your coins are available for sale at the receiving exchange, and the fluctuations in price during that time.
        <!--<h3>As Mentioned In</h3>-->
    </div>
    <div class="six columns" style="text-align: center;padding:19px;" id="hey-dude">
        <img data-src="./logos/logo-small-white.png" style="max-width: 280px" alt="MoneyStyle Logo" class="lazyload"><br>
            Tell your family.  
            <a href="https://www.facebook.com/MoneyStyle-1512843119008906/">
            <img src="./wp-content/plugins/simple-share-buttons-adder/buttons/somacro/facebook.png" style="max-width: 20px" alt="MoneyStyle's Facebook Page">
            </a>
            <a href="https://twitter.com/MoneyStyle_">
            <img src="./wp-content/plugins/simple-share-buttons-adder/buttons/somacro/twitter.png" style="max-width: 20px" alt="MoneyStyle's Twitter Page">
            </a>
    </div>
</div>
</div>

<script>
document.addEventListener("DOMContentLoaded", function () {
    new IOlazy();
});
</script>

<?php

mysqli_close($conn);
// End of page

//FUNCTIONS


function get_remittance_routes($conn, $sending_country)
{
    $remittance_routes = array();
    $remittanceroute1 = "";
    $remittanceroute2 = "";
    $remittanceroute3 = "";
    $remittanceroute4 = "";
    $remittanceroute5 = "";
    $remittanceroute6 = "";

    $sql    = "SELECT * FROM countries WHERE `Country` = '$sending_country'";
    $retval = mysqli_query($conn, $sql);
    if (!$retval) {
        die('Could not get data: ' . mysqli_error());
    }
    while ($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
        $remittanceroute1 = "{$row['RemittanceRoute1']}";
        $remittanceroute2 = "{$row['RemittanceRoute2']}";
        $remittanceroute3 = "{$row['RemittanceRoute3']}";
        $remittanceroute4 = "{$row['RemittanceRoute4']}";
        $remittanceroute5 = "{$row['RemittanceRoute5']}";
        $remittanceroute6 = "{$row['RemittanceRoute6']}";
        array_push($remittance_routes, $remittanceroute1);
        array_push($remittance_routes, $remittanceroute2);
        array_push($remittance_routes, $remittanceroute3);
        array_push($remittance_routes, $remittanceroute4);
        array_push($remittance_routes, $remittanceroute5);
        array_push($remittance_routes, $remittanceroute6);
    }

    $sql    = "SELECT DISTINCT `ToCountry` FROM `banks_and_mtos` WHERE `FromCountry` = '$sending_country'";
    $retval = mysqli_query($conn, $sql);
    if (!$retval) {
        die('Could not get data: ' . mysqli_error($conn));
    }
    while ($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
        $remittanceroute = "{$row['ToCountry']}";
        array_push($remittance_routes, $remittanceroute);
    }

    $sql    = "SELECT DISTINCT `Country` FROM `bitcoin_exchanges` WHERE `Country` != '$sending_country'";
    $retval = mysqli_query($conn, $sql);
    if (!$retval) {
        die('Could not get data: ' . mysqli_error($conn));
    }
    while ($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
        $remittanceroute = "{$row['Country']}";
        array_push($remittance_routes, $remittanceroute);
    }


    $remittance_routes = array_unique($remittance_routes);
    sort($remittance_routes);
    //print_r ($remittance_routes);
    return array($remittanceroute1, $remittanceroute2, $remittanceroute3, $remittanceroute4, $remittanceroute5, $remittanceroute6, $remittance_routes);
}

function get_counts_inline($conn, $firmtype)
{
    $mem_var = new Memcached();
    $mem_var->addServer("127.0.0.1", 11211);
    $key = md5("get_counts_inline$firmtype");
    $response = $mem_var->get("$key");
    if ($response) {
        $total = $response;
    }
    else 
    {

      if ($firmtype == 'BitcoinExchanges') {
          $result = mysqli_query($conn, "SELECT COUNT(DISTINCT `Name`) FROM `bitcoin_exchanges` ");
          
          if (!$result) {
              die('Could not get count: ' . mysqli_error($conn));
          }
          $row   = mysqli_fetch_array($result);
          $total = $row[0];
          return $total;
      } else {
          $result = mysqli_query($conn, "SELECT COUNT(DISTINCT `BankName`) FROM `banks_and_mtos` WHERE `FirmType` = '$firmtype'");
          if (!$result) {
              die('Could not get count: ' . mysqli_error($conn));
          }
          $row    = mysqli_fetch_array($result);
          $total  = $row[0];
          return $total;
      }

      mysqli_free_result($result);
      $mem_var->set($key, $total, time() + 14400);
    } 
}


function get_chuncked_remittance_routes($conn, $sending_country)
{
  $chunked_remittance_routes = "";
  $mem_var = new Memcached();
  $mem_var->addServer("127.0.0.1", 11211);
  $key = md5("get_chuncked_remittance_routes$sending_country");
  $response = $mem_var->get("$key");
  if ($response) 
  {
    $chunked_remittance_routes = $response;
  }
  else 
  {
    $remittance_routes = array();
    $sql    = "SELECT DISTINCT `tocountry` FROM `route_summaries` WHERE `fromcountry` = '$sending_country'";
    $retval = mysqli_query($conn, $sql);
    if (!$retval) {
        die('Could not get data: ' . mysqli_error($conn));
    }
    while ($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
        $remittanceroute = "{$row['tocountry']}";
        array_push($remittance_routes, $remittanceroute);
    }


    if(mysqli_num_rows($retval)>3)
    {
      sort($remittance_routes);
      $size_of_chunks = count($remittance_routes) / 3;
      $chunked_remittance_routes = (array_chunk($remittance_routes, $size_of_chunks));
    }

    mysqli_free_result($retval);
    $mem_var->set($key, $chunked_remittance_routes, time() + 14400);
  }
  return $chunked_remittance_routes;
}


function print_route_summary_group($conn, $chunked_remittance_routes, $sending_country, $amount, $sending_currency_symbol)
{
  /*
    foreach ($chunked_remittance_routes as $key => $route) 
    {
        $sql    = "SELECT * FROM route_summaries WHERE `ToCountry` = '" . $sending_country . "' and `FromCountry`='$route' order by `timestamp` DESC limit 1";
        //echo($sql);
        $retval = mysqli_query($conn, $sql);
        if (!$retval) 
        {
            die('Could not get data E: ' . mysqli_error($conn));
        }
        $row = mysqli_fetch_array($retval, MYSQLI_ASSOC); 
        
        $exchange_1               = "{$row['buyfrom']}";
        $exchange_2            = "{$row['sellto']}";
        $funding_name            = "{$row['fundingname']}";
        $withdrawal_name            = "{$row['withdrawalname']}";
        $total_in_receiving_currency         = "{$row['totalintocurrency']}";                                    
        $totalcost         = "{$row['total_cost']}";  
        $receiving_country            = "$route";

        // get tocurrency and receiving_currency_symbol
        list($tocurrency, $receiving_currency_symbol) = get_currency_currencysymbol($conn, $receiving_country);
        $remittanceroute = $receiving_country;

        $remittanceroute_lc = strtolower($receiving_country);
        $remittanceroute_lc = trim($remittanceroute_lc);
        $remittanceroute_lc = str_replace(' ', '', $remittanceroute_lc);

        if ($amount > 5000)
        {
            $totalcost = number_format((float) $totalcost, 0, '.', '');
        }
        else
        {
            $totalcost = number_format((float) $totalcost, 2, '.', '');
        }

        if ($totalcost < 0)
        {
            $total_color = "green";
        }
        else
        {
           $total_color = "red";
        }

        ?>
        <div class="row" style="border-bottom:none;">
            <div class="twelve columns" style="background-color: white;border-top:none;">
                <div style="font-size: 18px; display: inline;">
                    <span data-toggle="collapse" data-target="#<?php echo $remittanceroute_lc;?>">
                    <img data-src="./logos/flags/<?php echo $remittanceroute_lc;?>.png" style="max-width: 32px;max-height: 18px;" alt="<?php echo $remittanceroute;?>" class="lazyload"> 
                    
                        <?php echo $remittanceroute;?>: <a style="color:<?php echo $total_color;?>;font-size: 18px;"><?php echo "$sending_currency_symbol$totalcost";?> &#x25BC;</a>
                    </span>
                </div>
            </div>
        </div>
        <div class="row collapse" id="<?php echo $remittanceroute_lc;?>">
            <div class="twelve columns">
                <h4>Cheapest Provider<?php if ($exchange_2 != "") {echo "s";} ?></h4>
                <?php $url = "./d/?exchange_1=" . rawurlencode($exchange_1) . "&exchange_2=" . rawurlencode($exchange_2) . "&Amount=$amount&FromCountry=" . rawurlencode($sending_country) . "&ToCountry=" . rawurlencode($remittanceroute) . "&funding_name=" . rawurlencode($funding_name) . "&withdrawal_name=" . rawurlencode($withdrawal_name);

                ?>
                <h5><?php echo "<a href=\"$url\">$exchange_1"; if ($exchange_2 != "") {echo " to $exchange_2";}?></a></h5>
                Receive <?php echo "$receiving_currency_symbol$total_in_receiving_currency $tocurrency";?><br>
                <?php
                echo "<form style=\"display: inline\" action=\"$url\" method=\"post\"><button style=\"font-size: 1.3em;\">PAY $receiving_currency_symbol$totalcost</button></form>";
                $url = "./r/?Amount=$amount&FromCountry=" . rawurlencode($sending_country) . "&ToCountry=" . rawurlencode($remittanceroute);
                echo "<a href=\"$url\">Full Search</a>";?>
            </div>
        </div> 
        <?php
    }
*/

  $sql = "";
  if(is_array($chunked_remittance_routes))
  {
    foreach ($chunked_remittance_routes as $key => $route) 
    {
        
      $mem_var = new Memcached();
      $mem_var->addServer("127.0.0.1", 11211);
      $key = md5("route_summary$sending_country$route");
      $response = $mem_var->get("$key");
      if ($response) 
      {
        $row = $response;
      }
      else 
      {
        $sql = "SELECT * FROM route_summaries WHERE `FromCountry` = '" . $sending_country . "' and `ToCountry`='$route' order by `timestamp` DESC limit 1";
        //echo $sql;
        $retval = mysqli_query($conn ,$sql);
        if (!$retval) {
            die('Could not get route summary data: ' . mysqli_error($conn));
        }
        $row = mysqli_fetch_array($retval, MYSQLI_ASSOC);


        mysqli_free_result($retval);
        $mem_var->set($key, $row, time() + 14400);
      }

        $exchange_1               = "{$row['buyfrom']}";
        $exchange_2            = "{$row['sellto']}";
        $funding_name            = "{$row['fundingname']}";
        $withdrawal_name            = "{$row['withdrawalname']}";
        $total_in_receiving_currency         = "{$row['totalintocurrency']}";                                    
        $total_cost_sending_currency         = "{$row['total_cost_sending_currency']}";  
        $receiving_country            = "{$row['tocountry']}";
        $receiving_currency_symbol            = "{$row['tocurrencysymbol']}";
        $tocurrency            = "{$row['tocurrency']}";
        $crypto         = "{$row['crypto']}";
        $total_cost_receiving_currency         = "{$row['total_cost_receiving_currency']}";


        $remittanceroute = $receiving_country;

        $remittanceroute_lc = strtolower($receiving_country);
        $remittanceroute_lc = trim($remittanceroute_lc);
        $remittanceroute_lc = str_replace(' ', '', $remittanceroute_lc);

        if ($amount > 20000)
        {
            $total_cost_receiving_currency = number_format((float) $total_cost_receiving_currency, 0, '.', '');
        }
        else
        {
            $total_cost_receiving_currency = number_format((float) $total_cost_receiving_currency, 2, '.', '');
        }
        

        if ($total_cost_receiving_currency < 0)
        {
            $total_color = "green";
            $total_cost_receiving_currency = $total_cost_receiving_currency * -1;
            $cost_description = "Make";
        }
        else
        {
           $total_color = "red";
           $cost_description = "Pay";
        }

        ?>

        <style>
        #block<?php echo $remittanceroute_lc;?> {
            height: 0;
            overflow: hidden;
            -webkit-transition: height 300ms linear;
            -moz-transition: height 300ms linear;
            -o-transition: height 300ms linear;
            transition: height 300ms linear;
        }

        label {
            cursor: pointer;
        }

        #showblock<?php echo $remittanceroute_lc;?> {
            display: none;
        }

        #showblock<?php echo $remittanceroute_lc;?>:checked + #block<?php echo $remittanceroute_lc;?> {
            height: 200px;
            display: block;
        }
        </style>
                    <label for="showblock<?php echo $remittanceroute_lc;?>"><!--<span data-toggle="collapse" data-target="#<?php echo $remittanceroute_lc;?>">-->
                    <img data-src="./logos/flags/<?php echo $remittanceroute_lc;?>.png" style="max-width: 32px;max-height: 18px;" alt="<?php echo $remittanceroute;?>" class="lazyload"> 
                    
                        <?php echo $remittanceroute;?>: <a style="color:<?php echo $total_color;?>;font-size: 18px;"><?php echo "$receiving_currency_symbol$total_cost_receiving_currency";?> &#x25BC;</a>
                    <!--</span>-->
                    </label>
                    <input type="checkbox" id="showblock<?php echo $remittanceroute_lc;?>">
        <!--<div class="row collapse" id="<?php echo $remittanceroute_lc;?>">-->
        <div class="row" id="block<?php echo $remittanceroute_lc;?>">
          <h4>Cheapest Provider<?php if ($exchange_2 != "") {echo "s";} ?></h4>
          <?php $url = "./d/?exchange_1=" . rawurlencode($exchange_1) . "&exchange_2=" . rawurlencode($exchange_2) . "&amount=$amount&sending_country=" . rawurlencode($sending_country) . "&receiving_country=" . rawurlencode($remittanceroute) . "&funding_name=" . rawurlencode($funding_name) . "&withdrawal_name=" . rawurlencode($withdrawal_name)  . "&crypto=$crypto";

          ?>
          <h5><?php echo "<a href=\"$url\">$exchange_1"; if ($exchange_2 != "") {echo " to $exchange_2";}?></a></h5>
          Receive <?php echo "$receiving_currency_symbol$total_in_receiving_currency $tocurrency";?><br>
          <?php

          echo "<form style=\"display: inline\" action=\"$url\" method=\"post\"><button style=\"font-size: 1.3em;\">$cost_description  $receiving_currency_symbol$total_cost_receiving_currency</button></form>";
          $url = "./r/?amount=$amount&sending_country=" . rawurlencode($sending_country) . "&receiving_country=" . rawurlencode($remittanceroute);
          echo "<a href=\"$url\">Full Search</a>";?>
        </div> 
        <?php
    }
  }






/*
  // Execute multi query
  if (mysqli_multi_query($conn,$sql))
  {
    do
    {
      // Store first result set
      if ($result=mysqli_store_result($conn)) 
      {
        // Fetch one and one row
        while ($row=mysqli_fetch_row($result))
        {
        
          $exchange_1               = "{$row[3]}";
          $exchange_2            = "{$row[4]}";
          $funding_name            = "{$row[5]}";
          $withdrawal_name            = "{$row[6]}";
          $total_in_receiving_currency         = "{$row[7]}";                                    
          $total_cost_sending_currency         = "{$row[12]}";  
          $receiving_country            = "{$row[1]}";
          $receiving_currency_symbol            = "{$row[11]}";
          $tocurrency            = "{$row[9]}";
          $crypto         = "{$row[13]}";
          $total_cost_receiving_currency         = "{$row[14]}";

          //print_r($row);


          // get tocurrency and receiving_currency_symbol
          //list($tocurrency, $receiving_currency_symbol) = get_currency_currencysymbol($conn, $receiving_country);
          $remittanceroute = $receiving_country;

          $remittanceroute_lc = strtolower($receiving_country);
          $remittanceroute_lc = trim($remittanceroute_lc);
          $remittanceroute_lc = str_replace(' ', '', $remittanceroute_lc);

          if ($amount > 20000)
          {
              $total_cost_receiving_currency = number_format((float) $total_cost_receiving_currency, 0, '.', '');
          }
          else
          {
              $total_cost_receiving_currency = number_format((float) $total_cost_receiving_currency, 2, '.', '');
          }
          

          if ($total_cost_receiving_currency < 0)
          {
              $total_color = "green";
          }
          else
          {
             $total_color = "red";
          }

          ?>
           <div class="row" style="border-bottom:none;">
              <div class="twelve columns" style="background-color: white;border-top:none;">
                  <div style="font-size: 18px; display: inline;">
                      <span data-toggle="collapse" data-target="#<?php echo $remittanceroute_lc;?>">
                      <img data-src="./logos/flags/<?php echo $remittanceroute_lc;?>.png" style="max-width: 32px;max-height: 18px;" alt="<?php echo $remittanceroute;?>" class="lazyload"> 
                      
                          <?php echo $remittanceroute;?>: <a style="color:<?php echo $total_color;?>;font-size: 18px;"><?php echo "$receiving_currency_symbol$total_cost_receiving_currency";?> &#x25BC;</a>
                      </span>
                  </div>
              </div>
          </div>
          <div class="row collapse" id="<?php echo $remittanceroute_lc;?>">
            <h4>Cheapest Provider<?php if ($exchange_2 != "") {echo "s";} ?></h4>
            <?php $url = "./d/?exchange_1=" . rawurlencode($exchange_1) . "&exchange_2=" . rawurlencode($exchange_2) . "&amount=$amount&sending_country=" . rawurlencode($sending_country) . "&receiving_country=" . rawurlencode($remittanceroute) . "&funding_name=" . rawurlencode($funding_name) . "&withdrawal_name=" . rawurlencode($withdrawal_name)  . "&crypto=$crypto";

            ?>
            <h5><?php echo "<a href=\"$url\">$exchange_1"; if ($exchange_2 != "") {echo " to $exchange_2";}?></a></h5>
            Receive <?php echo "$receiving_currency_symbol$total_in_receiving_currency $tocurrency";?><br>
            <?php
            echo "<form style=\"display: inline\" action=\"$url\" method=\"post\"><button style=\"font-size: 1.3em;\">PAY $receiving_currency_symbol$total_cost_receiving_currency</button></form>";
            $url = "./r/?amount=$amount&sending_country=" . rawurlencode($sending_country) . "&receiving_country=" . rawurlencode($remittanceroute);
            echo "<a href=\"$url\">Full Search</a>";?>
          </div> 
          <?php
          // Free result set
          mysqli_free_result($result);
        }
      }    
    }
    while(mysqli_more_results($conn) && mysqli_next_result($conn));
  }
  print_r(error_get_last());
*/
}