 <?php
include 'headerfile.php';

// get variables
$receiving_country   = $_GET["receiving_country"];
$sending_country = $_GET["sending_country"];
$amount      = $_GET["amount"];

if (isset($_GET["volume"])) {
    $volumetocheck = $_GET["volume"];
} else {
    $volumetocheck = "50";
}

if (isset($_GET["pn"])) {
    $page = $_GET["pn"];
} else {
    $page = "1";
}

if (isset($_GET["fullsite"])) {
    $fullsite = $_GET["fullsite"];
} else {
    $fullsite = "";
}


// sanitise variables
$amount           = filter_var($amount, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
$sending_country      = filter_var($sending_country, FILTER_SANITIZE_SPECIAL_CHARS);
$receiving_country        = filter_var($receiving_country, FILTER_SANITIZE_SPECIAL_CHARS);
$fullsite  = filter_var($fullsite, FILTER_SANITIZE_SPECIAL_CHARS);
$page  = filter_var($page, FILTER_SANITIZE_SPECIAL_CHARS);
$volumetocheck    = filter_var($volumetocheck, FILTER_SANITIZE_SPECIAL_CHARS);

// connect to database
$conn = connect_to_database();

// get currency and currencysymbol
list($currency, $currencysymbol) = get_currency_currencysymbol($conn, $sending_country);
// get currency and currencysymbol
list($tocurrency, $tocurrencysymbol) = get_currency_currencysymbol($conn, $receiving_country);

if (($currency == 'EUR') && ($tocurrency == 'EUR' || $tocurrency == 'USD')) {
    $volumetocheck = '1000';
}
elseif (($currency == 'USD') && ($tocurrency == 'EUR' || $tocurrency == 'USD')) {
    $volumetocheck = '5000';
}

if (!is_numeric($amount))
{
    echo "<h1>We're going to need an amount to send!</h1><h2>Here's the results for $currencysymbol 1000.00 to get you started.</h2>";
    $amount = 1000;
}

//get the real exchange rate
$apicurrencies            = "$currency$tocurrency";
$realexchangerate         = get_exchange_rate($apicurrencies, $conn);

//get the real exchange rate when sent back
$apicurrencies            = "$tocurrency$currency";
$realexchangeratesentback = get_exchange_rate($apicurrencies, $conn);


// Get the list of results
$search_results  = generate_search_results($conn, $amount, $sending_country, $receiving_country, $volumetocheck);


$search_results  = remove_duplicate_routes($search_results);


$max_row = $page*20;

if ($page==1)
{
    $first_row = 0;
    $last_row = 19;
}
else
{
    $first_row = ($page*20)-20;
    $last_row = $page*20;
}

// If there are routes found
if (count($search_results) > 0) 
{
    // sort them by total
    foreach ($search_results as $key => $row) {
        $totalintocurrencies[$key] = $row[47];
    }
    array_multisort($totalintocurrencies, SORT_DESC, $search_results);

    //list($fundingname, $fundingfee, $fundingpercentage, $buyingbitcoinfee, $buyingbitcoinpercentage, $transactionfee, $transactionfeecurrency, $arbitrage, $sellingbitcoinpercentage, $sellingbitcoinpercentagecurrency, $sellingbitcoinfee, $sellingbitcoinfeecurrency, $withdrawalname, $withdrawalpercentage, $withdrawalpercentagecurrency, $withdrawalfee, $withdrawalfeecurrency, $buyfrom, $askprice, $volume, $sellto, $selltovolume, $sellprice, $selltowebsite, $exchangeratemargin, $exchangeratemarginfees, $sendinglocation, $firmtype, $product, $speedactual, $pickupmethod, $coverage, $periodfrom, $date, $fee, $coverage, $website, $amount, $totalfees, $totalcost, $totalintocurrency, $realexchangerate, $realexchangeratesentback, $sending_country, $receiving_country, $currency, $tocurrency, $last_updated_fees, $last_updated_prices, $last_updated_sellto_fees, $last_updated_sellto_prices, $listoffundingtowithdrawalmethods, $crypto, $withdraw_crypto_fee, $withdraw_crypto_fee_currency) = load_search_result_variables($result);

    //$buyfrom           = preg_replace('/[^A-Za-z0-9\-\' .]/', '', $buyfrom);
    ?>
    <h1 style="text-align:center;">MoneyStyle found <?php echo count($search_results);?> ways of sending money to <?php echo $receiving_country;?></h1>
    <?php
    is_amount_extra_large($conn, $amount, $sending_country);

    if (isset($_GET["fullsite"])) 
    {
        $fullsite = $_GET["fullsite"];
    }
    else 
    {
        $fullsite = "";
    }
    $fullsite  = filter_var($fullsite, FILTER_SANITIZE_SPECIAL_CHARS);
    ?>

    <div id="full_results">
    <h3 style="text-align:center;">We Checked...</h3>
    <?php
    print_route_table_header($sending_country, $receiving_country);
    
    $row=0;
    foreach ($search_results as $result)
    {
        $row++;
        if (($row>=$first_row)&&($row<=$last_row))
        {        
            //list($fundingname, $fundingfee, $fundingpercentage, $buyingbitcoinfee, $buyingbitcoinpercentage, $transactionfee, $transactionfeecurrency, $arbitrage, $sellingbitcoinpercentage, $sellingbitcoinpercentagecurrency, $sellingbitcoinfee, $sellingbitcoinfeecurrency, $withdrawalname, $withdrawalpercentage, $withdrawalpercentagecurrency, $withdrawalfee, $withdrawalfeecurrency, $buyfrom, $askprice, $volume, $sellto, $selltovolume, $sellprice, $selltowebsite, $exchangeratemargin, $exchangeratemarginfees, $sendinglocation, $firmtype, $product, $speedactual, $pickupmethod, $coverage, $periodfrom, $date, $fee, $coverage, $website, $amount, $totalfees, $totalcost, $totalintocurrency, $realexchangerate, $realexchangeratesentback, $sending_country, $receiving_country, $currency, $tocurrency, $last_updated_fees, $last_updated_prices, $last_updated_sellto_fees, $last_updated_sellto_prices, $listoffundingtowithdrawalmethods, $crypto, $withdraw_crypto_fee, $withdraw_crypto_fee_currency, $buyingbitcoinpercentagecurrency, $buyingbitcoinfeecurrency, $fundingpercentagecurrency) = load_search_result_variables($result);

            list($funding_name, $funding_fee, $funding_percentage, $buying_crypto_fee, $buying_crypto_percentage, $network_transaction_fee, $network_transaction_fee_currency, $arbitrage, $selling_crypto_percentage, $selling_crypto_percentage_currency, $selling_crypto_fee, $selling_crypto_fee_currency, $withdrawalname, $withdrawal_percentage, $withdrawal_percentage_currency, $withdrawal_fee, $withdrawal_fee_currency, $exchange_1, $ask_price, $volume, $exchange_2, $exchange_2_volume, $bid_price, $exchange_2_website, $exchange_rate_margin, $exchange_rate_margin_fees, $sending_location, $firm_type, $product, $speed_actual, $pick_up_method, $coverage, $period_from, $date, $fee, $coverage, $website, $amount, $total_fees, $total_cost, $total_in_receiving_currency, $exchange_rate, $exchange_rate_inverse, $sending_country, $receiving_country, $sending_currency, $receiving_currency, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_fee_amount_en_route, $withdrawal_percentage_amount_en_route, $withdrawal_fee_amount_en_route) = load_result_variables($search_results, $row);
            
            
            //print_search_result($currency, $conn, $sending_country, $receiving_country, $tocurrency, $fundingfee, $fundingname, $website, $buyfrom, $sendinglocation, $firmtype, $product, $speedactual, $pickupmethod, $coverage, $periodfrom, $date, $amount, $currencysymbol, $tocurrencysymbol, $sellto, $selltovolume, $sellprice, $selltowebsite, $transactionfee, $withdrawalname, $withdrawalfee, $totalfees, $arbitrage, $totalcost, $totalintocurrency, $realexchangeratesentback, $askprice, $transactionfeecurrency, $sellingbitcoinpercentagecurrency, $sellingbitcoinpercentage, $exchangeratemargin, $exchangeratemarginfees, $realexchangerate, $fee, $search_results, $volumetocheck, $fundingpercentage, $buyingbitcoinpercentage, $withdrawalfee, $withdrawalfeecurrency, $sellingbitcoinfee, $buyingbitcoinfee, $withdrawalpercentage, $withdrawalpercentagecurrency, $listoffundingtowithdrawalmethods, true, $last_updated_fees, $last_updated_prices, $last_updated_sellto_fees, $last_updated_sellto_prices, $crypto, $withdraw_crypto_fee, $sellingbitcoinfeecurrency);

            print_search_result($currency, $conn, $sending_country, $receiving_country, $tocurrency, $fundingfee, $fundingname, $website, $buyfrom, $sendinglocation, $firmtype, $product, $speedactual, $pickupmethod, $coverage, $periodfrom, $date, $amount, $currencysymbol, $tocurrencysymbol, $sellto, $selltovolume, $sellprice, $selltowebsite, $withdraw_crypto_fee, $withdrawalname, $withdrawalfee, $totalfees, $arbitrage, $totalcost, $totalintocurrency, $realexchangeratesentback, $askprice, $withdraw_crypto_fee_currency, $sellingbitcoinpercentagecurrency, $sellingbitcoinpercentage, $exchangeratemargin, $exchangeratemarginfees, $realexchangerate, $fee, $search_results, $volumetocheck, $fundingpercentage, $buyingbitcoinpercentage, $withdrawalfee, $withdrawalfeecurrency, $sellingbitcoinfee, $buyingbitcoinfee, $withdrawalpercentage, $withdrawalpercentagecurrency, $listoffundingtowithdrawalmethods, true, $last_updated_fees, $last_updated_prices, $last_updated_sellto_fees, $last_updated_sellto_prices, $crypto, $sellingbitcoinfeecurrency, $buyingbitcoinpercentagecurrency, $buyingbitcoinfeecurrency, $fundingpercentagecurrency);
        }
    }
    ?>
    </div>
    <?php
}
else 
{
    ?>
    <h1 style="">#SadFace! :(</h1><h4>We couldn't find a route in our database that matches your search criteria.</h4>
    <p>Please try another search.</p>
    <p>The minimum exchange volume option, in the "More Search Options" area, may show more routes</p>
    <p>Please choose your minimum volume carefully as lower volumes make it harder to trade bitcoins. We don't take market depth into account yet, and this can influence cost on low volume exchanges.</p>
    <?php
}


// PRINT PAGINATION
?>
<div style="text-align:center;">
    <ul class="pagination">
    <?php
        $page_number = 1;
        while (($page_number*20-20) < count($search_results))
        {
            if ($page_number == $page)
            {
                echo "<li class=\"active\"><a href=\"../r/?Amount=$amount&FromCountry=$sending_country&ToCountry=$receiving_country&pn=$page_number&Volume=$volumetocheck\">$page_number</a></li>";
            }
            else
            {
                echo "<li><a href=\"../r/?Amount=$amount&FromCountry=$sending_country&ToCountry=$receiving_country&pn=$page_number&Volume=$volumetocheck\">$page_number</a></li>";
            }
            $page_number++;
        }
    ?>
    </ul>
</div>
<?php 
// END OF PAGINATION
?>
<div style="text-align: center;">
If you believe any information provided is incorrect then please <a href="../contact/">contact MoneyStyle</a>.
</div>
</div>


<div class="row" id="fullsite" style="margin-top: 15px;<?php if ($fullsite!="") { echo " display: none; ";}?>" >
    <div class="twelve columns" style="text-align: center;">
        <h3><a href="../">Home</a> | <a href="<?php echo $_SERVER['REQUEST_URI'] . "&fullsite=yes";?>">Full Site</a></h3>
    </div>
</div>

<div style="margin:auto;text-align: center;">
    <img src="../logos/logo-small-white.png" style="max-width: 280px" alt="MoneyStyle Logo"><br>
    <h3>Follow The Money.</h3>
</div>



<?php
// FUNCTIONS
function load_search_result_variables($result)
{

    // bitcoin calculations
    $fundingname = $result[0];
    $fundingfee = $result[1];
    $fundingpercentage = $result[2];
    $fundingpercentagecurrency = $result[3];

    $buyingbitcoinfee = $result[4];
    $buyingbitcoinfeecurrency = $result[5];
    $buyingbitcoinpercentage = $result[6];
    $buyingbitcoinpercentagecurrency = $result[7];

    $transactionfee = $result[8];
    $transactionfeecurrency = $result[9];
    $arbitrage = $result[10];

    $sellingbitcoinpercentage = $result[11];
    $sellingbitcoinpercentagecurrency = $result[12];
    $sellingbitcoinfee = $result[13];
    $sellingbitcoinfeecurrency = $result[14];

    $withdrawalname = $result[15];
    $withdrawalpercentage = $result[16];
    $withdrawalpercentagecurrency = $result[17];
    $withdrawalfee = $result[18];
    $withdrawalfeecurrency = $result[19];

    // bitcoin descriptions
    $buyfrom = $result[20];
    $askprice = $result[21];
    $volume = $result[22];
    $sellto = $result[23];
    $selltovolume = $result[24];
    $sellprice = $result[25];
    $selltowebsite = $result[26];
    $last_updated_fees = $result[27];
    $last_updated_prices = $result[28];
    $last_updated_sellto_fees = $result[29];
    $last_updated_sellto_prices = $result[30];

    // mto calcs
    $exchangeratemargin = $result[31];
    $exchangeratemarginfees = $result[32];

    // general descriptions
    $sendinglocation = $result[33];
    $firmtype = $result[34];
    $product = $result[35];
    $speedactual = $result[36];
    $pickupmethod = $result[37];
    $coverage = $result[38];
    $periodfrom = $result[39];
    $date = $result[40];
    $fee = $result[41];
    $coverage = $result[42];
    $website = $result[43];
    
    // general calcs
    $amount = $result[44];
    $totalfees = $result[45];
    $totalcost = $result[46];
    $totalintocurrency = $result[47];
    
    $realexchangerate = $result[48];
    $realexchangeratesentback = $result[49];

    // route fields
    $sending_country = $result[50];
    $receiving_country = $result[51];
    $currency = $result[52];
    $tocurrency = $result[53];
    $crypto = $result[54];
    $withdraw_crypto_fee = $result[55];
    $withdraw_crypto_fee_currency = $result[56];
    $listoffundingtowithdrawalmethods = $result[57];

    print_r(error_get_last());

    return array($fundingname, $fundingfee, $fundingpercentage, $buyingbitcoinfee, $buyingbitcoinpercentage, $transactionfee, $transactionfeecurrency, $arbitrage, $sellingbitcoinpercentage, $sellingbitcoinpercentagecurrency, $sellingbitcoinfee, $sellingbitcoinfeecurrency, $withdrawalname, $withdrawalpercentage, $withdrawalpercentagecurrency, $withdrawalfee, $withdrawalfeecurrency, $buyfrom, $askprice, $volume, $sellto, $selltovolume, $sellprice, $selltowebsite, $exchangeratemargin, $exchangeratemarginfees, $sendinglocation, $firmtype, $product, $speedactual, $pickupmethod, $coverage, $periodfrom, $date, $fee, $coverage, $website, $amount, $totalfees, $totalcost, $totalintocurrency, $realexchangerate, $realexchangeratesentback, $sending_country, $receiving_country, $currency, $tocurrency, $last_updated_fees, $last_updated_prices, $last_updated_sellto_fees, $last_updated_sellto_prices, $listoffundingtowithdrawalmethods, $crypto, $withdraw_crypto_fee, $withdraw_crypto_fee_currency, $buyingbitcoinpercentagecurrency, $buyingbitcoinfeecurrency, $fundingpercentagecurrency);
}

mysqli_close($conn);
?>