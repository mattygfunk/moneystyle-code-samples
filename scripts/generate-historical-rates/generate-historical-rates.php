<?php
include '/home/webapp/pheanstalk/vendor/autoload.php';
use Pheanstalk\Pheanstalk;
$pheanstalk = new Pheanstalk('127.0.0.1');

include '/home/webapp/money.style/headerfile.php';

// connect to database
$conn = connect_to_database();


$route_message_string = array();


$sql = "SELECT DISTINCT `Country` FROM  `countries` WHERE 1 ";

$retval = mysqli_query($conn, $sql);
if (!$retval) {
    die('Could not get data A: ' . mysqli_error());
}
while ($row = mysqli_fetch_array($retval)) {
    $sending_country = "{$row['Country']}";
    //echo "$fromcountry being checked";

    $remittanceroutes = get_remittance_routes($conn, $sending_country);

	foreach ($remittanceroutes as $key => $receiving_country) 
	{    
		//echo "Checking $fromcountry to $remittanceroute\n";
	    //$tube_id=rand(1,9);
		//$pheanstalk->useTube('historical-tube'.$tube_id)->put('{"'.$fromcountry.'","'.$remittanceroute.'}"');

        $message = '{"'.$sending_country.'" , "'.$receiving_country.'"}';
        array_push($route_message_string, $message);
	}
}

//shuffle($route_message_string);

foreach ($route_message_string as $key => $message) {
    $tube_id=rand(1,9);
    $pheanstalk->useTube('historical-tube'.$tube_id)->put($message);
}


print_r(error_get_last());
mysqli_close($conn);
// End of page



function get_remittance_routes($conn, $sending_country)
{
    $remittance_routes = array();
    // get the rimmittance routes available from the world bank
    $sql    = "SELECT DISTINCT `ToCountry` FROM `banks_and_mtos` WHERE  `FromCountry` = '$sending_country'";
    $retval = mysqli_query($conn, $sql);
    if (!$retval) {
        die('Could not get data: ' . mysqli_error($conn));
    }
    while ($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
        $remittanceroute = "{$row['ToCountry']}";
        array_push($remittance_routes, $remittanceroute);
    }

    // get currency and currencysymbol
    list($currency, $currencysymbol) = get_currency_currencysymbol($conn, $sending_country);

    // if the country has one bitcoin exchange it can accept all bitcoin enabled countries
    $sql    = "SELECT `Country` FROM `bitcoin_exchanges` WHERE `Currency` = '$currency'";
    //echo $sql;
    $exchange_check = mysqli_query($conn, $sql);
    if (mysqli_num_rows($exchange_check)>0) {

        $sql    = "SELECT DISTINCT `Currency` FROM `bitcoin_exchanges` WHERE (1)";
        $retval = mysqli_query($conn, $sql);
        if (!$retval) {
            die('Could not get data: ' . mysqli_error($conn));
        }
        while ($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
            $tocurrency = "{$row['Currency']}";
            $sql    = "SELECT `Country` FROM `countries` WHERE `Currency` = '$tocurrency'";
            $country_result = mysqli_query($conn, $sql);
            if (!$country_result) {
                die('Could not get data: ' . mysqli_error($conn));
            }
            while ($row = mysqli_fetch_array($country_result, MYSQLI_ASSOC)) {
                $remittanceroute = "{$row['Country']}";
                array_push($remittance_routes, $remittanceroute);
            }
            
            print_r(error_get_last());
        }
    }

    $remittance_routes = array_unique($remittance_routes);
    sort($remittance_routes);

    return $remittance_routes;
}



