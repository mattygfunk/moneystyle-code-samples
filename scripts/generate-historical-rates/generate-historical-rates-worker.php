<?php
include '/home/webapp/pheanstalk/vendor/autoload.php';
use Pheanstalk\Pheanstalk;
$pheanstalk = new Pheanstalk('127.0.0.1');

include '/home/webapp/money.style/headerfile.php';


while(true)
{
    $tube_id=rand(1,9);
    $job=$pheanstalk->watch('historical-tube'.$tube_id)->ignore('default')->reserve();
    if($job)
    {
        $message = $job->getdata();

        $message = str_replace('{', '', $message);
        $message = str_replace('}', '', $message);

        $data = str_getcsv($message);
        $sending_country = $data[0];
        $receiving_country = $data[1];


        // connect to database
        $conn = connect_to_database();

        $amount = get_default_amount($conn, $sending_country);

        // get currency and currencysymbol
        list($sending_currency, $sending_currency_symbol) = get_currency_currencysymbol($conn, $sending_country);

        // get tocurrency and tocurrencysymbol
        list($receiving_currency, $receiving_currency_symbol) = get_currency_currencysymbol($conn, $receiving_country);

        //get the real exchange rate
        $apicurrencies      = "$sending_currency$receiving_currency";
        list($exchange_rate, $last_updated_exchange_rate)   = get_exchange_rate($apicurrencies, $conn);

        if (strlen($apicurrencies) != 6)
        {
            echo "<h1>error! apicurrencies should be 6 characters! (Currently: api:$apicurrencies c:$sending_currency tc:$receiving_currency)</h1>";
        }

        if ($exchange_rate!="")
        {
            //get the real exchange rate when sent back
            $apicurrencies            = "$receiving_currency$sending_currency";
            list($exchange_rate_inverse, $last_updated_exchange_rate_inverse) = get_exchange_rate($apicurrencies, $conn);

            if (($receiving_currency == 'EUR') && ($sending_currency == 'EUR' || $sending_currency == 'USD')) 
            {
                $volumetocheck = '1000';
            }
            elseif (($receiving_currency == 'USD') && ($sending_currency == 'EUR' || $sending_currency == 'USD')) 
            {
                $volumetocheck = '1000';
            }
            else
            {
                $volumetocheck = 50;
            }
            
            // Get the list of results
            $search_results  = generate_search_results($conn, $amount, $sending_country, $receiving_country, $volumetocheck);

            if(empty($search_results))
            {
                $volumetocheck = 0;
                $search_results  = generate_search_results($conn, $amount, $sending_country, $receiving_country, $volumetocheck);
            }

            if (!empty($search_results)) 
            {
                $totalintocurrencies = array();
                // SORT FULLRESULTS ARRAY
                foreach ($search_results as $key => $row) 
                {
                    // replace 0 with the field's index/key
                    $totalintocurrencies[$key] = $row[56];
                    //echo "$row[53];<br>";
                }
                //print_r($totalintocurrencies);
                //echo "<br>";
                array_multisort($totalintocurrencies, SORT_ASC, $search_results);
                $data_to_insert = array();
                foreach ($search_results as $key => $row) 
                {
                    list($funding_name, $funding_fee, $funding_percentage, $buying_crypto_fee, $buying_crypto_percentage, $network_transaction_fee, $network_transaction_fee_currency, $arbitrage, $selling_crypto_percentage, $selling_crypto_percentage_currency, $selling_crypto_fee, $selling_crypto_fee_currency, $withdrawal_name, $withdrawal_percentage, $withdrawal_percentage_currency, $withdrawal_fee, $withdrawal_fee_currency, $exchange_1, $ask_price, $volume, $exchange_2, $exchange_2_volume, $bid_price, $exchange_2_website, $exchange_rate_margin, $exchange_rate_margin_fees, $sending_location, $firm_type, $product, $speed_actual, $pick_up_method, $coverage, $period_from, $date, $fee, $coverage, $website, $amount, $total_fees, $total_cost, $total_in_receiving_currency, $exchange_rate, $exchange_rate_inverse, $sending_country, $receiving_country, $sending_currency, $receiving_currency, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_fee_amount_en_route, $withdrawal_percentage_amount_en_route, $withdrawal_fee_amount_en_route) = load_result_variables($search_results, $key);
                    
                    $route_exchange_rate =  $total_in_receiving_currency / $amount;
                    
                    $sending_country = rtrim($sending_country);

                    if ($exchange_2!="")
                    {
                        $data_to_insert[$key] = "('$sending_country$receiving_country$exchange_1$exchange_2$funding_name$withdrawal_name$crypto',  '$route_exchange_rate',  '$exchange_rate', '$total_cost',  now())";
                    }              
                }

                if (isset($data_to_insert[0]))
                {
                    $sql = "INSERT INTO  `historical_route_rates` ( `routecode` , `route_exchange_rate` , `real_exchange_rate` , `total_cost` , `timestamp` )
                        VALUES ";
                    foreach ($data_to_insert as $key => $data) {
                        $sql .= $data . ",";
                    }
                    $sql = rtrim($sql,",");

                    //echo "$sql\n\n";

                    $result = mysqli_query($conn, $sql);
                    if ( false===$result ) 
                    {
                        echo $sql;
                        printf("error: %s\n", mysqli_error($conn));
                    }   
                }
                print_r(error_get_last());
            }
        }

        unset($search_results);
        print_r(error_get_last());

        //$pheanstalk->delete($job);
        try {
            $pheanstalk->delete($job);
        } catch (\Exception $e) {
        // handled missing Beanstalkd job from the tube
        }
        
        mysqli_close($conn);
    }
}
?>