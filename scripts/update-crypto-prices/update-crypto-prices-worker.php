<?php
include '/home/webapp/pheanstalk/vendor/autoload.php';
use Pheanstalk\Pheanstalk;
$pheanstalk = new Pheanstalk('127.0.0.1');

include '/home/webapp/money.style/headerfile.php';

// connect to database
$conn = connect_to_database();

while(true)
{
	$tube_id=rand(1,9);
	$job=$pheanstalk->watch('crypto-tube'.$tube_id)->ignore('default')->reserve();
	if($job)
	{
		$message = $job->getdata();

		$message = str_replace('{', '', $message);
		$message = str_replace('}', '', $message);

		$data = str_getcsv($message);
		$name = $data[0];
		$currency = $data[1];
		$crypto = $data[2];


		if (($name=="Bitbank")&&($currency=="JPY")&&($crypto=="BTC"))
		{
			$json = curl_download('https://public.bitbank.cc/btc_jpy/ticker');
			$bitbank = json_decode($json, true);

			$bid        = $bitbank['data']['sell'];
			$ask        = $bitbank['data']['buy'];
			$volume     = $bitbank['data']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitbank")&&($currency=="JPY")&&($crypto=="BCH"))
		{
			$json = curl_download('https://public.bitbank.cc/bcc_jpy/ticker');
			$bitbank = json_decode($json, true);

			$bid        = $bitbank['data']['sell'];
			$ask        = $bitbank['data']['buy'];
			$volume     = $bitbank['data']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitbank")&&($currency=="JPY")&&($crypto=="XRP"))
		{
			$json = curl_download('https://public.bitbank.cc/xrp_jpy/ticker');
			$bitbank = json_decode($json, true);

			$bid        = $bitbank['data']['sell'];
			$ask        = $bitbank['data']['buy'];
			$volume     = $bitbank['data']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Upbit")&&($currency=="KRW")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.upbit.com/v1/ticker?markets=KRW-BTC');
			$upbit = json_decode($json, true);

			$bid        = $upbit[0]['trade_price'];
			$ask        = $upbit[0]['trade_price'];
			$volume     = $upbit[0]['acc_trade_volume_24h'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Upbit")&&($currency=="KRW")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.upbit.com/v1/ticker?markets=KRW-BCH');
			$upbit = json_decode($json, true);

			$bid        = $upbit[0]['trade_price'];
			$ask        = $upbit[0]['trade_price'];
			$volume     = $upbit[0]['acc_trade_volume_24h'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Upbit")&&($currency=="KRW")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.upbit.com/v1/ticker?markets=KRW-LTC');
			$upbit = json_decode($json, true);

			$bid        = $upbit[0]['trade_price'];
			$ask        = $upbit[0]['trade_price'];
			$volume     = $upbit[0]['acc_trade_volume_24h'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Upbit")&&($currency=="KRW")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.upbit.com/v1/ticker?markets=KRW-XRP');
			$upbit = json_decode($json, true);

			$bid        = $upbit[0]['trade_price'];
			$ask        = $upbit[0]['trade_price'];
			$volume     = $upbit[0]['acc_trade_volume_24h'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Upbit")&&($currency=="KRW")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.upbit.com/v1/ticker?markets=KRW-ETH');
			$upbit = json_decode($json, true);

			$bid        = $upbit[0]['trade_price'];
			$ask        = $upbit[0]['trade_price'];
			$volume     = $upbit[0]['acc_trade_volume_24h'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Simex")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://simex.global/api/pairs');
			$simex = json_decode($json, true);

			$bid        = $simex['data'][0]['sell_price'];
			$ask        = $simex['data'][0]['buy_price'];
			$volume     = $simex['data'][0]['base_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Simex")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://simex.global/api/pairs');
			$simex = json_decode($json, true);

			$bid        = $simex['data'][3]['sell_price'];
			$ask        = $simex['data'][3]['buy_price'];
			$volume     = $simex['data'][3]['base_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/BTC/USD');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="USD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/BCC/USD');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/ETH/USD');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="USD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/LTC/USD');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/BTC/EUR');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="EUR")&&($crypto=="BCH"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/BCC/EUR');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="EUR")&&($crypto=="ETH"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/ETH/EUR');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="EUR")&&($crypto=="LTC"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/LTC/EUR');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="GBP")&&($crypto=="BTC"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/BTC/GBP');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="GBP")&&($crypto=="BCH"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/BCC/GBP');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="GBP")&&($crypto=="ETH"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/ETH/GBP');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="GBP")&&($crypto=="LTC"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/LTC/GBP');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="PLN")&&($crypto=="BTC"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/BTC/PLN');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="PLN")&&($crypto=="BCH"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/BCC/PLN');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="PLN")&&($crypto=="ETH"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/ETH/PLN');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinroom")&&($currency=="PLN")&&($crypto=="LTC"))
		{
			$json = curl_download('https://coinroom.com/api/ticker/LTC/PLN');
			$coinroom = json_decode($json, true);

			$bid        = $coinroom['bid'];
			$ask        = $coinroom['ask'];
			$volume     = $coinroom['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}









		if (($name=="Coinsuper")&&($currency=="USD")&&($crypto=="BTC"))
		{

			$data = array("symbol" => "BTC/USD");                                                                    
			$data_string = json_encode($data);                                                                                   
			                                                                                                                     
			$ch = curl_init('https://api.coinsuper.com/api/v1/market/tickers');                                                                      
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_string))                                                                       
			);                                                                                                                   
			                                                                                                                     
			$json = curl_exec($ch);
			$coinsuper = json_decode($json, true);

			
			if (is_array($coinsuper))
			{
				$bid        = $coinsuper['data'][0]['sell_price'];
				$ask        = $coinsuper['data'][0]['buy_price'];
				$volume     = $coinsuper['data'][0]['base_volume'];
			}

			//echo "$bid $ask $volume";

			if (is_array($coinsuper) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $coinsuper);
		}

		if (($name=="Coinsuper")&&($currency=="USD")&&($crypto=="ETH"))
		{

			$data = array("symbol" => "ETH/USD");                                                                    
			$data_string = json_encode($data);                                                                                   
			                                                                                                                     
			$ch = curl_init('https://api.coinsuper.com/api/v1/market/tickers');                                                                      
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_string))                                                                       
			);                                                                                                                   
			                                                                                                                     
			$json = curl_exec($ch);
			$coinsuper = json_decode($json, true);

			if (is_array($coinsuper))
			{
				$bid        = $coinsuper['data'][3]['sell_price'];
				$ask        = $coinsuper['data'][3]['buy_price'];
				$volume     = $coinsuper['data'][3]['base_volume'];
			}

			if (is_array($coinsuper) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $coinsuper);
		}

		if (($name=="Bitinka")&&($currency=="ALL")&&($crypto=="BTC"))
		{

			$json = curl_download('https://www.bitinka.com/api/apinka/ticker?format=json');
			$bitinka = json_decode($json, true);

			$currency="ARS";

			$bid        = $bitinka[$currency]['bid'];
			$ask        = $bitinka[$currency]['ask'];
			$volume     = $bitinka[$currency]['volumen24hours']['BTC'];

			if (is_array($bitinka) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}

			$currency="BOB";

			$bid        = $bitinka[$currency]['bid'];
			$ask        = $bitinka[$currency]['ask'];
			$volume     = $bitinka[$currency]['volumen24hours']['BTC'];

			if (is_array($bitinka) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}

			$currency="BRL";

			$bid        = $bitinka[$currency]['bid'];
			$ask        = $bitinka[$currency]['ask'];
			$volume     = $bitinka[$currency]['volumen24hours']['BTC'];

			if (is_array($bitinka) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}


			$currency="COP";

			$bid        = $bitinka[$currency]['bid'];
			$ask        = $bitinka[$currency]['ask'];
			$volume     = $bitinka[$currency]['volumen24hours']['BTC'];

			if (is_array($bitinka) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}


			$currency="CLP";

			$bid        = $bitinka[$currency]['bid'];
			$ask        = $bitinka[$currency]['ask'];
			$volume     = $bitinka[$currency]['volumen24hours']['BTC'];

			if (is_array($bitinka) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}

			$currency="CNY";

			$bid        = $bitinka[$currency]['bid'];
			$ask        = $bitinka[$currency]['ask'];
			$volume     = $bitinka[$currency]['volumen24hours']['BTC'];

			if (is_array($bitinka) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}


			$currency="EUR";

			$bid        = $bitinka[$currency]['bid'];
			$ask        = $bitinka[$currency]['ask'];
			$volume     = $bitinka[$currency]['volumen24hours']['BTC'];

			if (is_array($bitinka) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}

			$currency="PEN";

			$bid        = $bitinka[$currency]['bid'];
			$ask        = $bitinka[$currency]['ask'];
			$volume     = $bitinka[$currency]['volumen24hours']['BTC'];

			if (is_array($bitinka) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}

			unset($bid, $ask, $volume, $bitinka);
		}

		if (($name=="Bitfinex")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.bitfinex.com/v2/ticker/tBTCUSD');
			$bitfinex = json_decode($json, true);

			$bid        = $bitfinex[0];
			$ask        = $bitfinex[2];
			$volume     = $bitfinex[7];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitfinex")&&($currency=="USD")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.bitfinex.com/v2/ticker/tXRPUSD');
			$bitfinex = json_decode($json, true);

			$bid        = $bitfinex[0];
			$ask        = $bitfinex[2];
			$volume     = $bitfinex[7];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitfinex")&&($currency=="USD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.bitfinex.com/v2/ticker/tBCHUSD');
			$bitfinex = json_decode($json, true);

			$bid        = $bitfinex[0];
			$ask        = $bitfinex[2];
			$volume     = $bitfinex[7];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitfinex")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.bitfinex.com/v2/ticker/tETHUSD');
			$bitfinex = json_decode($json, true);

			$bid        = $bitfinex[0];
			$ask        = $bitfinex[2];
			$volume     = $bitfinex[7];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitfinex")&&($currency=="USD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.bitfinex.com/v2/ticker/tLTCUSD');
			$bitfinex = json_decode($json, true);

			$bid        = $bitfinex[0];
			$ask        = $bitfinex[2];
			$volume     = $bitfinex[7];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Luno")&&($currency=="ZAR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.mybitx.com/api/1/ticker?pair=XBTZAR');
			$luno = json_decode($json, true);

			$bid        = $luno['bid'];
			$ask        = $luno['ask'];
			$volume     = $luno['rolling_24_hour_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Luno")&&($currency=="MYR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.mybitx.com/api/1/ticker?pair=XBTMYR');
			$luno = json_decode($json, true);
			
			$bid        = $luno['bid'];
			$ask        = $luno['ask'];
			$volume     = $luno['rolling_24_hour_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Luno")&&($currency=="IDR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.mybitx.com/api/1/ticker?pair=XBTIDR');
			$luno = json_decode($json, true);

			$bid        = $luno['bid'];
			$ask        = $luno['ask'];
			$volume     = $luno['rolling_24_hour_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Luno")&&($currency=="NGN")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.mybitx.com/api/1/ticker?pair=XBTNGN');
			$luno = json_decode($json, true);
			
			$bid        = $luno['bid'];
			$ask        = $luno['ask'];
			$volume     = $luno['rolling_24_hour_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="BTC Markets")&&($currency=="AUD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.btcmarkets.net/market/BTC/AUD/tick');
			$btcmarkets = json_decode($json);
			$bid        = $btcmarkets->bestBid;
			$ask        = $btcmarkets->bestAsk;
			$volume     = $btcmarkets->volume24h;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTC Markets")&&($currency=="AUD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.btcmarkets.net/market/BTC/AUD/tick');
			$btcmarkets = json_decode($json);
			$bid        = $btcmarkets->bestBid;
			$ask        = $btcmarkets->bestAsk;
			$volume     = $btcmarkets->volume24h;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTC Markets")&&($currency=="AUD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.btcmarkets.net/market/ETH/AUD/tick');
			$btcmarkets = json_decode($json);
			$bid        = $btcmarkets->bestBid;
			$ask        = $btcmarkets->bestAsk;
			$volume     = $btcmarkets->volume24h;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTC Markets")&&($currency=="AUD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.btcmarkets.net/market/BCH/AUD/tick');
			$btcmarkets = json_decode($json);
			$bid        = $btcmarkets->bestBid;
			$ask        = $btcmarkets->bestAsk;
			$volume     = $btcmarkets->volume24h;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="BTC Markets")&&($currency=="AUD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.btcmarkets.net/market/LTC/AUD/tick');
			$btcmarkets = json_decode($json);
			$bid        = $btcmarkets->bestBid;
			$ask        = $btcmarkets->bestAsk;
			$volume     = $btcmarkets->volume24h;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTC Markets")&&($currency=="AUD")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.btcmarkets.net/market/XRP/AUD/tick');
			$btcmarkets = json_decode($json);
			$bid        = $btcmarkets->bestBid;
			$ask        = $btcmarkets->bestAsk;
			$volume     = $btcmarkets->volume24h;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}



		if (($name=="Independent Reserve")&&($currency=="AUD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=xbt&secondaryCurrencyCode=aud');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Independent Reserve")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=xbt&secondaryCurrencyCode=usd');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Independent Reserve")&&($currency=="NZD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=xbt&secondaryCurrencyCode=nzd');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Independent Reserve")&&($currency=="AUD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=eth&secondaryCurrencyCode=aud');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Independent Reserve")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=eth&secondaryCurrencyCode=usd');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Independent Reserve")&&($currency=="NZD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=eth&secondaryCurrencyCode=nzd');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Independent Reserve")&&($currency=="AUD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=ltc&secondaryCurrencyCode=aud');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Independent Reserve")&&($currency=="USD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=ltc&secondaryCurrencyCode=usd');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}
		
		if (($name=="Independent Reserve")&&($currency=="NZD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=ltc&secondaryCurrencyCode=nzd');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Independent Reserve")&&($currency=="AUD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=bch&secondaryCurrencyCode=aud');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Independent Reserve")&&($currency=="USD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=bch&secondaryCurrencyCode=usd');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}
		
		if (($name=="Independent Reserve")&&($currency=="NZD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.independentreserve.com/Public/GetMarketSummary?primaryCurrencyCode=bch&secondaryCurrencyCode=nzd');
			$independentreserve = json_decode($json);

			$bid        = $independentreserve->CurrentHighestBidPrice;
			$ask        = $independentreserve->CurrentLowestOfferPrice;
			$volume     = $independentreserve->DayVolumeXbt;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}



		if (($name=="CoinSpot")&&($currency=="AUD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://www.coinspot.com.au/pubapi/latest');
			$coinspot = json_decode($json, true);

			$bid        = $coinspot['prices']['btc']['bid'];
			$ask        = $coinspot['prices']['btc']['ask'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="CoinSpot")&&($currency=="AUD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://www.coinspot.com.au/pubapi/latest');
			$coinspot = json_decode($json, true);

			$bid        = $coinspot['prices']['ltc']['bid'];
			$ask        = $coinspot['prices']['ltc']['ask'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="CoinSpot")&&($currency=="AUD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://www.coinspot.com.au/pubapi/latest');
			$coinspot = json_decode($json, true);

			$bid        = $coinspot['prices']['eth']['bid'];
			$ask        = $coinspot['prices']['eth']['ask'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}
		if (($name=="Unodax")&&($currency=="INR")&&($crypto=="ALL"))
		{
			$json = curl_download('https://api.unocoin.com/api/exchange/unodax-ticker');
			$unocoin = json_decode($json, true);

			$crypto="BTC";

			$bid        = $unocoin['prices']['inr']['BTC'];
			$ask        = $unocoin['prices']['inr']['BTC'];
			$volume     = $unocoin['stats']['inr']['BTC']['vol_24hrs'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $crypto);


			$crypto="ETH";

			$bid        = $unocoin['prices']['inr']['ETH'];
			$ask        = $unocoin['prices']['inr']['ETH'];
			$volume     = $unocoin['stats']['inr']['ETH']['vol_24hrs'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $crypto);


			$crypto="LTC";

			$bid        = $unocoin['prices']['inr']['LTC'];
			$ask        = $unocoin['prices']['inr']['LTC'];
			$volume     = $unocoin['stats']['inr']['LTC']['vol_24hrs'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $crypto);

			$crypto="BCH";

			$bid        = $unocoin['prices']['inr']['BCH'];
			$ask        = $unocoin['prices']['inr']['BCH'];
			$volume     = $unocoin['stats']['inr']['BCH']['vol_24hrs'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $crypto);

			$crypto="XRP";

			$bid        = $unocoin['prices']['inr']['XRP'];
			$ask        = $unocoin['prices']['inr']['XRP'];
			$volume     = $unocoin['stats']['inr']['XRP']['vol_24hrs'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $crypto);
		}




		if (($name=="Bitstamp")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://www.bitstamp.net/api/v2/ticker/btcusd/');
			$bitstamp = json_decode($json, true);

			$bid        = $bitstamp['bid'];
			$ask        = $bitstamp['ask'];
			$volume     = $bitstamp['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Bitstamp")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://www.bitstamp.net/api/v2/ticker/btceur/');
			$bitstamp = json_decode($json, true);

			$bid        = $bitstamp['bid'];
			$ask        = $bitstamp['ask'];
			$volume     = $bitstamp['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitstamp")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://www.bitstamp.net/api/v2/ticker/ethusd/');
			$bitstamp = json_decode($json, true);

			$bid        = $bitstamp['bid'];
			$ask        = $bitstamp['ask'];
			$volume     = $bitstamp['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitstamp")&&($currency=="EUR")&&($crypto=="ETH"))
		{
			$json = curl_download('https://www.bitstamp.net/api/v2/ticker/etheur/');
			$bitstamp = json_decode($json, true);

			$bid        = $bitstamp['bid'];
			$ask        = $bitstamp['ask'];
			$volume     = $bitstamp['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitstamp")&&($currency=="USD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://www.bitstamp.net/api/v2/ticker/ltcusd/');
			$bitstamp = json_decode($json, true);

			$bid        = $bitstamp['bid'];
			$ask        = $bitstamp['ask'];
			$volume     = $bitstamp['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitstamp")&&($currency=="EUR")&&($crypto=="LTC"))
		{
			$json = curl_download('https://www.bitstamp.net/api/v2/ticker/ltceur/');
			$bitstamp = json_decode($json, true);

			$bid        = $bitstamp['bid'];
			$ask        = $bitstamp['ask'];
			$volume     = $bitstamp['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitstamp")&&($currency=="USD")&&($crypto=="XRP"))
		{
			$json = curl_download('https://www.bitstamp.net/api/v2/ticker/xrpusd/');
			$bitstamp = json_decode($json, true);

			$bid        = $bitstamp['bid'];
			$ask        = $bitstamp['ask'];
			$volume     = $bitstamp['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitstamp")&&($currency=="EUR")&&($crypto=="XRP"))
		{
			$json = curl_download('https://www.bitstamp.net/api/v2/ticker/xrpeur/');
			$bitstamp = json_decode($json, true);

			$bid        = $bitstamp['bid'];
			$ask        = $bitstamp['ask'];
			$volume     = $bitstamp['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


/*
		if (($name=="OKCoin.cn")&&($currency=="CNY")&&($crypto=="BCH"))
		{
			$json = curl_download('https://www.okcoin.cn/api/v1/ticker.do?symbol=bcc_cny');
			$okcoin = json_decode($json, true);

			$bid        = $okcoin['ticker']['buy'];
			$ask        = $okcoin['ticker']['sell'];
			$volume     = $okcoin['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}
*/

		if (($name=="BTCC")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://spotusd-data.btcc.com/data/pro/ticker?symbol=BTCUSD');
			$btcc = json_decode($json, true);

			$bid        = $btcc['ticker']['BidPrice'];
			$ask        = $btcc['ticker']['AskPrice'];
			$volume     = $btcc['ticker']['Volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTCC")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://spotusd-data.btcc.com/data/pro/ticker?symbol=ETHUSD');
			$btcc = json_decode($json, true);

			$bid        = $btcc['ticker']['BidPrice'];
			$ask        = $btcc['ticker']['AskPrice'];
			$volume     = $btcc['ticker']['Volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTCC")&&($currency=="USD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://spotusd-data.btcc.com/data/pro/ticker?symbol=BCHUSD');
			$btcc = json_decode($json, true);

			$bid        = $btcc['ticker']['BidPrice'];
			$ask        = $btcc['ticker']['AskPrice'];
			$volume     = $btcc['ticker']['Volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTCC")&&($currency=="USD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://spotusd-data.btcc.com/data/pro/ticker?symbol=LTCUSD');
			$btcc = json_decode($json, true);

			$bid        = $btcc['ticker']['BidPrice'];
			$ask        = $btcc['ticker']['AskPrice'];
			$volume     = $btcc['ticker']['Volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTCChina")&&($currency=="CNY")&&($crypto=="BTC"))
		{
			$json = curl_download('https://data.btcchina.com/data/ticker?market=btccny');
			$btcchina = json_decode($json, true);

			$bid        = $btcchina['ticker']['sell'];
			$ask        = $btcchina['ticker']['buy'];
			$volume     = $btcchina['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}




		if (($name=="BitX")&&($currency=="ZAR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.mybitx.com/api/1/ticker?pair=XBTZAR');
			$mybitx = json_decode($json, true);

			$name       = "BitX";
			$currency   = "ZAR";
			$bid        = $mybitx['bid'];
			$ask        = $mybitx['ask'];
			$volume     = $mybitx['rolling_24_hour_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BitX")&&($currency=="MYR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.mybitx.com/api/1/ticker?pair=XBTMYR');
			$mybitx = json_decode($json, true);

			$bid        = $mybitx['bid'];
			$ask        = $mybitx['ask'];
			$volume     = $mybitx['rolling_24_hour_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="ICE3X")&&($currency=="ZAR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://ice3x.com/api/v1/stats/marketdepthbtcav');
			$ice3x = json_decode($json, true);

			$bid        = $ice3x['response']['entities']['0']['max_bid'];
			$ask        = $ice3x['response']['entities']['0']['min_ask'];
			$volume     = $ice3x['response']['entities']['0']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ICE3X")&&($currency=="ZAR")&&($crypto=="ETH"))
		{
			$json = curl_download('https://ice3x.com/api/v1/stats/marketdepthbtcav');
			$ice3x = json_decode($json, true);

			$bid        = $ice3x['response']['entities']['3']['max_bid'];
			$ask        = $ice3x['response']['entities']['3']['min_ask'];
			$volume     = $ice3x['response']['entities']['3']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ICE3X")&&($currency=="ZAR")&&($crypto=="BCH"))
		{
			$json = curl_download('https://ice3x.com/api/v1/stats/marketdepthbtcav');
			$ice3x = json_decode($json, true);

			$bid        = $ice3x['response']['entities']['7']['max_bid'];
			$ask        = $ice3x['response']['entities']['7']['min_ask'];
			$volume     = $ice3x['response']['entities']['7']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ICE3X")&&($currency=="ZAR")&&($crypto=="LTC"))
		{
			$json = curl_download('https://ice3x.com/api/v1/stats/marketdepthbtcav');
			$ice3x = json_decode($json, true);

			$bid        = $ice3x['response']['entities']['2']['max_bid'];
			$ask        = $ice3x['response']['entities']['2']['min_ask'];
			$volume     = $ice3x['response']['entities']['2']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}
		if (($name=="Bitso")&&($currency=="MXN")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.bitso.com/v2/ticker?book=btc_mxn');
			$bitso = json_decode($json);

			$bid        = $bitso->bid;
			$ask        = $bitso->ask;
			$volume     = $bitso->volume;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}




		if (($name=="Bitso")&&($currency=="MXN")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.bitso.com/v2/ticker?book=btc_mxn');
			$bitso = json_decode($json);

			$bid        = $bitso->bid;
			$ask        = $bitso->ask;
			$volume     = $bitso->volume;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitso")&&($currency=="MXN")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.bitso.com/v2/ticker?book=xrp_mxn');
			$bitso = json_decode($json);

			$bid        = $bitso->bid;
			$ask        = $bitso->ask;
			$volume     = $bitso->volume;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitso")&&($currency=="MXN")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.bitso.com/v2/ticker?book=ltc_mxn');
			$bitso = json_decode($json);

			$bid        = $bitso->bid;
			$ask        = $bitso->ask;
			$volume     = $bitso->volume;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitso")&&($currency=="MXN")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.bitso.com/v2/ticker?book=eth_mxn');
			$bitso = json_decode($json);

			$bid        = $bitso->bid;
			$ask        = $bitso->ask;
			$volume     = $bitso->volume;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitso")&&($currency=="MXN")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.bitso.com/v2/ticker?book=bch_mxn');
			$bitso = json_decode($json);

			$bid        = $bitso->bid;
			$ask        = $bitso->ask;
			$volume     = $bitso->volume;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Paymium")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://paymium.com/api/v1/data/eur/ticker');
			$paymium = json_decode($json);

			$bid        = $paymium->bid;
			$ask        = $paymium->ask;
			$volume     = $paymium->volume;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="coins.ph")&&($currency=="ALL")&&($crypto=="BTC"))
		{
			$json = curl_download('https://quote.coins.ph/v1/markets');
			$coinsph = json_decode($json, true);

			foreach ($coinsph['markets'] as $key => $markets)
			{
				$crypto="BTC";
				$currency = "CLP";

				if ($markets['symbol']=="BTC-CLP")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}

				$currency = "HKD";

				if ($markets['symbol']=="BTC-HKD")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}

				$currency = "IDR";

				if ($markets['symbol']=="BTC-IDR")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}

				$currency = "MYR";

				if ($markets['symbol']=="BTC-MYR")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}

				$currency = "THB";

				if ($markets['symbol']=="BTC-THB")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}

				$currency = "TWD";

				if ($markets['symbol']=="BTC-TWD")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}

				$currency = "USD";

				if ($markets['symbol']=="BTC-USD")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}

				$currency = "VND";

				if ($markets['symbol']=="BTC-VND")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}


				$currency = "PHP";

				if ($markets['symbol']=="BTC-PHP")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}




				$crypto = "BCH";

				if ($markets['symbol']=="BCH-PHP")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}

				$crypto = "ETH";

				if ($markets['symbol']=="ETH-PHP")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}

				$crypto = "XRP";

				if ($markets['symbol']=="XRP-PHP")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}


				$crypto = "LTC";

				if ($markets['symbol']=="LTC-PHP")
				{
					$bid        = $coinsph['markets'][$key]['bid'];
					$ask        = $coinsph['markets'][$key]['ask'];
					$volume     = "N/A";

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}






			}
		}

		if (($name=="Bitcoin Exchange Thailand")&&($currency=="THB")&&($crypto=="ALL"))
		{
			$crypto = "BTC";
			$json = curl_download('https://bx.in.th/api/');
			$bxinth = json_decode($json, true);

			$bid        = $bxinth['1']['orderbook']['bids']['highbid'];
			$ask        = $bxinth['1']['orderbook']['asks']['highbid'];
			$volume     = $bxinth['1']['volume_24hours'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $crypto);


			$crypto = "ETH";

			$bid        = $bxinth['21']['orderbook']['bids']['highbid'];
			$ask        = $bxinth['21']['orderbook']['asks']['highbid'];
			$volume     = $bxinth['21']['volume_24hours'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $crypto);

			$crypto = "XRP";

			$bid        = $bxinth['25']['orderbook']['bids']['highbid'];
			$ask        = $bxinth['25']['orderbook']['asks']['highbid'];
			$volume     = $bxinth['25']['volume_24hours'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $crypto);

			$crypto = "BCH";

			$bid        = $bxinth['27']['orderbook']['bids']['highbid'];
			$ask        = $bxinth['27']['orderbook']['asks']['highbid'];
			$volume     = $bxinth['27']['volume_24hours'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $crypto);

			$crypto = "LTC";

			$bid        = $bxinth['30']['orderbook']['bids']['highbid'];
			$ask        = $bxinth['30']['orderbook']['asks']['highbid'];
			$volume     = $bxinth['30']['volume_24hours'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $crypto);
		}





		if (($name=="Bter.com")&&($currency=="CNY")&&($crypto=="BTC"))
		{
			$json = curl_download('http://data.bter.com/api/1/ticker/btc_cny');
			$bterCNY = json_decode($json, true);

			$bid        = $bterCNY['buy'];
			$ask        = $bterCNY['sell'];
			$volume     = $bterCNY['vol_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}





		if (($name=="coinfloor")&&($currency=="GBP")&&($crypto=="BTC"))
		{
			$json = curl_download('https://webapi.coinfloor.co.uk:8090/bist/XBT/GBP/ticker/');
			$coinfloor = json_decode($json, true);

			$bid        = $coinfloor['bid'];
			$ask        = $coinfloor['ask'];
			$volume     = $coinfloor['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="coinfloor")&&($currency=="GBP")&&($crypto=="BCH"))
		{
			$json = curl_download('https://webapi.coinfloor.co.uk:8090/bist/BCH/GBP/ticker/');
			$coinfloor = json_decode($json, true);

			$bid        = $coinfloor['bid'];
			$ask        = $coinfloor['ask'];
			$volume     = $coinfloor['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Kraken")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=XXBTZUSD');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['XXBTZUSD']['b'][0];
			$ask        = $kraken['result']['XXBTZUSD']['a'][0];
			$volume     = $kraken['result']['XXBTZUSD']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Kraken")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=XXBTZEUR');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['XXBTZEUR']['b'][0];
			$ask        = $kraken['result']['XXBTZEUR']['a'][0];
			$volume     = $kraken['result']['XXBTZEUR']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}





		if (($name=="Kraken")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=XETHZUSD');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['XETHZUSD']['b'][0];
			$ask        = $kraken['result']['XETHZUSD']['a'][0];
			$volume     = $kraken['result']['XETHZUSD']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Kraken")&&($currency=="EUR")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=XETHZEUR');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['XETHZEUR']['b'][0];
			$ask        = $kraken['result']['XETHZEUR']['a'][0];
			$volume     = $kraken['result']['XETHZEUR']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Kraken")&&($currency=="JPY")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=XETHZJPY');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['XETHZJPY']['b'][0];
			$ask        = $kraken['result']['XETHZJPY']['a'][0];
			$volume     = $kraken['result']['XETHZJPY']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Kraken")&&($currency=="GBP")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=XETHZGBP');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['XETHZGBP']['b'][0];
			$ask        = $kraken['result']['XETHZGBP']['a'][0];
			$volume     = $kraken['result']['XETHZGBP']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Kraken")&&($currency=="USD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=BCHUSD');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['BCHUSD']['b'][0];
			$ask        = $kraken['result']['BCHUSD']['a'][0];
			$volume     = $kraken['result']['BCHUSD']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Kraken")&&($currency=="EUR")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=BCHEUR');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['BCHEUR']['b'][0];
			$ask        = $kraken['result']['BCHEUR']['a'][0];
			$volume     = $kraken['result']['BCHEUR']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Kraken")&&($currency=="USD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=XLTCZUSD');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['XLTCZUSD']['b'][0];
			$ask        = $kraken['result']['XLTCZUSD']['a'][0];
			$volume     = $kraken['result']['XLTCZUSD']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Kraken")&&($currency=="EUR")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=XLTCZEUR');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['XLTCZEUR']['b'][0];
			$ask        = $kraken['result']['XLTCZEUR']['a'][0];
			$volume     = $kraken['result']['XLTCZEUR']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}



		if (($name=="Kraken")&&($currency=="USD")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=XXRPZUSD');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['XXRPZUSD']['b'][0];
			$ask        = $kraken['result']['XXRPZUSD']['a'][0];
			$volume     = $kraken['result']['XXRPZUSD']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Kraken")&&($currency=="EUR")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.kraken.com/0/public/Ticker?pair=XXRPZEUR');
			$kraken = json_decode($json, true);

			$bid        = $kraken['result']['XXRPZEUR']['b'][0];
			$ask        = $kraken['result']['XXRPZEUR']['a'][0];
			$volume     = $kraken['result']['XXRPZEUR']['v'][1];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Bithumb")&&($currency=="KRW")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.bithumb.com/public/ticker/eth');
			$bithumb = json_decode($json, true);

			$bid        = $bithumb['data']['sell_price'];
			$ask        = $bithumb['data']['buy_price'];
			$volume     = $bithumb['data']['volume_1day'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bithumb")&&($currency=="KRW")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.bithumb.com/public/ticker/btc');
			$bithumb = json_decode($json, true);

			$bid        = $bithumb['data']['sell_price'];
			$ask        = $bithumb['data']['buy_price'];
			$volume     = $bithumb['data']['volume_1day'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bithumb")&&($currency=="KRW")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.bithumb.com/public/ticker/bch');
			$bithumb = json_decode($json, true);

			$bid        = $bithumb['data']['sell_price'];
			$ask        = $bithumb['data']['buy_price'];
			$volume     = $bithumb['data']['volume_1day'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}
		if (($name=="Bithumb")&&($currency=="KRW")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.bithumb.com/public/ticker/xrp');
			$bithumb = json_decode($json, true);

			$bid        = $bithumb['data']['sell_price'];
			$ask        = $bithumb['data']['buy_price'];
			$volume     = $bithumb['data']['volume_1day'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bithumb")&&($currency=="KRW")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.bithumb.com/public/ticker/ltc');
			$bithumb = json_decode($json, true);

			$bid        = $bithumb['data']['sell_price'];
			$ask        = $bithumb['data']['buy_price'];
			$volume     = $bithumb['data']['volume_1day'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}



		if (($name=="Bithumb")&&($currency=="KRW")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.bithumb.com/public/ticker/xrp');
			$bithumb = json_decode($json, true);

			$bid        = $bithumb['data']['sell_price'];
			$ask        = $bithumb['data']['buy_price'];
			$volume     = $bithumb['data']['volume_1day'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bithumb")&&($currency=="KRW")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.bithumb.com/public/ticker/ltc');
			$bithumb = json_decode($json, true);

			$bid        = $bithumb['data']['sell_price'];
			$ask        = $bithumb['data']['buy_price'];
			$volume     = $bithumb['data']['volume_1day'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bithumb")&&($currency=="KRW")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.bithumb.com/public/ticker/bch');
			$bithumb = json_decode($json, true);

			$bid        = $bithumb['data']['sell_price'];
			$ask        = $bithumb['data']['buy_price'];
			$volume     = $bithumb['data']['volume_1day'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinone")&&($currency=="KRW")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.coinone.co.kr/ticker/?format=json&currency=btc');
			$kraken = json_decode($json, true);

			$bid        = $kraken['last'];
			$ask        = $kraken['last'];
			$volume     = $kraken['yesterday_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinone")&&($currency=="KRW")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.coinone.co.kr/ticker/?format=json&currency=eth');
			$kraken = json_decode($json, true);

			$bid        = $kraken['last'];
			$ask        = $kraken['last'];
			$volume     = $kraken['yesterday_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Coinone")&&($currency=="KRW")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.coinone.co.kr/ticker/?format=json&currency=xrp');
			$kraken = json_decode($json, true);

			$bid        = $kraken['last'];
			$ask        = $kraken['last'];
			$volume     = $kraken['yesterday_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinone")&&($currency=="KRW")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.coinone.co.kr/ticker/?format=json&currency=bch');
			$kraken = json_decode($json, true);

			$bid        = $kraken['last'];
			$ask        = $kraken['last'];
			$volume     = $kraken['yesterday_volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}



		if (($name=="Coinify")&&($currency=="ALL")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.coinify.com/v3/rates');
			$coinify = json_decode($json, true);

			$currency = "AED";

			$bid        = $coinify['data']['AED']['sell'];
			$ask        = $coinify['data']['AED']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "AFN";

			$bid        = $coinify['data']['AFN']['sell'];
			$ask        = $coinify['data']['AFN']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "ALL";

			$bid        = $coinify['data']['ALL']['sell'];
			$ask        = $coinify['data']['ALL']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "AMD";
			
			$bid        = $coinify['data']['AMD']['sell'];
			$ask        = $coinify['data']['AMD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "ANG";

			$bid        = $coinify['data']['ANG']['sell'];
			$ask        = $coinify['data']['ANG']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "AOA";

			$bid        = $coinify['data']['AOA']['sell'];
			$ask        = $coinify['data']['AOA']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "ARS";

			$bid        = $coinify['data']['ARS']['sell'];
			$ask        = $coinify['data']['ARS']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "AUD";

			$bid        = $coinify['data']['AUD']['sell'];
			$ask        = $coinify['data']['AUD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "AWG";

			$bid        = $coinify['data']['AWG']['sell'];
			$ask        = $coinify['data']['AWG']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "AZN";

			$bid        = $coinify['data']['AZN']['sell'];
			$ask        = $coinify['data']['AZN']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BAM";

			$bid        = $coinify['data']['BAM']['sell'];
			$ask        = $coinify['data']['BAM']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BBD";

			$bid        = $coinify['data']['BBD']['sell'];
			$ask        = $coinify['data']['BBD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BDT";

			$bid        = $coinify['data']['BDT']['sell'];
			$ask        = $coinify['data']['BDT']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BGN";

			$bid        = $coinify['data']['BGN']['sell'];
			$ask        = $coinify['data']['BGN']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BHD";

			$bid        = $coinify['data']['BHD']['sell'];
			$ask        = $coinify['data']['BHD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BIF";

			$bid        = $coinify['data']['BIF']['sell'];
			$ask        = $coinify['data']['BIF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BMD";

			$bid        = $coinify['data']['BMD']['sell'];
			$ask        = $coinify['data']['BMD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BND";

			$bid        = $coinify['data']['BND']['sell'];
			$ask        = $coinify['data']['BND']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BOB";

			$bid        = $coinify['data']['BOB']['sell'];
			$ask        = $coinify['data']['BOB']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BRL";

			$bid        = $coinify['data']['BRL']['sell'];
			$ask        = $coinify['data']['BRL']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BSD";

			$bid        = $coinify['data']['BSD']['sell'];
			$ask        = $coinify['data']['BSD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);


			$currency = "BTN";

			$bid        = $coinify['data']['BTN']['sell'];
			$ask        = $coinify['data']['BTN']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BWP";

			$bid        = $coinify['data']['BWP']['sell'];
			$ask        = $coinify['data']['BWP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "BZD";

			$bid        = $coinify['data']['BZD']['sell'];
			$ask        = $coinify['data']['BZD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "CAD";

			$bid        = $coinify['data']['CAD']['sell'];
			$ask        = $coinify['data']['CAD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "CDF";

			$bid        = $coinify['data']['CDF']['sell'];
			$ask        = $coinify['data']['CDF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "CHF";

			$bid        = $coinify['data']['CHF']['sell'];
			$ask        = $coinify['data']['CHF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "CLF";

			$bid        = $coinify['data']['CLF']['sell'];
			$ask        = $coinify['data']['CLF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "CLP";

			$bid        = $coinify['data']['CLP']['sell'];
			$ask        = $coinify['data']['CLP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "CNY";

			$bid        = $coinify['data']['CNY']['sell'];
			$ask        = $coinify['data']['CNY']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "COP";

			$bid        = $coinify['data']['COP']['sell'];
			$ask        = $coinify['data']['COP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "CRC";

			$bid        = $coinify['data']['CRC']['sell'];
			$ask        = $coinify['data']['CRC']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);


			$currency = "CUP";

			$bid        = $coinify['data']['CUP']['sell'];
			$ask        = $coinify['data']['CUP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);


			$currency = "CVE";

			$bid        = $coinify['data']['CVE']['sell'];
			$ask        = $coinify['data']['CVE']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "CZK";

			$bid        = $coinify['data']['CZK']['sell'];
			$ask        = $coinify['data']['CZK']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "DJF";

			$bid        = $coinify['data']['DJF']['sell'];
			$ask        = $coinify['data']['DJF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);


			$currency = "DKK";

			$bid        = $coinify['data']['DKK']['sell'];
			$ask        = $coinify['data']['DKK']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);


			$currency = "DOP";

			$bid        = $coinify['data']['DOP']['sell'];
			$ask        = $coinify['data']['DOP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "DZD";

			$bid        = $coinify['data']['DZD']['sell'];
			$ask        = $coinify['data']['DZD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);


			$currency = "EGP";

			$bid        = $coinify['data']['EGP']['sell'];
			$ask        = $coinify['data']['EGP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "ERN";

			$bid        = $coinify['data']['ERN']['sell'];
			$ask        = $coinify['data']['ERN']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "ETB";

			$bid        = $coinify['data']['ETB']['sell'];
			$ask        = $coinify['data']['ETB']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "EUR";

			$bid        = $coinify['data']['EUR']['sell'];
			$ask        = $coinify['data']['EUR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "FJD";

			$bid        = $coinify['data']['FJD']['sell'];
			$ask        = $coinify['data']['FJD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "FKP";

			$bid        = $coinify['data']['FKP']['sell'];
			$ask        = $coinify['data']['FKP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "GBP";

			$bid        = $coinify['data']['GBP']['sell'];
			$ask        = $coinify['data']['GBP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "GEL";

			$bid        = $coinify['data']['GEL']['sell'];
			$ask        = $coinify['data']['GEL']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);


			$currency = "GHS";

			$bid        = $coinify['data']['GHS']['sell'];
			$ask        = $coinify['data']['GHS']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);


			$currency = "GIP";

			$bid        = $coinify['data']['GIP']['sell'];
			$ask        = $coinify['data']['GIP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "GMD";

			$bid        = $coinify['data']['GMD']['sell'];
			$ask        = $coinify['data']['GMD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "GNF";

			$bid        = $coinify['data']['GNF']['sell'];
			$ask        = $coinify['data']['GNF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "GTQ";

			$bid        = $coinify['data']['GTQ']['sell'];
			$ask        = $coinify['data']['GTQ']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "GYD";

			$bid        = $coinify['data']['GYD']['sell'];
			$ask        = $coinify['data']['GYD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "HKD";

			$bid        = $coinify['data']['HKD']['sell'];
			$ask        = $coinify['data']['HKD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "HNL";

			$bid        = $coinify['data']['HNL']['sell'];
			$ask        = $coinify['data']['HNL']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "HRK";

			$bid        = $coinify['data']['HRK']['sell'];
			$ask        = $coinify['data']['HRK']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);


			$currency = "HTG";

			$bid        = $coinify['data']['HTG']['sell'];
			$ask        = $coinify['data']['HTG']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "HUF";

			$bid        = $coinify['data']['HUF']['sell'];
			$ask        = $coinify['data']['HUF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "IDR";

			$bid        = $coinify['data']['IDR']['sell'];
			$ask        = $coinify['data']['IDR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "ILS";

			$bid        = $coinify['data']['ILS']['sell'];
			$ask        = $coinify['data']['ILS']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "INR";

			$bid        = $coinify['data']['INR']['sell'];
			$ask        = $coinify['data']['INR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "IQD";

			$bid        = $coinify['data']['IQD']['sell'];
			$ask        = $coinify['data']['IQD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "IRR";

			$bid        = $coinify['data']['IRR']['sell'];
			$ask        = $coinify['data']['IRR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "ISK";

			$bid        = $coinify['data']['ISK']['sell'];
			$ask        = $coinify['data']['ISK']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "JEP";

			$bid        = $coinify['data']['JEP']['sell'];
			$ask        = $coinify['data']['JEP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "JMD";

			$bid        = $coinify['data']['JMD']['sell'];
			$ask        = $coinify['data']['JMD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "JOD";

			$bid        = $coinify['data']['JOD']['sell'];
			$ask        = $coinify['data']['JOD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "JPY";

			$bid        = $coinify['data']['JPY']['sell'];
			$ask        = $coinify['data']['JPY']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "KES";

			$bid        = $coinify['data']['KES']['sell'];
			$ask        = $coinify['data']['KES']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "KGS";

			$bid        = $coinify['data']['KGS']['sell'];
			$ask        = $coinify['data']['KGS']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "KHR";

			$bid        = $coinify['data']['KHR']['sell'];
			$ask        = $coinify['data']['KHR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "KMF";

			$bid        = $coinify['data']['KMF']['sell'];
			$ask        = $coinify['data']['KMF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "KPW";

			$bid        = $coinify['data']['KPW']['sell'];
			$ask        = $coinify['data']['KPW']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "KRW";

			$bid        = $coinify['data']['KRW']['sell'];
			$ask        = $coinify['data']['KRW']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "KWD";

			$bid        = $coinify['data']['KWD']['sell'];
			$ask        = $coinify['data']['KWD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "KYD";

			$bid        = $coinify['data']['KYD']['sell'];
			$ask        = $coinify['data']['KYD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "KZT";

			$bid        = $coinify['data']['KZT']['sell'];
			$ask        = $coinify['data']['KZT']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "LAK";

			$bid        = $coinify['data']['LAK']['sell'];
			$ask        = $coinify['data']['LAK']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "LBP";

			$bid        = $coinify['data']['LBP']['sell'];
			$ask        = $coinify['data']['LBP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "LKR";

			$bid        = $coinify['data']['LKR']['sell'];
			$ask        = $coinify['data']['LKR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "LRD";

			$bid        = $coinify['data']['LRD']['sell'];
			$ask        = $coinify['data']['LRD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "LSL";

			$bid        = $coinify['data']['LSL']['sell'];
			$ask        = $coinify['data']['LSL']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "LYD";


			$bid        = $coinify['data']['LYD']['sell'];
			$ask        = $coinify['data']['LYD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MAD";

			$bid        = $coinify['data']['MAD']['sell'];
			$ask        = $coinify['data']['MAD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MDL";

			$bid        = $coinify['data']['MDL']['sell'];
			$ask        = $coinify['data']['MDL']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MGA";

			$bid        = $coinify['data']['MGA']['sell'];
			$ask        = $coinify['data']['MGA']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MKD";

			$bid        = $coinify['data']['MKD']['sell'];
			$ask        = $coinify['data']['MKD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MMK";

			$bid        = $coinify['data']['MMK']['sell'];
			$ask        = $coinify['data']['MMK']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MNT";

			$bid        = $coinify['data']['MNT']['sell'];
			$ask        = $coinify['data']['MNT']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MOP";

			$bid        = $coinify['data']['MOP']['sell'];
			$ask        = $coinify['data']['MOP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MRO";

			$bid        = $coinify['data']['MRO']['sell'];
			$ask        = $coinify['data']['MRO']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MUR";

			$bid        = $coinify['data']['MUR']['sell'];
			$ask        = $coinify['data']['MUR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MVR";

			$bid        = $coinify['data']['MVR']['sell'];
			$ask        = $coinify['data']['MVR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MWK";

			$bid        = $coinify['data']['MWK']['sell'];
			$ask        = $coinify['data']['MWK']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MXN";

			$bid        = $coinify['data']['MXN']['sell'];
			$ask        = $coinify['data']['MXN']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MYR";

			$bid        = $coinify['data']['MYR']['sell'];
			$ask        = $coinify['data']['MYR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "MZN";

			$bid        = $coinify['data']['MZN']['sell'];
			$ask        = $coinify['data']['MZN']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "NAD";

			$bid        = $coinify['data']['NAD']['sell'];
			$ask        = $coinify['data']['NAD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "NGN";

			$bid        = $coinify['data']['NGN']['sell'];
			$ask        = $coinify['data']['NGN']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "NIO";

			$bid        = $coinify['data']['NIO']['sell'];
			$ask        = $coinify['data']['NIO']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "NOK";

			$bid        = $coinify['data']['NOK']['sell'];
			$ask        = $coinify['data']['NOK']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "NPR";

			$bid        = $coinify['data']['NPR']['sell'];
			$ask        = $coinify['data']['NPR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "NZD";

			$bid        = $coinify['data']['NZD']['sell'];
			$ask        = $coinify['data']['NZD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "OMR";

			$bid        = $coinify['data']['OMR']['sell'];
			$ask        = $coinify['data']['OMR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "PAB";

			$bid        = $coinify['data']['PAB']['sell'];
			$ask        = $coinify['data']['PAB']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "PEN";

			$bid        = $coinify['data']['PEN']['sell'];
			$ask        = $coinify['data']['PEN']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "PGK";

			$bid        = $coinify['data']['PGK']['sell'];
			$ask        = $coinify['data']['PGK']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "PHP";

			$bid        = $coinify['data']['PHP']['sell'];
			$ask        = $coinify['data']['PHP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "PKR";
	
			$bid        = $coinify['data']['PKR']['sell'];
			$ask        = $coinify['data']['PKR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "PLN";

			$bid        = $coinify['data']['PLN']['sell'];
			$ask        = $coinify['data']['PLN']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "PYG";

			$bid        = $coinify['data']['PYG']['sell'];
			$ask        = $coinify['data']['PYG']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "QAR";
	
			$bid        = $coinify['data']['QAR']['sell'];
			$ask        = $coinify['data']['QAR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "RON";

			$bid        = $coinify['data']['RON']['sell'];
			$ask        = $coinify['data']['RON']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "RSD";

			$bid        = $coinify['data']['RSD']['sell'];
			$ask        = $coinify['data']['RSD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "RUB";

			$bid        = $coinify['data']['RUB']['sell'];
			$ask        = $coinify['data']['RUB']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "RWF";

			$bid        = $coinify['data']['RWF']['sell'];
			$ask        = $coinify['data']['RWF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SAR";

			$bid        = $coinify['data']['SAR']['sell'];
			$ask        = $coinify['data']['SAR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SBD";

			$bid        = $coinify['data']['SBD']['sell'];
			$ask        = $coinify['data']['SBD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SCR";

			$bid        = $coinify['data']['SCR']['sell'];
			$ask        = $coinify['data']['SCR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SDG";

			$bid        = $coinify['data']['SDG']['sell'];
			$ask        = $coinify['data']['SDG']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SHP";

			$bid        = $coinify['data']['SHP']['sell'];
			$ask        = $coinify['data']['SHP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SLL";

			$bid        = $coinify['data']['SLL']['sell'];
			$ask        = $coinify['data']['SLL']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SEK";

			$bid        = $coinify['data']['SEK']['sell'];
			$ask        = $coinify['data']['SEK']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SGD";

			$bid        = $coinify['data']['SGD']['sell'];
			$ask        = $coinify['data']['SGD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SOS";

			$bid        = $coinify['data']['SOS']['sell'];
			$ask        = $coinify['data']['SOS']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SRD";

			$bid        = $coinify['data']['SRD']['sell'];
			$ask        = $coinify['data']['SRD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
			
			$currency = "STD";

			$bid        = $coinify['data']['STD']['sell'];
			$ask        = $coinify['data']['STD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SVC";

			$bid        = $coinify['data']['SVC']['sell'];
			$ask        = $coinify['data']['SVC']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SYP";

			$bid        = $coinify['data']['SYP']['sell'];
			$ask        = $coinify['data']['SYP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "SZL";

			$bid        = $coinify['data']['SZL']['sell'];
			$ask        = $coinify['data']['SZL']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "THB";

			$bid        = $coinify['data']['THB']['sell'];
			$ask        = $coinify['data']['THB']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "TJS";

			$bid        = $coinify['data']['TJS']['sell'];
			$ask        = $coinify['data']['TJS']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "TMT";

			$bid        = $coinify['data']['TMT']['sell'];
			$ask        = $coinify['data']['TMT']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "TND";

			$bid        = $coinify['data']['TND']['sell'];
			$ask        = $coinify['data']['TND']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "TOP";

			$bid        = $coinify['data']['TOP']['sell'];
			$ask        = $coinify['data']['TOP']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "TRY";

			$bid        = $coinify['data']['TRY']['sell'];
			$ask        = $coinify['data']['TRY']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "TTD";

			$bid        = $coinify['data']['TTD']['sell'];
			$ask        = $coinify['data']['TTD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "TWD";

			$bid        = $coinify['data']['TWD']['sell'];
			$ask        = $coinify['data']['TWD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "TZS";

			$bid        = $coinify['data']['TZS']['sell'];
			$ask        = $coinify['data']['TZS']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "UAH";

			$bid        = $coinify['data']['UAH']['sell'];
			$ask        = $coinify['data']['UAH']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "UGX";

			$bid        = $coinify['data']['UGX']['sell'];
			$ask        = $coinify['data']['UGX']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "USD";

			$bid        = $coinify['data']['USD']['sell'];
			$ask        = $coinify['data']['USD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "UYU";

			$bid        = $coinify['data']['UYU']['sell'];
			$ask        = $coinify['data']['UYU']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "UZS";

			$bid        = $coinify['data']['UZS']['sell'];
			$ask        = $coinify['data']['UZS']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "VEF";

			$bid        = $coinify['data']['VEF']['sell'];
			$ask        = $coinify['data']['VEF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "VND";

			$bid        = $coinify['data']['VND']['sell'];
			$ask        = $coinify['data']['VND']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "VUV";

			$bid        = $coinify['data']['VUV']['sell'];
			$ask        = $coinify['data']['VUV']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "WST";

			$bid        = $coinify['data']['WST']['sell'];
			$ask        = $coinify['data']['WST']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "XAF";

			$bid        = $coinify['data']['XAF']['sell'];
			$ask        = $coinify['data']['XAF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "XAG";

			$bid        = $coinify['data']['XAG']['sell'];
			$ask        = $coinify['data']['XAG']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "XAU";

			$bid        = $coinify['data']['XAU']['sell'];
			$ask        = $coinify['data']['XAU']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "XCD";

			$bid        = $coinify['data']['XCD']['sell'];
			$ask        = $coinify['data']['XCD']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "XDR";


			$bid        = $coinify['data']['XDR']['sell'];
			$ask        = $coinify['data']['XDR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "XOF";


			$bid        = $coinify['data']['XOF']['sell'];
			$ask        = $coinify['data']['XOF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "XPF";


			$bid        = $coinify['data']['XPF']['sell'];
			$ask        = $coinify['data']['XPF']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "YER";

			$bid        = $coinify['data']['YER']['sell'];
			$ask        = $coinify['data']['YER']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "ZAR";

			$bid        = $coinify['data']['ZAR']['sell'];
			$ask        = $coinify['data']['ZAR']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "ZMW";

			$bid        = $coinify['data']['ZMW']['sell'];
			$ask        = $coinify['data']['ZMW']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);

			$currency = "ZWL";


			$bid        = $coinify['data']['ZWL']['sell'];
			$ask        = $coinify['data']['ZWL']['buy'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="The Rock")&&($currency=="EUR")&&($crypto=="ALL"))
		{
			$crypto = "BTC";
			$json = curl_download('https://www.therocktrading.com/api/tickers/');
			$therock = json_decode($json, true);

			$bid        = $therock['result']['tickers']['BTCEUR']['bid'];
			$ask        = $therock['result']['tickers']['BTCEUR']['ask'];
			$volume     = $therock['result']['tickers']['BTCEUR']['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			    unset($bid, $ask, $volume, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			

			$crypto = "ETH";

			$bid        = $therock['result']['tickers']['ETHEUR']['bid'];
			$ask        = $therock['result']['tickers']['ETHEUR']['ask'];
			$volume     = $therock['result']['tickers']['ETHEUR']['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			    unset($bid, $ask, $volume, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			

			$crypto = "LTC";

			$bid        = $therock['result']['tickers']['LTCEUR']['bid'];
			$ask        = $therock['result']['tickers']['LTCEUR']['ask'];
			$volume     = $therock['result']['tickers']['LTCEUR']['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			    unset($bid, $ask, $volume, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			/*

			$crypto = "XRP";

			$bid        = $therock['result']['tickers']['EURXRP']['bid'];
			$ask        = $therock['result']['tickers']['EURXRP']['ask'];
			$volume     = $therock['result']['tickers']['EURXRP']['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			    unset($bid, $ask, $volume, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			*/

			$crypto = "BCH";

			$bid        = $therock['result']['tickers']['BCHEUR']['bid'];
			$ask        = $therock['result']['tickers']['BCHEUR']['ask'];
			$volume     = $therock['result']['tickers']['BCHEUR']['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			    unset($bid, $ask, $volume, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}			




		}


		if (($name=="BITOK.COM")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.bitok.com/open_api/btc_usd/ticker');
			$bitok = json_decode($json, true);

			$bid        = $bitok['ticker']['sell'];
			$ask        = $bitok['ticker']['buy'];
			$volume     = $bitok['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BITOK.COM")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.bitok.com/open_api/btc_usd/ticker');
			$bitok = json_decode($json, true);

			$bid        = $bitok['ticker']['sell'];
			$ask        = $bitok['ticker']['buy'];
			$volume     = $bitok['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Quadriga CX")&&($currency=="CAD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.quadrigacx.com/v2/ticker?book=BTC_CAD');
			$quadriga = json_decode($json, true);

			$bid        = $quadriga['bid'];
			$ask        = $quadriga['ask'];
			$volume     = $quadriga['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Quadriga CX")&&($currency=="CAD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.quadrigacx.com/v2/ticker?book=BTC_CAD');
			$quadriga = json_decode($json, true);

			$bid        = $quadriga['bid'];
			$ask        = $quadriga['ask'];
			$volume     = $quadriga['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Quadriga CX")&&($currency=="CAD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.quadrigacx.com/v2/ticker?book=BCH_CAD');
			$quadriga = json_decode($json, true);

			$bid        = $quadriga['bid'];
			$ask        = $quadriga['ask'];
			$volume     = $quadriga['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Quadriga CX")&&($currency=="CAD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.quadrigacx.com/v2/ticker?book=LTC_CAD');
			$quadriga = json_decode($json, true);

			$bid        = $quadriga['bid'];
			$ask        = $quadriga['ask'];
			$volume     = $quadriga['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Quadriga CX")&&($currency=="CAD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.quadrigacx.com/v2/ticker?book=ETH_CAD');
			$quadriga = json_decode($json, true);

			$bid        = $quadriga['bid'];
			$ask        = $quadriga['ask'];
			$volume     = $quadriga['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}
		if (($name=="Quadriga CX")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.quadrigacx.com/v2/ticker?book=BTC_USD');
			$quadriga = json_decode($json, true);

			$bid        = $quadriga['bid'];
			$ask        = $quadriga['ask'];
			$volume     = $quadriga['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

/*
		if (($name=="Camp BX")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://campbx.com/api/xticker.php');
			$cbx = json_decode($json, true);

			$json = curl_download('http://api.bitcoincharts.com/v1/markets.json');
			$bitcoincharts = json_decode($json, true);

			$bid        = $cbx['Best Bid'];
			$ask        = $cbx['Best Ask'];
			$volume     = $bitcoincharts[104]['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}
*/

		if (($name=="FYB-SG")&&($currency=="SGD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://www.fybsg.com/api/SGD/tickerdetailed.json');
			$fyb = json_decode($json, true);

			$bid        = $fyb['bid'];
			$ask        = $fyb['ask'];
			$volume     = $fyb['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}




		if (($name=="BitMarket")&&($currency=="PLN")&&($crypto=="BTC"))
		{
			$json = curl_download('https://www.bitmarket.pl/json/BTCPLN/ticker.json');
			$bitmarket = json_decode($json, true);

			$bid        = $bitmarket['bid'];
			$ask        = $bitmarket['ask'];
			$volume     = $bitmarket['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BitMarket")&&($currency=="PLN")&&($crypto=="XRP"))
		{
			$json = curl_download('https://www.bitmarket.pl/json/XRPPLN/ticker.json');
			$bitmarket = json_decode($json, true);

			$bid        = $bitmarket['bid'];
			$ask        = $bitmarket['ask'];
			$volume     = $bitmarket['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BitMarket")&&($currency=="PLN")&&($crypto=="LTC"))
		{
			$json = curl_download('https://www.bitmarket.pl/json/LTCPLN/ticker.json');
			$bitmarket = json_decode($json, true);

			$bid        = $bitmarket['bid'];
			$ask        = $bitmarket['ask'];
			$volume     = $bitmarket['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BitMarket")&&($currency=="PLN")&&($crypto=="BCH"))
		{
			$json = curl_download('https://www.bitmarket.pl/json/BCCPLN/ticker.json');
			$bitmarket = json_decode($json, true);

			$bid        = $bitmarket['bid'];
			$ask        = $bitmarket['ask'];
			$volume     = $bitmarket['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Korbit")&&($currency=="KRW")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=btc_krw');
			$korbit = json_decode($json, true);

			$bid        = $korbit['bid'];
			$ask        = $korbit['ask'];
			$volume     = $korbit['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Korbit")&&($currency=="KRW")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=eth_krw');
			$korbit = json_decode($json, true);

			$bid        = $korbit['bid'];
			$ask        = $korbit['ask'];
			$volume     = $korbit['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Korbit")&&($currency=="KRW")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=bch_krw');
			$korbit = json_decode($json, true);

			$bid        = $korbit['bid'];
			$ask        = $korbit['ask'];
			$volume     = $korbit['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Korbit")&&($currency=="KRW")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=xrp_krw');
			$korbit = json_decode($json, true);

			$bid        = $korbit['bid'];
			$ask        = $korbit['ask'];
			$volume     = $korbit['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}





		if (($name=="BitBay")&&($currency=="PLN")&&($crypto=="BTC"))
		{
			$json = curl_download('https://bitbay.net/API/Public/BTCPLN/ticker.json');
			$bitbay = json_decode($json, true);

			$bid        = $bitbay['bid'];
			$ask        = $bitbay['ask'];
			$volume     = $bitbay['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BitBay")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://bitbay.net/API/Public/BTCUSD/ticker.json');
			$bitbay = json_decode($json, true);

			$bid        = $bitbay['bid'];
			$ask        = $bitbay['ask'];
			$volume     = $bitbay['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BitBay")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://bitbay.net/API/Public/BTCEUR/ticker.json');
			$bitbay = json_decode($json, true);

			$bid        = $bitbay['bid'];
			$ask        = $bitbay['ask'];
			$volume     = $bitbay['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="BitBay")&&($currency=="PLN")&&($crypto=="LTC"))
		{
			$json = curl_download('https://bitbay.net/API/Public/LTCPLN/ticker.json');
			$bitbay = json_decode($json, true);

			$bid        = $bitbay['bid'];
			$ask        = $bitbay['ask'];
			$volume     = $bitbay['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="BitBay")&&($currency=="PLN")&&($crypto=="BCH"))
		{
			$json = curl_download('https://bitbay.net/API/Public/BCCPLN/ticker.json');
			$bitbay = json_decode($json, true);

			$bid        = $bitbay['bid'];
			$ask        = $bitbay['ask'];
			$volume     = $bitbay['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BitBay")&&($currency=="PLN")&&($crypto=="XRP"))
		{
			$json = curl_download('https://bitbay.net/API/Public/XRPPLN/ticker.json');
			$bitbay = json_decode($json, true);

			$bid        = $bitbay['bid'];
			$ask        = $bitbay['ask'];
			$volume     = $bitbay['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BitBay")&&($currency=="EUR")&&($crypto=="XRP"))
		{
			$json = curl_download('https://bitbay.net/API/Public/XRPEUR/ticker.json');
			$bitbay = json_decode($json, true);

			$bid        = $bitbay['bid'];
			$ask        = $bitbay['ask'];
			$volume     = $bitbay['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BitBay")&&($currency=="PLN")&&($crypto=="ETH"))
		{
			$json = curl_download('https://bitbay.net/API/Public/ETHPLN/ticker.json');
			$bitbay = json_decode($json, true);

			$bid        = $bitbay['bid'];
			$ask        = $bitbay['ask'];
			$volume     = $bitbay['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="BitBay")&&($currency=="EUR")&&($crypto=="ETH"))
		{
			$json = curl_download('https://bitbay.net/API/Public/ETHEUR/ticker.json');
			$bitbay = json_decode($json, true);

			$bid        = $bitbay['bid'];
			$ask        = $bitbay['ask'];
			$volume     = $bitbay['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if ($name=="LocalBitcoins")
		{
			$json = curl_download('https://localbitcoins.com/bitcoinaverage/ticker-all-currencies/');
			$localbitcoins_stats = json_decode($json, true);

			if ($localbitcoins_stats == "")
			{
				$json = curl_download('https://localbitcoins.com/bitcoinaverage/ticker-all-currencies/');
				$localbitcoins_stats = json_decode($json, true);
			}
			
		}

		if (($name=="LocalBitcoins")&&($currency=="USD")&&($crypto=="BTC"))
		{
			//$currency="USD";
			//$crypto="BTC";
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/usd/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			if ($localbitcoins == "")
			{
				$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/usd/c/bank-transfers/.json');
				$localbitcoins = json_decode($json, true);
			}

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/usd/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			if ($localbitcoinssell == "")
			{
				$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/usd/c/bank-transfers/.json');
				$localbitcoinssell = json_decode($json, true);
			}

			
			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];


			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="INR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/inr/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/inr/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="GBP")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/gbp/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/gbp/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="CAD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/cad/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/cad/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="MXN")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/mxn/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/mxn/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);


			if(is_array($localbitcoins) && is_array($localbitcoinssell))
			{
				$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
				$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
				$volume     = $localbitcoins_stats[$currency]['volume_btc'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="LocalBitcoins")&&($currency=="SEK")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/sek/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/sek/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			if(is_array($localbitcoins) && is_array($localbitcoinssell))
			{
				$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
				$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
				$volume     = $localbitcoins_stats[$currency]['volume_btc'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != ""))
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/eur/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/eur/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="ZAR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/zar/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/zar/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="RUB")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/rub/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/rub/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="NZD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/nzd/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/nzd/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="BRL")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/BRL/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/BRL/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="NOK")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/nok/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/nok/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="VEF")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/vef/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/vef/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="AUD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/aud/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/aud/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="RON")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/ron/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/ron/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="PEN")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/pen/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/pen/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="COP")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/cop/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/cop/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="MYR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/myr/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/myr/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="ARS")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/ars/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/ars/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="PHP")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/php/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/php/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="SGD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/sgd/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/sgd/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="CNY")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/cny/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/cny/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="THB")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/thb/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/thb/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="KES")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/kes/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/kes/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="LocalBitcoins")&&($currency=="CLP")&&($crypto=="BTC"))
		{
			$json = curl_download('https://localbitcoins.com/buy-bitcoins-online/clp/c/bank-transfers/.json');
			$localbitcoins = json_decode($json, true);

			$json = curl_download('https://localbitcoins.com/sell-bitcoins-online/clp/c/bank-transfers/.json');
			$localbitcoinssell = json_decode($json, true);

			$bid        = $localbitcoinssell['data']['ad_list'][0]['data']['temp_price'];
			$ask        = $localbitcoins['data']['ad_list'][0]['data']['temp_price'];
			$volume     = $localbitcoins_stats[$currency]['volume_btc'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="xBTCe")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://cryptottlivewebapi.xbtce.net:8443/api/v1/public/ticker/BTCEUR');
			$xbtce = json_decode($json, true);

			$bid        = $xbtce['0']['BestBid'];
			$ask        = $xbtce['0']['BestAsk'];
			$volume     = $xbtce['0']['DailyTradedTotalVolume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="xBTCe")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://cryptottlivewebapi.xbtce.net:8443/api/v1/public/ticker/BTCUSD');
			$xbtce = json_decode($json, true);

			$bid        = $xbtce['0']['BestBid'];
			$ask        = $xbtce['0']['BestAsk'];
			$volume     = $xbtce['0']['DailyTradedTotalVolume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="xBTCe")&&($currency=="RUB")&&($crypto=="BTC"))
		{
			$json = curl_download('https://cryptottlivewebapi.xbtce.net:8443/api/v1/public/ticker/BTCRUB');
			$xbtce = json_decode($json, true);

			$bid        = $xbtce['0']['BestBid'];
			$ask        = $xbtce['0']['BestAsk'];
			$volume     = $xbtce['0']['DailyTradedTotalVolume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Livecoin")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.livecoin.net/exchange/ticker?currencyPair=BTC/USD');
			$livecoin = json_decode($json, true);

			if (is_array($livecoin))
			{
				$bid        = $livecoin['best_bid'];
				$ask        = $livecoin['best_ask'];
				$volume     = $livecoin['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $livecoin);
		}

		if (($name=="Livecoin")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.livecoin.net/exchange/ticker?currencyPair=BTC/EUR');
			$livecoin = json_decode($json, true);

			if (is_array($livecoin))
			{
				$bid        = $livecoin['best_bid'];
				$ask        = $livecoin['best_ask'];
				$volume     = $livecoin['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $livecoin);
		}

		if (($name=="Livecoin")&&($currency=="RUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.livecoin.net/exchange/ticker?currencyPair=BTC/RUR');
			$livecoin = json_decode($json, true);

			if (is_array($livecoin))
			{
				$bid        = $livecoin['best_bid'];
				$ask        = $livecoin['best_ask'];
				$volume     = $livecoin['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $livecoin);
		}

		if (($name=="Livecoin")&&($currency=="RUR")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.livecoin.net/exchange/ticker?currencyPair=ETH/RUR');
			$livecoin = json_decode($json, true);

			if (is_array($livecoin))
			{
				$bid        = $livecoin['best_bid'];
				$ask        = $livecoin['best_ask'];
				$volume     = $livecoin['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $livecoin);		
		}

		if (($name=="Livecoin")&&($currency=="USD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.livecoin.net/exchange/ticker?currencyPair=LTC/USD');
			$livecoin = json_decode($json, true);

			if (is_array($livecoin))
			{
				$bid        = $livecoin['best_bid'];
				$ask        = $livecoin['best_ask'];
				$volume     = $livecoin['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $livecoin);
		}

		if (($name=="Allcoin")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.allcoin.com/api/v1/ticker');
			$allcoin = json_decode($json, true);

			$bid        = $allcoin['ticker']['buy'];
			$ask        = $allcoin['ticker']['sell'];
			$volume     = $allcoin['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Indodax")&&($currency=="IDR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://vip.bitcoin.co.id/api/summaries');
			$bci = json_decode($json, true);

			$json = curl_download('http://api.bitcoincharts.com/v1/markets.json');
			$bitcoincharts = json_decode($json, true);

			$bid        = $bci['tickers']['btc_idr']['buy'];
			$ask        = $bci['tickers']['btc_idr']['sell'];
			$volume     = $bitcoincharts[0]['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ANXPRO")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://anxpro.com/api/2/BTCUSD/money/ticker');
			$anx = json_decode($json, true);

			$bid        = $anx['data']['buy']['value'];
			$ask        = $anx['data']['sell']['value'];
			$volume     = $anx['data']['vol']['value'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ANXPRO")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://anxpro.com/api/2/BTCEUR/money/ticker');
			$anx = json_decode($json, true);

			$bid        = $anx['data']['buy']['value'];
			$ask        = $anx['data']['sell']['value'];
			$volume     = $anx['data']['vol']['value'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ANXPRO")&&($currency=="HKD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://anxpro.com/api/2/BTCHKD/money/ticker');
			$anx = json_decode($json, true);

			$bid        = $anx['data']['buy']['value'];
			$ask        = $anx['data']['sell']['value'];
			$volume     = $anx['data']['vol']['value'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ANXPRO")&&($currency=="AUD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://anxpro.com/api/2/BTCAUD/money/ticker');
			$anx = json_decode($json, true);

			$bid        = $anx['data']['buy']['value'];
			$ask        = $anx['data']['sell']['value'];
			$volume     = $anx['data']['vol']['value'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ANXPRO")&&($currency=="CAD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://anxpro.com/api/2/BTCCAD/money/ticker');
			$anx = json_decode($json, true);

			$bid        = $anx['data']['buy']['value'];
			$ask        = $anx['data']['sell']['value'];
			$volume     = $anx['data']['vol']['value'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ANXPRO")&&($currency=="JPY")&&($crypto=="BTC"))
		{
			$json = curl_download('https://anxpro.com/api/2/BTCJPY/money/ticker');
			$anx = json_decode($json, true);

			$bid        = $anx['data']['buy']['value'];
			$ask        = $anx['data']['sell']['value'];
			$volume     = $anx['data']['vol']['value'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ANXPRO")&&($currency=="SGD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://anxpro.com/api/2/BTCSGD/money/ticker');
			$anx = json_decode($json, true);

			$bid        = $anx['data']['buy']['value'];
			$ask        = $anx['data']['sell']['value'];
			$volume     = $anx['data']['vol']['value'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ANXPRO")&&($currency=="GBP")&&($crypto=="BTC"))
		{
			$json = curl_download('https://anxpro.com/api/2/BTCGBP/money/ticker');
			$anx = json_decode($json, true);

			$bid        = $anx['data']['buy']['value'];
			$ask        = $anx['data']['sell']['value'];
			$volume     = $anx['data']['vol']['value'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ANXPRO")&&($currency=="NZD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://anxpro.com/api/2/BTCNZD/money/ticker');
			$anx = json_decode($json, true);

			$bid        = $anx['data']['buy']['value'];
			$ask        = $anx['data']['sell']['value'];
			$volume     = $anx['data']['vol']['value'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}




		if (($name=="Coinapult")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.coinapult.com/api/ticker');
			$coinapult = json_decode($json, true);

			$bid        = $coinapult['small']['bid'];
			$ask        = $coinapult['small']['ask'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Coinbase")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.gdax.com/products/BTC-USD/ticker/');
			$coinbase = json_decode($json, true);

			if(is_array($coinbase))
			{
				$bid        = $coinbase['bid'];
				$ask        = $coinbase['ask'];
				$volume     = $coinbase['volume'];
			}

			if ((isset($bid)) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto);
			}
			unset($bid, $ask, $volume, $coinbase);
		}

		if (($name=="Coinbase")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.gdax.com/products/BTC-EUR/ticker/');
			$coinbase = json_decode($json, true);

			if(is_array($coinbase))
			{
				$bid        = $coinbase['bid'];
				$ask        = $coinbase['ask'];
				$volume     = $coinbase['volume'];
			}

			if ((isset($bid)) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto);
			}
			unset($bid, $ask, $volume, $coinbase);
		}

		if (($name=="Coinbase")&&($currency=="GBP")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.gdax.com/products/BTC-GBP/ticker/');
			$coinbase = json_decode($json, true);

			if(is_array($coinbase))
			{
				$bid        = $coinbase['bid'];
				$ask        = $coinbase['ask'];
				$volume     = $coinbase['volume'];
			}

			if ((isset($bid)) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto);
			}
			unset($bid, $ask, $volume, $coinbase);
		}

		if (($name=="Coinbase")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.gdax.com/products/ETH-USD/ticker/');
			$coinbase = json_decode($json, true);

			if(is_array($coinbase))
			{
				$bid        = $coinbase['bid'];
				$ask        = $coinbase['ask'];
				$volume     = $coinbase['volume'];
			}

			if ((isset($bid)) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto);
			}
			unset($bid, $ask, $volume, $coinbase);
		}

		if (($name=="Coinbase")&&($currency=="EUR")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.gdax.com/products/ETH-EUR/ticker/');
			$coinbase = json_decode($json, true);

			if(is_array($coinbase))
			{
				$bid        = $coinbase['bid'];
				$ask        = $coinbase['ask'];
				$volume     = $coinbase['volume'];
			}

			if ((isset($bid)) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto);
			}
			unset($bid, $ask, $volume, $coinbase);
		}

		if (($name=="Coinbase")&&($currency=="GBP")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.gdax.com/products/ETH-GBP/ticker/');
			$coinbase = json_decode($json, true);

			if(is_array($coinbase))
			{
				$bid        = $coinbase['bid'];
				$ask        = $coinbase['ask'];
				$volume     = $coinbase['volume'];
			}

			if ((isset($bid)) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto);
			}
			unset($bid, $ask, $volume, $coinbase);
		}

		if (($name=="Coinbase")&&($currency=="USD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.gdax.com/products/LTC-USD/ticker/');
			$coinbase = json_decode($json, true);

			if(is_array($coinbase))
			{
				$bid        = $coinbase['bid'];
				$ask        = $coinbase['ask'];
				$volume     = $coinbase['volume'];
			}

			if ((isset($bid)) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto);
			}
			unset($bid, $ask, $volume, $coinbase);
		}

		if (($name=="Coinbase")&&($currency=="EUR")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.gdax.com/products/LTC-EUR/ticker/');
			$coinbase = json_decode($json, true);

			if(is_array($coinbase))
			{
				$bid        = $coinbase['bid'];
				$ask        = $coinbase['ask'];
				$volume     = $coinbase['volume'];
			}

			if ((isset($bid)) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto);
			}
			unset($bid, $ask, $volume, $coinbase);
		}

		if (($name=="Coinbase")&&($currency=="GBP")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.gdax.com/products/LTC-GBP/ticker/');
			$coinbase = json_decode($json, true);

			if(is_array($coinbase))
			{
				$bid        = $coinbase['bid'];
				$ask        = $coinbase['ask'];
				$volume     = $coinbase['volume'];
			}

			if ((isset($bid)) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto);
			}
			unset($bid, $ask, $volume, $coinbase);
		}



		if (($name=="ItBit")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.itbit.com/v1/markets/XBTUSD/ticker');
			$itbitusd = json_decode($json, true);

			$bid        = $itbitusd['bid'];
			$ask        = $itbitusd['ask'];
			$volume     = $itbitusd['volume24h'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="ItBit")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.itbit.com/v1/markets/XBTEUR/ticker');
			$itbitusd = json_decode($json, true);

			if(is_array($itbitusd))
			{
				$bid        = $itbitusd['bid'];
				$ask        = $itbitusd['ask'];
				$volume     = $itbitusd['volume24h'];
			}

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}



		if (($name=="Maicoin")&&($currency=="TWD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.maicoin.com/v1/prices/TWD');
			$maicoin = json_decode($json, true);

			$bid        = $maicoin['sell_price'];
			$ask        = $maicoin['buy_price'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitex.la")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://bitex.la/api-v1/rest/btc_usd/market/ticker');
			$bitexla = json_decode($json, true);

			$bid        = $bitexla['bid'];
			$ask        = $bitexla['ask'];
			$volume     = $bitexla['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['BTC_USD']['buy_price'];
			$ask        = $exmo['BTC_USD']['sell_price'];
			$volume     = $exmo['BTC_USD']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['BTC_EUR']['buy_price'];
			$ask        = $exmo['BTC_EUR']['sell_price'];
			$volume     = $exmo['BTC_EUR']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="RUB")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['BTC_RUB']['buy_price'];
			$ask        = $exmo['BTC_RUB']['sell_price'];
			$volume     = $exmo['BTC_RUB']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="PLN")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['BTC_PLN']['buy_price'];
			$ask        = $exmo['BTC_PLN']['sell_price'];
			$volume     = $exmo['BTC_PLN']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['ETH_USD']['buy_price'];
			$ask        = $exmo['ETH_USD']['sell_price'];
			$volume     = $exmo['ETH_USD']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="EUR")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['ETH_EUR']['buy_price'];
			$ask        = $exmo['ETH_EUR']['sell_price'];
			$volume     = $exmo['ETH_EUR']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="RUB")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['ETH_RUB']['buy_price'];
			$ask        = $exmo['ETH_RUB']['sell_price'];
			$volume     = $exmo['ETH_RUB']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="PLN")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['ETH_PLN']['buy_price'];
			$ask        = $exmo['ETH_PLN']['sell_price'];
			$volume     = $exmo['ETH_PLN']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="UAH")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['ETH_UAH']['buy_price'];
			$ask        = $exmo['ETH_UAH']['sell_price'];
			$volume     = $exmo['ETH_UAH']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="USD")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['XRP_USD']['buy_price'];
			$ask        = $exmo['XRP_USD']['sell_price'];
			$volume     = $exmo['XRP_USD']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="RUB")&&($crypto=="XRP"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['XRP_RUB']['buy_price'];
			$ask        = $exmo['XRP_RUB']['sell_price'];
			$volume     = $exmo['XRP_RUB']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="USD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['BCH_USD']['buy_price'];
			$ask        = $exmo['BCH_USD']['sell_price'];
			$volume     = $exmo['BCH_USD']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="RUB")&&($crypto=="BCH"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['BCH_RUB']['buy_price'];
			$ask        = $exmo['BCH_RUB']['sell_price'];
			$volume     = $exmo['BCH_RUB']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="USD")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['LTC_USD']['buy_price'];
			$ask        = $exmo['LTC_USD']['sell_price'];
			$volume     = $exmo['LTC_USD']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="EUR")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['LTC_EUR']['buy_price'];
			$ask        = $exmo['LTC_EUR']['sell_price'];
			$volume     = $exmo['LTC_EUR']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Exmo")&&($currency=="RUB")&&($crypto=="LTC"))
		{
			$json = curl_download('https://api.exmo.com/v1/ticker/');
			$exmo = json_decode($json, true);

			$bid        = $exmo['LTC_RUB']['buy_price'];
			$ask        = $exmo['LTC_RUB']['sell_price'];
			$volume     = $exmo['LTC_RUB']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="CEX.IO")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://cex.io/api/ticker/BTC/USD/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}

		if (($name=="CEX.IO")&&($currency=="RUB")&&($crypto=="BTC"))
		{
			$json = curl_download('https://cex.io/api/ticker/BTC/RUB/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}

		if (($name=="CEX.IO")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://cex.io/api/ticker/BTC/EUR/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}

		if (($name=="CEX.IO")&&($currency=="GBP")&&($crypto=="BTC"))
		{
			$json = curl_download('https://cex.io/api/ticker/BTC/GBP/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}


		if (($name=="CEX.IO")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://cex.io/api/ticker/ETH/USD/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}


		if (($name=="CEX.IO")&&($currency=="EUR")&&($crypto=="ETH"))
		{
			$json = curl_download('https://cex.io/api/ticker/ETH/EUR/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}

		if (($name=="CEX.IO")&&($currency=="GBP")&&($crypto=="ETH"))
		{
			$json = curl_download('https://cex.io/api/ticker/ETH/GBP/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}


		if (($name=="CEX.IO")&&($currency=="USD")&&($crypto=="XRP"))
		{
			$json = curl_download('https://cex.io/api/ticker/XRP/USD/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}


		if (($name=="CEX.IO")&&($currency=="EUR")&&($crypto=="XRP"))
		{
			$json = curl_download('https://cex.io/api/ticker/XRP/EUR/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (isset($bid) && ($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}

		if (($name=="CEX.IO")&&($currency=="USD")&&($crypto=="BCH"))
		{
			$json = curl_download('https://cex.io/api/ticker/BCH/USD/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}

		if (($name=="CEX.IO")&&($currency=="EUR")&&($crypto=="BCH"))
		{
			$json = curl_download('https://cex.io/api/ticker/BCH/EUR/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}

		if (($name=="CEX.IO")&&($currency=="GBP")&&($crypto=="BCH"))
		{
			$json = curl_download('https://cex.io/api/ticker/BCH/GBP/');
			$cexio = json_decode($json, true);

			if (is_array($cexio))
			{
				$bid        = $cexio['bid'];
				$ask        = $cexio['ask'];
				$volume     = $cexio['volume'];
			}

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume, $cexio);
		}




		if (($name=="Mercado Bitcoin")&&($currency=="BRL")&&($crypto=="BTC"))
		{
			$json = curl_download('https://www.mercadobitcoin.net/api/btc/ticker/');
			$mercadobitcoin = json_decode($json, true);

			$bid        = $mercadobitcoin['ticker']['buy'];
			$ask        = $mercadobitcoin['ticker']['sell'];
			$volume     = $mercadobitcoin['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Mercado Bitcoin")&&($currency=="BRL")&&($crypto=="BCH"))
		{
			$json = curl_download('https://www.mercadobitcoin.net/api/bch/ticker/');
			$mercadobitcoin = json_decode($json, true);

			$bid        = $mercadobitcoin['ticker']['buy'];
			$ask        = $mercadobitcoin['ticker']['sell'];
			$volume     = $mercadobitcoin['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Mercado Bitcoin")&&($currency=="BRL")&&($crypto=="LTC"))
		{
			$json = curl_download('https://www.mercadobitcoin.net/api/ltc/ticker/');
			$mercadobitcoin = json_decode($json, true);

			$bid        = $mercadobitcoin['ticker']['buy'];
			$ask        = $mercadobitcoin['ticker']['sell'];
			$volume     = $mercadobitcoin['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="FoxBit")&&($currency=="BRL")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.blinktrade.com/api/v1/BRL/ticker?crypto_currency=BTC');
			$foxbit = json_decode($json, true);

			$bid        = $foxbit['buy'];
			$ask        = $foxbit['sell'];
			$volume     = $foxbit['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="ChileBit")&&($currency=="CLP")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.blinktrade.com/api/v1/CLP/ticker?crypto_currency=BTC');
			$chilebit = json_decode($json, true);

			$bid        = $chilebit['buy'];
			$ask        = $chilebit['sell'];
			$volume     = $chilebit['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}




		if (($name=="VBTC")&&($currency=="VND")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.blinktrade.com/api/v1/VND/ticker?crypto_currency=BTC');
			$urdubit = json_decode($json, true);

			$bid        = $urdubit['sell'];
			$ask        = $urdubit['buy'];
			$volume     = $urdubit['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}



		if (($name=="CHBTC")&&($currency=="CNY")&&($crypto=="BTC"))
		{
			$json = curl_download('http://api.chbtc.com/data/v1/ticker?currency=btc_cny');
			$chbtc = json_decode($json, true);

			$bid        = $chbtc['ticker']['buy'];
			$ask        = $chbtc['ticker']['sell'];
			$volume     = $chbtc['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="CHBTC")&&($currency=="CNY")&&($crypto=="LTC"))
		{
			$json = curl_download('http://api.chbtc.com/data/v1/ticker?currency=ltc_cny');
			$chbtc = json_decode($json, true);

			$bid        = $chbtc['ticker']['buy'];
			$ask        = $chbtc['ticker']['sell'];
			$volume     = $chbtc['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="CHBTC")&&($currency=="CNY")&&($crypto=="ETH"))
		{
			$json = curl_download('http://api.chbtc.com/data/v1/ticker?currency=eth_cny');
			$chbtc = json_decode($json, true);

			$bid        = $chbtc['ticker']['buy'];
			$ask        = $chbtc['ticker']['sell'];
			$volume     = $chbtc['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="CHBTC")&&($currency=="CNY")&&($crypto=="BCH"))
		{
			$json = curl_download('http://api.chbtc.com/data/v1/ticker?currency=bcc_cny');
			$chbtc = json_decode($json, true);

			$bid        = $chbtc['ticker']['buy'];
			$ask        = $chbtc['ticker']['sell'];
			$volume     = $chbtc['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}		
		if (($name=="QUOINEX")&&($currency=="EUR")&&($crypto=="ALL"))
		{
			$json = curl_download('https://api.quoine.com/products');
			$quoine = json_decode($json, true);

			foreach ($quoine as $key => $currency_pair) {
				if($currency_pair['currency_pair_code'] == "BTCUSD")
				{
					$currency = "USD";
					$crypto = "BTC";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "ETHUSD")
				{
					$currency = "USD";
					$crypto = "ETH";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "BCHUSD")
				{
					$currency = "USD";
					$crypto = "BCH";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "BTCJPY")
				{
					$currency = "JPY";
					$crypto = "BTC";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "BTCJPY")
				{
					$currency = "JPY";
					$crypto = "BTC";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "BCHJPY")
				{
					$currency = "JPY";
					$crypto = "BCH";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "ETHJPY")
				{
					$currency = "JPY";
					$crypto = "ETH";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "XRPJPY")
				{
					$currency = "JPY";
					$crypto = "XRP";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "BTCSGD")
				{
					$currency = "SGD";
					$crypto = "BTC";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "ETHSGD")
				{
					$currency = "SGD";
					$crypto = "ETH";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "BCHSGD")
				{
					$currency = "SGD";
					$crypto = "BCH";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "XRPIDR")
				{
					$currency = "IDR";
					$crypto = "XRP";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "BTCAUD")
				{
					$currency = "AUD";
					$crypto = "BTC";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "BTCEUR")
				{
					$currency = "EUR";
					$crypto = "BTC";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
				elseif($currency_pair['currency_pair_code'] == "ETHEUR")
				{
					$currency = "EUR";
					$crypto = "ETH";

					$bid        = $currency_pair['market_bid'];
					$ask        = $currency_pair['market_ask'];
					$volume     = $currency_pair['volume_24h'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
				}
			}
		}




		if (($name=="gatecoin")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.gatecoin.com/Public/LiveTickers');
			$gatecoin = json_decode($json, true);

			$bid        = $gatecoin['tickers'][0]['bid'];
			$ask        = $gatecoin['tickers'][0]['ask'];
			$volume     = $gatecoin['tickers'][0]['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="gatecoin")&&($currency=="HKD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.gatecoin.com/Public/LiveTickers');
			$gatecoin = json_decode($json, true);
			
			$bid        = $gatecoin['tickers'][1]['bid'];
			$ask        = $gatecoin['tickers'][1]['ask'];
			$volume     = $gatecoin['tickers'][1]['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="gatecoin")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.gatecoin.com/Public/LiveTickers');
			$gatecoin = json_decode($json, true);
			
			$bid        = $gatecoin['tickers'][2]['bid'];
			$ask        = $gatecoin['tickers'][2]['ask'];
			$volume     = $gatecoin['tickers'][2]['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="bitFlyer")&&($currency=="JPY")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.bitflyer.jp/v1/ticker?product_code=BTC_JPY');
			$bitflyer = json_decode($json, true);

			$bid        = $bitflyer['best_bid'];
			$ask        = $bitflyer['best_ask'];
			$volume     = $bitflyer['volume'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTCBOX")&&($currency=="JPY")&&($crypto=="BTC"))
		{
			$json = curl_download('https://www.btcbox.co.jp/api/v1/ticker/');
			$btcbox = json_decode($json, true);

			$bid        = $btcbox['buy'];
			$ask        = $btcbox['sell'];
			$volume     = $btcbox['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTCBOX")&&($currency=="JPY")&&($crypto=="LTC"))
		{
			$json = curl_download('https://www.btcbox.co.jp/api/v1/ticker/?coin=ltc');
			$btcbox = json_decode($json, true);

			$bid        = $btcbox['buy'];
			$ask        = $btcbox['sell'];
			$volume     = $btcbox['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTCBOX")&&($currency=="JPY")&&($crypto=="BCH"))
		{
			$json = curl_download('https://www.btcbox.co.jp/api/v1/ticker/?coin=bch');
			$btcbox = json_decode($json, true);

			$bid        = $btcbox['buy'];
			$ask        = $btcbox['sell'];
			$volume     = $btcbox['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="BTCBOX")&&($currency=="JPY")&&($crypto=="ETH"))
		{
			$json = curl_download('https://www.btcbox.co.jp/api/v1/ticker/?coin=eth');
			$btcbox = json_decode($json, true);

			$bid        = $btcbox['buy'];
			$ask        = $btcbox['sell'];
			$volume     = $btcbox['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}
		if (($name=="BTCTurk")&&($currency=="TRY")&&($crypto=="ALL"))
		{
			$json = curl_download('https://www.btcturk.com/api/ticker');
			$btcturk = json_decode($json, true);

			foreach ($btcturk as $key => $currency_pair) 
			{
				if($currency_pair['pair']=="BTCTRY")
				{
					$crypto     = "BTC";
					$bid        = $btcturk[$key]['bid'];
					$ask        = $btcturk[$key]['ask'];
					$volume     = $btcturk[$key]['volume'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume,$currency_pair);
				}
				elseif($currency_pair['pair']=="ETHTRY")
				{
					$crypto     = "ETH";
					$bid        = $btcturk[$key]['bid'];
					$ask        = $btcturk[$key]['ask'];
					$volume     = $btcturk[$key]['volume'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume,$currency_pair);
				}
				elseif($currency_pair['pair']=="LTCTRY")
				{
					$crypto     = "LTC";
					$bid        = $btcturk[$key]['bid'];
					$ask        = $btcturk[$key]['ask'];
					$volume     = $btcturk[$key]['volume'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume,$currency_pair);
				}
				elseif($currency_pair['pair']=="XRPTRY")
				{
					$crypto     = "XRP";
					$bid        = $btcturk[$key]['bid'];
					$ask        = $btcturk[$key]['ask'];
					$volume     = $btcturk[$key]['volume'];

					if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
					{
					    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
					}
					else
					{
					    set_inactive($conn, $name, $currency, $crypto); 
					}
					unset($bid, $ask, $volume);
				}
			}
		}

		if (($name=="Bitcointoyou")&&($currency=="BRL")&&($crypto=="BTC"))
		{
			$json = curl_download('https://www.bitcointoyou.com/API/ticker.aspx');
			$bitcointoyou = json_decode($json, true);

			$bid        = $bitcointoyou['ticker']['buy'];
			$ask        = $bitcointoyou['ticker']['sell'];
			$volume     = $bitcointoyou['ticker']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="NegocieCoins")&&($currency=="BRL")&&($crypto=="BTC"))
		{
			$json = curl_download('https://broker.negociecoins.com.br/api/v3/btcbrl/ticker');
			$negociecoins = json_decode($json, true);

			$bid        = $negociecoins['sell'];
			$ask        = $negociecoins['buy'];
			$volume     = $negociecoins['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="NegocieCoins")&&($currency=="BRL")&&($crypto=="LTC"))
		{
			$json = curl_download('https://broker.negociecoins.com.br/api/v3/ltcbrl/ticker');
			$negociecoins = json_decode($json, true);

			$bid        = $negociecoins['sell'];
			$ask        = $negociecoins['buy'];
			$volume     = $negociecoins['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="NegocieCoins")&&($currency=="BRL")&&($crypto=="BCH"))
		{
			$json = curl_download('https://broker.negociecoins.com.br/api/v3/bchbrl/ticker');
			$negociecoins = json_decode($json, true);

			$bid        = $negociecoins['sell'];
			$ask        = $negociecoins['buy'];
			$volume     = $negociecoins['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Uphold")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.uphold.com/v0/ticker');
			$uphold = json_decode($json, true);

			$bid        = $uphold[6]['bid'];
			$ask        = $uphold[6]['ask'];
			$volume     = "N/A";

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		if (($name=="Gemini")&&($currency=="USD")&&($crypto=="BTC"))
		{
			$json = curl_download('https://api.gemini.com/v1/pubticker/btcusd');
			$gemini = json_decode($json, true);

			$bid        = $gemini['bid'];
			$ask        = $gemini['ask'];
			$volume     = $gemini['volume']['BTC'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Gemini")&&($currency=="USD")&&($crypto=="ETH"))
		{
			$json = curl_download('https://api.gemini.com/v1/pubticker/ethusd');
			$gemini = json_decode($json, true);

			$bid        = $gemini['bid'];
			$ask        = $gemini['ask'];
			$volume     = $gemini['volume']['ETH'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}



		if (($name=="DSX")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://dsx.uk/mapi/ticker/btceur');
			$dsx = json_decode($json, true);

			$bid        = $dsx['btceur']['sell'];
			$ask        = $dsx['btceur']['buy'];
			$volume     = $dsx['btceur']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="DSX")&&($currency=="RUB")&&($crypto=="BTC"))
		{
			$json = curl_download('https://dsx.uk/mapi/ticker/btcrub');
			$dsx = json_decode($json, true);

			$bid        = $dsx['btcrub']['sell'];
			$ask        = $dsx['btcrub']['buy'];
			$volume     = $dsx['btcrub']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="DSX")&&($currency=="EUR")&&($crypto=="BCH"))
		{
			$json = curl_download('https://dsx.uk/mapi/ticker/bcheur');
			$dsx = json_decode($json, true);

			$bid        = $dsx['bcceur']['sell'];
			$ask        = $dsx['bcceur']['buy'];
			$volume     = $dsx['bcceur']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="DSX")&&($currency=="EUR")&&($crypto=="ETH"))
		{
			$json = curl_download('https://dsx.uk/mapi/ticker/etheur');
			$dsx = json_decode($json, true);

			$bid        = $dsx['etheur']['sell'];
			$ask        = $dsx['etheur']['buy'];
			$volume     = $dsx['etheur']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="DSX")&&($currency=="EUR")&&($crypto=="LTC"))
		{
			$json = curl_download('https://dsx.uk/mapi/ticker/ltceur');
			$dsx = json_decode($json, true);

			$bid        = $dsx['ltceur']['sell'];
			$ask        = $dsx['ltceur']['buy'];
			$volume     = $dsx['ltceur']['vol'];

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}

		if (($name=="Bitonic")&&($currency=="EUR")&&($crypto=="BTC"))
		{
			$json = curl_download('https://bitonic.nl/api/buy');
			$bitonic = json_decode($json, true);
			$bitonicbid     = $bitonic['price'];

			$json = curl_download('https://bitonic.nl/api/sell');
			$bitonicsell = json_decode($json, true);
			$bitonicsell     = $bitonicsell['price'];

			$json = curl_download('https://bitonic.nl/api/price');
			$bitonicvol = json_decode($json, true);
			$bitonicvol     = $bitonicvol['volume'];

			$bid        = $bitonicsell;
			$ask        = $bitonicbid;
			$volume     = $bitonicvol;

			if (($bid != "") && ($ask != "") && ($volume != "") && ($currency != "") && ($crypto != "")) 
			{
			    update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto);
			}
			else
			{
			    set_inactive($conn, $name, $currency, $crypto); 
			}
			unset($bid, $ask, $volume);
		}


		try {
            $pheanstalk->delete($job);
        } catch (\Exception $e) {
        // handled missing Beanstalkd job from the tube
        }
	}
}

function update_record($conn, $name, $bid, $ask, $volume, $currency, $crypto)
{
	$sql = "UPDATE  `bitcoin_exchanges` SET  `AskPrice` = \"$ask\" , `BidPrice` = \"$bid\", `Volume` = \"$volume\" , `Last Updated Prices` = now(), `IsActive` = \"True\" WHERE  `Name` = \"$name\" AND `Currency` = \"$currency\" AND `Crypto` = \"$crypto\"";

	$result = mysqli_query($conn, $sql);

	if ($result == true) {
	  
	}
	else
	{
	   printf("error: %s\n", mysqli_error($conn));
	}
}

function set_inactive($conn, $name, $currency, $crypto)
{
    //print_r(error_get_last());
    $sql = "UPDATE  `bitcoin_exchanges` SET  `IsActive` = \"False\" WHERE  `Name` = \"$name\" AND `Currency` = \"$currency\" AND `Crypto` = \"$crypto\"";
    
    if (mysqli_query($conn ,$sql) === TRUE) {
        
    } else {
        echo "Error updating record: " .  mysqli_error($conn);
    }
    //echo "ERROR: $name / $currency / $crypto SET INACTIVE\n";
}




?>
