<?php
include '/home/webapp/pheanstalk/vendor/autoload.php';
use Pheanstalk\Pheanstalk;
$pheanstalk = new Pheanstalk('127.0.0.1');

include '/home/webapp/money.style/headerfile.php';



while(true)
{
    $tube_id=rand(1,9);
    $job=$pheanstalk->watch('route-summaries-tube'.$tube_id)->ignore('default')->reserve();

    if($job)
    {
        $message = $job->getdata();

        $message = str_replace('{', '', $message);
        $message = str_replace('}', '', $message);

        $data = str_getcsv($message);
        $sending_country = $data[0];
        $receiving_country = $data[1];

        //echo "$sending_country to $receiving_country\n";

                // connect to database
        $conn = connect_to_database();

        $amount = get_default_amount($conn, $sending_country);
        list($sending_currency, $sending_currency_symbol) = get_currency_currencysymbol($conn, $sending_country);

        // get tocurrency and tocurrencysymbol
        list($receiving_currency, $receiving_currency_symbol) = get_currency_currencysymbol($conn, $receiving_country);

        print_r(error_get_last());

        if (($receiving_currency == 'EUR') && ($sending_currency == 'EUR' || $sending_currency == 'USD')) 
        {
            $volumetocheck = '1000';
        }
        elseif (($receiving_currency == 'USD') && ($sending_currency == 'EUR' || $sending_currency == 'USD')) 
        {
            $volumetocheck = '1000';
        }
        else
        {
            $volumetocheck = 50;
        }

        //get the real exchange rate
        $apicurrencies      = "$sending_currency$receiving_currency";
        list($exchange_rate, $last_updated_exchange_rate)   = get_exchange_rate($apicurrencies, $conn);

        $apicurrencies      = $sending_currency . "USD";
        list($usd_exchange_rate, $usd_last_updated_exchange_rate)   = get_exchange_rate($apicurrencies, $conn);

        echo "$apicurrencies : $usd_exchange_rate";
        if ($usd_exchange_rate < 0.01)
        {
            $amount = 1000000;
        }

        if ($exchange_rate!="")
        {
            //get the real exchange rate when sent back
            $apicurrencies            = "$receiving_currency$sending_currency";
            list($exchange_ratesentback, $last_updated_exchange_rate_sent_back) = get_exchange_rate($apicurrencies, $conn);

            if (($sending_currency == 'EUR') && ($receiving_currency == 'EUR' || $receiving_currency == 'USD')) {
                $volumetocheck = '1000';
            }
            elseif (($sending_currency == 'USD') && ($receiving_currency == 'EUR' || $receiving_currency == 'USD')) {
                $volumetocheck = '5000';
            }

            // Get the list of results
            $search_results  = generate_search_results($conn, $amount, $sending_country, $receiving_country, $volumetocheck);
            
            if(empty($search_results))
            {
                $volumetocheck = 0;
                $search_results  = generate_search_results($conn, $amount, $sending_country, $receiving_country, $volumetocheck);
            }

            //print_r($search_results);

            if (!empty($search_results)) 
            {
                $totalintocurrencies = array();
                // SORT FULLRESULTS ARRAY
                foreach ($search_results as $key => $row) 
                {
                    $totalintocurrencies[$key] = $row[56];
                }

                array_multisort($totalintocurrencies, SORT_DESC, $search_results);
                /*
                echo count($totalintocurrencies) . ":" . count($search_results) . "\n";
                if (count($totalintocurrencies) != count($search_results))
                {
                    print_r($search_results);
                }
                */

                list($funding_name, $funding_fee, $funding_percentage, $buying_crypto_fee, $buying_crypto_percentage, $network_transaction_fee, $network_transaction_fee_currency, $arbitrage, $selling_crypto_percentage, $selling_crypto_percentage_currency, $selling_crypto_fee, $selling_crypto_fee_currency, $withdrawal_name, $withdrawal_percentage, $withdrawal_percentage_currency, $withdrawal_fee, $withdrawal_fee_currency, $exchange_1, $ask_price, $volume, $exchange_2, $exchange_2_volume, $bid_price, $exchange_2_website, $exchange_rate_margin, $exchange_rate_margin_fees, $sending_location, $firm_type, $product, $speed_actual, $pick_up_method, $coverage, $period_from, $date, $fee, $coverage, $website, $amount, $total_fees, $total_cost, $total_in_receiving_currency, $exchange_rate, $exchange_rate_inverse, $sending_country, $receiving_country, $sending_currency, $receiving_currency, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_fee_amount_en_route, $withdrawal_percentage_amount_en_route, $withdrawal_fee_amount_en_route) = load_result_variables($search_results, 0);

                //if (!empty($exchange_1))
                //{ 
                    $total_cost_receiving_currency = $total_cost * $exchange_rate;
                    $receiving_country_lc = strtolower($receiving_country);
                    $receiving_country_lc = trim($receiving_country_lc);
                    $receiving_country_lc = str_replace(' ', '', $receiving_country_lc);

                    $total_cost_percentage_of_amount = $total_cost / $amount;
                    

                    $sql = "SELECT * FROM `route_summaries` WHERE `FromCountry`='$sending_country' AND `tocountry`='$receiving_country'";
                    //echo $sql;
                    
                    $result = mysqli_query($conn, $sql);
                    $rows = mysqli_num_rows($result);
                    
                    if($rows == 0)
                    {
                        $sql = "INSERT INTO `route_summaries` (
                                `fromcountry` ,
                                `buyfrom` ,
                                `sellto` ,
                                `totalintocurrency` ,
                                `tocountry` ,
                                `fundingname` ,
                                `withdrawalname` ,
                                `currency` ,
                                `tocurrency` ,
                                `currencysymbol` ,
                                `tocurrencysymbol`,
                                `total_cost_sending_currency`,
                                `crypto`,
                                `total_cost_receiving_currency`,
                                `volume_checked`,
                                `total_cost_percentage_of_amount`,
                                `timestamp`
                                ) VALUES (
                                '$sending_country',  '$exchange_1',  '$exchange_2',  '$total_in_receiving_currency',  '$receiving_country',  '$funding_name', '$withdrawal_name', '$sending_currency' ,'$receiving_currency' ,'$sending_currency_symbol' ,'$receiving_currency_symbol', '$total_cost', '$crypto', '$total_cost_receiving_currency', '$volumetocheck', '$total_cost_percentage_of_amount', now()
                                )";
                    }
                    else
                    {
                        $sql = "UPDATE `route_summaries` SET
                                `fromcountry` = '$sending_country',
                                `buyfrom` = '$exchange_1',
                                `sellto` = '$exchange_2',
                                `totalintocurrency` = '$total_in_receiving_currency',
                                `tocountry` = '$receiving_country',
                                `fundingname` = '$funding_name',
                                `withdrawalname` = '$withdrawal_name',
                                `currency` = '$sending_currency',
                                `tocurrency` = '$receiving_currency',
                                `currencysymbol` = '$sending_currency_symbol',
                                `tocurrencysymbol` = '$receiving_currency_symbol',
                                `total_cost_sending_currency` = '$total_cost',
                                `total_cost_receiving_currency` = '$total_cost_receiving_currency',
                                `crypto` = '$crypto',
                                `volume_checked` = '$volumetocheck',
                                `total_cost_percentage_of_amount` = '$total_cost_percentage_of_amount',
                                `timestamp` = now() WHERE  `fromcountry`='$sending_country' AND `tocountry`='$receiving_country'";
                                }

                    $result = mysqli_query($conn, $sql);
                    if ( false===$result ) 
                    {
                        printf("error: %s\n", mysqli_error($conn));
                    }
                
                    //echo "$sending_country to $receiving_country";
                    print_r(error_get_last());
                //}
            }
        }
        

        unset($search_results);

        print_r(error_get_last());

        try {
            $pheanstalk->delete($job);
        } catch (\Exception $e) {
        // handled missing Beanstalkd job from the tube
        }
    }
}

