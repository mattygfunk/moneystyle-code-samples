 <?php
include 'headerfile.php';


// get variables
$receiving_country   = $_GET["receiving_country"];
$sending_country = $_GET["sending_country"];
$amount      = $_GET["amount"];
$exchange_1      = $_GET["exchange_1"];
$exchange_2      = $_GET["exchange_2"];
$crypto      = $_GET["crypto"];
$funding_name      = $_GET["funding_name"];
$withdrawal_name      = $_GET["withdrawal_name"];


// sanitise variables
$amount           = filter_var($amount, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
$sending_country      = filter_var($sending_country, FILTER_SANITIZE_SPECIAL_CHARS);
$receiving_country        = filter_var($receiving_country, FILTER_SANITIZE_SPECIAL_CHARS);
$exchange_1        = filter_var($exchange_1, FILTER_SANITIZE_SPECIAL_CHARS);
$exchange_2        = filter_var($exchange_2, FILTER_SANITIZE_SPECIAL_CHARS);
$crypto        = filter_var($crypto, FILTER_SANITIZE_SPECIAL_CHARS);
$funding_name        = filter_var($funding_name, FILTER_SANITIZE_SPECIAL_CHARS);
$withdrawal_name        = filter_var($withdrawal_name, FILTER_SANITIZE_SPECIAL_CHARS);

$receiving_country       = urldecode($receiving_country);
$sending_country       = urldecode($sending_country);
$exchange_1            = urldecode($exchange_1);
$exchange_2            = urldecode($exchange_2);
$funding_name          = urldecode($funding_name);
$withdrawal_name       = urldecode($withdrawal_name);



// connect to database
$conn = connect_to_database();




// get the exchange rates and the amount in to currency at the true exchange rate


// get currency and currencysymbol
list($sending_currency, $sending_currency_symbol) = get_currency_currencysymbol($conn, $sending_country);
// get currency and currencysymbol
list($receiving_currency, $receiving_currency_symbol) = get_currency_currencysymbol($conn, $receiving_country);

//get the real exchange rate
list($exchange_rate, $last_updated_exchange_rate) = get_exchange_rate("$sending_currency$receiving_currency", $conn);

//get the real exchange rate when sent back
list($exchange_rate_inverse, $last_updated_exchange_rate_sent_back) = get_exchange_rate("$receiving_currency$sending_currency", $conn);


$search_results = array();
$recordnumber = "";
$volumetocheck = "N/A";




$search_results = get_record_generate_fees_totals_and_add_to_array($conn, $volumetocheck, $exchange_1, $exchange_2, $crypto, $recordnumber, $funding_name, $withdrawal_name, $sending_currency, $receiving_currency, $search_results, $amount, $sending_currency_symbol, $receiving_currency_symbol, $sending_country, $receiving_country, $exchange_rate, $exchange_rate_inverse);


list($funding_name, $funding_fee, $funding_percentage, $buying_crypto_fee, $buying_crypto_percentage, $network_transaction_fee, $network_transaction_fee_currency, $arbitrage, $selling_crypto_percentage, $selling_crypto_percentage_currency, $selling_crypto_fee, $selling_crypto_fee_currency, $withdrawal_name, $withdrawal_percentage, $withdrawal_percentage_currency, $withdrawal_fee, $withdrawal_fee_currency, $exchange_1, $ask_price, $volume, $exchange_2, $exchange_2_volume, $bid_price, $exchange_2_website, $exchange_rate_margin, $exchange_rate_margin_fees, $sending_location, $firm_type, $product, $speed_actual, $pick_up_method, $coverage, $period_from, $date, $fee, $coverage, $website, $amount, $total_fees, $total_cost, $total_in_receiving_currency, $exchange_rate, $exchange_rate_inverse, $sending_country, $receiving_country, $sending_currency, $receiving_currency, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_fee_amount_en_route, $withdrawal_percentage_amount_en_route, $withdrawal_fee_amount_en_route) = load_result_variables($search_results, 0);




$exchange_1 = str_replace("\\","", $exchange_1);

if ($last_updated_exchange_1_prices>$last_updated_exchange_2_prices)
{
    $last_updated = $last_updated_exchange_1_prices;
}
else
{
    $last_updated = $last_updated_exchange_2_prices;
}


$extra_at_destination = $total_cost * -1 * $exchange_rate;
$extra_at_destination = number_format($extra_at_destination, 2);

$total_cost_receiving_currency = $total_cost * $exchange_rate;





$exchange_1_logo = "../logos/providers-70px/$exchange_1-70px.png";
$exchange_2_logo = "../logos/providers-70px/$exchange_2-70px.png";

$exchange_1_logo = strtolower($exchange_1_logo);
$exchange_2_logo = strtolower($exchange_2_logo);

$exchange_1_logo = str_replace(' ','',$exchange_1_logo );
$exchange_2_logo = str_replace(' ','',$exchange_2_logo );

//$total_cost_receiving_currency = $total_cost_receiving_currency * -1;
//$total_cost = $total_cost * -1;







if ($total_cost<0)
{
    $total_cost_receiving_currency = $total_cost_receiving_currency * -1;
    $total_cost_receiving_currency = number_format($total_cost_receiving_currency, 2);
    $make_pay = "Make $receiving_currency_symbol$total_cost_receiving_currency $receiving_currency Extra!";
    $total_cost_positive = $total_cost * -1; 
}
else
{
    $total_cost_receiving_currency = number_format($total_cost_receiving_currency, 2);
    $make_pay = "Pay $receiving_currency_symbol$total_cost_receiving_currency $receiving_currency In Fees!";
    $total_cost_positive = $total_cost; 
}  


//$total_cost_receiving_currency = number_format($total_cost_receiving_currency, 2);
$total_in_receiving_currency = number_format($total_in_receiving_currency, 2);


$last_updated               = time_elapsed_string($last_updated);
$last_updated_exchange_1_fees          = time_elapsed_string($last_updated_exchange_1_fees);
$last_updated_exchange_1_prices        = time_elapsed_string($last_updated_exchange_1_prices);
$last_updated_exchange_2_fees   = time_elapsed_string($last_updated_exchange_2_fees);
$last_updated_exchange_2_prices = time_elapsed_string($last_updated_exchange_2_prices);
$last_updated_exchange_rate = time_elapsed_string($last_updated_exchange_rate);

$total_in_receiving_currency = strtr($total_in_receiving_currency, array(',' => ''));
$effective_exchange_rate =   $total_in_receiving_currency / $amount;

$amount = number_format($amount, 2);
$total_cost = number_format($total_cost, 2);

if ($exchange_2!="")
{

    if ($crypto == "BTC")
    {
        $crypto_name = "Bitcoin";
    }
    elseif ($crypto == "ETH")
    {
        $crypto_name = "Ethereum";
    }
    elseif ($crypto == "XRP")
    {
        $crypto_name = "Ripple";
    }
    elseif ($crypto == "BCH")
    {
        $crypto_name = "Bitcoin Cash";
    }
    elseif ($crypto == "LTC")
    {
        $crypto_name = "Litecoin";
    }

    $crypto_logo = "../logos/angle-right-$crypto_name.png";
    $crypto_logo = strtolower($crypto_logo);

    $total_cost_positive = number_format($total_cost_positive, 2);
    $buying_crypto_fee_amount_en_route = number_format($buying_crypto_fee_amount_en_route, 8);
    $buying_crypto_percentage_amount_en_route = number_format($buying_crypto_percentage_amount_en_route, 8);
    $network_transaction_fee_amount_en_route = number_format($network_transaction_fee_amount_en_route, 8);
    $selling_crypto_fee_amount_en_route = number_format($selling_crypto_fee_amount_en_route, 8);
    $selling_crypto_percentage_currency = number_format($selling_crypto_percentage_currency, 2);
    $selling_crypto_percentage_amount_en_route = number_format($selling_crypto_percentage_amount_en_route, 2);
    $buying_crypto_percentage_currency = number_format($buying_crypto_percentage_currency, 2);
    $buying_crypto_fee_currency = number_format($buying_crypto_fee_currency, 2);
    $selling_crypto_fee_currency = number_format($selling_crypto_fee_currency, 2);
    $network_transaction_fee_currency = number_format($network_transaction_fee_currency, 2);
    $withdrawal_fee_currency = number_format($withdrawal_fee_currency, 2);
    $withdrawal_percentage_currency = number_format($withdrawal_percentage_currency, 2);
    $withdrawal_percentage_amount_en_route = number_format($withdrawal_percentage_amount_en_route, 2);
    $withdrawal_fee_amount_en_route = number_format($withdrawal_fee_amount_en_route, 2);
    $total_in_receiving_currency = number_format($total_in_receiving_currency, 2);
    $arbitrage = number_format($arbitrage, 2);
}
else
{
    $crypto_logo = "";
}


$number_of_search_results = count($search_results);

echo "{ \"result\" : [{ \"total_cost\":\"$total_cost\" , \"sending_currency\":\"$sending_currency\" , \"receiving_currency\":\"$receiving_currency\" , \"total_in_receiving_currency\":\"$total_in_receiving_currency\", \"make_pay\":\"$make_pay\" , \"crypto\":\"$crypto\", \"exchange_1_logo\":\"$exchange_1_logo\", \"exchange_2_logo\":\"$exchange_2_logo\", \"crypto_logo\":\"$crypto_logo\", \"last_updated_exchange_1_fees\":\"$last_updated_exchange_1_fees\", \"last_updated_exchange_1_prices\":\"$last_updated_exchange_1_prices\", \"last_updated_exchange_2_fees\":\"$last_updated_exchange_2_fees\", \"last_updated_exchange_2_prices\":\"$last_updated_exchange_2_prices\", \"last_updated_exchange_rate\":\"$last_updated_exchange_rate\", \"effective_exchange_rate\":\"$effective_exchange_rate\", \"exchange_rate\":\"$exchange_rate\", \"lastupdated\":\"" .$last_updated . "\", \"buying_crypto_fee\":\"$buying_crypto_fee\", \"buying_crypto_fee_currency\":\"$buying_crypto_fee_currency\", \"buying_crypto_fee_amount_en_route\":\"$buying_crypto_fee_amount_en_route\", \"buying_crypto_percentage\":\"$buying_crypto_percentage\", \"buying_crypto_percentage_currency\":\"$buying_crypto_percentage_currency\", \"buying_crypto_percentage_amount_en_route\":\"$buying_crypto_percentage_amount_en_route\", \"network_transaction_fee\":\"$network_transaction_fee\", \"network_transaction_fee_currency\":\"$network_transaction_fee_currency\", \"network_transaction_fee_amount_en_route\":\"$network_transaction_fee_amount_en_route\", \"selling_crypto_fee\":\"$selling_crypto_fee\", \"selling_crypto_fee_currency\":\"$selling_crypto_fee_currency\", \"selling_crypto_fee_amount_en_route\":\"$selling_crypto_fee_amount_en_route\", \"selling_crypto_percentage\":\"$selling_crypto_percentage\", \"selling_crypto_percentage_currency\":\"$selling_crypto_percentage_currency\", \"selling_crypto_percentage_amount_en_route\":\"$selling_crypto_percentage_amount_en_route\", \"withdrawal_fee\":\"$withdrawal_fee\", \"withdrawal_fee_currency\":\"$withdrawal_fee_currency\", \"withdrawal_fee_amount_en_route\":\"$withdrawal_fee_amount_en_route\", \"withdrawal_percentage\":\"$withdrawal_percentage\", \"withdrawal_percentage_currency\":\"$withdrawal_percentage_currency\", \"withdrawal_percentage_amount_en_route\":\"$withdrawal_percentage_amount_en_route\", \"arbitrage\":\"$arbitrage\", \"total_cost_positive\":\"$total_cost_positive\"}] }"; 

print_r(error_get_last());

