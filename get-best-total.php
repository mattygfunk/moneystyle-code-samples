<?php
include 'headerfile.php';


// get variables
$receiving_country   = $_GET["receiving_country"];
$sending_country = $_GET["sending_country"];
$amount      = $_GET["amount"];


if (isset($_GET["volume"])) {
    $volumetocheck = $_GET["volume"];
    $volumetocheck    = filter_var($volumetocheck, FILTER_SANITIZE_SPECIAL_CHARS);
} else {

}


if (isset($_GET["data"])) {
    $data = $_GET["data"];
} else {
    $data = "";
}

$data      = filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
// sanitise variables
$amount           = filter_var($amount, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
$sending_country      = filter_var($sending_country, FILTER_SANITIZE_SPECIAL_CHARS);
$receiving_country        = filter_var($receiving_country, FILTER_SANITIZE_SPECIAL_CHARS);





// connect to database
$conn = connect_to_database();


// get currency and currencysymbol
list($sending_currency, $sending_currency_symbol) = get_currency_currencysymbol($conn, $sending_country);
// get currency and currencysymbol
list($receiving_currency, $receiving_currency_symbol) = get_currency_currencysymbol($conn, $receiving_country);



if (($sending_currency == 'EUR') && ($receiving_currency == 'EUR' || $receiving_currency == 'USD')) {
    $volumetocheck = '500';
}
elseif (($sending_currency == 'USD') && ($receiving_currency == 'EUR' || $receiving_currency == 'USD')) {
    $volumetocheck = '1000';
}
elseif (isset($volume)==true)
{

}
else
{
    $volumetocheck = '50';
}


if (!is_numeric($amount))
{
   $amount = 1000;
}


// get the exchange rates and the amount in to currency at the true exchange rate
//get the real exchange rate
$apicurrencies            = "$sending_currency$receiving_currency";
list($exchange_rate, $last_updated_exchange_rate)         = get_exchange_rate($apicurrencies, $conn);
$amountintocurrency       = ($amount * $exchange_rate);
//get the real exchange rate when sent back
$apicurrencies            = "$receiving_currency$sending_currency";
list($exchange_rate_sent_back, $last_updated_exchange_rate) = get_exchange_rate($apicurrencies, $conn);


// Get the list of results
$search_results  = generate_search_results($conn, $amount, $sending_country, $receiving_country, $volumetocheck);

if (count($search_results) < 5)
{
    $volumetocheck = 0;
    $search_results  = generate_search_results($conn, $amount, $sending_country, $receiving_country, $volumetocheck);
}


$search_results = remove_duplicate_routes($search_results);


// If there are routes found
if (count($search_results) > 0) {
    // sort them by total
    foreach ($search_results as $key => $row) {
        $totalintocurrencies[$key] = $row[56];
    }
    array_multisort($totalintocurrencies, SORT_DESC, $search_results);
}

if (count($search_results) < 1) {
echo "N/A";
} else {    // bitcoin calculations

    list($funding_name, $funding_fee, $funding_percentage, $buying_crypto_fee, $buying_crypto_percentage, $network_transaction_fee, $network_transaction_fee_currency, $arbitrage, $selling_crypto_percentage, $selling_crypto_percentage_currency, $selling_crypto_fee, $selling_crypto_fee_currency, $withdrawal_name, $withdrawal_percentage, $withdrawal_percentage_currency, $withdrawal_fee, $withdrawal_fee_currency, $exchange_1, $ask_price, $volume, $exchange_2, $exchange_2_volume, $bid_price, $exchange_2_website, $exchange_rate_margin, $exchange_rate_margin_fees, $sending_location, $firm_type, $product, $speed_actual, $pick_up_method, $coverage, $period_from, $date, $fee, $coverage, $website, $amount, $total_fees, $total_cost, $total_in_receiving_currency, $exchange_rate, $exchange_rate_inverse, $sending_country, $receiving_country, $sending_currency, $receiving_currency, $last_updated_exchange_1_fees, $last_updated_exchange_1_prices, $last_updated_exchange_2_fees, $last_updated_exchange_2_prices, $crypto, $buying_crypto_percentage_currency, $buying_crypto_fee_currency, $funding_percentage_currency, $funding_fee_amount_en_route, $funding_percentage_amount_en_route, $buying_crypto_fee_amount_en_route, $buying_crypto_percentage_amount_en_route, $network_transaction_fee_amount_en_route, $selling_crypto_percentage_amount_en_route, $selling_crypto_fee_amount_en_route, $withdrawal_percentage_amount_en_route, $withdrawal_fee_amount_en_route) = load_result_variables($search_results, 0);

    $exchange_1 = str_replace("\\","", $exchange_1);

    if ($last_updated_exchange_1_prices>$last_updated_exchange_2_prices)
    {
        $last_updated = $last_updated_exchange_1_prices;
    }
    else
    {
        $last_updated = $last_updated_exchange_2_prices;
    }


        $effectiveexchangerate = $total_in_receiving_currency / $amount;
        $effectiveexchangerate = number_format($effectiveexchangerate, 4);

        $extra_at_destination = $total_cost * -1 * $exchange_rate;
        $extra_at_destination = number_format($extra_at_destination, 2);

        $total_cost_in_receiving_currency = $total_cost * $exchange_rate;

        $total_in_receiving_currency = number_format($total_in_receiving_currency, 2);
        //$total_cost_in_receiving_currency = number_format($total_cost_in_receiving_currency, 2);
 
        echo "{ \"result\" : [{ \"total_in_receiving_currency\":\"$total_in_receiving_currency\" , \"effectiveexchangerate\":\"$effectiveexchangerate\" , \"total_cost_in_receiving_currency\":\"$total_cost_in_receiving_currency\" , \"exchange_1\":\"$exchange_1\" , \"exchange_2\":\"$exchange_2\" , \"funding_name\":\"$funding_name\" , \"withdrawal_name\":\"$withdrawal_name\" , \"currency\":\"$sending_currency\"  , \"receiving_country\":\"$receiving_country\", \"sending_currency_symbol\":\"$sending_currency_symbol\"  , \"receiving_currency_symbol\":\"$receiving_currency_symbol\", \"sending_currency\":\"$sending_currency\", \"receiving_currency\":\"$receiving_currency\", \"extra_at_destination\":\"$extra_at_destination\", \"sending_country\":\"$sending_country\",\"receiving_country\":\"$receiving_country\",  \"crypto\":\"$crypto\", \"lastupdated\":\"" .time_elapsed_string($last_updated) . "\",  \"volume\":\"$volumetocheck\"}] }"; 

}
print_r(error_get_last());
