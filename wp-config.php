<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache




/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
 

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'money_style_db');

/** MySQL database username */
define('DB_USER', '');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8fe9421mgeprus87cczrqevfk8xhh4eol3kkjxqodzezdos8lnzwanlfgs6yoaej');
define('SECURE_AUTH_KEY',  'suyhivu1irkaltqwqt3lgzg4caqbpmqnrtecgl829lqmcysplca5t5rudap3316t');
define('LOGGED_IN_KEY',    'v3je54ekegzy8zpuwa8axlrbiofwjrffcqh6fdp4kxqjnthwd01bsv06eipg9zhe');
define('NONCE_KEY',        'w99r7ionnps1vy1agmw7ewhswdlbxjvuf21gz81q4remzmhlqhpwtb07ntaxuuzm');
define('AUTH_SALT',        'w2rbhi3mjyiswck6x60su7l2zuoe8ck5f8jrfvntjay4sjmxw2ljf91nirdv04a3');
define('SECURE_AUTH_SALT', 'lcanvcgp3mejqmxubux92eawyy4dgxmzp0vvfzcrxh98fkbn7cjmoa6e3hgweqom');
define('LOGGED_IN_SALT',   'm4hul3aymc195zvyrgykj5deiy2w7xhhwndfw1ciqcpxxxy442ms9pnttducfxwr');
define('NONCE_SALT',       'cqfq3yt4vgjl8gl7tdch763g3ueslfy7nwdtrexknhtcqfhoqpn5pwwc1lzmr2hm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


