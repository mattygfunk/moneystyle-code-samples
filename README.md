MoneyStyle Code Samples
=======================

This is sample code from the [MoneyStyle Crypto Remittances](http://money.style) website, 
published for the purpose of code review by potential employers.

The code isn't complete, most notably missing the proprietary database. Do not attempt to run the code, as it simply wont work without the database to drive it.


Code Highlights
---------------

You'd probably like to look at the headerfile.php which does most of the processing.

There's also the /scripts folder, which contains cronjobs that update the live data in the database.
